Demonstrate non-type template arguments
#include <iostream>
#include <cstdlib>
using namespace std;

//int size is a non-type agument
template <class T, int size> 

class MyType {
  T a[size];
public:
  MyType() {
    for(int i=0; i<size; i++)
       a[i] = i;
  }

  T &operator[](int i){

      if(i<0 || i> size-1) {
        cout << "\nIndex value of ";
        cout << i << " is out-of-bounds.\n";
        exit(1);
      }
      return a[i];
  }
};


int main()
{
  MyType<int, 10> intob;
  MyType<double, 15> doubleob;

  cout << "Integer array: ";
  for(int i=0; i<10; i++)
     intob[i] = i;
  for(int i=0; i<10; i++)
     cout << intob[i] << "  ";
  cout << '\n';

  cout << "Double array: ";
  for(int i=0; i<15; i++)
     doubleob[i] = (double) i/3;

  for(int i=0; i<15; i++)
     cout << doubleob[i] << "  ";
  cout << '\n';

  intob[12] = 100; // generates runtime error

  return 0;
}