#include<iostream>
#include<string>
#include<vector>
using namespace std;

class Employee
{
	string name;
	int age;
public:
	Employee() : age(0) {}
	Employee(string nm,int a) : name(nm), age(a) {}
	friend ostream& operator<<(ostream& stream,const Employee& e)
	{
		stream<<endl<<e.name<<"\t"<<e.age;		
		return stream;
	}
	bool operator>(const Employee& e)
	{
		return age > e.age;
	}
};

template<class T>
class Array
{
	T *arr;
	int size;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = initVal;
	}
	Array(int sz,T *x) : size(sz)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = x[i];
	}
	Array(const Array<T>& a) : size(a.size)
	{
		arr = new T[size];
		for(int i=0;i<size;++i)
			arr[i] = a[i];
	}
	Array<T>& operator=(const Array<T>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			arr = new T[size];

			for(int i=0;i<size;++i)
				arr[i] = a[i];
		}
		return *this;
	}
	~Array() 
	{
		delete[] arr;
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

void main()
{
	Employee x[] = {Employee("ABC",23),Employee("XYZ",19),Employee("PQR",22)};

	Array<Employee> a1(3,x);
	cout<<a1;
}