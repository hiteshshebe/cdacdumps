#include<iostream>
#include<string>
using namespace std;

template<class T>
class A
{
	T x;
	static T y;
public:
	A() : x(T()) {}
	void showX()
	{
		cout<<x<<"\t"<<&x<<endl;
	}
	static void showY()
	{
		cout<<y<<"\t"<<&y<<endl;
	}
};

template<class T>
T A<T>::y;

int A<int>::y = 100;

void main()
{
	A<int> a1,a2,a3;
	a1.showX();
	a1.showY();

	a2.showX();
	a2.showY();

	a3.showX();
	a3.showY();

	A<int>::showY();
}