#include<iostream>
#include<string>
using namespace std;

class Base
{
public:
	virtual void x()
	{
		cout<<"Base.x()"<<endl;
	}
};

class Derived1 : public Base
{
public:
	void x()
	{
		cout<<"Dervied1.x()"<<endl;
	}
};

class Derived2 : public Base
{
public:
	void x()
	{
		cout<<"Dervied2.x()"<<endl;
	}
};

template<class T>
void demo(T arg1,T arg2)
{
	arg1->x();
	arg2->x();
}

void main()
{
	Derived1* d1 = new Derived1;
	Derived2* d2 = new Derived2;

	demo<Base*>(d1,d2);
}