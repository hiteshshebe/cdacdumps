import java.util.Iterator;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class BsLogic 
{
	
	public boolean validate(String unm,String pwd)
	{
		boolean status=false;
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		Transaction t=session.beginTransaction();
		
		/*Query query=session.createQuery("from HibernatePojo as hbp where hbp.username=?");*/
		/*String sql="from HibernatePojo as hbp where hbp.username='"+unm+"'";*/
/*		String sql = "select {supplier.*} from Supplier supplier";
        
        String hql = "update Supplier set name = :newName where name = :name";
        Query query = session.createQuery(hql);
        query.setString("name","Supplier Name 1");
        query.setString("newName","s1");
        int rowCount = query.executeUpdate();
        System.out.println("Rows affected: " + rowCount);

        query = session.createQuery("from Supplier");
        List results = query.list();            

        displaySupplierList(results);            
    
    
        HibernateUtil.checkData("select * from Supplier");
        HibernateUtil.checkData("select * from Product");*/

	
		
		String sql="update HibernatePojo set password = :newPassword where username = :name";
		
		/*Query query=session.createQuery(sql);*/
		
		Query query=session.createQuery(sql);
		
		query.setString("newPassword",pwd);
		query.setString("name", unm);
		
		int rows=query.executeUpdate();
		t.commit();
		System.out.println(rows+" updated...");
		status=true;
		
		/*Iterator it=query.iterate();
		
		while(it.hasNext())
		{
			HibernatePojo lp=(HibernatePojo)it.next();
			System.out.println(lp.getUsername());
			System.out.println(lp.getPassword());
			
			if(lp.getUsername().equals(unm) && lp.getPassword().equals(pwd))
				return true;
		}
	*/
		
		return status;
	}
}
