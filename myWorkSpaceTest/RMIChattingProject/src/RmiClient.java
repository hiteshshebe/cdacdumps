import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RmiClient
{
	public RmiClient()
	{
		
		try 
		{
			Registry registry=LocateRegistry.getRegistry("localhost", 5525);
			
			RmiService rs=(RmiService)registry.lookup("myservice");
			
			System.out.println(rs.add(25, 35));
		} 
		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		RmiClient rc=new RmiClient();
	}
}