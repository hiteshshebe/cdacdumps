import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class RmiServer extends UnicastRemoteObject implements RmiService
{

	public RmiServer() throws RemoteException{}
	
	
	@Override
	public int add(int a, int b) {
		// TODO Auto-generated method stub
		return a+b;
	}
	
	
	public static void main(String[] args) {
		
		try 
		{
			RmiServer rs=new RmiServer();
			
			Registry reg=LocateRegistry.createRegistry(5525);
			
			reg.bind("myservice", rs);
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
}