package com.bslogics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.connections.DBConnection;

public class RegisterBsLogic 
{
	boolean status=false;
	Connection conn=null;
	String sql=null;
	PreparedStatement pstmt=null;
	int rows;
	
	
	
	public void getConnect()
	{
		conn=new DBConnection().getConnection();
	}
	
	
	public boolean saveSeekers(String username,String password,String qualification)
	{
		getConnect();
		sql="insert into seekers_reg_tb(seekers_id,username,password,qualification) values(?,?,?,?)";
		try 
		{
				
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, 0);
			pstmt.setString(2, username);
			pstmt.setString(3, password);
			pstmt.setString(4, qualification);
			
			
			rows=pstmt.executeUpdate();
			
			status=true;
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				conn.close();
			} 
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return status;
		}
		
		
		
	public boolean saveProviders(String username,String password)
		{
			getConnect();
			sql="insert into providers_reg_tb(providers_id,username,password) values(?,?,?)";
			try 
			{
					
				pstmt=conn.prepareStatement(sql);
				pstmt.setInt(1, 0);
				pstmt.setString(2, username);
				pstmt.setString(3, password);
				
				
				rows=pstmt.executeUpdate();
				
				status=true;
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally
			{
				try 
				{
					conn.close();
				} 
				catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		return status;
	}


		
		
		
}
