package com.actions;

import com.bslogics.RegisterBsLogic;

public class ProvidersRegistrationAction 
{	
	private String username;
	private String password;
	
	
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}



	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}



	public String execute()
	{
		System.out.println(username+"\n"+password+"\n");
		
		if(new RegisterBsLogic().saveProviders(username, password))
			return "success";
			else
				return "failure";
	}
	
}
