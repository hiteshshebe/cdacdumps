package com.actions;

public class PostAdsAction 
{
	
	private String job_title;
	private String qualification;
	private String no_of_seats;
	private int providers_id;
	
	
	/**
	 * @return the job_title
	 */
	public String getJob_title() {
		return job_title;
	}

	/**
	 * @param job_title the job_title to set
	 */
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the no_of_seats
	 */
	public String getNo_of_seats() {
		return no_of_seats;
	}

	/**
	 * @param no_of_seats the no_of_seats to set
	 */
	public void setNo_of_seats(String no_of_seats) {
		this.no_of_seats = no_of_seats;
	}

	/**
	 * @return the providers_id
	 */
	public int getProviders_id() {
		return providers_id;
	}

	/**
	 * @param providers_id the providers_id to set
	 */
	public void setProviders_id(int providers_id) {
		this.providers_id = providers_id;
	}
	

	
	public String execute()
	{
		System.out.println(job_title+"\n"+qualification+"\n"+no_of_seats+"\n"+providers_id);
		return "success";
	}

	
}
