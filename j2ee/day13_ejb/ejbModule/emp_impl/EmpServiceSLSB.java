package emp_impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;

import javax.sql.DataSource;

import pojos.EmpPOJO;
import cust_exc.EmpNotFoundException;
import emp.EmpService;

@Stateless(mappedName = "emp_slsb123")
public class EmpServiceSLSB implements EmpService {
	// EJBC automatically locates DS inst form JNDI namespace & injects it in ds
	// internally InitialContext ctx=new IC(), & performs ctx.lookup(.....)
	@Resource(mappedName = "jdbc/mysql_ds")
	private DataSource ds;

	public EmpServiceSLSB() {
		System.out.println("in slsb constr " + ds);
	}

	@PostConstruct
	public void init() {
		System.out.println("in post con " + ds);
	}

	@PreDestroy
	public void destroy() {
		System.out.println("in destroy");
	}

	@Override
	public EmpPOJO getEmpDetails(int empId) throws EmpNotFoundException {
		String sql = "select * from my_emp where empid=?";
		PreparedStatement pst = null;

		ResultSet rst = null;
		try (Connection cn = ds.getConnection()) {
			// pst
			pst = cn.prepareStatement(sql);
			pst.setInt(1, empId);
			rst = pst.executeQuery();
			if (rst.next())
				return new EmpPOJO(empId, rst.getString(2), rst.getString(3),
						rst.getDouble(4), rst.getString(5), rst.getDate(6));
			throw new EmpNotFoundException("Emp dtls not found!!!!!!");
		} catch (SQLException e) {
			throw new EmpNotFoundException("Server Internal err "+e);
		} finally {
			try {
				if (rst != null)
					rst.close();
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				throw new EmpNotFoundException("Server Internal err");
			}
		}
	
	}

}
