package calc_impl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

import calc.Calculator;

@Stateless(mappedName="my_slsb")
public class CalcSLSB implements Calculator{
	private double result;
	//Bean constr
	public CalcSLSB() {
		System.out.println("slsb constr");
	}
	@PostConstruct
	//who invokes --EJBC --- how many times --once per bean inst 
	//when -- after inst.
	public void myInit()
	{
		System.out.println("in post constr");
	}
	@PreDestroy
	//invoker -- EJBC , how many -- once per bean inst when --- 
	//before GCing slsb inst
	public void myDestroy()
	{
		System.out.println("in pre destroy");
	}
	//	adding state member to confirm stateless behaviour
	

	@Override
	public double add(double d1, double d2) {
		// TODO Auto-generated method stub
		result += (d1+d2);
		return result;
	}

	@Override
	@Remove
	//to tell EJBC to destroy Bean inst
	public void doLogOut() {
		System.out.println("in do log out");
		
	}
	
	

}
