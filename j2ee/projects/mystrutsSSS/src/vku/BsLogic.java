package vku;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BsLogic 
{
	Connection conn=null;
	PreparedStatement pstmt=null;
	String sql=null;
	ResultSet rs=null;
	boolean flag=false;
	
	public boolean check(String userid,String psw)
	{
		conn=new ConnectionDB().connect();
		String username="";
		String password="";
		
		
		sql="select * from login where userid=?";
		
		try 
		{
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, userid);
			
			rs=pstmt.executeQuery();
			
			while(rs.next())
			{
				username=rs.getString("userid");
				password=rs.getString("psw");
			}
			
			if(userid.equals(username) && psw.equals(password))
				flag=true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		
		
		return flag;
	}
	
	
}
