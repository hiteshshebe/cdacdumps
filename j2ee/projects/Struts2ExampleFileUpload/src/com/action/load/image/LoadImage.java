package com.action.load.image;

import java.io.File;
import com.opensymphony.xwork2.ActionSupport;

public class LoadImage extends ActionSupport{
	
	
	File file;
	
	String path ;
	
	
	
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	private File userImage;
	
	private String userImageContentType;
	
	private String userImageFileName;

	public File getUserImage() {
		
		return userImage;
	}

	public void setUserImage(File userImage) {
		this.userImage = userImage;
	}

	public String getUserImageContentType() {
		return userImageContentType;
	}

	public void setUserImageContentType(String userImageContentType) {
		this.userImageContentType = userImageContentType;
	}

	public String getUserImageFileName() {
		return userImageFileName;
	}

	public void setUserImageFileName(String userImageFileName) {
		file = new File(userImageFileName);
		System.out.println(file.getAbsolutePath());
		setPath(file.getAbsolutePath());
		this.userImageFileName = userImageFileName;
	}
	
}
