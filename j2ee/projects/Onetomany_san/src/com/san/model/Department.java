package com.san.model;

import java.util.HashSet;
import java.util.Set;

public class Department 
{
	private Integer did;
	private String dname;
	private Set employees=new HashSet();
	
	//costructor 
	public Department()
	{
		
	}
	
	public Department(Integer did,String dname,Set employees)
	{
		
		this.did=did;
		this.dname=dname;
		this.employees=employees;
	}

	public Integer getDid() 
	{
		return did;
	}

	public void setDid(Integer did) 
	{
		this.did = did;
	}

	public String getDname() 
	{
		return dname;
	}

	public void setDname(String dname) 
	{
		this.dname = dname;
	}

	
	public Set getEmployees() 
	{
		return employees;
	}

	public void setEmployees(Set employees) {
		this.employees = employees;
	}
	
	
}
