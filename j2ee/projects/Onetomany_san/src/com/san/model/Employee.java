package com.san.model;

public class Employee 
{
	private Integer eid;
	private String ename;
	private Department deparment;
	
	public Employee()
	{
		
		
	}
	//calling constructor
	public Employee(Integer eid,String ename,Department department)
	{
		
		this.eid=eid;
		this.ename=ename;
		this.deparment=department;
	}


	public Integer getEid() {
		return eid;
	}


	public void setEid(Integer eid) {
		this.eid = eid;
	}


	public String getEname() {
		return ename;
	}


	public void setEname(String ename) {
		this.ename = ename;
	}
	public Department getDeparment() {
		return deparment;
	}
	public void setDeparment(Department deparment) {
		this.deparment = deparment;
	}
	


	
	
}
