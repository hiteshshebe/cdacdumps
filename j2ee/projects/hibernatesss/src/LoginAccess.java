

import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class LoginAccess 
{
	Session session=null;
	Configuration cnfg;
	SessionFactory sf=null;
	SessionFactory sessionFactory=null;
	Transaction transaction=null;
	
		
	
	public void get()
	{
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
			System.out.println("Session factory...");
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			LoginPojo login = (LoginPojo) session.get(LoginPojo.class, new String("deep") );
			System.out.println("Userid. : " + login.getUsername());
			System.out.println("Password : " + login.getPassword());
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
			
		}
	}
	
	public static void main(String[] args) {
		
		new LoginAccess().get();
	}
}
