<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
<head>
<title>Updating a database using the sql:update tag</title>
<sql:setDataSource var="dataSource" driver="oracle.jdbc.driver.OracleDriver"
	url="jdbc:oracle:thin:@localhost:1521:XE" user="system" password="admin"
	scope="session" />  
</head>
<body>
<h3>This is example of update query using JSTL SQL Tags.</h3>
<form method="post">
<table>
	<tr>
		<td>Enter Employee Number</td>
		<td><input type="text" name="eno"></td>
	</tr>
	<tr>
		<td>Enter Employee Name</td>
		<td><input type="text" name="name"></td>
	</tr>
	
	
	<tr>
		<td></td>
		<td><input type="submit" value="submit"></td>
	</tr>
</table>
</form>
<c:if test="${pageContext.request.method=='POST'}">
	<c:catch var="exception">
		<sql:update dataSource="${dataSource}" var="updatedTable">
    INSERT INTO emp (empno,ename) VALUES (?, ?)
   
			<sql:param value="${param.eno}" />
			<sql:param value="${param.name}" />
			
		</sql:update>
		<c:if test="${updatedTable>=1}">
			<font size="5" color='green'> 
			Congratulations ! Data inserted
			successfully.</font>
		</c:if>
		
	</c:catch>
	<c:if test="${exception!=null}">
		<c:out value="Unable to insert data in database." />
	</c:if>
</c:if>
</body>
</html>
