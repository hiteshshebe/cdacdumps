<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!-- SETLOCALE: -->
<fmt:setLocale value="en_US"/>


<!-- SETBUNDLE -->
<fmt:setBundle basename='offer'/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

FORMATNUMBER:
     <c:set var="price" value="29090"/>
          <fmt:formatNumber value="${price}" type="currency"/>
          <br/>
          <fmt:formatNumber type="number" value="20.9"
     maxFractionDigits="3" minFractionDigits="2" />
          
          <br/>
          
          
MESSAGE:  
          <fmt:message key="log"/>
          <br/>
          
PARSE DATE:
        
    <c:set var="us">4/1/03 10:15 AM</c:set>
<fmt:parseDate value="${us}" parseLocale="en_US"
              type="both" dateStyle="short" timeStyle="short" var="usDate"/>
              
<br/>
Parsing <c:out value="${us}"/> against the U.S. English locale yields a date of
  <c:out value="${usDate}"/>
  
  

<c:set var="gb">4/1/03 10:16</c:set>
<fmt:parseDate value="${gb}" parseLocale="en_GB"
  type="both" dateStyle="short" timeStyle="short" 
  var="gbDate"/>

<br/>
 Parsing <c:out value="${gb}"/> against the British English locale yields a date of
 <c:out value="${gbDate}"/>
    
    
    <br/>
 FORMAT DATE: 
    <jsp:useBean id="now" class="java.util.Date" />
 
 <fmt:formatDate value="${now}" pattern="dd MM yyyy" />
   
          
      

		

</body>
</html>