import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

class Connection extends Thread
{
	Socket netClient;
	BufferedReader br;
	PrintStream ps;
	
	public Connection(Socket client)
	{
		netClient=client;
		
		try 
		{
			br=new BufferedReader(new InputStreamReader(netClient.getInputStream()));
			ps=new PrintStream(netClient.getOutputStream());
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		this.start();	
	}
	
	public void run()
	{
		String msg;
		
		try 
		{
			msg=br.readLine();
			
			System.out.println(msg);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}

public class Servers extends Thread{
	
	ServerSocket ssocket;
	
	public Servers()
	{
		try 
		{
			ssocket=new ServerSocket(1234);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		System.out.println("Server Started.....");
		this.start();
	}
	
	
	public void run()
	{
		try 
		{
			Socket socket=ssocket.accept();
			Connection conn=new Connection(socket);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		

	}

}
