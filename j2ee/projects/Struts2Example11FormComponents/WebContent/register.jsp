<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="/struts-tags"  prefix="s"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
</head>
<body>
<center>Register Here.......... </center>
<br/>
<s:form action="Register">
	<s:textfield name="userName" label="User Name" />
	<s:password name="password" label="Password" />
	<s:password name="repassword" label="Retype Password" />
	<s:radio name="gender" label="Gender" list="{'Male','Female'}" />
	<s:select name="country" list="countryList" listKey="countryId"
		listValue="countryName" headerKey="0" headerValue="Select Country"
		label="Select a Country" />
		<s:checkboxlist list="communityList" name="community" label="Technology" />
		<s:textarea name="about" label="SkillSet" />
	<s:checkbox name="mailingList" label="Would you like to join our mailing list?" />
	<s:submit />
</s:form>

</body>
</html>