package com.dbconnect;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dao.EmpBean;
import com.dao.EmpDAOUpdateInterface;

public class EmpDAOUpdateClient {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
		EmpDAOUpdateInterface result = (EmpDAOUpdateInterface) applicationContext.getBean("updateDAO");
		
		EmpBean empBean = new EmpBean(2, "saranya", 50000, "IT");
		EmpBean empBean1 = new EmpBean(1);
		
		result.updateEmp(empBean);
		System.out.println("Updated successfully");
		
		//result.deleteEmp(empBean1);
		//System.out.println("Deleted Successfully");
	}
}
