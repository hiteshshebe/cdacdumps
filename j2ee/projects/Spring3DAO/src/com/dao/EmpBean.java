package com.dao;

public class EmpBean {

		//properties
		private int empno;
		private String ename;
		private int sal;
		private String deptname;
		
		//constructors
		public EmpBean(int empno, String ename, int sal,String deptname)
		{
			this.empno = empno;
			this.ename = ename;
			this.sal = sal;
			this.deptname=deptname;
			
		}
		
		public EmpBean(int sno){
			this.empno = sno;
		}

		//setters and getters
		public int getEmpno() {
			return empno;
		}

		public void setEmpno(int empno) {
			this.empno = empno;
		}

		public String getEname() {
			return ename;
		}

		public void setEname(String ename) {
			this.ename = ename;
		}

		/**
		 * @return the sal
		 */
		public int getSal() {
			return sal;
		}

		/**
		 * @param sal the sal to set
		 */
		public void setSal(int sal) {
			this.sal = sal;
		}

		public void setDeptname(String deptname) {
			this.deptname = deptname;
		}

		public String getDeptname() {
			return deptname;
		}
		
		public String toString()
		{
			return getEmpno() + "  " + getEname() + " " + getSal();
		}
	}
