<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sample jsp</title>
</head>
<body>
<%--scriptlet tag --%>
<% out.print("<br/>ServletContext::"+request.getServletContext()); %>
<% out.print("<br/>ServletURL::"+request.getServletPath()); %>
<% out.print("<br/>Method::"+request.getMethod()); %>

<%--declarative tag --%>
<%! int count=0; 

public int invoke(int a,int b)                 
{
	return a+b;
}
%>

<%--expression tag --%>
<%= "<br/>No. of user visited ::"+count++  %>
<%= "<br/>Result::"+invoke(12,12)%>
</body>
</html>