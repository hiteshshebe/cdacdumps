package com.cdac.dac3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	
	RequestDispatcher rd = null;
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/css");
		PrintWriter out = response.getWriter();
		
		ServletConfig config = getServletConfig();
		
		
		String role = config.getInitParameter("role");
		
		//Enumeration e = request.getParameterNames();
		
		/*while (e.hasMoreElements())
		{
			String s = e.nextElement().toString();
		//	System.out.println(request.getParameter(s));
			
		}*/
		
		/*Enumeration<String> ss = request.getHeaderNames();
		while (ss.hasMoreElements())
		{
			String s = ss.nextElement().toString();
			System.out.println(s+" : "+request.getHeader(s));
			
		}*/
		
		
		
		/*System.out.println("length : "+request.getContentLength());
		System.out.println("Scheme : "+request.getScheme());
		System.out.println("Method : "+request.getMethod());
		System.out.println("Context Path : "+request.getContextPath());
		System.out.println("Server Name : "+request.getServerName());
		System.out.println("Port : "+request.getLocalPort());
		
		System.out.println("Committed : "+response.isCommitted());
		System.out.println("Content Type "+response.getContentType());
		System.out.println( "Buffer Size "+response.getBufferSize());
		
		*/
		
		
		//get the data from html page
		
		String uid = request.getParameter("userid");
		String pwd = request.getParameter("password");
		
		LoginBean loginBean = new LoginBean();
		
		loginBean.setUserid(uid);
		loginBean.setPassword(pwd);
		boolean flag = loginBean.validate(uid, pwd);
		
		ServletContext context = getServletContext();
		
		
		
		
		if (flag && (role.equals("manager")))
			{
			
			out.println("Role : "+role);
			out.println(context.getInitParameter("role"));
		//	rd = request.getRequestDispatcher("/hai");
		//	rd.forward(request, response);
			}
		
		else
		{
		     out.println("Incorrect Userid and password");
		     out.println("Enter correct data........");
		     rd = request.getRequestDispatcher("login.html");
		     rd.include(request, response);
			
			//response.sendRedirect("login.html");
		}	
		
		out.close();	
		
	}

}
