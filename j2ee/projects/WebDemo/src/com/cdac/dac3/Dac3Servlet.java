package com.cdac.dac3;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Dac3Servlet
 */
public class Dac3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Dac3Servlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set the content type
		response.setContentType("text/html");
		// obtain the printerwriter object
		PrintWriter out = response.getWriter();
		ServletContext context = getServletContext();
		
		String contextdata = context.getInitParameter("role");
		
		out.println("Hello " + contextdata +"<br/><B> Welcome to Servlet</B>");
		out.close();
					
	}

}
