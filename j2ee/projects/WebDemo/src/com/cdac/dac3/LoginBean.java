package com.cdac.dac3;

public class LoginBean {
	//properties
	
	private String userid;
	private String password;
	
	//getters - setters
	
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public boolean validate(String uid,String pwd)
	{
		LoginBsLogic bslogic = new LoginBsLogic();
		return bslogic.loginValidate(uid, pwd);
	}
	
	

}
