package com.bslogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.dbconnection.DBConnection;

public class LoginLogic {
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	boolean status = false;
	
	public boolean validateLogin(String uid, String pwd)
	{
		DBConnection dbconn = new DBConnection();
		conn = dbconn.dbConnection();
		String sql = "Select userid from login where userid = ? and password=?";
		
		try
		{
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1,uid);
			pstmt.setString(2,pwd);
			rs = pstmt.executeQuery();
			if (rs.next())
				status = true;
		}catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return status;
		
	}

}
