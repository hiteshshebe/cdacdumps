package another.com;

import one.com.Address;
import one.com.Student;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class MappingDao 
{
	
	
	public void insert()
	{

		SessionFactory sf=null;
		Session sess=null;
		Transaction tx=null;
		try
		{
		sf=new Configuration().configure().buildSessionFactory();
		sess=sf.openSession();
		tx=sess.beginTransaction();
		
		System.out.println("data inserted..");
		
		Address a = new Address(7878,"Nagpur");
		sess.save(a);
		Student s = new Student(326,"Chhotu",a);
		sess.save(s);
		tx.commit();
		}
		catch (HibernateException e)
		{
			e.printStackTrace();
		}
		finally
		{
			sess.flush();
			sess.close();
		}
		
	}
	public static void main(String[] args)
	{
		new MappingDao().insert();
	}

}
