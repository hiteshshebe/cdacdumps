package one.com;

public class Student 
{
	int sid;
	String name;
	int address;
	
	public Student(){}
	
	public Student(int sid,String name, int add)
	{
		this.sid=sid;
		this.name=name;
		this.address=add;
	}
	
	
	public int getSid() 
	{
		return sid;
	}
	public void setSid(int sid)
	{
		this.sid = sid;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public Address getAddress()
	{
		return address;
	}
	public void setAddress(Address address) 
	{
		this.address = address;
	}
	
	
}
