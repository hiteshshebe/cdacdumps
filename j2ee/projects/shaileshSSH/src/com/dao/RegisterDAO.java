package com.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.pojo.LoginPojo;
import com.pojo.RegisterPojo;

public class RegisterDAO 
{
	private SessionFactory sessionFactory;
	private String status = "failure";
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public String saveUser(RegisterPojo login)
	{
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(login);
		System.out.println("data saved ........");
		tx.commit();
		status="success";
		return status;
	}
	
	public String getUser(String username)
	{
		/*System.out.println(username);
		Session session=getSessionFactory().openSession();
		Query query=session.createQuery("from LoginPojo");
		
		Iterator it=query.iterate();
		
		while(it.hasNext())
		{
			LoginPojo lp=(LoginPojo)it.next();
			System.out.println(lp.getUsername());
			System.out.println(lp.getPassword());
		}*/
		
		Session session=getSessionFactory().openSession();
		Criteria criteria=session.createCriteria(LoginPojo.class);
		criteria.add(Restrictions.ilike("username",username));
		
		List list=criteria.list();
		Iterator it=list.iterator();
		
		while(it.hasNext())
		{
			LoginPojo lp=(LoginPojo)it.next();
			System.out.println(lp.getUsername());
			System.out.println(lp.getPassword());
		}
		
		return "success";
	}
}
