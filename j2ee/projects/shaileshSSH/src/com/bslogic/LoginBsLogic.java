package com.bslogic;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dao.RegisterDAO;
import com.pojo.RegisterPojo;

public class LoginBsLogic 
{
	public String list(String username)
	{
		ApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		RegisterDAO rdao=(RegisterDAO)context.getBean("logindao");
		
		return rdao.getUser(username);
	}
}
