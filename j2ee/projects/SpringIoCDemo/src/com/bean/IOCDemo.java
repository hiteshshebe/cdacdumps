package com.bean;

import org.springframework.core.io.ClassPathResource;
import org.springframework.beans.factory.xml.XmlBeanFactory;


public class IOCDemo {

	
	
	static void getBean()
	{
		ClassPathResource resource = new ClassPathResource("beans.xml");
		XmlBeanFactory factory = new XmlBeanFactory(resource);
		HelloSpring hello = (HelloSpring)factory.getBean("helloSpring");
		hello.display();
		System.out.println(hello.getMessage());
	}
	
	public static void main(String[] args) {
		getBean();

	}

}
