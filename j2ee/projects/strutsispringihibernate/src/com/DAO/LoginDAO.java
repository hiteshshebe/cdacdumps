package com.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.bean.Login;

public class LoginDAO {
	
	private SessionFactory sessionFactory;
	private String status = "failure";
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public String saveUser(Login login)
	{
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(login);
		System.out.println("data saved ........");
		tx.commit();
		status="success";
		return status;
	}

}
