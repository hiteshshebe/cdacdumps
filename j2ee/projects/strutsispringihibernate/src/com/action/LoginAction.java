package com.action;

import com.BSLogic.BSLoginLogic;

public class LoginAction {

	private String userid;
	private String password;
	
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String execute()
	{
		BSLoginLogic bslogic = new BSLoginLogic();
		return bslogic.insertUser(userid, password);
	}
}
