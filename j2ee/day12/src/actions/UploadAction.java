package actions;
import java.io.File;

import com.opensymphony.xwork2.ActionSupport;



public class UploadAction extends ActionSupport
{
	
	private File abc;
	private String abcFileName,abcContentType,uploadDest;
	public File getAbc() {
		return abc;
	}
	public void setAbc(File abc) {
		this.abc = abc;
	}
	public String getAbcFileName() {
		return abcFileName;
	}
	public void setAbcFileName(String abcFileName) {
		this.abcFileName = abcFileName;
	}
	public String getAbcContentType() {
		return abcContentType;
	}
	public void setAbcContentType(String abcContentType) {
		this.abcContentType = abcContentType;
	}
	public String getUploadDest() {
		return uploadDest;
	}
	public void setUploadDest(String uploadDest) {
		this.uploadDest = uploadDest;
	}
	@Override
	public String execute() throws Exception {
		// file upload interceptor has alrdy uploaded contents --- tmp folder 
		//so simply store them in specified folder --- sepcified by action parameter.
		System.out.println(abc.getAbsolutePath());
		File dest=new File(uploadDest,abcFileName);
		boolean sts=abc.renameTo(dest);
		if (sts)
		return SUCCESS;
		return ERROR;
	}
	

}
