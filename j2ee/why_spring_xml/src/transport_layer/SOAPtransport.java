package transport_layer;

import java.util.Arrays;

public class SOAPtransport implements Transport {

	@Override
	public void communicateWithBank(byte[] dataPkt) {
		System.out.println("sent data over SOAP layer "
				+ Arrays.toString(dataPkt));

	}

}
