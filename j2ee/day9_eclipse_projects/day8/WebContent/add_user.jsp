<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="org.hibernate.*,hib_pojos.UserPOJO,utils.HibernateUtils"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form>
Email <input type="text" name="f1" /><br/>
Name<input type="text" name="f2" /><br/>
Password <input type="text" name="f3" /><br/>

<input type="submit" name="btn" value="Add User" /><br/>
</form>
<c:if test="${param.btn == 'Add User'}">
<%
//create transient User pojo 
    UserPOJO u1 = new UserPOJO(request.getParameter("f1"),
				request.getParameter("f2"), request.getParameter("f3"));

	Session hibSession=null;
	Transaction tx=null;
	try {
		//from sf --- get session
		hibSession=HibernateUtils.getSession();
	
		//begin tx
		tx=hibSession.beginTransaction();
		//CRUD work
		
		out.print("User  added with ID "+hibSession.save(u1));
	
	
	
		tx.commit();
	} catch (Exception e)
	{
		System.out.println("err in add user "+e);
		if (tx != null)
			tx.rollback();
	} finally {
		if (hibSession != null) {
		
			hibSession.close();
		}
	}

   
%>
</c:if>
</body>
</html>