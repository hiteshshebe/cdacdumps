<%@page import="rev_pojos.MoviePOJO"%>
<%@page import="java.util.List"%>

<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>

		<br />
		<br />

		<%
			Session hs = null;
			Transaction tx = null;
			List<MoviePOJO> movies = null;
			try {
				hs = HibernateUtils.getSession();
				System.out.println("sess bef  "+hs);
				tx = hs.beginTransaction();
				//CRUD
				//from session ---query ---exec it ---get rdy made populated book list

				movies = hs.createQuery("select m from MoviePOJO m").list();
				System.out.println(movies);
				tx.commit();
			} catch (Exception e) {
				if (tx != null)
					tx.rollback();
			} finally {
				if (hs != null) {
					System.out.println("sess aft "+hs);
					hs.close();
				}
			}
			pageContext.setAttribute("movie_list", movies);
		%>
		<c:forEach var="mv" items="${movie_list}">
   Title ${mv.name} Category ${mv.category} Price ${mv.price}<br/>
</c:forEach>

	</center>
</body>
</html>