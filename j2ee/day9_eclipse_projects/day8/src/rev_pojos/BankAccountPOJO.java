package rev_pojos;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the bank_accounts database table.
 * 
 */
@Entity
@Table(name="bank_accounts")
public class BankAccountPOJO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ac_no")
	private int acNo;

	@Column(name="bal")
	private double balance;

	private String type;

	//bi-directional many-to-one association to BankUserPOJO
    @ManyToOne
	@JoinColumn(name="id")
	private BankUserPOJO bankUser;

    public BankAccountPOJO() {
    }

	public int getAcNo() {
		return this.acNo;
	}

	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}

	public double getBalance() {
		return this.balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BankUserPOJO getBankUser() {
		return this.bankUser;
	}

	public void setBankUser(BankUserPOJO bankUser) {
		this.bankUser = bankUser;
	}
	
}