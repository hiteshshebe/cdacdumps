package rev_pojos;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the bank_users database table.
 * 
 */
@Entity
@Table(name="bank_users")
public class BankUserPOJO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int userId;

	private String name;

	private String password;

	//bi-directional many-to-one association to BankAccountPOJO
	@OneToMany(mappedBy="bankUser")
	private List<BankAccountPOJO> bankAccounts;

    public BankUserPOJO() {
    }

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<BankAccountPOJO> getBankAccounts() {
		return this.bankAccounts;
	}

	public void setBankAccounts(List<BankAccountPOJO> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}
	
}