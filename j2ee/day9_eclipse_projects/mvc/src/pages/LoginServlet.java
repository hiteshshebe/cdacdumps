package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CustomerBean;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login_servlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		//get user dtls
		String email=request.getParameter("emailID");
		String p=request.getParameter("pass");
		//create model(bean) 
		CustomerBean c1=new CustomerBean(email, p);
		//add it to reqd scope --- request scope is suff --- 
		//as entire mvc getting over in 1 single req.
		request.setAttribute("cust_bean", c1);
		//invoke B.L from bean , chk outcome & forward user to suitable view page
		String sts=c1.validateCustomer();
		RequestDispatcher rd=request.getRequestDispatcher(sts.concat(".jsp"));
		if (rd != null)
			rd.forward(request, response);
		else
			System.out.println("rd for jsp not created in "+getClass().getName());
		} catch (Exception e) {
			throw new ServletException("err in do-get of LoginServlet", e);
		}
	}

}
