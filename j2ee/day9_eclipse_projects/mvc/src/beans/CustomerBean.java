package beans;

import java.io.Serializable;

import rev_pojos.CustomerPOJO;

import dao.CustomerDAO;

public class CustomerBean implements Serializable {
	private String emailID, pass;
	private CustomerDAO dao;
	private CustomerPOJO cust;

	// def constr --- for the sake of JB specs
	// 1 option ---controller calling def constr , followed by setters
	// or paramed constr.
	public CustomerBean() {
		// TODO Auto-generated constructor stub
	}

	public CustomerBean(String emailID, String pass) {
		super();
		this.emailID = emailID;
		this.pass = pass;
		dao=new CustomerDAO();
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public CustomerPOJO getCust() {
		return cust;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	// B.L -- no need of pseudo getters or cust tags since servlet controller
	// can directly invoke any named B.L method of model layer.
	public String validateCustomer() throws Exception
	{
		String outcome="user_err";
		cust=dao.validateCustomer(emailID, pass);
		if (cust != null)
			outcome="user_ok";
		return outcome;
	}

}
