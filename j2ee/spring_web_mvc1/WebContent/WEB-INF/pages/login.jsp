<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.error {
	color: red
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form:form method="post" modelAttribute="contact">
		<form:label path="email">Enter User Email</form:label>
		<form:input path="email" />
		<form:errors cssClass="error" path="email" /><br/>
		<form:label path="password">Enter User Password</form:label>
		<form:password path="password" />
		<form:errors cssClass="error" path="password" /><br/>
		<input type="submit"  value="Login"/>
	</form:form>
</body>
</html>