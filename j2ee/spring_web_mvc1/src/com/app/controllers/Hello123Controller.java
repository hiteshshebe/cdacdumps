package com.app.controllers;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Hello123Controller {

	public Hello123Controller() {
		System.out.println("in constr " + getClass().getName());
	}

	// method level anno
	// def value --- url pattern
	// mapping key= /hello value ---- Hello123Controller : anyName
	@RequestMapping("/hello")
	public String anyName() {
		System.out.println("in any name");
		return "welcome";
	}

	@RequestMapping("/hello1")
	public ModelAndView anyName1() {
		System.out.println("in any name1");
		//API of ModelAndView class
		//ModelAndView(String viewName,String modelName,Object val)
		//model = req scope attribute
		ModelAndView mv = new ModelAndView("welcome1", "abc", "Hello again , @"
				+ new Date());
		return mv;
	}
	
	@RequestMapping("/hello2")
	public String anyName2(Model m) {
		//Model --- represents map of rq scoped attrs.
		System.out.println("in any name2");
		m.addAttribute("attr1", new Date());
		
		return "welcome2";
	}

}
