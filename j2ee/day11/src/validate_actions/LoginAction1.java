package validate_actions;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction1 extends ActionSupport {
	private String email,pass;
	public LoginAction1() {
		System.out.println("in constr "+getClass().getName());
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	@Override
	public String execute() throws Exception {
		//B.L validations --- currently success always
		System.out.println("in exec ");
		return SUCCESS;
		
	}
	@Override
	public void validate() {
		//P.L validation rules
		System.out.println("in validate");
		if (email == null || "".equals(email))
			addFieldError("email", getText("email.required"));
		if (email == null || !email.contains("@"))
			addFieldError("email",getText("email.invalid"));
		if (pass == null || "".equals(pass))
			addFieldError("pass",getText("password.required"));
	}
	
}
