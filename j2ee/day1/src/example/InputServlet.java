package example;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputServlet
 */
@WebServlet("/test_input")
public class InputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			pw.print("<h3>");
			pw.print("Hello , " + request.getParameter("f1") + "<br/>");
			pw.print("Favourite clrs "
					+ Arrays.toString(request.getParameterValues("clr"))
					+ "<br/>");
			pw.print("Browser selected  " + request.getParameter("browser") + "<br/>");
			pw.print("Selected Value  " + request.getParameter("myselect") + "<br/>");
			pw.print("<a href='s2'>Next</a>");
			pw.print("</h3>");
		}
	}

}
