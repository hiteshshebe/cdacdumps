<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<body>
	<center>
		<h3>
			<table border="1">
				<tr>
					<th>Acct ID</th>
					<th>Acct Type</th>
					<th>Balance</th>
				</tr>
				<c:forEach var="abc"
					items="${sessionScope.bank_user.userDetails.accts}">
					<tr>
						<td>${abc.acctId}</td>
						<td>${abc.type}</td>
						<td>${abc.balance}</td>
					</tr>
				</c:forEach>
			</table>
		</h3>
		<c:url var="url" value="bank_user_ok.jsp"/>
		<a href="${url}">Back</a>
	</center>
</body>
</html>