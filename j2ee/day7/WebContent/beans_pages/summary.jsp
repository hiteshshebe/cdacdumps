<%@page import="pojos.BankAcctPOJO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<jsp:useBean id="bank_user" class="beans.BankBean" scope="session" />
<body>
	<%
		//get array of accts
		ArrayList<BankAcctPOJO> l1 = bank_user.getUserDetails().getAccts();
		for (BankAcctPOJO b : l1) {
	%>
	Acct ID
	<%=b.getAcctId()%>
	Acct Type
	<%=b.getType()%>
	Bal
	<%=b.getBalance()%><br />
	<%
		}
	%>
	<a href="bank_user_ok.jsp">Back</a>
</body>
</html>