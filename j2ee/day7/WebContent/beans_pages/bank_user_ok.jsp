<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<center>
<h3>Successful login
Hello , ${sessionScope.bank_user.userName}<br/>
<%-- <%
  String encodeURL1=response.encodeURL("summary_jstl.jsp");
  String encodeURL2=response.encodeURL("logout.jsp");
%>
 --%>
 <c:url  var="url1" value="summary_jstl.jsp"/>
<a href="${url1}">View Account Summary</a><br/>

 <c:url  var="url2" value="logout.jsp"/>
<a href="${url2}">Log Out</a>
</h3>
</center>
</body>
</html>