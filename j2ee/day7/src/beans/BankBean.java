package beans;

import java.io.Serializable;

import pojos.BankUserPOJO;

import dao.BankDAO;

public class BankBean implements Serializable {
	//props
	private String userName,userPass;
	private BankDAO dao;
	private BankUserPOJO userDetails;
	//constr
	public BankBean() throws Exception{
		System.out.println("in bank bean constr");
		dao=new BankDAO();
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public BankUserPOJO getUserDetails() {
		return userDetails;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	//B.L  --pseudo getter --- to avoid scriptlets in JSP
	public String getStatus() throws Exception
	{
		String sts="bank_user_err";
		userDetails=dao.validateUser(userName, userPass);
		if (userDetails != null)
			sts="bank_user_ok";
		return sts;
	}
	public void logout() throws Exception{
		if (dao != null)
			dao.cleanup();
		dao=null;
	}
	

}
