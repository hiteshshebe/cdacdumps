package cust_tags;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import beans.BankBean;

public class LogOutHandler extends SimpleTagSupport {
	@Override
	public void doTag() throws JspException, IOException {
		/*
		 * //dao's clean up bank_user.logout(); session.invalidate();
		 */
		try {
			// get Http session
			HttpSession hs = ((PageContext) getJspContext()).getSession();
			// from HS -- bean ref
			BankBean ref = (BankBean) hs.getAttribute("bank_user");
			if (ref != null)
				// call logout
				ref.logout();
			// invalidate session
			hs.invalidate();
		} catch (Exception e) {
			throw new JspException("err in do-tag", e);
		}
	}

}
