package simple_actions;

import java.util.HashMap;

import pojos.UserPOJO;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class LoginAction1 extends ActionSupport implements ModelDriven<UserPOJO> {
	private UserPOJO u,details;
	private HashMap<String, UserPOJO> users;
	
	public LoginAction1() {
		System.out.println("in la constr");
		u=new UserPOJO();
		//create n populate user map
		users=new HashMap<>();
		users.put("a@gmail", new UserPOJO("a@gmail","123", 500));
		users.put("b@gmail", new UserPOJO("b@gmail","1234", 600));
	}
	@Override
	public UserPOJO getModel()
	{
		System.out.println("in getModel");
		return u;
	}
	@Override
	public String execute() throws Exception
	{
		System.out.println("in la1 exec");
		//get email pass sent by clnt
		String em=u.getEmail();
		String pa=u.getPass();
		System.out.println(em +"  "+pa);
		if (users.containsKey(em))
			if(users.get(em).getPass().equals(pa)) {
				details=users.get(em);
				return SUCCESS;
		}
		return ERROR;
						
	}
	public UserPOJO getDetails() {
		System.out.println("in get dtls");
		return details;
	}
	public UserPOJO getU() {
		System.out.println("in getU  ");
		return u;
	}
	
	
	
	

}
