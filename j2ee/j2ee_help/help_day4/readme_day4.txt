How to get ServletContext from Servlet?
public ServletContext getServletContext() --- method of HttpServlet
eg ---doGet(....)
{
//how to add attr under appln scope -in thrd safe manner?

ServletContext sc=getServletContext();
synchronized(sc) {
sc.setAttribute("name",val);
}
eg ---- /day3,/day4,/day5----how many SC objs will be created by the WC?

What are context params & where they are stored & their usage?
=parameters global(shared) across entire web appln.
Have name(String) & value(String)
Stored in web.xml ---
<context-param>
<param-name>aa</param-name>
<param-value>bb</param-value>
</context-param>
Usage -- to store string data shared across entire web appln.(Read only)
How to read ctx param value from servlet ?

  ServletContext API ---
String getInitParameter(String name) --- rets ctx param value.

eg --- doGet(....)
getServletContext().getInitParameter("aa")-----bb

About javax.servlet.ServletConfig ---i/f
Represents i/f to store servlet specific init params.
need of servlet specific init params ---for parameterizing the servlet (eg to make servlet absolutely DB inde --- u can pass DB specific params via init params)

where to store servlet specific init params --- either via annotations or in web.xml
In web.xml----
<servlet>
<servlet-name>.....</>
<servlet-class>....</>
<init-param>
<param-name>drvr</param-name>
<param-value>oracle.jdbc.OracleDriver</param-value>
</init-param>
</servlet>

who creates its inst? --- WC
when?---- after inst servlet class , WC creates populated inst of ServletConfig & then invokes public void init() method.
how is it made available to servlet? ---- via ServletConfig API
& API 
String getInitParameter(String name) --- rets param value.










