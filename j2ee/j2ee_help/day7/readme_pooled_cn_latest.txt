Config. steps for the pooled connectivity
0. stop the srvr.

MUST Copy JDBC drvr jar(ojdbc6.jar, my sql connector) to tomcat-home/lib

Ref Doc : <TomCat_Home>\webapps\docs\jndi-datasource-examples-howto.html

1. Create context.xml under web-appln's <META-INF> & add resource tag as follows.


<Resource name="jdbc/mysql_pool" auth="Container" type="javax.sql.DataSource"
		initialSize="1" maxActive="2" maxIdle="1" maxWait="-1" username="root"
		password="root" driverClassName="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/testjdbc"
		removeAbandoned="true" />


2. Open ur web-appln's web.xml(WebContent\web-inf\web.xml)

copy the Resource - ref. tag in web.xml

eg :
<resource-ref>
 <description>My SQL Datasource example</description>
 <res-ref-name>jdbc/mysql_pool</res-ref-name>
 <res-type>javax.sql.DataSource</res-type>
 <res-auth>Container</res-auth>
</resource-ref>

ENSURE : res-ref-name matches with Resource name added in context.xml

3. Continue to API


4. For performing look-up : from DAO layer 
InitialContext ctx = new InitialContext();
ds = (DataSource) ctx.lookup("java:comp/env/jdbc/mysql_pool");

5. In clean up 
close initial context.

6. In CRUD methods
MOST IMPORTANT --- get connection from Pool just before CRUD & return connection to pool immediately after CRUD is done.

try (Connection cn = ds.getConnection();
//prepare stmt)
{ // CRUD logic} finally { close RST if opened}


				




