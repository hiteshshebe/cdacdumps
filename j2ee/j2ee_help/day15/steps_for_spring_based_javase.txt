Steps in creating spring based Java SE appln
1. Add external Jars --- spring 3.1.x related jars
2. Create Spring configuration xml file -in run time classpath --- , containing metdata info (info --- how to create POJOS--spring JBs,D.I, how to manage life-cycle) OR replace by spring 3 annotations
3. Create Tester spring application(main based)
3.1 Load spring frmwork & start the SC(spring container) ----
org.springframework ----can use either BeanFactory as i/f to SC OR use its sub-i/f ApplicationContext -- which  supports i18n,message resource bundles, event handling......
3.2 Get spring bean from container & invoke B.L

Ways of Dependency Injection
1. setter based D.I ---SC will auto call matching setters --- when it comes across ---<property> tag from xml file
2. Constructor based D.I ---- SC will auto call matching paramterized constr & inject the dependencies.
eg --- HelloBean --- property -- message -- supply B.L only  & supply paameterized constr ---- constr arg --- mesg
TestHelloBean --- 
Testing sequence 

1. change scope from singleton to prototype & observe & understand difference.
2. add init-method , destroy-method, lazy-init , observe & understand difference.
3. supply meta-data info using spring 3 annotations. (i.e replace xml by annotations)
3.1 
How to remove setter based D.I or constr arg from bean def
Annotation --  auto -wiring approach 
@Resource(name="bean id")
private data member
wiring = making dependencies available to dependent objs(setter or constr based)
auto-wiring --- w/o any of these types.
or 
@AutoWired
@Qualifier("bean id")
private data member













