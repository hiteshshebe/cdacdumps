create table bank_users(id int(3) primary key,name varchar(20),
password varchar(20));
insert into bank_users values(1,'a','b');
insert into bank_users values(2,'c','d');
insert into bank_users values(3,'e','f');


create table bank_accounts(ac_no int(3) primary key,id int(3) ,type varchar(10),bal double(6,1),CONSTRAINT fk_users
    	  FOREIGN KEY (id)
    	  REFERENCES bank_users(id));
insert into bank_accounts values(101,1,'NRO',1000);
insert into bank_accounts values(102,1,'NRE',2000);
insert into bank_accounts values(103,2,'SAV',3000);
insert into bank_accounts values(104,20,'SAV',3000);//this will not work as integrity constr. parent key not found.

//to selecte all account's info for given user id
select a.ac_no,a.type,a.bal from bank_users u,bank_accounts a where
u.id=? and u.id=a.id;







