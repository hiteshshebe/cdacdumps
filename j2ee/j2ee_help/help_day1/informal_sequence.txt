http://localhost:9090/my_web

Servlet API details
servlet-api.jar ---- consists of java pkges.
javax.servlet.Servlet ---- i/f --- GenericServlet(class) --- protoco inde. -- doesn't understand HTTP features.---- extended by HttpServlet (javax.servlet.http)  ----will be super-class for developing HTTP servlets.
init --- called by WC --- for initializing servlet(optional)
service --- called by WC --- per clnt req -- for servicing logic --- HttpServlet class has alrdy overridden service method -- it chks by which HTTP method clnt has sent req --- if its get ---- it will call doGet, for post ---it will call doPost , put ---doPut.......


protected void doGet(HttpServletRequest rq,HttpServletResponse rs) throws ServletException,IOException

protected void doPost(HttpServletRequest rq,HttpServletResponse rs) throws ServletException,IOException


override doGet ----
1. set response content type.
API of HttpServletResponse 
public void setContentType(String type)

2. send data(response)
open o/p stream using API of HttpServletResponse 
public PrintWriter getWriter()
 write data using print/println/write methods
PrintWriter --- represents o/p strm, char oriented , buffered -- connected from server to clnt


how to compile?
E:\apache-tomcat-7.0.25\webapps\my_web\src>javac -d ..\WEB-INF\classes simple\*.
java

clnt sends a request for accessing servlet ---
http://localhost:9090/my_web/hello


http://localhost:9090/my_web/hello_again

To add multiple servlets using XML copy entire servlet & servlet-mapping tag & change the servlet name,class, url-pattern properly.


http://localhost:9090/day1/test_name?f1=abcde


http://localhost:9090/day1/test_name























