1. Revise CRUD with Hibernate session ---org.hibernate.Session
create ---save/saveOrUpdate/persist
Retrieve --- 1. get---id found -- rets inited proxy ref(loaded with state from DB---- select fired)---id not found --- null.
2. load  ---args & ret type same as get --- rets Object --- args are Class ref, Serializable id ----id found --- rets un-inited proxy ref ((not loaded with state from DB---- select not fired) ---if trying to access proxy outside HS scope --- Lazy init exc.
How to avoid --- reco soln --- invoke non-id getter from within scope of persistence ctx(env --- scope starting from open session ----till session.close)
Actual updation of DB state(synchronization) -- will be ensured iff ---tx.commit followed by session.close();
3. HQL/JPQL --- Hib 4.0.x -- JPA 2.0 compliant --- all JPQL queries will work in same manner on Hibernate.
hql="select b from BookPOJO b";
List<BP> l1=hs.createQuery(hql).list();
OR 
hql="select u from UserPOJO u where u.email = :em and u.pass = :pas";
UserPOJO ref=(UserPOJO) hs.createQuery(hql).setParameter("em",email).setParameter("pas",password).uniqueResult();   -- rets --- null or not null
when will this method raise exc? --- NonUniqueResult ---upon multiple POJOs matching the criteria

4. Update --- 
hql --- selected POJO/s --- updating state of obj ---obj being persistent --- auto hib is responsible in ---synchro. db state.(update queries fired auto).
4.1 OR
update query syntax ---- 
1. String hqlUpdate =	"update Customer c " +
		"set c.name = :newName " +
		"where c.name = :oldName";
int updatedEntities = hibSession.createQuery( hqlUpdate )
        .setString( "newName", newName )
        .setString( "oldName", oldName )
        .executeUpdate();

5. Delete --- 
hql--selected POJO/s --- for-each --hibSession.delete(pojoRef) ----pojo ref is ONLY marked for removal(removed state of POJO)  --- actual deletion from DB --- after tx.commit() , followed by hibSession.close() -----POJO becomes transient 

OR ---results  in delete query only
int updatedPojos = hibSession.createQuery ("DELETE s FROM Subscription s WHERE s.subscriptionDate < :today").setParameter ("today", new Date ()).executeUpdate ();




delete query syntax & compare it with session.delete

Enter associatons ---- lazy Vs eager fetching 
1. one--many bi-directional association
eg scenario ---single bank user --- having multiple bank accts.
get anno pojos using JPA --- rev eng tool---

BankUserPOJO ---....+ Set<BAP> accts; OR  List<BAP> accts
 --- if u choose a Set ---MUST override matching version of hashCode & equals ---- in BankAcctPOJO class.

BankAcctPOJO ---- 

Complete example with layers.











POJO w/o surrogate key 
POJO with surrogate key


Enter MVC
why MVC ---Front servlet controller pattern
MVC+hibernate demo
servlet filters
Enter Struts 2