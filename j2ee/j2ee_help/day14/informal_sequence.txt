slsb Vs sfsb
how to attach thick clnt to EJB?
1. Create appln clnt project
2. copy (via jar or directly) B.I , POJOs , custom excs (if any)
3. For remote (thick) clnts --- before performing JNDI look-up, appln clnt has to locate the host, hosting app server , port no & JNDI details.
These dtls are specified using jndi.properties file --- location under run tm class path of appln clnt.
For different vendors of appln servers --- keys will be same BUT vals are vendor specific.
4. Add external JAR in the classpath of appln client --- appserv-rt.jar (from application server  lib  D:\glassfish3\glassfish\lib)
5. Write client (main based appln) -- create IC, perform lookup & invoke B.M of EJB.
RULE for SLSB programming 
In SLSB , can add state(private non-static ,non-tx d.m ) --- iff --- its common , same (read only ) for all clients. eg --- DataSource 
BUT if state of SLSB reflects any clnt specific state --- it will not be maintained by EJBC
soln -- use SFSB instead.