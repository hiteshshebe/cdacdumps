<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3>Successful Registration, </h3>
<h3> Details -- Via  EL syntax ${param.userName}</h3>
<h3> User Name  -- via OGNL <s:property value="userName"/></h3>
<h3> Sub Amt -- via OGNL <s:property value="subAmt"/></h3>
<h3> Reg Date -- via OGNL <s:property value="regDate"/></h3>
<h3> Formatted Date  -- <s:date name="regDate" format="dd-MMM-yyyy"/></h3>
</body>
</html>