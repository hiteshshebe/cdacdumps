<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<s:form action="register.action" validate="true">
		<s:textfield name="userName" label="Enter User Name" />
		<s:password name="password" label="Enter Password" />
		<s:textfield name="subAmt" label="Enter Subscription Amt" />
		<s:textfield name="regDate" label="Enter Reg Date--MM/dd/YYYY" />
		<s:submit value="Register User" />
	</s:form>
</body>
</html>