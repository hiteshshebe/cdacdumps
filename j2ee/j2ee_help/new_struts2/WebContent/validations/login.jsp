
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
</head>

<body>

<h1>Struts 2 localization example</h1>

<h3><s:text name="xyz.welcome"/></h3>
<s:form action="validateUser" >

	<s:textfield key="global1.username" name="username" />
	<s:password key="global.password" name="password"/>	
	<s:submit key="global.submit" name="submit" />
	
</s:form>

<s:url var="localeEN"  action="locale" >
   <s:param name="request_locale" >en</s:param>
</s:url>

<s:url var="localeDE"  action="locale" >
   <s:param name="request_locale" >de</s:param>
</s:url>
<s:url var="localeFR"  action="locale" >
   <s:param name="request_locale" >fr</s:param>
</s:url>

<s:url var="localeMR"  action="locale" >
   <s:param name="request_locale" >mr_IN</s:param>
</s:url>

<s:url var="localeHI"  action="locale" >
   <s:param name="request_locale" >hi_IN</s:param>
</s:url>


<s:a href="%{localeEN}" >English</s:a>
<s:a href="%{localeDE}" >German</s:a>
<s:a href="%{localeFR}" >France</s:a>
<s:a href="%{localeMR}" >Marathi</s:a>
<s:a href="%{localeHI}" >Hindi</s:a>
 
</body>
</html>