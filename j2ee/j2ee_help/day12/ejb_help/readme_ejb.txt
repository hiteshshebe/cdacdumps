EJB : Enterprise Java Beans(EJB 3.x)

What is EJB ?
EJB - a server-side component , which resides within the ejb-container(run-time env of the EJBs) &  contains the B.L  of an application. 

In an inventory control application, for example, the enterprise
beans might implement the business logic in methods called checkInventoryLevel and
orderProduct. 

Thin clients via web-tier (HTTP protocol : using servlets/JSP/JBs/Struts Actions) can invoke these methods or Thick clnts  can access the inventory services provided
by the application.

Why use EJBs?(Benefits)

1. EJBs  simplify the development of large, distributed
applications -scalable,reliable,secure 

2. EJB container provides system-level services(Primary services : JNDI, conn. pooling, Transactions,security:authorization,authentication ,persistence: JPA) to the bean developer can concentrate on solving business problems.

3.For thick clnts : as the EJBs rather than the clients contain the application�s business logic, the client developer can focus on the presentation(view) of the client. As a result, the clients are thinner, especially suitable  for clients that run on small devices.(embedded)

4.  EJBs are portable & reusable components. The application assembler can build
new enterprise applications from existing beans.-- compliant on any Java EE
server(appln server)



Different types of Bean : Session bean & Message Driven bean

Business scenarios for using Session Beans : Stock price quote,video compression,bank transactions,shopping cart .

A session bean represents a single client inside the Application Server. 

To access an application(services) -  deployed on the server 
- the client invokes the session bean�s methods.(remote method invocation or Local method invocation)
 The session bean executes B.L , shielding the client from complexity by executing business tasks inside the server.

- session bean is similar to an interactive session.

- relatively short-lived object

- 2 categories - SLSB : stateless session bean & SFSB  - stateful session bean.

- For SLSB : clnt state is valid only for 1 method invocation.(equivalent to HTTP rqs without session tracking) eg : credit card verifier bean.
- For SFSB : clnt state is valid for the entire session.(equivalent to HTTP + session) : eg -
e-commerce web store

- session bean is not shared among multiple clnts,   can have only one client or 1 user. 

-  a session bean is not persistent (i.e , its data is not saved to a database auto) . - i.e session beans instances themselves DO NOT represent any DB data(as opposed to persistenet POJO instances) - but can definitely perform DB operations.

- When the client terminates, its session bean appears to terminate and is no longer associated with the client.


Mastering EJB pages : 
1. 31 ---overview of J2EE
2. chapter 2 : Pre-EJB 3 world (EJB 2 architecture)
3. 63 : EJB clnts & Beans
4. 82 : structure of EJB jar
5. 94 :SLSB pooling
6. 111 : SLSB life-cycle
7. 113 : SFSB life-cycle
8. 99 : SFSB : activation/passivation 




To create WAR outside IDE
cd to root of web -appln
jar cvf test.war .

To remove earlier deployments  
<jboss-install-dir>\server\default\work\jboss.web\localhost

Hot deployment  folder for JBoss
<jboss-install-dir>\server\default\deploy
(.war,.jar,.ear)

What is Dependency Injection ?
A technique ---- available to EJB 3 onwards ---EJB prog supplies list of resources(eg --- Conn pool,EntityManager,JMS conn factory) needed ----by giving JNDI names ---- EJBC contacts root of JNDI tree- performs lookup ---- & gets the resource ref --- copies the resource ref(injects res ref) in to the field supplied.















