package spring_tester;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import atm.ATMIntf;

public class TestSpringBank {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// load spring frmwork & init Spring container
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"spring-config.xml");
		System.out.println("started Spring container");
		//get bean ref- atm ref & perform B.L exec
		ATMIntf ref=(ATMIntf) ctx.getBean("my_atm");
		ref.deposit(5000);

	}

}
