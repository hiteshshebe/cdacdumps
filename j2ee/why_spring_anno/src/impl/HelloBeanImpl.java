package impl;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import intf.HelloBeanIntf;

@Component("hello_bean")
@Lazy(value=true)
//@Scope(value="singleton")
public class HelloBeanImpl implements HelloBeanIntf {
	
	@Value("some test message")
	private String message;
	
	@Value(value="510")
	private double subAmt;
	
	
/*	public HelloBeanImpl(String mesage,double subAmt) {
		System.out.println("in constr hello bean");
		this.message=mesage;
		this.subAmt=subAmt;
	}
*/	
	public HelloBeanImpl() {
		System.out.println("in constr "+message+"  "+subAmt);
	}
	@PostConstruct
	public void myInit()
	{
		System.out.println("in myInit "+message+"  "+subAmt);
	}
	@PreDestroy
	public void myDestroy()
	{
		System.out.println("in myDestroy");
	}


	@Override
	public void show() {
		System.out.println("from show : "+message);
		System.out.println("sub Amt "+subAmt);

	}

}
