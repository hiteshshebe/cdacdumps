package cust_tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class HelloTagHandler extends SimpleTagSupport {
	@Override
	public void doTag() throws JspException,IOException
	{
		// WC has alrdy invoked --- custom tag life cycle method
		// void setJspContext(JspContext ctx) ---Tag handler class has pageContext inst
		getJspContext().getOut().write("<h3> Welcome 2 Custom Tags!!!!!</h3>");
	}

}
