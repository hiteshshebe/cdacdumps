package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.BillPOJO;

import dao.BillDAO;

/**
 * Servlet implementation class DisplayCellDetails
 */
@WebServlet("/display_dtls")
public class DisplayCellDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			// create session ---WC actually rets existing sess inst
			HttpSession hs = request.getSession();
			BillDAO dao = (BillDAO) hs.getAttribute("bill_dao");
			BillPOJO b=(BillPOJO) hs.getAttribute("bill_dtls");
			if (b == null) {
			// invoke dao method to get cell dtls iff no BillPOJO alrdy added in session scope
			 b = dao.getCellDetails(request.getParameter("cell_no"));
			 //add BP in sess scope
			 hs.setAttribute("bill_dtls",b);
			}
			System.out.println("Bill dtls "+b);
			// disp details to user
			pw.print(b);
			String encodeURL=response.encodeURL("compute_bill");
			System.out.println("encoded url "+encodeURL);
			// generate form to accept new dtls
			pw.print("<html><body><form action='"+encodeURL+"'>");
			pw.print("Enter Call Duration <input type='text' name='call_duration'/><br/>");
			pw.print("Choose Plan <input type='radio' name='plan' value='plan-A'/>plan-A");
			pw.print("Choose Plan <input type='radio' name='plan' value='plan-B'/>plan-B");
			pw.print("Choose Plan <input type='radio' name='plan' value='plan-C'/>plan-C");
			pw.print("<input type='submit' value='Compute N Update Bill'/>");
			pw.print("</form><br/>");
			encodeURL=response.encodeURL("logout");
			System.out.println("encoded url "+encodeURL);
			pw.print("Click <a href='"+encodeURL+"'>LogOut<a/>to end ");
			pw.print("</body></html>");
			
		} catch (Exception e) {
			throw new ServletException("err in do-get of "
					+ getClass().getName(), e);
		}
	}

}
