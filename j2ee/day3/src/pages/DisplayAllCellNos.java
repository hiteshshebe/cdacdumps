package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BillDAO;

/**
 * Servlet implementation class DisplayAllCellNos
 */
@WebServlet("/get_all_cells")
public class DisplayAllCellNos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private BillDAO dao;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			//WC creates new HTTP session obj
			HttpSession hs=request.getSession();
			BillDAO dao=(BillDAO) hs.getAttribute("bill_dao");
			if (dao == null)
				dao=new BillDAO();
		
			//adding dao inst to session scope --- other pages can re-use
			hs.setAttribute("bill_dao",dao);
			//invoke dao method ---to get cell nos
			ArrayList<String> l1=dao.getCellNos();
			//generate form dyn --- display the same to user
			String encodeURL=response.encodeURL("display_dtls");
			System.out.println("encoded url "+encodeURL);
			pw.print("<html><body><form action='"+encodeURL+"'>");
			pw.print("<select name='cell_no'>");
			for(String s : l1)
				pw.print("<option value='"+s+"'>"+s);
			pw.print("</select>");
			pw.print("<input type='submit' value='Choose Cell No'/>");
			pw.print("</form></body></html>");
			
		} catch (Exception e) {
			throw new ServletException("err in do-get of "
					+ getClass().getName(), e);
		}
	}

}
