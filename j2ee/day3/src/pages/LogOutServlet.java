package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.BillPOJO;

import dao.BillDAO;

/**
 * Servlet implementation class LogOutServlet
 */
@WebServlet("/logout")
public class LogOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			// create session ---WC actually rets existing sess inst
			HttpSession hs = request.getSession();
			//dao
			BillDAO dao=(BillDAO) hs.getAttribute("bill_dao");
			
			//bp
			BillPOJO b=(BillPOJO) hs.getAttribute("bill_dtls");
		    //invoke dao method to sync state of Obje(BP) to underlying DB table
			
			pw.print("Updation Status   "+dao.updateCellDetails(b));
			dao.cleanup();
			hs.invalidate();
			pw.print("Thanks for visitng ....<br/>");
			pw.print("<a href='welcome.html'>Visit Again</a>");
		
		} catch (Exception e) {
			throw new ServletException("err in do-get of "
					+ getClass().getName(), e);
		}
	}

}
