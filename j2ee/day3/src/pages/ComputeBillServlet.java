package pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.BillPOJO;

/**
 * Servlet implementation class ComputeBillServlet
 */
@WebServlet("/compute_bill")
public class ComputeBillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// HS ---WC rets actually existing session 
		HttpSession hs = request.getSession();
		// get BillPOJO
		BillPOJO b = (BillPOJO) hs.getAttribute("bill_dtls");
		// rq params call_duration,plan=plan-B
		int callDuration = Integer.parseInt(request
				.getParameter("call_duration"));
		String plan = request.getParameter("plan");
		double bill = 0;
		switch (plan) {
		case "plan-A":
			bill = callDuration;// pulse rate=1
			break;
		case "plan-B":
			bill = callDuration * 1.5;// pulse rate=1.5
			break;
		case "plan-C":
			bill = callDuration * 2;// pulse rate=2
			break;
		}
		//update BillPOJO & place it in sess
		b.setCallDuration(callDuration);
		b.setPlan(plan);
		b.setBillAmt(bill);
		hs.setAttribute("bill_dtls", b);
		System.out.println("updated dtls "+b);
		//forward user in the SAME req to DisplayDtls page
		//RD
		RequestDispatcher rd=request.getRequestDispatcher("display_dtls");
		if (rd != null)
			rd.forward(request, response);
		else
			System.out.println("rd error");
	}

}
