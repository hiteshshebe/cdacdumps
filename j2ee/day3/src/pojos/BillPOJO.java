package pojos;

import java.io.Serializable;

public class BillPOJO implements Serializable {
	//D.M  -- props --- reflecting table colmns
	private String mobileNO,name,plan;
	private int callDuration;
	private double billAmt;
	public BillPOJO() {
		// TODO Auto-generated constructor stub
	}
	public BillPOJO(String mobileNO, String name, String plan,
			int callDuration, double billAmt) {
		super();
		this.mobileNO = mobileNO;
		this.name = name;
		this.plan = plan;
		this.callDuration = callDuration;
		this.billAmt = billAmt;
	}
	public String getMobileNO() {
		return mobileNO;
	}
	public void setMobileNO(String mobileNO) {
		this.mobileNO = mobileNO;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public int getCallDuration() {
		return callDuration;
	}
	public void setCallDuration(int callDuration) {
		this.callDuration = callDuration;
	}
	public double getBillAmt() {
		return billAmt;
	}
	public void setBillAmt(double billAmt) {
		this.billAmt = billAmt;
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder("Hello , "+name+"\n");
		sb.append("Cell NO "+mobileNO+"\n");
		sb.append("Current Plan "+plan+"\n");
		sb.append("Duration "+callDuration+"\n");
		sb.append("Total Bill "+billAmt);
		return sb.toString();
		
	}
	

}
