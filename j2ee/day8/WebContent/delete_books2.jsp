<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="hib_pojos.BookPOJO"%>
<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%!SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");%>
<body>
	<center>
		<form>
			Enter Publish Date <input type="text" name="f1" /><br /> 
			 <input type="submit" 	name="btn" value="Delete Books" /><br />
		</form>


		<br /> <br />
		<c:if test="${param.btn == 'Delete Books'}">
			<%
			String sts="Books Not Deleted";
				Date d1 = sdf.parse(request.getParameter("f1"));
				
					Session hs = null;
					Transaction tx = null;
					List<BookPOJO> books = null;
					try {
						hs = HibernateUtils.getSession();
						tx = hs.beginTransaction();
						//CRUD
						//delete hql
						String hql="delete BookPOJO b where b.publicationDate < :dt";
						int deletedPOJOS=hs.createQuery(hql).setParameter("dt", d1).executeUpdate();
						sts="Deleted Books successfully!!!!   "+ deletedPOJOS;
						
						tx.commit();
					} catch (Exception e) {
						if (tx != null)
							tx.rollback();
					} finally {
						if (hs != null)
							hs.close();
					}
					
			%>
			<h3><%= sts %></h3>
		</c:if>
	</center>
</body>
</html>