<%@page import="hib_pojos.BookPOJO"%>
<%@page import="utils.HibernateUtils"%>
<%@page import="org.hibernate.*"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<center>
<form>
Enter Book ID <input type="text" name="f1" /><br/>
<input type="submit" name="btn" value="Get Book Details" /><br/>
</form>
</center>
<br/><br/>
<c:if test="${param.btn == 'Get Book Details'}">
<%
//if btn is clicked 
	int bookId=Integer.parseInt(request.getParameter("f1"));
	Session hs=null;
    Transaction tx=null;
    BookPOJO b1=null;
    try {
    	hs=HibernateUtils.getSession();
    	tx=hs.beginTransaction();
    	//CRUD work
    	b1=(BookPOJO)hs.get(BookPOJO.class, bookId);
    	
    	tx.commit();
    } catch (Exception e)
    {
    	if (tx != null)
    		tx.rollback();
    } finally {
    	if (hs != null)
    		hs.close();
    }
    if (b1 != null)
    	out.print(b1);
    else
    	out.print("Book Not found!!!!!!!");
%>
</c:if> 
</body>
</html>