package emp;

import javax.ejb.Remote;

import cust_exc.EmpNotFoundException;

import pojos.EmpPOJO;

@Remote
public interface EmpService {
	 EmpPOJO getEmpDetails(int empId) throws EmpNotFoundException;

}
