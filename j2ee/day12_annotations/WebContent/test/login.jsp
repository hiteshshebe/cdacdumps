<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h3>
			<s:text name="test.mesg" />
		</h3>
	</center>
	<s:form action="login">
		<s:textfield name="email" key="any_name.email" />
		<s:password name="pass" key="global.password" />
		<s:submit key="global.submit" />
	</s:form>
	<s:a action="locale">
		<s:param name="request_locale">en</s:param>English
	</s:a><br/>
	
	<s:a action="locale">
		<s:param name="request_locale">de</s:param>German
	</s:a><br/>
	<s:a action="locale">
		<s:param name="request_locale">mr_IN</s:param>Marathi
	</s:a><br/>
</body>
</html>