<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="org.hibernate.*,utils.HibernateUtils,hib_pojos.BookPOJO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		//create BookPOJO inst --- transient
		BookPOJO b1 = new BookPOJO(request.getParameter("f1"),
				request.getParameter("f2"), Double.parseDouble(request
						.getParameter("f3")));

		Session hibSession = null;
		Transaction tx = null;
		try {
			//from SF --- open hib session
			hibSession = HibernateUtils.getSession();
			//begin tx
			tx = hibSession.beginTransaction();
			//perform crud --- insert
            out.print("<h3> Book saved with ID "+hibSession.save(b1)+"</h3>");
			//success -- commit tx
			tx.commit();
			
			
		} catch (Exception e) {
			//error --- rollback
			if (tx != null)
				tx.rollback();
		} finally {
			//close hib session
			if (hibSession != null)
				hibSession.close();
		}
	%>
</body>
</html>