package utils;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.service.*;

public class HibernateUtils {
	private static SessionFactory factory;
	private static Session session;
	static {
		System.out.println("in static init block");
		// create empty hib config inst , load it from hibernate.cfg.xml
		Configuration cfg = new Configuration().configure();
		// create service registry inst ---using ServiceRegistryBuilder class
		// inst
		ServiceRegistry regsitry = new ServiceRegistryBuilder().applySettings(
				cfg.getProperties()).buildServiceRegistry();
		//create sf inst associated with hib cfg
		factory=cfg.buildSessionFactory(regsitry);
	}
	public static SessionFactory getFactory() {
		return factory;
	}
	public static Session getSession() {
		return factory.openSession();
	}
	

}
