package hib_pojos;

import java.io.Serializable;
import javax.persistence.*;//annotations support

@Entity //tells hib frmwork whatever follows has to be persisted on DB --mandatory
@Table(name="new_books")
public class BookPOJO implements Serializable {
	@Id //mandatory
	@GeneratedValue(strategy=GenerationType.AUTO) //to tell hib frmwork --- to generate
	//ids using auto inc generator.
	@Column(name="book_id")
	private int bookId;
	@Column(name="title",length=20)
	private String bookTitle;
	private String author;
	private double price;
	public BookPOJO() {
		System.out.println("in book def constr");
	}
	public BookPOJO(String bookTitle, String author, double price) {
		super();
		this.bookTitle = bookTitle;
		this.author = author;
		this.price = price;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Book Details [bookId=" + bookId + ", bookTitle=" + bookTitle
				+ ", author=" + author + ", price=" + price + "]";
	}
	
	

}
