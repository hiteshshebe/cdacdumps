package thick_clnt;

import java.util.Random;
import java.util.Scanner;

import javax.naming.InitialContext;

import calc.Calculator;

public class SFSBClnt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Random r1=new Random();
			// ic
			InitialContext ic = new InitialContext();
			// perform lookup
			Calculator ref = (Calculator) ic.lookup("sfsb_calculator");
			System.out.println("ref" + ref);
			System.out.println("Enter 2 nums");
			Scanner sc = new Scanner(System.in);
			double d1 = sc.nextDouble();
			double d2 = sc.nextDouble();
			int cnter = 0;
			while (cnter < 20) {
				System.out.println("Num 1" + d1 + " Num 2 " + d2
						+ "  Result = " + ref.add(d1, d2));
				Thread.sleep((r1.nextInt(500)+100));
				cnter++;
			}
			ref.doLogOut();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
