package thick_clnt;

import java.util.Scanner;

import javax.naming.InitialContext;

import calc.Calculator;

public class SLSBClnt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// ic
			InitialContext ic = new InitialContext();
			// perform lookup
			Calculator ref=(Calculator) ic.lookup("slsb_calculator");
			System.out.println("ref"+ref);
			System.out.println("Enter 2 nums");
			Scanner sc=new Scanner(System.in);
			double d1=sc.nextDouble();
			double d2=sc.nextDouble();
			System.out.println("Result = "+ref.add(d1, d2));
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
