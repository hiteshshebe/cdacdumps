package com.app.service;

import java.util.List;

import rev_pojos.MovieNewPOJO;

public interface MovieService {
 List<MovieNewPOJO> getMovies();
}
