package pages_session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojos.EmpPOJO;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/display_session")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {
			//get cookie array
			String sts="Emp Not Found!!!!!!";
			HttpSession session=request.getSession();
			System.out.println("From 2nd page Sess ID "+session.getId());
			EmpPOJO ref=(EmpPOJO) session.getAttribute("emp_dtls");
			if (ref != null)
			pw.print("<h3>"+ref+"</h3>");
			else 
				pw.print("<h3>"+sts+"</h3>");
						
			
	
		}
	}

}
