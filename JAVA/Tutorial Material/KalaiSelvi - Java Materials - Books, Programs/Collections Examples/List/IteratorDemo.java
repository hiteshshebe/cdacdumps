// Demonstrate iterators.

import java.util.*;

class  book {

int bookno;
String bookname;
String author;

book(int bookno,String bookname, String author)
{
 this.bookno=bookno;
 this.bookname=bookname;
this.author=author;
}

String getBookname()
{
	return bookname;
}
public String toString()
{
	return "" +bookno +":" +bookname +" : " + author;
}

}



class IteratorDemo {
  public static void main(String args[]) {

    ArrayList al = new ArrayList();

book b1= new book(100,"java","herbert");
book b2= new book(101,"j2ee","kathy siera");
book b3= new book(100,"java","herbert");

    al.add(b1);
    al.add(b2);
    al.add(b3);

    System.out.print("Original contents of al: ");
    Iterator itr = al.iterator();
    while(itr.hasNext()) {
      book element = (book)itr.next();

       System.out.print("Book Name "+element.getBookname());
    }
    System.out.println();


    ListIterator litr = al.listIterator();
    while(litr.hasNext()) {
      Object element = litr.next();
      litr.set(element + "+");
    }

    System.out.print("Modified contents of al: ");
    itr = al.iterator();
    while(itr.hasNext()) {
      Object element = itr.next();
      System.out.print(element + " ");
    }
    System.out.println();


    System.out.print("Modified list backwards: ");
    while(litr.hasPrevious()) {
      Object element = litr.previous();
      System.out.print(element + " ");
    }
    System.out.println();
  }
}


