
import java.util.*;

public class CLinkList {

	public static void main(String[] args) { 
		LinkedList ll=new LinkedList();
		System.out.println("The Size of the LinkedList :"+ ll.size());  
		ll.add("RMI");
		ll.add("CORBA");
		ll.add("NET");
		ll.add("JNI");
		ll.addFirst("JCL");
		System.out.println(ll);
		Iterator itlr=ll.iterator();
		while (itlr.hasNext())
		{Object ele = itlr.next();
		  System.out.println("Object is "+ele);
			}
		System.out.println("After Updating");
		ll.addFirst("JVM");
		ll.addLast("JSP");
		System.out.println("elements in LinkedList :"+ ll);
		//ll.add(4,"IIOP");
		ListIterator lit=ll.listIterator();
		while (lit.hasNext())
		{Object ele1 = lit.next();
		  System.out.println("Object is "+ele1);
			}
		System.out.println("The Size of the LinkedList :"+ ll.size());
		ll.remove(2);
		System.out.println("Elements in LinkedList :"+ ll);
		ll.remove("JSP");
		System.out.println("elements in LinkedList :"+ ll);
		ll.removeLast();System.out.println("elements in LinkedList :"+ ll);
		ll.removeFirst();System.out.println("elements in LinkedList :"+ ll);
		Object val=ll.get(2);
		ll.set(2,(String)val+" Changed");
		System.out.println("elements in LinkedList :"+ ll);
		//al.clear();
		System.out.println("The Size of the LinkedList :"+ ll.size());
	}
}