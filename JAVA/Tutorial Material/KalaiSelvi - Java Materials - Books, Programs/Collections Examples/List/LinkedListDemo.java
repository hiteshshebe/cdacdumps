import java.util.*;

class  book {

int bookno;
String bookname;
String author;

book(int bookno,String bookname, String author)
{
 this.bookno=bookno;
 this.bookname=bookname;
this.author=author;
}
String getBookname()
{
	return bookname;
}

public String toString()
{
	return "\n" +bookno +":" +bookname +" : " + author+"\n";
}

}

class LinkedListDemo{
  public static void main(String args[]) {
    // create a linked list
    LinkedList ll = new LinkedList();

book b1= new book(100,"java","herbert");
book b2= new book(101,"j2ee","kathy siera");

    // add elements to the linked list
    ll.add(b1);
    ll.add(b2);
    ll.addLast(new book(102,"EJB","philip kheller"));
    ll.addFirst(new book(999,"Struts","Orielly"));

   ListIterator litr = ll.listIterator();
    while(litr.hasNext()) {
      book element = (book)litr.next();
      System.out.println("Book Name " +element.getBookname());
    }
	System.out.println("\n");

    System.out.println("Original contents of ll: " + ll);

    // remove elements from the linked list
    ll.remove(b1);


    System.out.println("Contents of ll after deletion: "
                       + ll);

    // remove first and last elements
    ll.removeFirst();
    ll.removeLast();

    System.out.println("ll after deleting first and last: "
                       + ll);

     }
}