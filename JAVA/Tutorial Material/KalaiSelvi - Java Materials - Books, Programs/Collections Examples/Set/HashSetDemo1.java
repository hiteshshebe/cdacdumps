/**********************************************************************
*	Demo program for HashSet which is implementation of Set Interface
*	
*
*
***********************************************************************/


import java.util.HashSet;
import java.util.Iterator;

	public class HashSetDemo {
	  public static void main(String args[]) {
	    HashSet<String> hs = new HashSet<String>();

	    hs.add("G");
	    hs.add("B");
	    hs.add("A");
	    hs.add("D");
	    hs.add("E");
	    hs.add("C");
	    hs.add("F");
	    hs.add("Z");

	    System.out.println(hs);
	    
	    // Traversing Collections using Iterator
	    System.out.println("Traversing Collections using Iterator");
	    Iterator iter=hs.iterator();
	    while (iter.hasNext()) {
			String element = (String) iter.next();
			System.out.println(element);
		}
	    
	    // Traversing Collections using for:each loop
	    System.out.println("Traversing Collections using for:each loop");
	    for (Iterator iterator = hs.iterator(); iterator.hasNext();) {
			String element = (String) iterator.next();
			System.out.println(element);
		}
	    
	    // Collection Interface Array Operations
	    
	    String[] a = hs.toArray(new String[0]);
	    System.out.println("The HashSet after converting to array "+a[0]+" the length of array is="+a.length);
	  }
	}

