// Demonstrate HashSet.
import java.util.*;
class  book {

int bookno;
String bookname;
String author;

book(int bookno,String bookname, String author)
{
 this.bookno=bookno;
 this.bookname=bookname;
this.author=author;
}

public String toString()
{
	return "" +bookno +":" +bookname +" : " + author;
}

}

class HashSetDemo {
  public static void main(String args[]) {
    // create a hash set
    HashSet hs = new HashSet();
book b1= new book(100,"java","herbert");

book b2= new book(101,"j2ee","kathy siera");
book b3= b2;
book b4= new book(44,"struts","bala guruswamy");
book b5= new book(22,"oralce","malcom speed");
//Duplicates are not allowed
    // add elements to the hash set
    hs.add(b1);
    hs.add(b2);
    hs.add(b5);
    hs.add(b3);
    hs.add(b4);
    System.out.println(hs);

    System.out.println(hs.contains(b1));
System.out.println("before removal \n"+hs);
    hs.remove(b1);
   System.out.println(hs.contains(b1));
System.out.println("After removal \n"+hs);
    System.out.println(hs);
  }
}

