/***************************************************
*	Demo to show that main HashMap contains all the elements(i.e both key-value pare)
*	of submap object
*
*
*****************************************************/


import java.util.HashMap;
import java.util.Map;

public class MapContainsAll {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map mainmap=new HashMap();
		mainmap.put("1", "anand");
		mainmap.put("2", "babu");
		mainmap.put("3", "zahir");
		mainmap.put("6", "thanvir");
		mainmap.put("5", "younis");
		
		Map submap=new HashMap();
		submap.put("1", "anand");
		submap.put("2", "babu");
		submap.put("5", "younis");
		
		if(mainmap.entrySet().containsAll(submap.entrySet()))
			System.out.println("the mainmap contains all the elements of submap");
		else
			System.out.println("the main map does not match");
	}

}
