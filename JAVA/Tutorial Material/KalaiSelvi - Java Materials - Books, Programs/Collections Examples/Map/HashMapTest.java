import java.util.*;
public class HashMapTest {
   // Statics
   public static void main( String [] args ) {
      System.out.println( "Collection HashMap Test" );

      HashMap collection1 = new HashMap();

      // Test the Collection interface
      System.out.println( "Collection 1 created, size=" + collection1.size() + 
         ", isEmpty=" + collection1.isEmpty() );

      // Adding
      collection1.put( new String( "Harriet" ), new String( "Bone" ) );
      collection1.put( new String( "Bailey" ),  new String( "Big Chair" ) );
      collection1.put( new String( "Max" ),     new String( "Tennis Ball" ) );

      System.out.println( "Collection 1 populated, size=" + collection1.size() + 
         ", isEmpty=" + collection1.isEmpty() );

      // Test Containment/Access
      String key = new String( "Harriet" );
      if ( collection1.containsKey( key ) )
         System.out.println( "Collection 1 access, key=" + key + ", value=" + 
            (String) collection1.get( key ) );

      // Test iteration of keys and values
      Set keys = collection1.keySet();
      System.out.println( "Collection 1 iteration (unsorted), collection contains keys:" );
      Iterator iterator = keys.iterator();
      while ( iterator.hasNext() )
         System.out.println( "   " + iterator.next() );

      collection1.clear();
      System.out.println( "Collection 1 cleared, size=" + collection1.size() + 
         ", isEmpty=" + collection1.isEmpty() );
   }
}


