import java.util.*;
class AddressBook {
	private String name;
	private String address;
	private double handphone;
	private double landphone;

	public AddressBook(String name,String address,double handphone,double landphone)
	{
		this.name=name;
		this.address=address;
		this.handphone=handphone;
		this.landphone=landphone;
		}

    public String toString()
    {
		return name+" : " +address+" : " + handphone + ":" +landphone;
	}

}
// Demonstrate a Hashtable

class HTDemo{
  public static void main(String args[]) {

    Hashtable abook = new Hashtable();

    abook.put("ram", new AddressBook("ram","bglr",223552629D,94456633443D));
    abook.put("shyam", new AddressBook("shyam","pune",3343434D,33432466D));

    Enumeration names = abook.keys();
    while(names.hasMoreElements()) {
      String str = (String) names.nextElement();
      System.out.println(str + ": " +
                         abook.get(str));
    }
    System.out.println();
  }
}

