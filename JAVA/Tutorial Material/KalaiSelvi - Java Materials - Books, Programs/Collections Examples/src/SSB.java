class StringDemo
{
	public void checkString()
	{
		String sl1="Java";
		String sl2="Java";
		String s1 =new String("Java");
		String s2=new String("Java");
		if (s1.equals(sl2))
		{
			System.out.println("equal objects");
		}
		else
			System.out.println("unequal objects");
		if (sl1==sl2)
		{
			System.out.println("equal");
		}
		else
		{
			System.out.println("unequal");
		}
		System.out.println(s1.length());
		System.out.println(s1.concat(" java"));
	}
}
public class SSB {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        StringDemo obj=new StringDemo();
        obj.checkString();
        StringBuffer sb=new StringBuffer("Java");
        System.out.println(sb.capacity());
        System.out.println(sb.length());
        sb.insert(4, " Language");
        System.out.println(sb);
        sb.append(" System");
        System.out.println(sb);
	}

}
