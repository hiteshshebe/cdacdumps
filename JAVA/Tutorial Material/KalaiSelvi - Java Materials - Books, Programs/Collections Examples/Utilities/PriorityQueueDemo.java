import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo {

//	 this is to show how priority que will work
	static  List heapSort(Collection c) {
	    Queue queue = new PriorityQueue(c);
	    List result = new ArrayList();
	    while (!queue.isEmpty())
	        result.add(queue.remove());
	    return result;
	}
	
    public static void main(String[] args)
            throws InterruptedException {
        int time = Integer.parseInt(args[0]);
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = time; i >= 0; i--)
            queue.add(i);
        
        System.out.println("the heapSort o/p :"+heapSort(queue));
        
        while (!queue.isEmpty()) {
            System.out.println(queue.remove());
            Thread.sleep(1000);
        }
        
        System.out.println("after queue remove ="+queue);
        
    }

}