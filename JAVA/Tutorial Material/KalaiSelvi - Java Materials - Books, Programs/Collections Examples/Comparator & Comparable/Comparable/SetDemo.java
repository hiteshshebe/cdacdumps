import java.util.*;
public class SetDemo{

public static void main(String[] ags){
  TreeSet ts=new TreeSet();

  RecordDemo rd1 = new RecordDemo();
  RecordDemo rd2 = new RecordDemo();
  RecordDemo rd3 = new RecordDemo();
  RecordDemo rd4 = new RecordDemo();

  rd1.setRecord(2,"Gaurav");
  rd2.setRecord(3,"Anand");
  rd3.setRecord(1,"Baby");
  rd4.setRecord(4,"Murty");

  ts.add(rd1);
  ts.add(rd2);
  ts.add(rd3);
  ts.add(rd4);
  Iterator it=ts.iterator();
  while(it.hasNext()){
    rd1=(RecordDemo)it.next();
    System.out.println("id="+rd1.eno);
    System.out.println("Name:="+rd1.name);
  } } }