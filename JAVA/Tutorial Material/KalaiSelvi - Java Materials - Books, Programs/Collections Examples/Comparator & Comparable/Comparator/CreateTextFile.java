package project1;

import java.io.*;
public class CreateTextFile {
	
	public void fileCreation() throws IOException{
		File fTextFile = new File("C:\\Account.txt");
		PrintWriter pWrt = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String accno=null;
		String accNameHldr = null;
		String Balance = null;
		int check=0;
		
		try {
			
			pWrt = new PrintWriter(new FileWriter(fTextFile));
			while(true){
				System.out.println("Enter AccNumber: ");
				accno = in.readLine();
				System.out.println("Enter AccountName Holder: ");
				accNameHldr = in.readLine();
				System.out.println("Enter the Balance: ");
				Balance = in.readLine();
				// writing data into a file
				pWrt.println(accno+":"+accNameHldr+":"+Balance);
				System.out.println("Do u want to continue(0 to end or 1 to continue):  ");
				check = Integer.parseInt(in.readLine());
				if(check==0){
					break;
				}
				
			} 
		}	
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		finally{
			pWrt.close();
			
			}
	}
	
	
	public static void main(String args[]) throws IOException{
		CreateTextFile createTextFile = new CreateTextFile();
		createTextFile.fileCreation();
		
	}

}
