package project1;

import java.util.Comparator;


public class AccountBalanceComparator implements Comparator
 {
	public int compare(Object bankAccount1, Object bankAccount2) 
{
	    float balance1 = ((BankAccountDetails) bankAccount1).getBalance();
	    float balance2 = ((BankAccountDetails) bankAccount2).getBalance();
	return(int) ( balance1-balance2);
}
}
