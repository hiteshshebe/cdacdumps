package project1;
public class BankAccountDetails {

	/**
	 * @param args
	 */
	
	 String accHolderName;
	 int accNumber;
	 float balance;
	
public String toString(){
	return accNumber+":"+accHolderName+":"+balance;
}
	
public String getAccHolderName() {
		return accHolderName;
	}


	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}


	public int getAccNumber() {
		return accNumber;
	}


	public void setAccNumber(int accNumber) {
		this.accNumber = accNumber;
	}


	public float getBalance() {
		return balance;
	}


	public void setBalance(float balance) {
		this.balance = balance;
	}



}
