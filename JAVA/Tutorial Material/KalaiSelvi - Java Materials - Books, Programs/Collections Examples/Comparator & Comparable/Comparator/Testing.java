import java.util.ArrayList;
import java.util.*;

class LastNameComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    String lastName1 = ((Person) person).getLastName().toUpperCase();
    String lastName2 = ((Person) anotherPerson).getLastName().toUpperCase();
return( lastName1.compareTo(lastName2));

  }
}

class AgeComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    int age1 = ((Person) person).getAge();
    int age2 = ((Person) anotherPerson).getAge();
return( age1-age2);

  }
}

class FirstNameComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    String firstName1 = ((Person) person).getFirstName().toUpperCase();
    String firstName2 = ((Person) anotherPerson).getFirstName().toUpperCase();

      return firstName1.compareTo(firstName2);
     }
}
class Person  {
  private String firstName;
  private String lastName;
  private int age;

public Person(String firstName,String lastName,int age)
  {
	    this.firstName = firstName;
	  this.lastName = lastName;
    this.age = age;
}
  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
    public int getAge() {
    return age;
  }
  public String toString()
  {
	  return firstName + " " + lastName + " "+ age;
}
}
public class Testing {
  public static void main(String[] args) {
    Person p1 = new Person("ram","sureh",56);
    Person p2 = new Person("chandru","ram",32);
    Person p4 = new Person("baskar","Gopal",76);
    Person p3 = new Person("jagan","zak",27);
ArrayList alist = new ArrayList();

    alist.add(p1);
    alist.add(p2);
    alist.add(p3);
    alist.add(p4);

Collections.sort(alist,new LastNameComparator());

System.out.println("Last Name Sort" +alist);

Collections.sort(alist,new FirstNameComparator());

System.out.println("First Name Sort"+alist);
Collections.sort(alist,new AgeComparator());

System.out.println("Age Sort"+alist);

      }
  }
