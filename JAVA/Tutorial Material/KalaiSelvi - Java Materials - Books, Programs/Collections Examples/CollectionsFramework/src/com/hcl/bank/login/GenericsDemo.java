package com.hcl.bank.login;

import java.util.ArrayList;

class Temp<T1, T2>
{
	T1 t1;  //instance vars
	T2 t2;
	
	Temp(T1 t1 , T2 t2)
	{
		this.t1 = t1;
		this.t2 = t2;
	}
	
	T1 get()
	{
		return t1;
	}
	
}







public class GenericsDemo {
	
	
	public static void main(String[] args) {
	
		ArrayList<Number> al = new ArrayList<Number>();
		/*al.add("Java");
		al.add("JNI");*/
		al.add(12);
		al.add(789.56f);
		al.add(456L);
		
		Temp<String,Integer> obj = new Temp<String,Integer>("Hello",4567); 
		String s = obj.get();
		System.out.println(s);
		
	}

}
