package com.hcl.bank.login;

import junit.framework.TestCase;

public class OperationsTest extends TestCase {

	public void testInvoke() {
		Operations op = new Operations();
		int res = op.invoke(56);
		assertEquals(156, res);
	}

}
