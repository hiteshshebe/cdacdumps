package Apr7;

import java.util.LinkedList;
import java.util.ListIterator;

public class demo2LinkedList {

	
	public static void main(String[] args) {
		LinkedList<student> link_list=new LinkedList<student>();
		link_list.add(new student("salman","khan"));
		link_list.add(new student("Imran","khan"));
		link_list.add(new student("Shilpa","Shetty"));
		link_list.add(new student("Aishwarya","Rai"));
		link_list.add(new student("Preethi","Zintha"));
		link_list.add(new student("Naushad","Ali"));
		link_list.add(new student("Md Ali","Zinna"));
		
		System.out.println(link_list.getFirst());
		System.out.println(link_list.getLast());
		ListIterator<student> st=link_list.listIterator();
		st.hasNext();
		System.out.println(st.next());
		st.hasPrevious();
		System.out.println(st.previous());
		
	}

}
