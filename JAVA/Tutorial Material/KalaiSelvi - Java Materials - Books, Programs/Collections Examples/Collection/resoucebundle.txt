private void readPropertyFile()
	{
		try
		{
			if(!m_bInitialized)
			{
				ResourceBundle property =
					ResourceBundle.getBundle("WCPropertyInfo", Locale.ENGLISH);
				if(property != null)
				{
					m_sDriver = property.getString("dbDriver");
					m_sUrl = property.getString("dbUrl");
					m_sdbName = property.getString("dbName");
					m_sUserName = property.getString("dbUserName");
					m_sUserPassword = property.getString("dbPassword");
					m_sMinDBConnection = property.getString("dbMinConnection");
					m_sMaxDBConnection = property.getString("dbMaxConnection");
					m_sLogFile = property.getString("dbLogFileName");
}
}
}
