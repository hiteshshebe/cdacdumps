import java.util.*;

public class Descarte {

	private Carta baraja[][];	//baraja es una matriz
	
	private static final int OROS = 0;
	private static final int COPAS = 1;
	private static final int ESPADAS = 2;
	private static final int BASTOS = 3;
	
	private	Carta cartaJugador;
	private	Carta cartaMaquina;
	
	public Carta getCartaJugador() {
		return cartaJugador;
	}

	public void setCartaJugador(Carta cartaJugador) {
		this.cartaJugador = cartaJugador;
	}

	public Carta getCartaMaquina() {
		return cartaMaquina;
	}

	public void setCartaMaquina(Carta cartaMaquina) {
		this.cartaMaquina = cartaMaquina;
	}

	public Descarte(){	
		this.cartaMaquina = new Carta();
		this.cartaJugador = new Carta();
		this.baraja = new Carta[4][12];
	}
	
	private void LlenarBaraja(){
		for (int x = 0; x < baraja.length; x++) {
			for (int y = 0; y < baraja[x].length; y++) {
				baraja[x][y] = new Carta();
				baraja[x][y].setNumero(y+1);
				baraja[x][y].setPalo(x+1);
				baraja[x][y].setUsada(false);
			}			
		}
	}
	private void MostrarBaraja() {		
		for (int x = 0; x < baraja.length; x++) {
			for (int y = 0; y < baraja[x].length; y++) {
				System.out.print(" ");
				System.out.print(baraja[x][y].getPalo() + ","+ baraja[x][y].getNumero());
			}
			System.out.println();
		}
	}
	
	/*private void EscogerNumeroAzar(){
		//for (int x = 0; x < baraja.length; x++) {
			//for (int y = 0; y < baraja[x].length; y++) {
				double numeroAzar = Math.random();
				double numeroMakinaAzar = baraja[0][0];
				System.out.println(numeroMakinaAzar);
			//}
		
		//}
		
		//int numeroMaquina = Math.random();
	}
	*/
	private Carta dameCarta(Carta carta){
		Carta myCartaAzar = new Carta();
		Random myRandom = new Random();
	
		int numero = myRandom.nextInt(baraja.length);
		int palo = myRandom.nextInt(baraja[numero].length);
			
		carta.setNumero(numero);
		carta.setPalo(palo);
		if (baraja[numero][palo].getUsada()){
			dameCarta(carta);
		}else{
			baraja[numero][palo].setUsada(true);
		}		
		return myCartaAzar;
			
	}
	

	
	//creo un objeto carta
	private class Carta{
		private int numero;
		private int palo;
		private boolean usada;
		
		public void setNumero(int valor){
			this.numero=valor;
		}
		public void setPalo(int tipoPalo){
			this.palo=tipoPalo;
		}
		public void setUsada(boolean uso){
			this.usada=uso;
		}

		public int getNumero(){
			return this.numero;
		}
		public int getPalo(){
			return this.palo;
		}
		public boolean getUsada(){
			return this.usada;
		}
	}
	
	public static void main(String args[]){
		boolean salir = false;
		
		Descarte juego = new Descarte();
		juego.LlenarBaraja();
		juego.MostrarBaraja();
		
		//declara variable entrada, q cogera el nombre del jugador
		Scanner entrada = new Scanner(System.in);

		//pregunta al jugador su nombre
		System.out.println("Wenas, como te llamas, amigo?");
		//espera a q se lo digas
		String nombre = entrada.nextLine();
		
		System.out.println("Muy bien "+nombre+", Vamos all�!\n");
		
		
		System.out.println("Quieres una carta? s/n");
		
		String contest = entrada.nextLine();
		if (contest.equalsIgnoreCase("s")){
			System.out.println("Muy bien, vamos all�...\n");
			salir=false; }
		else {
			System.out.println("Hasta otra, Rajao :)");
			salir=true; 
		}
		
						
		while(!salir){
			juego.dameCarta(juego.getCartaJugador());
			juego.dameCarta(juego.getCartaMaquina());

			if (juego.getCartaJugador().getNumero() >= juego.getCartaMaquina().getNumero()){
				System.out.println("Gana Jugador \n");
				System.out.println("Quieres seguir?, "+nombre+"? s/n");

				String respuesta = entrada.nextLine();

				if (respuesta.equalsIgnoreCase("s")){
					System.out.println("Vale vamos otra carta \n!!!");
					salir=false;
				}else {
					System.out.println("Hasta otra, Rajao :)");
					salir=true; 
				}

			}else{
				System.out.println("Gana M�quina \n");
				System.out.println("Hasta otra, perdedor :)");
				salir=true;
			}
		}
	}
	
}
