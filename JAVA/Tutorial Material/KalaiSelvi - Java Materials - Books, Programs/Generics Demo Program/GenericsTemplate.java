//Generic for type safety
class Gen1<T>
{
	T obj;
	
	Gen1(T ob)
	{
	  obj=ob;	
	}
	
	T getObj()
	{
		return obj;
	}
	
	void showType()
	{
		System.out.println("Type of T : "+obj.getClass().getName());
	}
	
}

public class GenericsTemplate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Gen1<Integer> iobj;
		
		iobj = new Gen1<Integer>(100);
		
		iobj.showType();
		
		int val = iobj.getObj();
		
		System.out.println("Value : "+val);
		
        Gen1<String> sobj = new Gen1<String>("Hello");
		
        sobj.showType();
        
        String str = sobj.getObj();
        
        System.out.println("Value : "+str);
		      
        //The process of removing generic type is called erasure
        Gen1 g = new Gen1(12); 
        
        g.showType();
        
        System.out.println("Object after erasure : "+g.getObj());
        
       // iobj = new Gen<Double>(45.00);
        
        //Generics work only with objects
      
        //Gen<int> intv = new Gen<int>(90);
        
        Gen1 obj = sobj; //compiles, but conceptually wrong
        
        int v = (Integer)obj.getObj();//ClassCastException - runtime
        
	//	System.out.println(v);

	}

}
