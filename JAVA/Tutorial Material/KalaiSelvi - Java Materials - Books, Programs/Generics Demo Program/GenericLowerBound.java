import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GenericLowerBound {
	//Lower Bound
  public static double getAverage(List<? super Double> numberList) 
  {
    double total = 0.0;
    
    	Iterator I = numberList.iterator();
    	double d;
    	while (I.hasNext())
    	{
    		   Object o = I.next();
    		   d = Double.parseDouble(o.toString());
    		   // System.out.println(d);
    		    total += d;
    	}
    	
    return total / numberList.size();
  }
//Upper Bound
  
  
  public static void main(String[] args) {
    List<Number> integerList = new ArrayList<Number>();
    integerList.add(3);
    integerList.add(30);
    integerList.add(300);
    System.out.println(getAverage(integerList)); // 111.0
    List<Double> doubleList = new ArrayList<Double>();
    doubleList.add(3.0);
    doubleList.add(33.0);
    System.out.println(getAverage(doubleList)); // 18.0
  }
}
