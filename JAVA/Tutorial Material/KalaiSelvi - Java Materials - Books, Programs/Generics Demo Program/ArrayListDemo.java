import java.util.ArrayList;
import java.util.TreeMap;


public class ArrayListDemo {
	
	
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(78);
		//al.add("sdfds);//compile time error
		
		TreeMap<Number, Float> obj = new TreeMap<Number, Float>();
		Short s =78;
		obj.put(s, 45.56f);
		obj.put(34, 34.56f);
		
		obj.put(s, 34.56f);
		
		ArrayList<String> all = new ArrayList<String>();
		
		System.out.println(al.getClass());
		System.out.println(all.getClass());
	}

}
