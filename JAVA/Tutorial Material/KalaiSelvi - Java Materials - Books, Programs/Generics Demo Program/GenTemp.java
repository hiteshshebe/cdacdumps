class Gene<T,V>
{
	T o1;  //instance variables - type T type Object
	V o2;
	
	Gene(T o1,V o2)
	{
	  this.o1=o1;
	  this.o2=o2;
	}
	
	T getO1()  //method return T type object
	{
		return o1;
	}
	
	V getO2()
	{
		return o2;
	}
	
	void showType()
	{
		System.out.println("Type of T : "+o1.getClass().getName());
		System.out.println("Type of T : "+o2.getClass().getName());
	}
	
}

public class GenTemp{

	
	public static void main(String[] args) {
	Gene<Integer,String> iobj;
		
		iobj = new Gene<Integer,String>(100,"Kalai");
		System.out.println(" Id : "+iobj.getO1());
		System.out.println(" Name : "+iobj.getO2());
		iobj.showType();
		

	}

}
