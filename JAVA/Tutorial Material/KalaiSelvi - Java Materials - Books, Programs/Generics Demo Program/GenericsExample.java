import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class Project
{
	String pId;
	String pName;
	String pTech;
	String pClient;
	
	public Project(String pId,String pName,String pTech,String pClient)
	{
		this.pId = pId;
		this.pName = pName;
		this.pTech = pTech;
		this.pClient = pClient;
	}
	
	public void display()
	{
		System.out.println("\tPROJECT DETAILS");
		System.out.println("ID         : "+pId);
		System.out.println("TITLE      : "+pName);
		System.out.println("TECHNOLOGY : "+pTech);
		System.out.println("CLIENT     : "+pClient);
	}
	
	
}


public class GenericsExample {
	
	public static void addData()
	{
		short s = 12;
		byte b = 10;
		List<Number> list = new ArrayList<Number>();
		list.add(100.8);
		list.add(890.9f);
		list.add(456);
		list.add(s);
		list.add(b);
		list.add(new Long(456456456));
		//list.add("Two");
		
		Iterator<Number> I = list.iterator();
			
		while(I.hasNext())
		{
			Object obj =I.next();
			//int ii = Integer.parseInt(obj.toString());
			String name = obj.getClass().getSimpleName(); 
			if (name.equals("Integer"))
			{
				int i = Integer.parseInt(obj.toString());
				System.out.println("Integer Value : "+i);
			}
			else if (name.equals("Float"))
			{
				float f = Float.parseFloat(obj.toString());
				System.out.println("Float Value : "+f);
				
			}
			else if (name.equals("Double"))
			{
				double d = Double.parseDouble(obj.toString());
				System.out.println("Double Value : "+d);
				
			}
			else if (name.equals("Short"))
			{
				short s1 = Short.parseShort(obj.toString());
				System.out.println("Short Value : "+s1);
				
			}
			else if (name.equals("Byte"))
			{
				byte bb = Byte.parseByte(obj.toString());
				System.out.println("Byte Value : "+bb);
				
			}
			else if (name.equals("Long"))
			{
				long ll = Long.parseLong(obj.toString());
				System.out.println("Long Value : "+ll);
				
			}
					
		}
			
	}
	
	public static void userDefinedObjects()
	{
		Map<Integer,Project> map = new HashMap<Integer,Project>();
		map.put(1000,new Project("10","Trading","Java","L&T"));
		map.put(1001,new Project("11","Civil","Oracle","L&T"));
		map.put(1002,new Project("12","HMS","Java","Appollo"));
		map.put(1003,new Project("13","TravelTourism","Java","IRCTC"));
		
		Set<Entry<Integer, Project>> set = map.entrySet();
		Iterator<Entry<Integer, Project>> i = set.iterator();
		
		while (i.hasNext())
		{
			Map.Entry<Integer, Project> mapobj = (Map.Entry<Integer, Project>) i.next();
			
			int key = mapobj.getKey();
			System.out.println("\nEmp Code : "+key);
			Project proobj = mapobj.getValue();
			proobj.display();
			
		}
		
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//addData();
		userDefinedObjects();

	}

}
