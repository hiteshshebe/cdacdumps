import java.util.ArrayList;
import java.util.List;

public class GenericTemplate2Demo {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car c1 = new Car(5000);
		Car c2 = new Car(3000);
		List<Car> carList = new ArrayList<Car>();
		carList.add(c1);
		carList.add(c2);
		
		RentalGeneric<Car> carRental = new RentalGeneric<Car>(2, carList);
		 
		for(Car car: carList){
			System.out.println(car.carPrice);
		}
		 
		Car carToRent = carRental.getRental();
		System.out.println(carToRent.carPrice);
		Car car3 = new Car(4000);
		carRental.returnRental(car3);
		carList = carRental.getCarList();
		for(Car car: carList){
			System.out.println(car.carPrice);
		}
	}

}

class RentalGeneric<T> {

	private List<T> rentalPool;

	private int maxNum;

	public RentalGeneric(int maxNum, List<T> rentalPool) {
		this.maxNum = maxNum;
		this.rentalPool = rentalPool;
	}

	public T getRental() {
		return rentalPool.get(0);
	}

	public void returnRental(T returnedThing) {
		System.out.println("size of return p " + rentalPool.size());
		rentalPool.add(returnedThing);
		System.out.println("size of return p " + rentalPool.size());
	}
	
	public List<T>  getCarList(){
		return rentalPool;
	}
}

class Car {
	int carPrice;
	
	Car(){
		
	}
	Car(int price){
		this.carPrice = price;
	}
}