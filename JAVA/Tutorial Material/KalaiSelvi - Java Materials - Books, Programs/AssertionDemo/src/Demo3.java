import java.io.*;

public class Demo3 {

   public static void main(String argv[]) throws IOException {
      System.out.print("Enter your project code (H/I/T): ");
      int c = System.in.read();
      switch ((char) c) {
         case 'H':
         case 'h': System.out.println("HealthCare"); break;
         case 'I':
         case 'i': System.out.println("Insurance"); break;
         case 'T':
         case 't': System.out.println("Trading"); break;
         default: assert !true : "Invalid Option"; break;
      }
   }
}
