package banking;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.net.*;
class Timer extends JFrame implements Runnable
{
   JLabel num1,tim1,res,start;
   JTextField num,tim,shr,smin;
   JComboBox time;
   JButton connect,reset,exit,number,disconnect;
   JPanel p2,p1,p3,p4,p5;
   Container con;
   int j=60,c=0,duration,hr,min;
   boolean n1=false,n2=false,disp=true,cont=false;
   Thread tt;
   Timer(String t)
   {
       super(t);
     tt=new Thread(this);
       con=getContentPane();
        con.setLayout(new FlowLayout());
       num1=new JLabel("Phone Number");
        num=new JTextField(12);
       tim1=new JLabel("Timer");
        tim=new JTextField(2);
       
       p4=new JPanel();
       p4.setLayout(new GridLayout(4,3,1,1));
        for(int i=0;i<=9;i++)
          {
            p4.add(number=new JButton(i+""));
       number.addActionListener(new ActionListener()
       {
          public void actionPerformed(ActionEvent e)
          {
              int n=Integer.parseInt(e.getActionCommand().trim());
              String s=num.getText().trim()+n;
              c++;
                num.setText(s);
              if(c>8)
                {n1=true;
                  res.setText("Invalid Number...Click Reset......");
                  connect.setEnabled(false);
                  disconnect.setEnabled(false);
                  disp=false; 
                // number[1].setEnabled(false);
                 }
               j=30;
           }
         });
           }
       con.add(p4);
       
       p1=new JPanel();
       p1.setLayout(new GridLayout(2,2,1,1));
       p1.add(num1);
       p1.add(num);
        num.setEditable(false);
       p1.add(tim1);
       p1.add(tim);
        tim.setEditable(false);
       con.add(p1);
       p3=new JPanel();
      p3.setLayout(new GridLayout(2,1,1,1));
       p2=new JPanel();
       p2.setLayout(new GridLayout(1,4,1,1));
       connect=new JButton("Connect");
       disconnect=new JButton("Disconnect");
                  connect.setEnabled(false);
                  disconnect.setEnabled(false);
       reset=new JButton("Reset");
       exit=new JButton("Exit");
       p2.add(connect);
       p2.add(disconnect);
       p2.add(exit);
       p2.add(reset);
       p3.add(p2);
       p3.add(res=new JLabel("Enter Phone No"));
       con.add(p3);
       p5=new JPanel();
      p5.setLayout(new GridLayout(1,4,1,1));
       start=new JLabel("Start Time");
       shr=new JTextField("00",2);
       smin=new JTextField("00",2);
        time=new JComboBox();
        time.addItem("am");
        time.addItem("pm");
         p5.add(start);
         p5.add(shr);
         p5.add(smin);
         p5.add(time);
         con.add(p5);
          res.setText("Enter 8 digit Local Number..........");

       exit.addActionListener(new ActionListener()
       {
          public void actionPerformed(ActionEvent e)
          {
             System.exit(0);
           }
        });
       connect.addActionListener(new ActionListener()
       {
          public void actionPerformed(ActionEvent e)
          {
            disp=false;
            cont=true;
          }
         });
       reset.addActionListener(new ActionListener()
       {
          public void actionPerformed(ActionEvent e)
          {
            cont=false;
              num.setText("");
              tim.setText("");
                  connect.setEnabled(false);
                  disconnect.setEnabled(false);
          res.setText("Enter 8 digit Local Number..........");
             c=0;
             disp=true;
             j=60;
           }
       });
     tt.start();
     }
   public void run()
   {
     for(;;) 
    {
     try{
          tt.sleep(1000);
         if(disp)
          { 
          tim.setText(j+"");
            j--;
             }
           if(j==0 && c<8)
            {
                if(c==0)
                 res.setText("Time out for the number1... Click Reset.......");
                else
                 res.setText("Time out for the number...Click Reset.........");
              tim.setText("");
              
              disp=false;
              n1=false;
             }
          if(c==8)
           {
            res.setText("Click Connect.......................");
                  connect.setEnabled(true);
                  disconnect.setEnabled(true);
             }

        
      }catch(Exception e1)
        { res.setText("Exception...");}
   }
   }
   public static void main(String args[])
  {
    Timer t= new Timer("Timer");
    t.setBounds(100,100,400,400);
    t.setVisible(true);
  }
}
     
       
         
