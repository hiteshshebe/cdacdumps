import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/* <applet code="LoginSwing" height=250 width=300>
   </applet> */

public class LoginSwing extends JApplet implements ActionListener
{
    JLabel pass,user;
    JTextField username,message;
    JPasswordField password;
    JButton okbutton,cancelbutton;

  public void init()
   {
     user = new JLabel("UserName");
     pass = new JLabel("Password");
     username= new JTextField(20);
     message = new JTextField(20);
     password = new JPasswordField(20);
     okbutton = new JButton("OK");
     cancelbutton = new JButton("Cancel");
      Container con = getContentPane();
      con.setLayout(new FlowLayout());
         con.add(user);
         con.add(username);
         con.add(pass);
         con.add(password);
         con.add(okbutton);
         con.add(cancelbutton);
         con.add(message);
     okbutton.addActionListener(this);
     cancelbutton.addActionListener(this);
   }
   public void actionPerformed(ActionEvent e)
   {
     if(e.getActionCommand().equals("OK"))
	message.setText(username.getText()+"   You are a valid user");
     else
	 if(e.getActionCommand().equals("Cancel"))
	  {
	    message.setText("Invalid Entry");
	    username.setText("");
	    password.setText("");
            username.requestFocus();
          }
    }
}



