import java.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

import banking.Timer;


public class Tele extends JFrame 
{
	JLabel dislab,timlab,cntlab,Rtimlab,Btimlab;
	JButton numbut,conbut,extbut,rstbut,strbut,hshbut;
	static JTextField timtxt;
	JTextField distxt,cnttxt,Rtimtxt,Btimtxt;
	JTextArea txtarea;
	Container con;
	JPanel pan1,pan2,pan3,pan4,pan5,pan6;
	Color c1,c2,c3,f1,f2,f3;
	Font g,r,b;
	Timer t1;
        public static	boolean flag1 =true;
        public static	boolean flag2 =true;
	
	public Tele()
	{
		super("Phone Display");
		setSize(500,400);
		con = getContentPane();
		con.setLayout(new GridLayout(2,1,5,5));
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan3 = new JPanel();
		pan4 = new JPanel();
		pan5 = new JPanel();
		pan6 = new JPanel();
		
		c1 = new Color(160,160,149);
		c2 = new Color (200,176,189);
		c3 = new Color (213,123,213);
		f1 = new Color (200,200,200);
		f2 = new Color (190,150,190);
		f3 = new Color (123,243,232);
			
		dislab = new JLabel("Display");
		timlab = new JLabel("Time");
		cntlab = new JLabel("Counter");
		Rtimlab = new JLabel("Dial-Timer/");
		Btimlab = new JLabel("Button-Timer/");

		distxt = new JTextField(60);distxt.setText("");
		timtxt = new JTextField(40);
		cnttxt = new JTextField(40);
		Rtimtxt = new JTextField(5);
		Btimtxt = new JTextField(5);

		pan1.setLayout(new GridLayout(3,2,5,5));
				
		pan1.add(dislab);
		pan1.add(distxt);
		distxt.setBackground(c2);
		distxt.setForeground(f1);
		
		pan1.add(timlab);
		pan1.add(timtxt);
		timtxt.setBackground(c1);
		distxt.setForeground(f2);

		pan1.add(cntlab);
		pan1.add(cnttxt);
		cnttxt.setBackground(c2);
		distxt.setForeground(f3);

		extbut = new JButton("Exit");
		rstbut = new JButton("Reset");
		conbut = new JButton("Connect");
		
		pan3.setLayout(new GridLayout(3,1,5,1));
		pan3.add(extbut);
		pan3.add(rstbut);
		pan3.add(conbut);
			
		pan4.setLayout(new GridLayout(1,2));
		pan4.add(pan3);
		pan4.add(pan2);
		
		pan6.setLayout(new FlowLayout());
		pan6.add(Rtimlab);
		pan6.add(Rtimtxt);
		pan6.add(Btimlab);
		pan6.add(Btimtxt);
		
		txtarea = new JTextArea(3,9);
//		txtarea.setBackground(c3);
		txtarea.setText("");

		pan2.setLayout(new GridLayout(4,3,25,5));
		
		Time clock = new Time(timtxt); 
		Timer1 tim1 =  new Timer1(txtarea,conbut,Rtimtxt,Btimtxt);	// Instance for Receiver Time Thread;		

		for(int i =0;i<10;i++)
		{
			numbut = new JButton(i+"");
			pan2.add(numbut);
			numbut.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try{	
												
					if(	e.getActionCommand().equals("0") ||
						e.getActionCommand().equals("1") ||
						e.getActionCommand().equals("2") ||
						e.getActionCommand().equals("3") ||
						e.getActionCommand().equals("4") ||
						e.getActionCommand().equals("5") ||
						e.getActionCommand().equals("6") ||
						e.getActionCommand().equals("7") ||
						e.getActionCommand().equals("8") ||
						e.getActionCommand().equals("9") 
					)
					{
						flag1 = false;
						distxt.setText(distxt.getText()+e.getActionCommand());
						Time clock = new Time(timtxt); 
						Timer tim = new Timer(txtarea,conbut,Rtimtxt,Btimtxt); 		// Instance for ButtonTimer 
						
						System.out.println("Object for Timer is Created");
						/*if(flag == true)
						System.exit(0);
						System.out.println("flag value is"+tim.fg1);*/	
					}
					}catch(Exception ne){}
				}
			});
		}
		pan2.add(new JButton("#"));
		pan2.add(new JButton("*"));

		pan5.setLayout(new GridLayout(4,1));
		pan5.add(pan1);
		pan5.add(pan4);
		pan5.add(pan6);
		pan5.add(txtarea);
		
		con.add(pan5);
		
	 	rstbut.addActionListener( new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				conbut.setEnabled(true);
				distxt.setText("");
				cnttxt.setText("0");
				txtarea.setText("");
				flag1 = true;
				Timer1 tim1 =  new Timer1(txtarea,conbut,Rtimtxt,Btimtxt);	
			}
		});
		
		extbut.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
			
		conbut.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				flag1 = true;
				String str = distxt.getText();
				int no = str.length();
				Rtimtxt.setText("0");
				Btimtxt.setText("0");
				switch(no)
				{
					case 8 : 
						txtarea.setText("Entered Local Number is: "+str);
						break;
					case 11:
						if(str.charAt(0) == '0' )
							txtarea.setText("Entered STD Number is: "+str);	
						else
						txtarea.setText("Invalid Number : "+str);
						break;
					case 14:
						if(str.substring(0,2).equals( "00")){
						txtarea.setText("Entered ISD Number is: "+str);
						}
						else
						txtarea.setText("Invalid Number length :14 : "+str);
						break;	
					default:
						txtarea.setText("Invalid Number : "+str);
						break;		
				}
			}	
		});
	}	//End of Constructor	
				
	public static void main(String abc[])throws InterruptedException
	{
		Tele t = new Tele();
		t.setVisible(true);
	}
}

class Timer implements Runnable
{
	Thread th;
	int i=0;
	private JTextArea txtArea;
	private JTextField tmr2;
	private JTextField tmr1;
	private JButton conBut;	
	public Timer(JTextArea txtArea,JButton conbut,JTextField time1,JTextField time2)
	{
		this.txtArea = txtArea;
		tmr1 = time1;
		tmr2 = time2;
		conBut = conbut;
		th = new Thread(this);
		th.start();
	}
		
	public void run()
	{

		try{
			while(true)
			{	System.out.println("1-Inside of Timer- run:"+Tele.flag2);
				tmr1.setText("0");
				Tele.flag2 = !(Tele.flag2);
				System.out.println("2-~Inside of Timer- run:"+Tele.flag2);
				boolean b = Tele.flag2;System.out.println("value of b is:"+b);
				for(int i=0;i<6;i++)
				{
					if(b == Tele.flag2)
					{
						tmr2.setText(i+"");
						Thread.sleep(1000);
						if(i == 5)
						{
							if(!Tele.flag1)
							{
								txtArea.setText("Timeout Occured for pressing the next Button");
								Thread.sleep(2000);
								//System.exit(0);
								conBut.setEnabled(false);
							}
						}
					}
				}
				break;
			}
		} catch(InterruptedException ie){}	   
	}

}

class Timer1 implements Runnable
{
	private JTextArea txtA;
	private JTextField time1;
	private JTextField time2;
	private JButton conBut;
	Thread t1;
	Calendar cal;
	
	Timer1(JTextArea txt,JButton conbut,JTextField tmr1,JTextField tmr2)
	{
		System.out.println("Object for Timer1 is Created");
		txtA = txt;
		time1 = tmr1;
		time2 = tmr2;
		conBut = conbut;
		t1 = new Thread(this);
		cal = Calendar.getInstance();
		t1.start();
		
	}
	
	public void run()
	{
		try{
			System.out.println("flag1 : value is"+Tele.flag1);
			
			time2.setText("0");
		/*while(true)
		{
			int hh = cal.get(Calendar.HOUR);
			int mm = cal.get(Calendar.MINUTE);
			int ss = cal.get(Calendar.SECOND);
			timer.setText(""+hh+":"+mm+":"+ss);*/
			for(int i=0;i<11;i++)
			{
				if(Tele.flag1)
				{
					System.out.print(Tele.flag1);
					time1.setText(""+i);
					Thread.sleep(1000);
			
					if(i == 9)
					{
						txtA.setText("Timeout Occured from the moment You take the receiver"); 
						//Thread.sleep(2000);
						//System.exit(0);
						conBut.setEnabled(false);
					}
				}
			}
		}		
		catch(InterruptedException Ie){}
	}
		
}

class Time implements Runnable
{
	Thread time;
	int mm,hh,ss;
	String str;
	JTextField timer;
	Calendar cal;
	
	public Time(JTextField tm)
	{
		timer = tm;
		time = new Thread(this);
		//cal = Calendar.getInstance();
		time.start();
		
	}
	
	public void run()
	{

		System.out.println("Timer Thread starts");	
		while(true)
		{
			try{
			hh = Calendar.getInstance().get(Calendar.HOUR);
			mm = Calendar.getInstance().get(Calendar.MINUTE);
			ss = Calendar.getInstance().get(Calendar.SECOND);
			str = hh+":"+mm+":"+ss;
			timer.setText(hh+":"+mm+":"+ss);
			Thread.sleep(1000);
			System.out.println("Time: "+str);	
			}catch(InterruptedException ee){}
		}
	}
}			
				
