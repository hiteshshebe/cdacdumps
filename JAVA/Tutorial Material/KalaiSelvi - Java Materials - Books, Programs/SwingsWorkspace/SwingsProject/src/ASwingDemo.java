import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ASwingDemo extends JApplet{
	
	JLabel lblData;
	JTextField txtData;
	JButton btnOk;
	Icon icon = new ImageIcon("Desert.jpg");
	public void init()
	{
		lblData = new JLabel(icon);
		btnOk = new JButton();
		
	}

	public void start()
	{
		Container c =getContentPane();
		
		c.add(lblData);
		c.add(btnOk);
		c.setLayout(new FlowLayout());
	}

}
