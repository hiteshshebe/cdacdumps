import javax.swing.*;
import java.awt.*;

public class card_layout extends JApplet
{
	JButton b1,b2,b3;
	CardLayout card;
	
	public void init()
	{
		JPanel p=new JPanel();
		getContentPane().add(p);

		card=new CardLayout();
		p.setLayout(card);

		b1=new JButton("Button 1");
		b2=new JButton("Button 2");
		b3=new JButton("Button 3");
	
		p.add("Button 1",b1);
		p.add("Button 2",b2);
		p.add("Button string 3",b3);

		card.last(p);
		card.show(p,"Button 2");
	}
}

