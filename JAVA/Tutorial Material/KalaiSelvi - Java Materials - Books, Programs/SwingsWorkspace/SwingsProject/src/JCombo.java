
import java.awt.*;
import java.awt.color.*;
import java.awt.event.*;
import javax.swing.*;

public class JCombo implements ActionListener, ItemListener {
	JFrame jf;

	JComboBox jc;

	Container cp;

	public JCombo() {
		jf = new JFrame("COMBO DEMO");
		String data[] = { "Red", "Green", "Blue", "Cyan" };
		jc = new JComboBox(data);
		cp = jf.getContentPane();
		jf.setSize(200, 200);
		cp.add(jc, "Center");
		jc.addActionListener(this);
		jc.addItemListener(this);
		jc.setEditable(true);
		ComboBoxEditor ed = jc.getEditor();
		jf.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		boolean awb = false;
		for (int i = 0; i < jc.getItemCount(); i++) {
			if (jc.getItemAt(i).equals(jc.getSelectedItem())) {
				awb = true;
				break;
			} else {
				jc.insertItemAt(jc.getSelectedItem(), jc.getItemCount());
			}
		}
	}

	public void itemStateChanged(ItemEvent e) {
		String s = (String) e.getItem();
		if (s.equals("Red")) {
			cp.setBackground(Color.red);
		} else if (s.equals("Green")) {
			cp.setBackground(Color.green);
		} else if (s.equals("Blue")) {
			cp.setBackground(Color.blue);
		} else if (s.equals("Cyan")) {
			cp.setBackground(Color.cyan);
		}
	}

	public static void main(String arg[]) {
		new JCombo();
	}
}