import javax.swing.*;
import java.awt.*;

public class reservation_box extends JApplet
{

	JPanel pcal,preserv,pmain;

	//FlowLayout flow;
	GridLayout grid,grid2;

	BoxLayout box;

	String strain[]={"TamilNadu Express","Agra Express","Nelli Express","Covai Express"};
	String sdepart[]={"Chennai","Trichy","Banglore","Agra"};
	String sarival[]={"Chennai","Banglore","Arakonam","Hydrabad","Bopal","Puna"};

	JLabel lTrain;
	JComboBox cTrain;
	
	JLabel dStation,aStation;
	JComboBox dList,aList;
	
	JRadioButton class1,class2,class3;

	JRadioButton adult,child;
	
	JLabel persons;
	JTextField num;

	JButton submit;

	public void init()
	{

		
		pcal=new JPanel();
		preserv=new JPanel();
		pmain=new JPanel();


		
//setting the flowlayout for preserv panel
		//flow=new FlowLayout(FlowLayout.CENTER); //initializing the FlowLayout
		grid2=new GridLayout(4,2);
		preserv.setLayout(grid2);//setting the panel to flow layout


//setting the gridlayout for pcal pannel

		grid=new GridLayout(5,2);
		pcal.setLayout(grid);		

//add both the panel to the main panel
		//initialize the boxlayout
		box=new BoxLayout(pmain,BoxLayout.X_AXIS);

//add the two panels to the box layout
		box.addLayoutComponent("Stations",preserv);
		box.addLayoutComponent("Class",pcal);

//add the mainpanel to the container
		getContentPane().add(pmain);

		pmain.add(preserv);
		pmain.add(pcal);


//initializing the elements and adding to preserv panel		
		lTrain=new JLabel("Choose The Train");
		cTrain=new JComboBox(strain);
		
		preserv.add(lTrain);
		preserv.add(cTrain);

		dStation=new JLabel("Departure Station");
		dList=new JComboBox(sdepart);
		preserv.add(dStation);
		preserv.add(dList);
		
		aStation=new JLabel("Arrival Station");
		aList=new JComboBox(sarival);
		aList.setEditable(true);
		preserv.add(aStation);
		preserv.add(aList);		




//initializing the elements and adding to pcal panel
		class1=new JRadioButton("First Class",true);
		class2=new JRadioButton("Second Class");
		class3=new JRadioButton("Third Class");
		
		ButtonGroup bg=new ButtonGroup();
		bg.add(class1);
		bg.add(class2);
		bg.add(class3);

		pcal.add(class1);		
		pcal.add(class2);
		pcal.add(class3);


		JLabel age=new JLabel("Choose ");
		pcal.add(age);
	
		adult=new JRadioButton("Adult",true);
		child=new JRadioButton("Child");
		
		ButtonGroup bg1=new ButtonGroup();
		bg1.add(adult);
		bg1.add(child);
		
		pcal.add(adult);
		pcal.add(child);

		persons=new JLabel("NUm of Persons");
		num=new JTextField(2);
		pcal.add(persons);
		pcal.add(num);

		submit=new JButton("Submit");
	
		pcal.add(submit);
		

	}
}

/* <applet code=reservation_box height=200 width=500>
</applet>
*/
		
	
