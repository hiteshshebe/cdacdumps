import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.*;

class Addr extends JFrame
{
   JButton exit,clear,ok;
   JTextField num;
   JLabel res,lab;
  Container con;
  JPanel p1,p2,p3;
  Addr(String t)
  { 
   super(t);
   con=getContentPane();
   con.setLayout(new FlowLayout());
   p1=new JPanel();
   p1.setLayout(new GridLayout(1,2,1,1));
   lab=new JLabel("Enter IP Address: ");
   num=new JTextField(15);
   p1.add(lab);
   p1.add(num);
   con.add(p1);
   p2=new JPanel();
   p2.setLayout(new GridLayout(1,3,1,1));
   exit=new JButton("Exit");
   clear=new JButton("Clear");
   ok=new JButton("OK");
   p2.add(exit);
   p2.add(clear);
   p2.add(ok);
   con.add(p2);
   res=new JLabel("");
   con.add(res);
   exit.addActionListener(new ActionListener()
   {
    public void actionPerformed(ActionEvent e)
    {
        System.exit(0);
     }
    });
   clear.addActionListener(new ActionListener()
   {
    public void actionPerformed(ActionEvent e)
    {
       num.setText("");
     }
   });
   ok.addActionListener(new ActionListener()
   {
    public void actionPerformed(ActionEvent e)
    {
      StringTokenizer st=new StringTokenizer(num.getText().trim(),".");
      int c=0,n=0;
       while(st.hasMoreTokens())
       {
         n++;
         int f=Integer.parseInt(st.nextToken().trim());
             System.out.println(f+"");
         if(!(f>=0 && f<=255))
           { c=1;
             System.out.println(c+"");}
        }
       
       if(c==1 || n!=4)
         res.setText("Invalid IP Address.......");
       else
         res.setText("Valid IP Address.......");
     }
    });
           
  }
 public static void main(String args[])
{
  Addr d=new Addr("IP Check");
  d.setBounds(100,100,400,400);
  d.setVisible(true);
 }
}