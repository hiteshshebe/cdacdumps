import javax.swing.*;

public class reservation extends JApplet
{

	JPanel p;

	String strain[]={"TamilNadu Express","Agra Express","Nelli Express","Covai Express"};
	String sdepart[]={"Chennai","Trichy","Banglore","Agra"};
	String sarival[]={"Chennai","Banglore","Arakonam","Hydrabad","Bopal","Puna"};

	JLabel lTrain;
	JComboBox cTrain;
	
	JLabel dStation,aStation;
	JComboBox dList,aList;
	
	JRadioButton class1,class2,class3;

	JRadioButton adult,child;
	
	JLabel persons;
	JTextField num;

	JButton submit;

	public void init()
	{

		
		p=new JPanel();
		
		getContentPane().add(p);//this is to add the components to container directly
		
		lTrain=new JLabel("Choose The Train");
		cTrain=new JComboBox(strain);//list of trains declaration
		
		p.add(lTrain);
		p.add(cTrain);//add train list to pannel

		dStation=new JLabel("Departure Station");
		dList=new JComboBox(sdepart);
		p.add(dStation);
		p.add(dList);// departure station list
		
		aStation=new JLabel("Arrival Station");
		aList=new JComboBox(sarival);
		aList.setEditable(true);
		p.add(aStation);
		p.add(aList);//arival station list

		class1=new JRadioButton("First Class",true);
		class2=new JRadioButton("Second Class");
		class3=new JRadioButton("Third Class");
		
		ButtonGroup bg=new ButtonGroup();
		bg.add(class1);
		bg.add(class2);
		bg.add(class3);

		p.add(class1);		
		p.add(class2);
		p.add(class3);//choosing the class of coach


		JLabel age=new JLabel("Choose ");
		p.add(age);
	
		adult=new JRadioButton("Adult",true);
		child=new JRadioButton("Child");
		
		ButtonGroup bg1=new ButtonGroup();
		bg1.add(adult);
		bg1.add(child);
		
		p.add(adult);
		p.add(child);

		persons=new JLabel("NUm of Persons");
		num=new JTextField(2);
		p.add(persons);
		p.add(num);// enter the number of persons

		submit=new JButton("Submit");
	
		p.add(submit);//submit button
		

	}
}

/* <applet code=reservation height=200 width=500>
</applet>
*/
		
	
