import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
//import javax.swing.border.*;


public class RegForm extends JFrame //implements ActionListener
 {

	public RegForm(String title) 
	{
		super ( title );
		getContentPane().setLayout(null);

		JPanel p1 = getTitlePanel();
		JPanel p2 = getDataPanel();
		p1.setBounds(200, 10, 400, 80);
		p2.setBounds(250, p1.getY() + 130, 300, 540);

		getContentPane().add(p1);
		getContentPane().add(p2);
		setBounds(0, 0, 400, 400);
		setVisible( true );
	}

	private JPanel getTitlePanel() {
		JLabel titLab = new JLabel("Registration Form", JLabel.CENTER);
		titLab.setFont(new Font("Times New Roman", Font.BOLD, 24));
		titLab.setForeground(Color.blue);

		JLabel logoLab = new JLabel("Form Logo", JLabel.RIGHT);
		logoLab.setFont(new Font("Verdana", Font.BOLD, 12));
		logoLab.setForeground(Color.red);

		JPanel pan = new JPanel(new GridLayout(2, 1, 5, 10));
		pan.add(titLab);
		pan.add(logoLab);
		
		return pan;
	}

	private JPanel getDataPanel() {
		JPanel mainPan = new JPanel(new GridLayout(8, 1, 5, 5));
		Font fnt = new Font("Arial", 0, 12);

		JLabel regLab = new JLabel("Reg. No : ", JLabel.RIGHT);
		regLab.setFont(fnt);
final		JTextField regTxt = new JTextField(20);

		JLabel nameLab = new JLabel("Name : ", JLabel.RIGHT);
		nameLab.setFont(fnt);
final		JTextField nameTxt = new JTextField(20);

		JLabel ageLab = new JLabel("Age : ", JLabel.RIGHT);
		ageLab.setFont(fnt);
	final	JTextField ageTxt = new JTextField(3);

		JPanel p1 = new JPanel(new GridLayout(3, 2, 5, 5));
		p1.add(regLab);		p1.add(regTxt);
		p1.add(nameLab);	p1.add(nameTxt);
		p1.add(ageLab);		p1.add(ageTxt);

		
		JLabel dobLab = new JLabel("D.O.B. : ", JLabel.RIGHT);
		dobLab.setFont(fnt);

		JLabel dayLab = new JLabel("Day : ", JLabel.RIGHT);
		dayLab.setFont(fnt);
		JComboBox dayCom = new JComboBox();

		for(int i=1;i<32;i++)
		dayCom.addItem(i+"");

		JLabel monLab = new JLabel("Month : ", JLabel.RIGHT);
		monLab.setFont(fnt);
		JComboBox monCom = new JComboBox();
		
		JLabel str = new JLabel(" ");
		JLabel str1 = new JLabel(" ");
		JLabel str2 = new JLabel(" ");

		for(int i=1;i<13;i++)
		monCom.addItem(i+"");

		JLabel yeaLab = new JLabel("Year : ", JLabel.RIGHT);
		yeaLab.setFont(fnt);
		JComboBox yeaCom = new JComboBox();

		for(int i=1970;i<2005;i++)
		yeaCom.addItem(i+"");
		
		JPanel p21 = new JPanel(new GridLayout(1,4,0,0));
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		p21.add(dobLab);		p2.add(str);
		p21.add(dayLab);		p2.add(dayCom);p2.add(str1);
		p21.add(monLab);		p2.add(monCom);p2.add(str2);
		p21.add(yeaLab);		p2.add(yeaCom);

		JLabel addLab = new JLabel("Address : ", JLabel.RIGHT);
		addLab.setFont(fnt);
		JTextArea txtArea = new JTextArea(6, 2);
		JScrollPane jsp = new JScrollPane(txtArea);
		jsp.getViewport().add(txtArea);
		JPanel p3 = new JPanel(new GridLayout(1, 2, 5, 5));
		p3.add(addLab);		p3.add(jsp);

		
		JLabel sexLab = new JLabel("Sex : ", JLabel.RIGHT);
		sexLab.setFont(fnt);
		ButtonGroup bg = new ButtonGroup();
		JRadioButton malBut = new JRadioButton("Male");
		JRadioButton femBut = new JRadioButton("Female");
		bg.add(malBut);		bg.add(femBut);
		
		JPanel p4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		p4.add(sexLab); p4.add(malBut); p4.add(femBut);

		JLabel hobLab = new JLabel("Hobbies : ", JLabel.RIGHT);
		hobLab.setFont(fnt);
		JCheckBox hob1 = new JCheckBox("Hobbie 1");
		JCheckBox hob2 = new JCheckBox("Hobbie 2");
		JCheckBox hob3 = new JCheckBox("Hobbie 3");
		jcheckBox hob4 = new JCheckBox("Hobbie 4");
		JCheckBox hob5 = new JCheckBox("Hobbie 5");

		JPanel tmpPan1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		tmpPan1.add(hobLab);

		JPanel tmpPan2 = new JPanel(new GridLayout(5, 1, 5, 5));
		tmpPan2.add(hob1);
		tmpPan2.add(hob2);
		tmpPan2.add(hob3);
		tmpPan2.add(hob4);
		tmpPan2.add(hob5);

		JPanel p5 = new JPanel(new GridLayout(1, 2, 5, 5));
		p5.add(tmpPan1); 	p5.add(tmpPan2); 

		Font f2 = new Font("Arial", 1, 12);
		JButton b1 = new JButton("Submit");
		b1.setFont(f2);
		JButton b2 = new JButton("Reset");
		b2.setFont(f2);
		JButton b3 = new JButton("Exit");
		b3.setFont(f2);
		
		JPanel p6 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		p6.add(b1);
		p6.add(b2);
		p6.add(b3);

	final	JTextArea message = new JTextArea(5,10);
		JPanel p7 = new JPanel(new GridLayout(1,1,10,10));
		JScrollPane mess = new JScrollPane(message);
		mess.getViewport().add(message);
		p7.add(mess);
		

		mainPan.add(p1);
		mainPan.add(p21);
		mainPan.add(p2);
		mainPan.add(p3);
		mainPan.add(p4);
		mainPan.add(p5);
		mainPan.add(p6);
		mainPan.add(p7);

		b1.addActionListener(new ActionListener()
		{
		public void actionPerformed(ActionEvent e)
		{
		message.setText(regTxt.getText()+":"+nameTxt.getText()+":"+	ageTxt.getText());
		}
		}
		);

		return mainPan;
		
	}

	public static void main(String args[]) {
		new RegForm( "Student Registration...");
	}
}
