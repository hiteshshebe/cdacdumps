package components.demo;

import java.awt.*;
import java.awt.event.*;
public class textlistener extends Frame implements TextListener ,WindowListener
{
 TextField t1,t2;
 public textlistener()
 {
  setLayout(new FlowLayout());
  t1=new TextField(10);
  t2=new TextField(10);
  add(t1);add(t2);
  setVisible(true);
  t1.addTextListener(this);
  addWindowListener(this);
  setSize(200,200);
  setLocation(250,250);
  setResizable(false);
  t1.requestFocus();
  t2.setEditable(false);
  validate();
}
 public void textValueChanged(TextEvent e)
 {
  t2.setText(t1.getText().toUpperCase());
 }
 public void windowClosing(WindowEvent e)
{
System.exit(0);
}
public void windowClosed(WindowEvent e)
{}
public void windowOpened(WindowEvent e)
{}
public void windowIconified(WindowEvent e)
{}
public void windowDeiconified(WindowEvent e)
{}
public void windowActivated(WindowEvent e)
{}
public void windowDeactivated(WindowEvent e)
{}
 public static void main(String s[])
 {
  new textlistener();
  }
}