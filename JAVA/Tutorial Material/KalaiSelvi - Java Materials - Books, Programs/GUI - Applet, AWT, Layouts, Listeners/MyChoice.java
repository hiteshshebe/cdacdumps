package components.demo;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/*<applet code="MyChoice" height=350 width=350></applet>*/
public class MyChoice extends JApplet
{
   JLabel country,dob;
   JComboBox nation,day,mon,year;
   JButton ok;
   JTextField msg,dmsg;
   Container con;
   public void init()
   { setBackground(Color.pink);
     country=new JLabel("My Country");
     nation=new JComboBox();
     day=new JComboBox();
     mon=new JComboBox();
     year=new JComboBox();
     dob=new JLabel("Date of Birth");
     msg=new JTextField(20);
     dmsg=new JTextField(20);
     ok=new JButton("OK");
     con=getContentPane();
     con.setLayout(new FlowLayout());
     JPanel panel=new JPanel();
     JPanel panel1=new JPanel();
     JPanel panel2=new JPanel();
     nation.addItem("India");
     nation.addItem("USA");
     nation.addItem("UK");
     nation.addItem("Denmark");
     for(int i=1;i<=31;i++)
        day.addItem(i+" ");
     for(int i=1;i<=12;i++)
        mon.addItem(i+" ");
     for(int i=1;i<=35;i++)
        year.addItem((1970+i)+" ");
     panel.setLayout(new GridLayout(1,2,3,3));
     panel.add(country);
     panel.add(nation);
     panel2.setLayout(new GridLayout(1,4,3,3));
     panel2.add(dob);
     panel2.add(day);
     panel2.add(mon);
     panel2.add(year);
     panel1.setLayout(new GridLayout(3,1,25,25));
     panel1.add(ok);
     panel1.add(msg);
     panel1.add(dmsg);
     con.add(panel);
     con.add(panel2);
     con.add(panel1);
     ok.addActionListener(new MyListener());
//     nation.addItemListener(new MyListener());
 //    day.addItemListener(new MyListener());
  //   mon.addItemListener(new MyListener());
   //  year.addItemListener(new MyListener());
  }
class MyListener implements ActionListener
{
     public void actionPerformed(ActionEvent e)
     {
       if(e.getSource()==ok)
        {
            msg.setText("My Country is "+ nation.getSelectedItem());
            dmsg.setText("My Date of Birth is "+ day.getSelectedItem()+"/"+mon.getSelectedItem()+"/"+year.getSelectedItem());
         }
     }
}
}
   


