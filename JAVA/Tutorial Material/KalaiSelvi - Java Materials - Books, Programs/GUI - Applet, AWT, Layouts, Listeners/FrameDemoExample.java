package frame.demos;

import java.awt.CheckboxMenuItem;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


class FrameDemoExample implements ActionListener{
	  
	public static void main(String a[])
	{
		new FrameDemoExample().start();
	}
	
	public void start()
	{
	
	String msg = "";
	  CheckboxMenuItem debug, test;
       
	  
	  Frame frame = new Frame();

	    // create menu bar and add it to frame
	    MenuBar mbar = new MenuBar();
	     frame.setMenuBar(mbar);

	    // create the menu items
	    Menu file = new Menu("File");
	    MenuItem item1, item2, item3, item4, item5;
	    file.add(item1 = new MenuItem("New..."));
	    file.add(item2 = new MenuItem("Open..."));
	    file.add(item3 = new MenuItem("Close"));
	    file.add(item4 = new MenuItem("-"));
	    file.add(item5 = new MenuItem("Quit..."));
	    mbar.add(file);

	    Menu edit = new Menu("Edit");
	    MenuItem item6, item7, item8, item9;
	    edit.add(item6 = new MenuItem("Cut"));
	    edit.add(item7 = new MenuItem("Copy"));
	    edit.add(item8 = new MenuItem("Paste"));
	    edit.add(item9 = new MenuItem("-"));

	    //submenu
	    Menu sub = new Menu("Special");
	    
	    MenuItem item10, item11, item12;
	    sub.add(item10 = new MenuItem("First"));
	    sub.add(item11 = new MenuItem("Second"));
	    sub.add(item12 = new MenuItem("Third"));
	  
	    edit.add(sub);

	    // these are checkable menu items
	    debug = new CheckboxMenuItem("Debug");
	    edit.add(debug);
	    test = new CheckboxMenuItem("Testing");
	    edit.add(test);

	    mbar.add(edit);

	    // register it to receive those events
	    item1.addActionListener(this);
	    item2.addActionListener(this);
	    item3.addActionListener(this);
	    item4.addActionListener(this);
	    item5.addActionListener(this);
	    item6.addActionListener(this);
	    item7.addActionListener(this);
	    item8.addActionListener(this);
	    item9.addActionListener(this);
	    item10.addActionListener(this);
	    item11.addActionListener(this);
	    item12.addActionListener(this);
	    
   frame.setVisible(true);
	    
	  }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}