package components.demo;

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
public class copytext extends Applet implements ActionListener
{
 TextField t1,t2;
 Button b1,b2;
 public void init()
 {
  setLayout(new GridLayout(2,2,10,10));
  Color c=new Color(175,140,123);
  setBackground(c);
  t1=new TextField(10);
  t2=new TextField(10);
  b1=new Button("copy");
  b2=new Button("clear");
  add(t1);
  add(b1);
  add(b2);
  add(t2);
//  setSize(250,250);

  b1.addActionListener(this);
  b2.addActionListener(this);
  t1.requestFocus();
  t2.setEditable(false);
  }
 public void actionPerformed(ActionEvent e)
 {
	
  String s=b1.getLabel();
 if (e.getSource() == b1)
  {
   String st=t1.getText();
   t2.setText(st.toUpperCase());
   }

  else
  {
   t1.setText("");
   t2.setText("");
   t1.requestFocus();
  }
 }
}