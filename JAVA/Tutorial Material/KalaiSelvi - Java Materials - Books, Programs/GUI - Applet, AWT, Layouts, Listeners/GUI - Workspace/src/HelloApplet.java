import java.applet.Applet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;


public class HelloApplet extends Applet{
	Image image;
	public void init()
	{
		showStatus("Applet is Initialized");
		String img = getParameter("img");
		image = getImage(getCodeBase(),img);
	}
	
	public void start()
	{
		showStatus("Applet is Started");
	}
	
	public void paint(Graphics g)
	{
		
		showStatus("Paint Method");	
		
		/*g.drawString("Hello "+arr, 200, 200);
		
		g.drawArc(100, 50, 35, 50, 100, 200);*/
		//Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		
		g.drawImage(image,1, 1,230,230,this);			
			
	}
	
}
