import java.applet.Applet;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;


public class FocusAdaptorDemo extends Applet //implements FocusListener
{

	Label lblValue1, lblValue2,lblResult;
	TextField txtValue1, txtValue2, txtResult;
	
	
	public void init()
	{
		lblValue1 = new Label("Value 1");
		lblValue2 = new Label("Value 2");
		lblResult = new Label("Result");
		txtValue1 = new TextField(15);
		txtValue2 = new TextField(15);
		txtResult = new TextField(15);
	}
	
	public void start()
	{
		add(lblValue1);
		add(txtValue1);
		add(lblValue2);
		add(txtValue2);
		add(lblResult);
		txtResult.addFocusListener(new FocusAdapter() 
		{
			public void focusGained(FocusEvent fe)
			{
				if (fe.getSource()==txtResult)
				{
					int v1 = Integer.parseInt(txtValue1.getText());
					int v2 = Integer.parseInt(txtValue2.getText());
					Integer v3 = v1+v2;
					txtResult.setText(v3.toString());
				}
			}
			
		});//end of the statement
		add(txtResult);
		
		
	}
	
	public void paint()
	{
		
	}

	/*public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}
*/	
	
	
}
