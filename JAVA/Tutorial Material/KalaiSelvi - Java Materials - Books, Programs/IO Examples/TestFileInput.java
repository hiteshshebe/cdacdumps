import java.io.*;

public class TestFileInput
{
    public static void main(String args[])
     {
         //Allocates a buffer of 50 bytes
         byte buffer[] = new byte[50];
         try
         {
             //Opens the filein.txt file for reading the contents using the FileInputStream class File.
	File f = new File("source.txt");
	FileInputStream file = new FileInputStream(f);
    System.out.println("Length of file : "+f.length());
	
	//Reads 40 bytes from the start of filein.txt file
	file.read(buffer,0,33);
         }
         catch(IOException e)
          {
             System.out.println("Exception: "+e.toString());
           }
           String str = new String(buffer);
           System.out.println(str);
        }
}
	
                       