import java.io.*;

public class FileWriting {

	public static void main(String[] args) {
				
		try
		{
			DataOutputStream dos=null;
			File f=new File("test.txt");
			FileOutputStream fos=new FileOutputStream(f);
			BufferedOutputStream bos=new BufferedOutputStream(fos);
			dos=new DataOutputStream(bos);
			String s="";
			
			DataInputStream in=new DataInputStream(new BufferedInputStream(System.in));
			
				System.out.println("Please enter the line of text :");
				s=in.readLine();
				dos.writeChars(s);
				System.out.println(s);
				System.out.println("Your line has been saved in file.");
				
			dos.close();
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		

	}

}
