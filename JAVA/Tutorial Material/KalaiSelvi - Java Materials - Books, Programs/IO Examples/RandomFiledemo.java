

import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomFiledemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RandomAccessFile file;
		try
		{  
			file=new RandomAccessFile("rand.txt","r");
		
			/*file.writeChar('M');
			file.writeInt(7);
			file.writeDouble(3.14d);*/
			file.seek(0); //Moves the file pointer to the beginning
	
			//System.out.println("char   = "+file.readChar());
			int i =file.readInt();
			System.out.println("Int    = "+i);
			//System.out.println("Double = "+file.readDouble());
			
			file.close();		
		}
		catch(IOException e)
		{
		System.out.print(e);
		}
	}

}
