import java.io.*;

public class TestFileOutput
{
    public static void main(String args[])
     {
         //Allocates a buffer of 100 bytes
         byte buffer[] = new byte[100];
         try
         {

	//Reads 100 bytes from the start of filein.txt file
	System.in.read(buffer,0,100);
         }
         catch(IOException ioe)
          {
             System.out.println("Exception: "+ioe.toString());
           }
           try
            {
                 FileOutputStream fout= new FileOutputStream("text.txt");
	   fout.write(buffer);
            }catch(FileNotFoundException fnfe)
	{
                  System.out.println("Exception: "+fnfe.toString());
	}
	catch(IOException ioe)
          {
             System.out.println("Exception: "+ioe.toString());
           }
}


}

	
                       