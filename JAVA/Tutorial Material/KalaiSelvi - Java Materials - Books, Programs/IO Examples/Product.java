import java.io.Serializable;

public class Product implements Serializable{
	
	transient int  pid;
	String pname;
	int qty;
	float price;
	
	public String toString()
	{
		return "Pid : "+pid+" Name : "+pname+" Quantity : "+qty
		+" Price : "+price;
	}
	
	

}
