import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Formatter;

public class FormatterDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		String line;
		int i = 1;
		while ((line = br.readLine()) != null) {
		System.out.printf("Line%d: %s\n", i++, line);
		}

	}

}
