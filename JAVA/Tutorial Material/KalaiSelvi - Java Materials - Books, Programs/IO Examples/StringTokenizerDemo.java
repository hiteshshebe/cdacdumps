import java.util.StringTokenizer;

public class StringTokenizerDemo {

	static String in = "title=Java;"
			+ "author=KalaiSelvi;" + "publisher=Sun Microsystem;"
			+ "copyright=2008";

	public static void main(String args[]) {
		StringTokenizer st = new 
		StringTokenizer(in, "=;");
		while (st.hasMoreTokens()) {
			String key = st.nextToken(); // first Token is =
			String val = st.nextToken(); // second token is ;
			System.out.println(key + "\t" + val);
		}
	}
}