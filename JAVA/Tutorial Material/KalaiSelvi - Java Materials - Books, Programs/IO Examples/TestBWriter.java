package Byte.file.buffer;

import java.io.*;

public class TestBWriter
{
    public static void main(String args[])
     {
         int ch;
         BufferedWriter bw = null;
         System.out.println("Enter text(terminate by pressing ctrl +z):");
         try
          {
             bw = new BufferedWriter(new FileWriter("buffer.txt"));
             while((ch = System.in.read()!=-1)
              {
                   bw.write(ch);
              }
              bw.flush();
           }
           catch(IOException ioe)
            {

               System.out.println("Exception: " +ioe.toString());
            }
       }
}
     