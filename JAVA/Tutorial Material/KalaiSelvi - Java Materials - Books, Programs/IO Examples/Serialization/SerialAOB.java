import java.io.*;
class SerialObject4 implements Serializable
{
	int accno1;
	int sal1;
	String name;
	SerialObject4()
	{
		
	}
	SerialObject4(int i,int f,String s)
	{
		this.accno1=i;
		this.sal1=f;
		this.name=s;
		
	}
	public String toString()
	{
		return "Name= "+name+"\nEmp no :="+accno1+"\nBasic Salary ="+sal1;
	}
	public byte[] toByte(String st)
	{
	return(st.getBytes());	
	}
	
}
public class SerialAOB extends SerialObject4
{
	int eno,basic;
	String name;
	byte b[]=new byte[20];
	byte b1[]=new byte[20];
	float da,ta,hra,la,pf,loan,allow,ded,net;
	float da1,hra1,la1,pf1,ta1,loan1,ded1,net1,allow1;
		public void getValues(int ff,OutputStream fo)
	{
		try
		{
			DataInputStream dis=new DataInputStream(System.in);
			
		System.out.println("Enter the empcode");
			eno=Integer.parseInt(dis.readLine());
			System.out.println("Enter your name");
			name=dis.readLine();
			b1=name.getBytes();

			
			System.out.println("Enter your basic salary");
			basic=Integer.parseInt(dis.readLine());
			
			SerialObject4 sob=new SerialObject4(eno,basic,name);
			System.out.println(sob);
			FileOutputStream fos=new FileOutputStream("Pay");
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(sob);
			oos.flush();
			
			
			da=basic*0.09f;
			ta=basic*0.05f;
			hra=basic*0.04f;
			la=basic*0.03f;
			pf=basic*0.35f;
			loan=basic*0.04f;
			allow=(da+ta+hra+la);
			ded=pf+loan;
			net=basic+allow-ded;
			da1=((basic*0.09f)/31);
			ta1=((basic*0.05f)/31);
			hra1=((basic*0.04f)/31);
			la1=((basic*0.03f)/31);
			pf1=((basic*0.35f)/31);
			loan1=((basic*0.04f)/31);
			allow1=(da1+ta1+hra1+la1);
			ded1=pf1+loan1;
			net1=(basic/31)+allow1-ded1;
			
				
				System.out.println("For which month (1-12)");
				String month=dis.readLine();
				int month1=Integer.parseInt(month);
				switch(month1)
				{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
						fo.write(toByte("\n\n"));
						System.out.println("\t\t\t\t\t\t  SALARY SLIP OF "+name);
						fo.write(toByte("\t\t\t\t\t\t  SALARY SLIP OF "+name));
						System.out.println("\t\t\t\t\t\tEmployee Id of "+name+" is "+eno);
						fo.write(toByte("\n\t\t\t\t\t\tEmployee Id of "+name+" is "+eno));
						System.out.println("\t\t\t\t\t\t-------------------------");
						fo.write(toByte("\n\t\t\t\t\t\t-------------------------"));
						System.out.println("\t\t\tHouse Rent Allowance   :"+hra);
						fo.write(toByte("\n\t\t\tHouse Rent Allowance   :"+hra));
						System.out.println("\t\t\tDearness Allowance     :"+da);
						fo.write(toByte("\n\t\t\tDearness Allowance     :"+da));
						System.out.println("\t\t\tTravel Allowance       :"+ta);
						fo.write(toByte("\n\t\t\tTravel Allowance     :"+ta));
						System.out.println("\t\t\tLeave Allowance        :"+la);
						fo.write(toByte("\n\t\t\tLeave Allowance     :"+da));
						System.out.println("\t\t\t\tTotal Allowance of "+name+" is "+allow);
						fo.write(toByte("\n\t\t\t\tTotal Allowance of "+name+" is "+allow));
						System.out.println("\t\t\tProvident Fund         :"+pf);
						fo.write(toByte("\n\t\t\t\tProvident Fund         :"+pf));
						System.out.println("\t\t\tInterest for loan taken :"+loan);
						fo.write(toByte("\n\t\t\t\tInterest for loan taken :"+loan));
						System.out.println("\t\t\t\tTotal Deduction of "+name+" is "+ded);
						fo.write(toByte("\n\t\t\t\tTotal Deduction of "+name+" is "+ded));
						System.out.println("\t\t\t\tFinal Salary of "+name+" is Rs "+net);
						fo.write(toByte("\n\t\t\t\tFinal Salary of "+name+" is Rs "+net));
						break;
				  case 2:  
				  	fo.write(toByte("\n\n"));
				  	System.out.println("\t\t\t\t\t\t  SALARY SLIP OF "+name);
					fo.write(toByte("\t\t\t\t\t\t  SALARY SLIP OF "+name));
					System.out.println("\t\t\t\t\t\tEmployee Id of "+name+" is "+eno);
					fo.write(toByte("\n\t\t\t\t\t\tEmployee Id of "+name+" is "+eno));
					System.out.println("\t\t\t\t\t\t-------------------------");
					fo.write(toByte("\n\t\t\t\t\t\t-------------------------"));
					System.out.println("\t\t\tHouse Rent Allowance   :"+hra1*28);
					fo.write(toByte("\n\t\t\tHouse Rent Allowance   :"+hra1*28));
					System.out.println("\t\t\tDearness Allowance     :"+da1*28);
					fo.write(toByte("\n\t\t\tDearness Allowance     :"+da1*28));
					System.out.println("\t\t\tTravel Allowance       :"+ta1*28);
					fo.write(toByte("\n\t\t\tTravel Allowance     :"+ta1*28));
					System.out.println("\t\t\tLeave Allowance        :"+la1*28);
					fo.write(toByte("\n\t\t\tLeave Allowance     :"+da1*28));
					System.out.println("\t\t\t\tTotal Allowance of "+name+" is "+allow1*28);
					fo.write(toByte("\n\t\t\t\tTotal Allowance of "+name+" is "+allow1*28));
					System.out.println("\t\t\tProvident Fund         :"+pf1*28);
					fo.write(toByte("\n\t\t\t\tProvident Fund         :"+pf1*28));
					System.out.println("\t\t\tInterest for loan taken :"+loan1*28);
					fo.write(toByte("\n\t\t\t\tInterest for loan taken :"+loan1*28));
					System.out.println("\t\t\t\tTotal Deduction of "+name+" is "+ded1*28);
					fo.write(toByte("\n\t\t\t\tTotal Deduction of "+name+" is "+ded1*28));
					System.out.println("\t\t\t\tFinal Salary of "+name+" is Rs "+net1*28);
					fo.write(toByte("\n\t\t\t\tFinal Salary of "+name+" is Rs "+net1*28));
					break;
					 
			      case 4:
			      case 6:
			      case 9:
			      case 11: 	
			      	fo.write(toByte("\n\n"));
			      	System.out.println("\t\t\t\t\t\t  SALARY SLIP OF "+name);
					fo.write(toByte("\t\t\t\t\t\t  SALARY SLIP OF "+name));
					System.out.println("\t\t\t\t\t\tEmployee Id of "+name+" is "+eno);
					fo.write(toByte("\n\t\t\t\t\t\tEmployee Id of "+name+" is "+eno));
					System.out.println("\t\t\t\t\t\t-------------------------");
					fo.write(toByte("\n\t\t\t\t\t\t-------------------------"));
					System.out.println("\t\t\tHouse Rent Allowance   :"+hra1*30);
					fo.write(toByte("\n\t\t\tHouse Rent Allowance   :"+hra1*30));
					System.out.println("\t\t\tDearness Allowance     :"+da1*30);
					fo.write(toByte("\n\t\t\tDearness Allowance     :"+da1*30));
					System.out.println("\t\t\tTravel Allowance       :"+ta1*30);
					fo.write(toByte("\n\t\t\tTravel Allowance     :"+ta1*30));
					System.out.println("\t\t\tLeave Allowance        :"+la1*30);
					fo.write(toByte("\n\t\t\tLeave Allowance     :"+da1*30));
					System.out.println("\t\t\t\tTotal Allowance of "+name+" is "+allow1*30);
					fo.write(toByte("\n\t\t\t\tTotal Allowance of "+name+" is "+allow1*30));
					System.out.println("\t\t\tProvident Fund         :"+pf1*30);
					fo.write(toByte("\n\t\t\t\tProvident Fund         :"+pf1*30));
					System.out.println("\t\t\tInterest for loan taken :"+loan1*30);
					fo.write(toByte("\n\t\t\t\tInterest for loan taken :"+loan1*30));
					System.out.println("\t\t\t\tTotal Deduction of "+name+" is "+ded1*30);
					fo.write(toByte("\n\t\t\t\tTotal Deduction of "+name+" is "+ded1*30));
					System.out.println("\t\t\t\tFinal Salary of "+name+" is Rs "+net1*30);
					fo.write(toByte("\n\t\t\t\tFinal Salary of "+name+" is Rs "+net1*30));
					break;
				}
		
				
		}
			
		catch(Exception e)
		{ 
			System.out.println(e);
			}
		try
		{
			SerialObject4 sobj3;
			FileInputStream fis=new FileInputStream("Pay");
			ObjectInputStream ois=new ObjectInputStream(fis);
			sobj3=(SerialObject4)ois.readObject();
			ois.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception during De Serialization");
			System.exit(0);
		}
		
		}
	public static void main(String[] args) {
		int num=0;
		SerialAOB ps[]=new SerialAOB[4];
		try
		{
			DataInputStream di=new DataInputStream(System.in);
			System.out.println("How many employee need to see their PaySlips..");
			System.out.println("Maximum 4 employees at a time");
			num=Integer.parseInt(di.readLine());
			OutputStream fo=new FileOutputStream("PayFile1");
			for(int j=0;j<num;j++)
			{
				ps[j]=new SerialAOB();
				ps[j].getValues(j,fo);
			}
			fo.close();
		
	}
		catch(Exception e1){}
}
}


