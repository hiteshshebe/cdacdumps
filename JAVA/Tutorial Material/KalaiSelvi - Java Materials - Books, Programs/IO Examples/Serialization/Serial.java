
import java.io.*;
public class Serial {

	public static void main(String[] args) {
		try{
		SerialObject sobj=new SerialObject(300,50.5f,"Pradakshina");
		System.out.println(sobj);
		FileOutputStream fos=new FileOutputStream("SerialFile");
		ObjectOutputStream  oos = new ObjectOutputStream (fos);
		oos.writeObject(sobj);
		oos.flush();
		oos.close();
		}catch(Exception e)
		{System.out.println("Exception during Serialization");
		System.exit(0);}
		
		try{
			SerialObject sobj1;
			FileInputStream fis=new FileInputStream("SerialFile");
			ObjectInputStream  ois = new ObjectInputStream (fis);
			sobj1=(SerialObject)ois.readObject();
			ois.close();
			}catch(Exception e)
			{System.out.println("Exception during De-Serialization");
			System.exit(0);}
		}
}

/*

class A  //implements Serializable 
{ A(){Syso("dsf");
}
class B extends A //implements Serializable 
{Syso("sdsdf");
}

*/		
class SerialObject extends B implements Serializable 
{int i; float f; String s;
    SerialObject(int i,float f,String s)
    {
        this.i=i;
        this.f=f;
        this.s=s;
    }
    public String toString()
    {
       
      return i+f+s;
}
}