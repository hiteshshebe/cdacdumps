import java.awt.*;
import java.io.*;
public class ExternalizableObject 
{
  String filePath;
  public static void main( String args[] ) {
      MyVersionObject orw = new MyVersionObject();
  }
  ExternalizableObject () {
    try {
      //  Create instances of each data class to be serialized.
      MySerialObject serialObject = new MySerialObject();
      MyExternObject externObject = new MyExternObject();
      MyVersionObject versionObject = new MyVersionObject();
      //  Allow the user to specify an output file.
      FileDialog fd = new FileDialog( new Frame(), 
        "Save As...", FileDialog.SAVE );
      fd.show();
      filePath = new String( fd.getDirectory() + fd.getFile() );
      //  Create a stream for writing.
      FileOutputStream fos = new FileOutputStream( filePath );
      //  Next, create an object that can write to that file.
      ObjectOutputStream outStream = 
        new ObjectOutputStream( fos );
      //  Save each object.
      outStream.writeObject( serialObject );
      externObject.writeExternal( outStream );
      outStream.writeObject( versionObject );
      //  Finally, we call the flush() method for our object, which 
         // forces the data to 
      //  get written to the stream:
      outStream.flush();
      //  Allow the user to specify an input file.
      fd = new FileDialog( new Frame(), "Open...",
          FileDialog.LOAD );
      fd.show();
      filePath = new String( fd.getDirectory() + fd.getFile() );
      //  Create a stream for reading.
      FileInputStream fis = new FileInputStream( filePath );
      //  Next, create an object that can read from that file.
      ObjectInputStream inStream = new ObjectInputStream( fis );
      // Retrieve the Serializable object.
      serialObject = ( MySerialObject )inStream.readObject();
      //  Display what we retrieved:
      System.out.println( serialObject.getS() );
      System.out.println( "i = " + serialObject.getI() );
      serialObject.displayInternalObjectAttrs();
      // Retrieve the Externalizable object.
      externObject.readExternal( inStream );
      //  Display what we retrieved:
      System.out.println( externObject.getS() );
      System.out.println( "i = " + externObject.getI() );
      // Retrieve the versioned object.
      versionObject = ( MyVersionObject )
        inStream.readObject();
      //  Display what we retrieved:
      System.out.println( versionObject.getS() );
      System.out.println( "i = " + versionObject.getI() );
      // Display the SUID of the versioned class in the VM, 
      // not necessarily the serialized object.
      ObjectStreamClass myObject = ObjectStreamClass.lookup(
          Class.forName( "MyVersionObject" ) );
      long theSUID = myObject.getSerialVersionUID();
      System.out.println
        ( "The SUID of class MyVersionObject = " + theSUID );
    }
    catch ( InvalidClassException e ) {
      System.out.println( "InvalidClassException..." );
    }
    catch ( ClassNotFoundException e ) {
      System.out.println( "ClassNotFoundException..." );
    }
    catch ( OptionalDataException e ) {
      System.out.println( "OptionalDataException..." );
    }
    catch ( FileNotFoundException e ) {
      System.out.println( "FileNotFoundException..." );
    }
    catch ( IOException e ) {
      System.out.println( "IOException..." );
    }
  }
}
 class MySerialObject implements Serializable {
  private transient int i;
  private String s;
  MyInternalObject mio;
  MySerialObject() {
    i = 64;
    s = new String( "Instance of MySerialObject..." );
    mio = new MyInternalObject();
  }
  public int getI() {
    return i;
  }
  public String getS() {
    return s;
  }
  public void displayInternalObjectAttrs() {
    System.out.println( mio.getS() );
    System.out.println( "i = " + mio.getI() );
  }
}

 class MyInternalObject implements Serializable {
  private int i;
  private String s;
  MyInternalObject() {
    i = 128;
    s = new String( "Instance of MyInternalObject..." );
  }
  public int getI() {
    return i;
  }
  public String getS() {
    return s;
  }
}

 class MyExternObject implements Externalizable {
  private int i;
  private String s;
  MyExternObject() {
    i = 256;
    s = new String( "Instance of MyExternObject..." );
  }
  public int getI() {
    return i;
  }
  public String getS() {
    return s;
  }
  public void writeExternal( ObjectOutput out ) throws
      IOException {
    out.writeInt( this.i );
    out.writeObject( this.s );
  }
  public void readExternal( ObjectInput in ) throws
      IOException, ClassNotFoundException {
    this.i = in.readInt();
    this.s = ( String )in.readObject();
  }
}

 class MyVersionObject implements Serializable {
  static final long serialVersionUID = 1L;
  private int i;
  private String s;
  //  Uncomment the next two lines to verify that default values will be substituted if
  //  the value is not present in the stream at deserialization time.
  //  private int i2 = -1; private String s2 = "This is the new String field";
  MyVersionObject() {
    i = 512;
    s = new String( "Instance of MyVersionObject..." );
  }
  public int getI() {
    return i;
  }
  public String getS() {
    return s;
  }
}

