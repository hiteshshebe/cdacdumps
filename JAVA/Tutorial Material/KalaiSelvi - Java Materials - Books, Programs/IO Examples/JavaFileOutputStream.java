import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class JavaFileOutputStream {
  public static void main(String[] args) {
    File originFile = new File("source.txt");
    File destinationFile = new File("destination.txt");
   // if (!originFile.exists() || destinationFile.exists()) {
      
      //System.out.println("exists");
      //return;
    //}
    try {
      byte[] readData = new byte[1024];
      FileInputStream fis = new FileInputStream(originFile);
      FileOutputStream fos = new FileOutputStream(destinationFile);
      int i = fis.read(readData);

      while (i != -1) {
        fos.write(readData, 0, i);
       
        i = fis.read(readData);
        System.out.println("Inside  :"+i);
      }
      fis.close();
      fos.close();
      byte[] vals = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
      FileOutputStream fout = new FileOutputStream("Test.dat");
      for (int ii = 0; ii < vals.length; ii += 2)
        fout.write(vals[ii]);
      fout.close();
    } catch (IOException e) {
      System.out.println(e);
    }
    
    
      
  }
}