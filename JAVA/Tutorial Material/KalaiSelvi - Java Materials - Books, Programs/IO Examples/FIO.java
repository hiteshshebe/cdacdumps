import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FIO 
{

	public static void main(String[] args)
	{
		try{
			
			File f=new File("input.txt");
			File f1 = new File("read.txt");
			FileInputStream fi = new FileInputStream(f);
			BufferedInputStream bi = new BufferedInputStream(fi);
			
			FileOutputStream fw = new FileOutputStream(f1);
			BufferedOutputStream bo = new BufferedOutputStream(fw);
		
		    int c  = bi.read();
		   
			while (c!=-1)
			{ 
				System.out.print((char)c);
				c = bi.read();
				bo.write(c);
						
			}
			bi.close();
			fi.close();
		    bo.close();
		    fw.close();
		    
			
			
			
			
			}catch(IOException ioe){System.out.print("Error"+ioe);}
		     	

	}

}
