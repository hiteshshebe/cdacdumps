import java.io.*;
public class TestDataOutput
{
     public static void main(String args[])
     {
        try
         {
               DataOutputStream dos = new DataOutputStream(new FileOutputStream("text.txt"));
	dos.writeInt(25);
	dos.writeDouble(45.80);
	dos.writeUTF("Don Allen");
	dos.close();
	DataInputStream dis = new DataInputStream(new FileOutputStream("text.txt"));
	System.out.println("Integer is: " +dis.readInt());
	System.out.println("Double is: " +dis.readDouble());
	System.out.println("String is: " +dis.readUTF());
         }catch(FileNotFoundException fnfe)
         {
            	System.out.println("Exception: " +fnfe.toString());
         }
         catch(IOException ioe)
         {
	System.out.println("Exception: " +ioe.toString());
         }
     }
}
