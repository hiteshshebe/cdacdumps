package Byte.file.stream;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriteStreamDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try
		{
			FileOutputStream fos=new FileOutputStream("vam.txt");
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			DataOutputStream dos=new DataOutputStream(fos);
			dos.writeInt(21);
			dos.writeChar('M');
			dos.writeDouble(5.0);
			System.out.println("");
			dos.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

}
