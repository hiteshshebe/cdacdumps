import java.io.*;

public class CopyFile{
	
	static FileInputStream fin;
	static FileOutputStream fout;
	
  public static void main(String args[]) throws IOException{
	int i;
	
	try{
		fin = new FileInputStream(args[0]);
		fout = new FileOutputStream(args[1]);
		do {
			i=fin.read();
			if(i!=-1)
			fout.write(i);
		} while (i!=-1);
	
	
	}	
	catch(FileNotFoundException e){
		System.out.println("File Not Found");
		return;	
	}
		
	catch(IOException e){
	      System.out.println("Error..........");
	      return;	
	}
	finally
	{
		fin.close();		
		fout.close();
	}
  }
}
