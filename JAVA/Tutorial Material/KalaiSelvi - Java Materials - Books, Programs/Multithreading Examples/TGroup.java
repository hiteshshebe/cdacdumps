class T1 extends Thread
{public void run(){
    System.out.println("Thread One");}
    
}
class T2 extends Thread
{public void run(){
    System.out.println("Thread Two");}
    
}


public class TGroup {

    public static void main(String[] args) {
        T1 tt=new T1();
        T2 ss=new T2();
        
        ThreadGroup tg=new ThreadGroup("Group One");
        Thread t=new Thread(tg,tt);
        tt.setPriority(10);
        Thread t1=new Thread(tg,ss);  
        System.out.println(tt.getPriority());
        t.setDaemon(true);
        tg.setMaxPriority(11);
        ThreadGroup tg1=new ThreadGroup("Group Two");
        tg1.setDaemon(true);
        Thread t2=new Thread(tg1,tt);
        Thread t3=new Thread(tg1,ss);
        System.out.println(tg.activeCount());
        System.out.println(tg.getName());
        System.out.println("Max priority"+tg.getMaxPriority());
        System.out.println(tg.activeGroupCount());
        System.out.println(tg1.getMaxPriority());
        System.out.println(tg.getParent());
        System.out.println(t.isDaemon());
        System.out.println(tg1.isDaemon());
        System.out.println(tg.getMaxPriority());
       // System.out.print(tg.list());
        
        
    }
}
