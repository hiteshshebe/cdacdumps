class NewThread implements Runnable 
{
	String name;
	Thread t;

	NewThread (String threadname)
	{
	name = threadname;
	t=new Thread(this,name);
	System.out.println("New thread:"+t);
	t.start();
	}

	public void run()
	{
	try
	{
		for(int i=5;i>0;i--)
		{
		System.out.print(name);// +":"+i);
		Thread.sleep(1000);
		}
	}catch(InterruptedException e)

		{
		System.out.println(name +"interrupted");
		}
		System.out.println(name+"exiting.:");
	}
	}
class DemoSym
{
public static void main(String abc[])
{
	NewThread b = new  NewThread("*");
	NewThread c = new  NewThread("$");
	NewThread d = new  NewThread("#");

	System.out.println("thread *  is alive:"+b.t.isAlive());
	System.out.println("thread $  is alive:"+c.t.isAlive());
	System.out.println("thread #  is alive:"+d.t.isAlive());
	try
	{
	System.out.println("Waiting for threads to finish.");
	b.t.join();
	c.t.join();
	d.t.join();
	}
	catch(InterruptedException e)
	{
	System.out.println("Main thread Interrupted");
	}

	System.out.println("thread * is alive:"+b.t.isAlive());
	System.out.println("thread $ is alive:"+c.t.isAlive());
	System.out.println("thread # is alive:"+d.t.isAlive());
	System.out.println("Main thread exiting");
	}
}




