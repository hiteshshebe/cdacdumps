class Tread implements Runnable
{
String name;
char a; 
Thread t;

Tread(String title,char ch)
 {
 name=title;
 t=new Thread(this,name);
 a=ch;
 System.out.println("New Thread " +t);
 t.start();
 }

public void run()
{
try
 {
 for(int i=5;i>0;i--)
 {
 System.out.print(a);
 Thread.sleep(250);
 }
 }
 catch(InterruptedException e)
 {
 System.out.println(name+"Interrupted");
 }
 System.out.println();
 System.out.println(name+"exiting");
 }
}

class ThreadDemo2
{
 public static void main(String args[])
  {
   Tread t1=new Tread("First",'*');
   Tread t2=new Tread("Second",'$');
   Tread t3=new Tread("Third",'#');
   t1.t.setPriority(8);
   t2.t.setPriority(7);
   t3.t.setPriority(6);
   /*System.out.println("First alive is "+t1.t.isAlive());
   System.out.println("Second alive is "+t2.t.isAlive());
   System.out.println("Third alive is "+t3.t.isAlive());*/
    try
     {
      System.out.println("Waiting for Threads to finish.....");
      t1.t.join();
      t2.t.join();
      t3.t.join();
     }
     catch(InterruptedException e)
     {
     System.out.println("Main thread interrupted..");
     }
   /*System.out.println("First alive is "+t1.t.isAlive());
   System.out.println("Second alive is "+t2.t.isAlive());
   System.out.println("Third alive is "+t3.t.isAlive());*/
    System.out.println("Main thread Exiting..");
  }
}
