class testthread extends Thread  // implements Runnable
{
	Thread t1;
	public testthread()
	{
		System.out.println("cons called.");
		t1 = new Thread(this);
		t1.start();
	}
	public void run()
	{		
		System.out.println("run method called.");
		for(int i=1;i<=10;i++)
		{
			System.out.println("hello"+i);
			try{
				System.out.println(t1.isAlive());
				t1.suspend();
			System.out.println(t1.isAlive());
			}catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}
	public static void main(String arg[])
	{
		testthread tt=null;
		tt = new testthread();
		tt.t1.resume();
		testthread tt1 = new testthread();
		tt1.t1.resume();		
		System.out.println("I am here ");
		testthread tt2 = new testthread();
		tt2.t1.resume();		

	}
}