class Syn
 {
 boolean value=false;
 synchronized void call(String msg)
 {
  System.out.print("["+msg);
  if(!value)
   try
    {
    wait();
    }
   catch(InterruptedException e)
   {
   System.out.println("Interrupted...");
   }
 System.out.println("]");
 value=false;
 notify();
 }
 synchronized void call1(String msg)
 {
  System.out.print("]");
  if(!value)
   try
    {
    wait();
    }
   catch(InterruptedException e)
   {
   System.out.println("Interrupted...");
   }
 System.out.println("]");
 value=true;
 notify();
 }
 }

 class CallSyn implements Runnable
 {
 String msg;
 Syn target;
 Thread t;

 public CallSyn(Syn targ,String s)
 {
 target=targ;
 msg=s;
 t=new Thread(this);
 t.start();
 }

 public void run()
 {
 //synchronized(target)
 //{
 target.call(msg);
 //}
 }
 }

 class SynThreadDemo
 {
  public static void main(String args[])
  {
  Syn target=new Syn();
  CallSyn cs1=new CallSyn(target,"TCIL");
  CallSyn cs2=new CallSyn(target,"synchronised");
  CallSyn cs3=new CallSyn(target,"Axes");
  try
   {
    cs1.t.join();
    cs2.t.join();
    cs3.t.join();
   }
   catch(InterruptedException e)
   {
   System.out.println("Interrupted...");
   }
 }
}
