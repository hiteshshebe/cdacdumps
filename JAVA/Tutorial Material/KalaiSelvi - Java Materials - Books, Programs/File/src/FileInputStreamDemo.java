import java.io.FileInputStream;
import java.io.IOException;


public class FileInputStreamDemo {

	
	public static void main(String[] args) {
	   int size;
	   try ( FileInputStream f = new  FileInputStream("Test.txt") )
	   { 
		   System.out.println("total available bytes:" + (size=f.available()));
		   int n=size/40;
		   System.out.println("first" + n + "bytes of the file one read() at a time");
		   for(int i=0;i<n;i++)
		   {
			   System.out.println((char) f.read());
		   }
		   
		   System.out.println("\n still available:" + f.available());
           System.out.println("reading the next" + n + "with one read(b[])");
           byte b[]=new byte[n];
           if(f.read(b)!=n)
           {
        	   System.out.println("couldn't read" + n + "bytes.");
           }
           System.out.println(new String(b,0,n));
           System.out.println("\n still available:" + (size=f.available()));
           System.out.println("skipping half of remaining bytes with skip()");
           f.skip(size/2);
           System.out.println("still available:" + f.available());
           System.out.println("reading" + n/2 + "into the end of array");
           if(f.read(b,n/2,n/2)!=n/2)
            {
        	   System.err.println("couldn't read" + n/2 + "bytes.");
        	   
           }
           System.out.println(new String(b, 0, b.length));
           System.out.println("\n Still Available:" + f.available());
           }
	   catch( IOException e){
		   System.out.println("I/O Error:" + e);
	   }
	}

}
