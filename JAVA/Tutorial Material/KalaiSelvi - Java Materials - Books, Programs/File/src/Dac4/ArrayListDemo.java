package Dac4;

import java.util.ArrayList;
import java.util.ListIterator;

public class ArrayListDemo {
public static void main(String[] args) {
	ArrayList al =new ArrayList();
	al.add(new Product(1,"soap"));
	al.add(new Product(2,"shampoo"));
	ArrayList alist =new ArrayList();
	alist.add(new Product1(34,"TV"));
	alist.addAll(al);
	alist.add(0,new Product1(3,"ipod"));
	System.out.println(alist.size());
	System.out.println(alist.get(2));
	System.out.println(alist.remove(2));
	System.out.println(alist.size());
	alist.trimToSize();
	ListIterator list = alist.listIterator();
	while(list.hasNext())
	{
		Object obj = list.next();
		if(obj instanceof Product1)
		{
			Product1 p = (Product1)obj;
			System.out.println("PRODUCT DETAILS");
			System.out.println(p.pid);
			System.out.println(p.pname);
		}
		else
		{
			System.out.println(obj);
		}
	}
}
}
