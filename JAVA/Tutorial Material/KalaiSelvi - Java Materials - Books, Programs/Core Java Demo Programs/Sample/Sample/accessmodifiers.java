/*
 * Created on Mar 26, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author kalaiselvi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

class abc
{
    private void pmethod()
    {
        System.out.println("I am private method in abc class");
        System.out.println("sdf");
    }
    static void sub1()
    {System.out.println("I am static method in abc class");
    abc ob=new abc(); //Creating the object of the abc class
    ob.pmethod();  //calling private method within the same class
    }
    final void fsub()
    {
        sub1(); //calling the static method within the same class
        int a=10;
        int b=20;
        int sub=b-a;
    System.out.println("I am final method in abc class");
        }
    protected void psub()
    {System.out.println("I am protected method in abc class");
        }
    
    }

public class accessmodifiers {

    public void add()
    {System.out.println("I am public method");
        }
    
    static void sub()
    {System.out.println("I am static method");
        }
    
    public static void main(String[] args) {
        accessmodifiers obj=new  accessmodifiers();
        obj.add(); //calling public method within the same class
        sub(); //calling static method from a the same class 
        abc.sub1(); //calling static method from a different class 
        abc obj1=new abc();
        obj1.fsub(); //calling final method from a different class 
        obj1.psub(); //calling protected method from a different class 
    }    
        
}
