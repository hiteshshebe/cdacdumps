import java.io.*;

/**
*This class demonstrates documentation comments
*@author Herbert Schield
*@version java 1.4.1
*/

public class SquareNum1
{

/**
*This method returns the square of number
*This method is a multiline description.You can see
*as many as u like
*/
public double Square(double num)
{
	return num*num;
}

/**
*This inputs a number from the user
*@ return the value input as a double
*/

public double getNumber() throws IOException
{
BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
String str=br.readLine();
return(new Double(str)).doubleValue();
}

/**
*This method demonstrates Square()
*@param args unused
*@return nothing
*@exception IoException on i/p error
*@see IoException
*/

public static void main(String args[])
throws IOException
{
{
SquareNum ob=new SquareNum();
double val;
System.out.println("Enter value to be squared:");
val=ob.getNumber();
val=ob.Square(val);
System.out.println("Square value:"+val);
}
}
}






