/*
 * Created on Feb 14, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author kalaiselvi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class string {

    public static void main(String[] args) {
      String s;
      int i=0,arr[]=new int[10];
      arr[i++]=i+++i++;
      System.out.println("i= "+i +"arr=" +arr[i]);
        String s1="abc"; //==
      String s2="Abc";  //equals() equalsIgnoreCase()
     // StringBuffer n="sd";
    //  System.out.println(n.length());
      StringBuffer sb=new StringBuffer();
      String ss=new String();
      
      System.out.println(s1.length());
     // System.out.println(s.length());
      System.out.println(ss.length());
      System.out.println(sb.length());
      if (s1==s2)
      System.out.println("Equal");
      else
                   System.out.println("Un Equal");
      String s3=new String("abc");
      String s4=new String("abc");
      StringBuffer cc=new StringBuffer(s3);
      System.out.println(s3.length());
      System.out.println(s3+s4);
      System.out.println(s3.concat(s4));
      if (s3==s4)
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
      
      if (s3.equals(s4))
      System.out.println("Equal");
      else
                   System.out.println("Un Equal");
      
      if (s1==s2)
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
      
      if (s1==s3)
          System.out.println(" == s1 and s3 are Equal");
          else
                       System.out.println("==s1 and s3 are Un Equal");
      if (s1.equals(s3))
          System.out.println("  s1 and s3 are Equal");
          else
                       System.out.println("s1 and s3 are Un Equal");
      
      StringBuffer s5=new StringBuffer("abc");
      StringBuffer s6=new StringBuffer("abc");
      
      System.out.println(s5.length());
      if (s5==s6)
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
      if (s5.equals(s6))
          System.out.println("SB Equal");
          else
                       System.out.println("SB Un Equal");
   
      if (s1.equals(s3))
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
      if (s1==s3)
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
   
      if (s1.equals(s5))  //cannot compare with ==
          System.out.println("Equal");
          else
                       System.out.println("Un Equal");
      
      
      //Methods for String Buffer
      System.out.println(s5.capacity());
     
      System.out.println(s5.length());
      System.out.println(s5.hashCode());
      System.out.println(s5.toString());
      System.out.println(s5.append("ddd"));
      System.out.println(s5.indexOf("d"));
      System.out.println(s5.lastIndexOf("d"));
      System.out.println(s5.substring(3));
      System.out.println(s5.substring(2,5));
      System.out.println(s5.append(45.56));
      System.out.println(s5.append(s6));
      System.out.println(s5);
      System.out.println(s5.insert(3,0));
      System.out.println(s5.insert(0,s3));
      System.out.println(s5.deleteCharAt(6));
      System.out.println(s5.getClass());
      System.out.println(s5);
      System.out.println(s5.subSequence(2,5));
      s5.ensureCapacity(45);
      System.out.println(s5.length());
      System.out.println(s5.capacity());
      System.out.println(s5.replace(2,5,"gg"));  //if string is "fggggg" all are replaced
      char ch[]=new char[2];
     //s5.getChars(2,3,ch,5);
     //System.out.println(s5);
      s5.setLength(6);
      System.out.println(s5.length());
      System.out.println(s5);
      System.out.println(s5.capacity());
      System.out.println(s5.reverse());
      
      
      //Methods for String 
      
      System.out.println(s1.charAt(2));
      
      System.out.println(s1.compareTo(s2));
      System.out.println(s1.compareTo(s3));
      System.out.println(s2.compareTo(s3));
      System.out.println(s1.endsWith("c"));
      System.out.println(s1.contentEquals(s6));
      System.out.println(s1.split("c"));
      System.out.println(s1.intern());
      System.out.println(s1);
      System.out.println(s1.regionMatches(1,"c",3,2));
      System.out.println(s1);    
    }
}
