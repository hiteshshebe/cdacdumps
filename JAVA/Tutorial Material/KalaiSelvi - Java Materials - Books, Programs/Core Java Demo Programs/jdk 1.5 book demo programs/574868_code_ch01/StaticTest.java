import static Colors.*;
import static Fruits.*;

public class StaticTest {
    public static void main(String args[])
    {
        System.out.println("orange = " + orange);
        System.out.println("color orange = " + Colors.orange);
        System.out.println("Fruity orange = " + Fruits.orange);
    }
}