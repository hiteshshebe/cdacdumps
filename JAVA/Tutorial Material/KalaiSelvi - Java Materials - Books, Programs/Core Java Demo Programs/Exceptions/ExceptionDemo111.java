class ExceptionDemo
{
   public static void main(String args[])
   {

   try
    {
	int i = Integer.parseInt(args[0]);
	int j = Integer.parseInt(args[1]);
	int k = i/j;
	System.out.println("k = "+k);
    }
   
    catch(ArithmeticException a)
    {
	System.out.println("ArithmeticException generated:  "+a);
        a.printStackTrace();
    }
    catch(ArrayIndexOutOfBoundsException a)
    {
	System.out.println("ArrayIndexOutofBoundsException generated:  "+a);
    }
    catch(NumberFormatException a)
    {
	System.out.println("NumberException generated:  "+a);
    }
    catch(Exception e)
    {
	System.out.println("Exception generated:  "+e);
    }
    
    finally
    {
      System.out.println("Inside Finally block");
    }
      System.out.println("Exiting Main method");
   } 

}
