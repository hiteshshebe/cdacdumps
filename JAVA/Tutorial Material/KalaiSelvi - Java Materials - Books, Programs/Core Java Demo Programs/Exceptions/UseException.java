import exceptionpack.*;

class UseException
{
   public static void main(String args[])
     {
       try
	  {
	    int i=Integer.parseInt(args[0]);

	    if(i<0) throw 
		  new MyException(i);
            else
		  System.out.println("You entered a valid no:"+i);
           }
        catch(MyException m)
	   {
	       System.out.println(m);
	       m.printStackTrace();
           }
       
      }
}
