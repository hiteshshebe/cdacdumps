import exceptionpack.IndiaException;
class UException1
{
 public static void main(String args[]) 
 {
 try
  {
   if(!args[0].equals("India"))
      throw new IndiaException(args[0]);
   else
      System.out.println("String : "+args[0]);
  }

  catch(IndiaException a)
  {
   StackTraceElement s[]=a.getStackTrace();
   for(int i=0;i<s.length;i++)
   {
  System.out.println("File Name : "+s[i].getFileName());
  System.out.println("Class Name : "+s[i].getClassName());
  System.out.println("Method Name : "+s[i].getMethodName());
  System.out.println("Line No. : "+s[i].getLineNumber());
   }
 }
 }
}

