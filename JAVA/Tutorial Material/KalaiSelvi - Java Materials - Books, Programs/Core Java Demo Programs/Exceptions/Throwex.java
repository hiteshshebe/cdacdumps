class Throwex 
{
  static void disp() 
  {
    try
      {
	throw new NullPointerException("Null");
      }
     catch(NullPointerException e)
      {
	System.out.println("Caught by Throwex");
	throw e;
      }
   }

   public static void main(String a[])
   {
     try
       {
	 disp();
       }
     catch(NullPointerException e)
       {
	 System.out.println("Recaught"+e);
       }
   }
}
