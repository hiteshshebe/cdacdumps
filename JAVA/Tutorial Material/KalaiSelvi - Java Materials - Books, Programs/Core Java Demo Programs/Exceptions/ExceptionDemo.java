import java.io.*;

class Test1
{
	public void m1() throws Exception
	{
		System.out.println("m1 in base");
		//throw new ArrayIndexOutOfBoundsException();
	}
	public void m1(int a) throws ArrayIndexOutOfBoundsException,NullPointerException,Exception
	{
		System.out.println("m1(a) in base");
		throw new ArrayIndexOutOfBoundsException();
	}
}




public class ExceptionDemo {
	public void m1()
	{
		System.out.println("m1 in child");
		//throw new ArrayIndexOutOfBoundsException();
	}
	public void m1(int k)
	{
		System.out.println("m1 in child");
		//throw new ArrayIndexOutOfBoundsException();
	}
	  public void add() throws Exception
	  {
		  System.out.println("Default");
		 // throw new Exception();
	  }

	  public void add(int a) throws Exception, IOException
	  {
		  System.out.println("1 int param");
		  throw new IOException();
	  }

	  public void add(float a) 
	  {
		  System.out.println("1 param");
	  }
	
	
	public static void main(String[] args) {
     ExceptionDemo obj=new ExceptionDemo();
     Test1 obj1=new Test1();
     
     try
     {obj1.m1(2);obj1.m1(2);
    	 obj.m1();
    	 obj.m1(2);
    obj.add(23);
    obj.add();
    obj.add(23);
     }catch(Exception e)
		{
			System.out.println(e);
		}
		
     }


}
