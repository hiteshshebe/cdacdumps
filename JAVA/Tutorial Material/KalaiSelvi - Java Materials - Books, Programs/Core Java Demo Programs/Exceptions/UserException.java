 import java.io.*;
class StringException extends Exception
  	  {
		private String userid; 		//instance variable
			StringException(String uid)
			{
				this.userid=uid;	//initializing using constructor
			}
			public String toString() //Overriding toString built in Function
			{
				return "Invalid ID -between 4 to 8:  "+ userid;
			}
      	 }
	public class UserException
		{
		static void checkUserId(String det) throws StringException
		{
				int len=0;
				len=det.length();		// Length of user Id

				if(len<4 || len >8 )	// Validating
				{
					throw new StringException (det);
				}

				System.out.println("Valid User Id");
		}
	public static void main(String[] args) throws java.io.IOException
		{
       		 // Creating buffer for storing character
BufferedReader in = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter the User Id : ");
		String id= in.readLine();

			try
			{
				checkUserId(id);//Checking user Id
			}
				catch(StringException e)
			{
				System.out.println("ERROR !!!  "+e);

			}
		}
	}
