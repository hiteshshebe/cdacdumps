import java.lang.* ;
import java.io.* ;

public class Square 
{

  public static void main ( String[] a ) throws IOException
  {
    BufferedReader stdin = 
        new BufferedReader ( new InputStreamReader( System.in ) );
 
    String inData;
    int    num ;

    System.out.println("Enter an integer:");
    inData = stdin.readLine();

    num    = Integer.parseInt( inData );     // convert inData to int

    System.out.println("The square of " + inData + " is " + num*num );

  }
}
