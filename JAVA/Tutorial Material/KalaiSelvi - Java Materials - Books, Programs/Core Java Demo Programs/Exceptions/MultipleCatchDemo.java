package ExceptionDemos;

public class MultipleCatchDemo 
{

	public static void main(String[] args) 
	{
		int a = 0;
		int b  = 5;
		int arr[] = {0,0};
		int result;
		try
		{
			result = b/a;
			System.out.println(b/arr[2]);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("Error Division by 0");
		}
		catch(ArrayIndexOutOfBoundsException ar)
		{
			System.out.println("Array Out of Bounds Exception");
			
		}
		catch(Exception e) 
		{
		  System.out.println("Some other error");
		}
	}
}
	
			




