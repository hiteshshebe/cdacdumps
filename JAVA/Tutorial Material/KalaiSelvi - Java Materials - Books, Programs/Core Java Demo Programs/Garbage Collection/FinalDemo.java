class FinalDemo
{
   int rno;
   String name;

   void set(int r,String n)
   {
    name=n;
    rno=r;
   }

   void show()
   {
    System.out.println("Reg. no."+rno+"Name"+name);
   }

   protected void finalize()
   {
    System.out.println("Inside finalize method ........");
   }
  public static void main(String args[])
   {
    FinalDemo s1=new FinalDemo();
    FinalDemo s2=new FinalDemo();
    s1.set(1001,"Ram");
    s2.set(1002,"Shyam");
    s1.finalize();
    s2.show();
    s1=null;
    System.gc();
    s1.show();
   }
}

 
