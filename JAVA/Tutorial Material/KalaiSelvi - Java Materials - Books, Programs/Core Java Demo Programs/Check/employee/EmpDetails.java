package com.employee;
import com.employee.DbConnect;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class EmpDetails {
	String name;
	double bpay;
	double allow;
	double da;
	double hra;
	double ma;
	double ca;
	double ta;
	double totalallowance;
	double gpay;
    String sql,sql2,sql3;
    int eid;
    
    
	Scanner sc = new Scanner(System.in);
	DbConnect db1=new DbConnect();
	
	public void bPay() {
		try
		{
		
		
		db1.connect1();
		
		
		sql=("insert into Emp_det values(?,?,?)");
		System.out.println("Enter employee id: ");
		db1.ps=db1.con.prepareStatement(sql);
		eid=sc.nextInt();
		System.out.println("Enter the employee name: ");
		name=sc.next();
		System.out.println("Enter the Basic Pay: ");
		bpay = sc.nextInt();
		System.out.println("Basic pay: " + bpay);
	
	}
		
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}

	public void dAllowance() {
		System.out.println("Dearness Allowance: ");
		da = 0.1 * bpay;
		System.out.println(da);
	}

	public void hRAllowance() {
		System.out.println("Home Rental Allowance: ");
		hra = 0.05 * bpay;
		System.out.println(hra);
	}

	public void mAllowance() {
		System.out.println("Medical Allowance: ");
		ma = 0.03 * bpay;
		System.out.println(ma);
	}

	public void cAllowance() {
		System.out.println("City Allowance: ");
		ca = 0.02 * bpay;
		System.out.println(ca);
	}

	public void tAllowance() {
		System.out.println("Travel Allowance: ");
		ta = 0.03 * bpay;
		System.out.println(ta);
	}

	public void Allowances() {
		bPay();
		dAllowance();
		hRAllowance();
		mAllowance();
		cAllowance();
		tAllowance();
		totalallowance = da + hra + ma + ca + ta;
		System.out.println("Total Allowance: " + totalallowance);
		gpay = bpay + totalallowance;
		System.out.println("Gross Pay: " + gpay);
		
		insertEmployee();
		allTable();
		
	}
	public void allTable()
	{
		try
		{
		sql2="insert into Sal_det values(?,?,?,?,?,?)";
		db1.ps1=db1.con.prepareStatement(sql2);
		PreparedStatement ps1=new DbConnect().connect1().prepareStatement(sql2);
		ps1.setInt(1,eid);
		ps1.setDouble(2, ma);
		ps1.setDouble(3,ta);
		ps1.setDouble(4, ca);
		ps1.setDouble(5, da);
		ps1.setDouble(6, hra);
//		sql3="insert into FinalSal values(?)";
//		db1.ps2=db1.con.prepareStatement(sql3);
//		PreparedStatement ps2=new DbConnect().connect1().prepareStatement(sql3);
//		ps2.setDouble(1, gpay);
		int a = ps1.executeUpdate();
		//ps2.executeUpdate();
	}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}
	
	
	public void insertEmployee()
	{
		try {
			PreparedStatement ps=new DbConnect().connect1().prepareStatement(sql);
			ps.setInt(1, eid);
			ps.setString(2, name);
			ps.setDouble(3, bpay);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}	