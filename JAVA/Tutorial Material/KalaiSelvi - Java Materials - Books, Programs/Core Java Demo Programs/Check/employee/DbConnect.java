package com.employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect
{

public Connection con;
public Statement st;
public PreparedStatement ps,ps1,ps2;
public ResultSet rs;
public Connection connect1()
{
	try
	{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","scott","tiger");
		System.out.println("successfully connected");
	}
	catch(ClassNotFoundException cnfe)
	{
		cnfe.printStackTrace();
	}
	catch(SQLException se)
	{
		se.printStackTrace();
	}
	return con;
}

}
