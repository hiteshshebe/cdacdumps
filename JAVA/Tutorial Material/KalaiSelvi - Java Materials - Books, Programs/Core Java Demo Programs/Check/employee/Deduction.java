package com.employee;
import com.employee.DbConnect;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Deduction extends EmpDetails {
	double pf;
	double tax;
	double insuamt;
	double loanamt;
	double totaldeductions;
	double npay;
	String sql3,sql4;

	Scanner sc = new Scanner(System.in);
	DbConnect db2=new DbConnect();

	public void pFund() {

		System.out.println("Provident Fund: ");
		pf = 0.05 * bpay;
		System.out.println(pf);

	}

	public void tax() {

		System.out.println("Taxable Income: ");
		tax = 0.01 * bpay;
		System.out.println(tax);

	}

	public void lic() {
		System.out.println("Enter insurance amount: ");
		insuamt = sc.nextDouble();
		if (insuamt < (0.05 * bpay)) {
			System.out.println("Valid insurance amount");
		} else {
			System.out
					.println("Invalid insurance amount...pls enter a lesser amount");
		}
		System.out.println(insuamt);
	}

	public void loan() {
		System.out.println("Enter loan amount: ");
		loanamt = sc.nextDouble();
		System.out.println(loanamt);

	}

	public void deductions() {
		pFund();
		tax();
		lic();
		loan();
		ded();
		
		totaldeductions = pf + tax + insuamt + loanamt;
		System.out.println("Total Deductions: " + totaldeductions);
		npay = gpay - totaldeductions;
		System.out.println("Net Pay: " + npay);
	}

	public void printDetails() {

		System.out.println(name);
		System.out.println("Basic Pay: " + bpay);
		System.out.println("Dearness Allowance: " + da);
		System.out.println("Medical Allowance: " + ma);
		System.out.println("City Allowance: " + ca);
		System.out.println("Travel Allowance: " + ta);
		System.out.println("Total Allowance: " + totalallowance);
		System.out.println("Gross Pay: " + gpay);
		System.out.println("Providant Fund: " + pf);
		System.out.println("Taxable Income: " + tax);
		System.out.println("Insurance Amount: " + insuamt);
		System.out.println("Loan Amount: " + loanamt);
		System.out.println("Total Deductions: " + totaldeductions);
		System.out.println("Net Pay: " + npay);
	}
	public void ded()
	{
		try
		{
		
		
		db1.connect1();
		
		
		sql3=("insert into Deduction values(?,?,?,?)");
		PreparedStatement ps2=new DbConnect().connect1().prepareStatement(sql3);
		ps2.setInt(1, eid);
		ps2.setDouble(2, tax);
		ps2.setDouble(3, insuamt);
		ps2.setDouble(4, loanamt);
		//ps2.setDouble(5, totaldeductions);
		ps2.executeUpdate();
		/*sql3=("insert into FinalSal values(?)");
		PreparedStatement ps3=new DbConnect().connect1().prepareStatement(sql3);
		ps3.setDouble(1, npay);
		
		ps3.executeUpdate();*/
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	
	}
		
	}
	

