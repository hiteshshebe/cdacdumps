1.  
void start() {  
    A a = new A(); 
    B b = new B(); 
    a.s(b);  
    b = null; /* Line 5 */
    a = null;  /* Line 6 */
    System.out.println("start completed"); /* Line 7 */
} 

When is the B object, created in line 3, eligible for garbage collection?
 
A. after line 5 
B. after line 6 
C. after line 7 
D. There is no way to be absolutely certain. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct. I think there are too many unknowns about the method s and the classes A and B to be able to answer this question with any certainty.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
2.  
class HappyGarbage01 
{ 
    public static void main(String args[]) 
    {
        HappyGarbage01 h = new HappyGarbage01(); 
        h.methodA(); /* Line 6 */
    } 
    Object methodA() 
    {
        Object obj1 = new Object(); 
        Object [] obj2 = new Object[1]; 
        obj2[0] = obj1; 
        obj1 = null; 
        return obj2[0]; 
    } 
}

Where will be the most chance of the garbage collector being invoked?
 
A. After line 9 
B. After line 10 
C. After line 11 
D. Garbage collector never invoked in methodA() 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct. Garbage collection takes place after the method has returned its reference to the object. The method returns to line 6, there is no reference to store the return value. so garbage collection takes place after line 6. 

Option A is wrong. Because the reference to obj1 is stored in obj2[0]. The Object obj1 still exists on the heap and can be accessed by an active thread through the reference stored in obj2[0]. 

Option B is wrong. Because it is only one of the references to the object obj1, the other reference is maintained in obj2[0]. 

Option C is wrong. The garbage collector will not be called here because a reference to the object is being maintained and returned in obj2[0]. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
3.  
class Bar { } 
class Test 
{  
    Bar doBar() 
    {
        Bar b = new Bar(); /* Line 6 */
        return b; /* Line 7 */
    } 
    public static void main (String args[]) 
    { 
        Test t = new Test();  /* Line 11 */
        Bar newBar = t.doBar();  /* Line 12 */
        System.out.println("newBar"); 
        newBar = new Bar(); /* Line 14 */
        System.out.println("finishing"); /* Line 15 */
    } 
}

At what point is the Bar object, created on line 6, eligible for garbage collection?
 
A. after line 12 
B. after line 14 
C. after line 7, when doBar() completes 
D. after line 15, when main() completes 
 
Answer & ExplanationAnswer: Option B

Explanation:


Option B is correct. All references to the Bar object created on line 6 are destroyed when a new reference to a new Bar object is assigned to the variable newBar on line 14. Therefore the Bar object, created on line 6, is eligible for garbage collection after line 14. 

Option A is wrong. This actually protects the object from garbage collection.

Option C is wrong. Because the reference in the doBar() method is returned on line 7 and is stored in newBar on line 12. This preserver the object created on line 6. 

Option D is wrong. Not applicable because the object is eligible for garbage collection after line 14. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
4.  
class Test 
{  
    private Demo d; 
    void start() 
    {  
        d = new Demo(); 
        this.takeDemo(d); /* Line 7 */
    } /* Line 8 */
    void takeDemo(Demo demo) 
    { 
        demo = null;  
        demo = new Demo(); 
    } 
}

When is the Demo object, created on line 4, eligible for garbage collection?
 
A. After line 7 
B. After line 8 
C. After the start() method completes 
D. When the instance running this code is made eligible for garbage collection. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct. By a process of elimination.

Option A is wrong. The variable d is a member of the Test class and is never directly set to null. 

Option B is wrong. A copy of the variable d is set to null and not the actual variable d. 

Option C is wrong. The variable d exists outside the start() method (it is a class member). So, when the start() method finishes the variable d still holds a reference.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
5.  
public class X 
{
    public static void main(String [] args) 
    {
        X x = new X();
        X x2 = m1(x); /* Line 6 */
        X x4 = new X();
        x2 = x4; /* Line 8 */
        doComplexStuff();
    }
    static X m1(X mx) 
    {
        mx = new X();
        return mx;
    }
}

After line 8 runs. how many objects are eligible for garbage collection?
 
A. 0 B. 1 
C. 2 D. 3 
 
Answer & ExplanationAnswer: Option B

Explanation:


By the time line 8 has run, the only object without a reference is the one generated as a result of line 6. Remember that "Java is pass by value," so the reference variable x is not affected by the m1() method.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
 
6.  
public Object m() 
{  
    Object o = new Float(3.14F); 
    Object [] oa = new Object[l];
    oa[0] = o; /* Line 5 */
    o = null;  /* Line 6 */
    oa[0] = null; /* Line 7 */
    return o; /* Line 8 */
}

When is the Float object, created in line 3, eligible for garbage collection?
 
A. just after line 5 B. just after line 6 
C. just after line 7 D. just after line 8 
 
Answer & ExplanationAnswer: Option C

Explanation:


Option A is wrong. This simply copies the object reference into the array.

Option B is wrong. The reference o is set to null, but, oa[0] still maintains the reference to the Float object.

Option C is correct. The thread of execution will then not have access to the object.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
7.  
class X2 
{
    public X2 x;
    public static void main(String [] args) 
    {
        X2 x2 = new X2();  /* Line 6 */
        X2 x3 = new X2();  /* Line 7 */
        x2.x = x3;
        x3.x = x2;
        x2 = new X2();
        x3 = x2; /* Line 11 */
        doComplexStuff();
    }
}

after line 11 runs, how many objects are eligible for garbage collection?
 
A. 0 B. 1 
C. 2 D. 3 
 
Answer & ExplanationAnswer: Option C

Explanation:


This is an example of the islands of isolated objects. By the time line 11 has run, the objects instantiated in lines 6 and 7 are referring to each other, but no live thread can reach either of them.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
8.  What allows the programmer to destroy an object x?
 
A. x.delete() 
B. x.finalize() 
C. Runtime.getRuntime().gc() 
D. Only the garbage collection system can destroy an object. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct. When an object is no longer referenced, it may be reclaimed by the garbage collector. If an object declares a finalizer, the finalizer is executed before the object is reclaimed to give the object a last chance to clean up resources that would not otherwise be released. When a class is no longer needed, it may be unloaded. 

Option A is wrong. I found 4 delete() methods in all of the Java class structure. They are:


delete() - Method in class java.io.File : Deletes the file or directory denoted by this abstract pathname. 
delete(int, int) - Method in class java.lang.StringBuffer : Removes the characters in a substring of this StringBuffer. 
delete(int, int) - Method in interface javax.accessibility.AccessibleEditableText : Deletes the text between two indices 
delete(int, int) - Method in class : javax.swing.text.JTextComponent.AccessibleJTextComponent; Deletes the text between two indices 

None of these destroy the object to which they belong. 

Option B is wrong. I found 19 finalize() methods. The most interesting, from this questions point of view, was the finalize() method in class java.lang.Object which is called by the garbage collector on an object when garbage collection determines that there are no more references to the object. This method does not destroy the object to which it belongs. 

Option C is wrong. But it is interesting. The Runtime class has many methods, two of which are: 


getRuntime() - Returns the runtime object associated with the current Java application. 
gc() - Runs the garbage collector. Calling this method suggests that the Java virtual machine expend effort toward recycling unused objects in order to make the memory they currently occupy available for quick reuse. When control returns from the method call, the virtual machine has made its best effort to recycle all discarded objects. Interesting as this is, it doesn't destroy the object. 

Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  




Which statement is true?
 
A. Programs will not run out of memory. 
B. Objects that will never again be used are eligible for garbage collection. 
C. Objects that are referred to by other objects will never be garbage collected. 
D. Objects that can be reached from a live thread will never be garbage collected. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct.

Option C is wrong. See the note above on Islands of Isolation (An object is eligible for garbage collection when no live thread can access it - even though there might be references to it).

Option B is wrong. "Never again be used" does not mean that there are no more references to the object. 

Option A is wrong. Even though Java applications can run out of memory there another answer supplied that is more right.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
2.  Which statement is true?
 
A. All objects that are eligible for garbage collection will be garbage collected by the garbage collector. 
B. Objects with at least one reference will never be garbage collected. 
C. Objects from a class with the finalize() method overridden will never be garbage collected. 
D. Objects instantiated within anonymous inner classes are placed in the garbage collectible heap. 
 
Answer & ExplanationAnswer: Option D

Explanation:


All objects are placed in the garbage collectible heap.

Option A is incorrect because the garbage collector makes no guarantees.

Option B is incorrect because islands of isolated objects can exist.

Option C is incorrect because finalize() has no such mystical powers.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
3.  Which statement is true?
 
A. Memory is reclaimed by calling Runtime.gc(). 
B. Objects are not collected if they are accessible from live threads. 
C. An OutOfMemory error is only thrown if a single block of memory cannot be found that is large enough for a particular requirement. 
D. Objects that have finalize() methods always have their finalize() methods called before the program ends. 
 
Answer & ExplanationAnswer: Option B

Explanation:


Option B is correct. If an object can be accessed from a live thread, it can't be garbage collected.

Option A is wrong. Runtime.gc() asks the garbage collector to run, but the garbage collector never makes any guarantees about when it will run or what unreachable objects it will free from memory. 

Option C is wrong. The garbage collector runs immediately the system is out of memory before an OutOfMemoryException is thrown by the JVM.

Option D is wrong. If this were the case then the garbage collector would actively hang onto objects until a program finishes - this goes against the purpose of the garbage collector.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
4.  Which statement is true?
 
A. Calling Runtime.gc() will cause eligible objects to be garbage collected. 
B. The garbage collector uses a mark and sweep algorithm. 
C. If an object can be accessed from a live thread, it can't be garbage collected. 
D. If object 1 refers to object 2, then object 2 can't be garbage collected. 
 
Answer & ExplanationAnswer: Option C

Explanation:


This is a great way to think about when objects can be garbage collected.

Option A and B assume guarantees that the garbage collector never makes.

Option D is wrong because of the now famous islands of isolation scenario.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
