 What will be the output of the program? 

class s1 extends Thread
{ 
    public void run() 
    { 
        for(int i = 0; i < 3; i++) 
        { 
            System.out.println("A"); 
            System.out.println("B"); 
        } 
    } 
} 
class Test120 extends Thread 
{ 
    public void run() 
    { 
        for(int i = 0; i < 3; i++) 
        { 
            System.out.println("C"); 
            System.out.println("D"); 
        } 
    } 
    public static void main(String args[]) 
        { 
        s1 t1 = new s1(); 
        Test120 t2 = new Test120(); 
        t1.start(); 
        t2.start(); 
    } 
}


 
A. Compile time Error There is no start() method 
B. Will print in this order AB CD AB... 
C. Will print but not be able to predict the Order 
D. Will print in this order ABCD...ABCD... 
 
Answer & ExplanationAnswer: Option C

Explanation:


We cannot predict the order in which threads are going to run.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
7.  What will be the output of the program? 

class s implements Runnable 
{ 
    int x, y; 
    public void run() 
    { 
        for(int i = 0; i < 1000; i++) 
            synchronized(this) 
            { 
                x = 12; 
                y = 12; 
            } 
        System.out.println(x + " " + y); 
    } 
    public static void main(String args[]) 
    { 
        s run = new s(); 
        Thread t1 = new Thread(run); 
        Thread t2 = new Thread(run); 
        t1.start(); 
        t2.start(); 
    } 
}


 
A. DeadLock B. It print 12 12 12 12 
C. Compilation Error D. Cannot determine output. 
 
Answer & ExplanationAnswer: Option B

Explanation:


The program will execute without any problems and print 12 12 12 12.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
8.  What will be the output of the program? 

public class ThreadDemo 
{ 
    private int count = 1; 
    public synchronized void doSomething() 
    { 
        for (int i = 0; i < 10; i++) 
            System.out.println(count++); 
    } 
    public static void main(String[] args) 
    { 
        ThreadDemo demo = new ThreadDemo(); 
        Thread a1 = new A(demo); 
        Thread a2 = new A(demo); 
        a1.start(); 
        a2.start(); 
    } 
} 
class A extends Thread 
{ 
    ThreadDemo demo; 
    public A(ThreadDemo td) 
    { 
        demo = td; 
    } 
    public void run() 
    { 
        demo.doSomething(); 
    } 
}


 
A. It will print the numbers 0 to 19 sequentially 
B. It will print the numbers 1 to 20 sequentially 
C. It will print the numbers 1 to 20, but the order cannot be determined 
D. The code will not compile. 
 
Answer & ExplanationAnswer: Option B

Explanation:


You have two different threads that share one reference to a common object. 

The updating and output takes place inside synchronized code. 

One thread will run to completion printing the numbers 1-10. 

The second thread will then run to completion printing the numbers 11-20. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
9.  What will be the output of the program? 

public class WaitTest 
{
    public static void main(String [] args) 
    {
        System.out.print("1 ");
        synchronized(args)
        {
            System.out.print("2 ");
            try 
            {
                    args.wait(); /* Line 11 */
            }
            catch(InterruptedException e){ }
        }
        System.out.print("3 ");
    }
}


 
A. It fails to compile because the IllegalMonitorStateException of wait() is not dealt with in line 11. 
B. 1 2 3 
C. 1 3 
D. 1 2 
 
Answer & ExplanationAnswer: Option D

Explanation:


1 and 2 will be printed, but there will be no return from the wait call because no other thread will notify the main thread, so 3 will never be printed. The program is essentially frozen at line 11.

A is incorrect; IllegalMonitorStateException is an unchecked exception so it doesn't have to be dealt with explicitly.

B and C are incorrect; 3 will never be printed, since this program will never terminate because it will wait forever.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
10.  What will be the output of the program? 

public class SyncTest 
{
    public static void main (String [] args) 
    {
        Thread t = new Thread() 
        {
            Foo f = new Foo();
            public void run() 
            {
                f.increase(20);
            }
        };
    t.start();
    }
}
class Foo 
{
    private int data = 23;
    public void increase(int amt) 
    {
        int x = data;
        data = x + amt;
    }
}

and assuming that data must be protected from corruption, what—if anything—can you add to the preceding code to ensure the integrity of data?
 
A. Synchronize the run method. 
B. Wrap a synchronize(this) around the call to f.increase(). 
C. The existing code will cause a runtime exception. 
D. Synchronize the increase() method 
 
Answer & ExplanationAnswer: Option D

Explanation:


Option D is correct because synchronizing the code that actually does the increase will protect the code from being accessed by more than one thread at a time.

Option A is incorrect because synchronizing the run() method would stop other threads from running the run() method (a bad idea) but still would not prevent other threads with other runnables from accessing the increase() method. 

Option B is incorrect for virtually the same reason as A—synchronizing the code that calls the increase() method does not prevent other code from calling the increase() method. 
 
