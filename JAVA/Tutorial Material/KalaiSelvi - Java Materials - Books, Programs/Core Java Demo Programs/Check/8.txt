.  What will be the output of the program? 

class Happy extends Thread 
{ 
    final StringBuffer sb1 = new StringBuffer(); 
    final StringBuffer sb2 = new StringBuffer(); 

    public static void main(String args[]) 
    { 
        final Happy h = new Happy(); 

        new Thread() 
        { 
            public void run() 
            { 
                synchronized(this) 
                { 
                    h.sb1.append("A"); 
                    h.sb2.append("B"); 
                    System.out.println(h.sb1); 
                    System.out.println(h.sb2); 
                } 
            } 
        }.start(); 

        new Thread() 
        { 
            public void run() 
            { 
                synchronized(this) 
                { 
                    h.sb1.append("D"); 
                    h.sb2.append("C"); 
                    System.out.println(h.sb2); 
                    System.out.println(h.sb1); 
                } 
            } 
        }.start(); 
    } 
}


 
A. ABBCAD 
B. ABCBCAD 
C. CDADACB 
D. Output determined by the underlying platform. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Can you guarantee the order in which threads are going to run? No you can't. So how do you know what the output will be? The output cannot be determined. 


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
12.  
class Test 
{
    public static void main(String [] args) 
    {
        printAll(args);
    }

    public static void printAll(String[] lines) 
    {
        for(int i = 0; i < lines.length; i++)
        {
            System.out.println(lines[i]);
            Thread.currentThread().sleep(1000);
        }
    }
}

the static method Thread.currentThread() returns a reference to the currently executing Thread object. What is the result of this code?
 
A. Each String in the array lines will output, with a 1-second pause. 
B. Each String in the array lines will output, with no pause in between because this method is not executed in a Thread. 
C. Each String in the array lines will output, and there is no guarantee there will be a pause because currentThread() may not retrieve this thread. 
D. This code will not compile. 
 
Answer & ExplanationAnswer: Option D

Explanation:


D. The sleep() method must be enclosed in a try/catch block, or the method printAll() must declare it throws the InterruptedException.

A is incorrect, but it would be correct if the InterruptedException was dealt with. 

B is incorrect, but it would still be incorrect if the InterruptedException was dealt with because all Java code, including the main() method, runs in threads.

C is incorrect. The sleep() method is static, so even if it is called on an instance, it still always affects the currently executing thread.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
13.  What will be the output of the program? 

class MyThread extends Thread 
{
    public static void main(String [] args) 
    {
        MyThread t = new MyThread(); /* Line 5 */
        t.run();  /* Line 6 */
    }

    public void run() 
    {
        for(int i=1; i < 3; ++i) 
        {
            System.out.print(i + "..");
        }
    }
}


 
A. This code will not compile due to line 5. 
B. This code will not compile due to line 6. 
C. 1..2.. 
D. 1..2..3.. 
 
Answer & ExplanationAnswer: Option C

Explanation:


Line 6 calls the run() method, so the run() method executes as a normal method should and it prints "1..2.."

A is incorrect because line 5 is the proper way to create an object. 

B is incorrect because it is legal to call the run() method, even though this will not start a true thread of execution. The code after line 6 will not execute until the run() method is complete.

D is incorrect because the for loop only does two iterations.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
14.  What will be the output of the program? 

class Test116 
{ 
static final StringBuffer sb1 = new StringBuffer(); 
static final StringBuffer sb2 = new StringBuffer(); 
public static void main(String args[]) 
{ 
    new Thread() 
    { 
        public void run() 
        { 
            synchronized(sb1) 
            { 
                sb1.append("A"); 
                sb2.append("B"); 
            } 
        } 
    }.start(); 

    new Thread() 
    { 
        public void run() 
        { 
            synchronized(sb1) 
            { 
                sb1.append("C"); 
                sb2.append("D"); 
            } 
        } 
    }.start(); /* Line 28 */

    System.out.println (sb1 + " " + sb2); 
    } 
}


 
A. main() will finish before starting threads. 
B. main() will finish in the middle of one thread. 
C. main() will finish after one thread. 
D. Cannot be determined. 
 
Answer & ExplanationAnswer: Option D

Explanation:


Can you guarantee the order in which threads are going to run? No you can't. So how do you know what the output will be? The output cannot be determined. 

add this code after line 28:

try { Thread.sleep(5000); } catch(InterruptedException e) { } 

and you have some chance of predicting the outcome.


Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  

--------------------------------------------------------------------------------
15.  What will be the output of the program? 

public class ThreadTest extends Thread 
{ 
    public void run() 
    { 
        System.out.println("In run"); 
        yield(); 
        System.out.println("Leaving run"); 
    } 
    public static void main(String []argv) 
    { 
        (new ThreadTest()).start(); 
    } 
}


 
A. The code fails to compile in the main() method 
B. The code fails to compile in the run() method 
C. Only the text "In run" will be displayed 
D. The text "In run" followed by "Leaving run" will be displayed 
 
Answer & ExplanationAnswer: Option D

Explanation:

No answer description available for this question. Let us discuss. 

Workspace 
 
Report ErrorsKindly mention the details of the error here...




[Your Name]
[Your Email]  
View Answer Workspace Report Discuss in Forum  
