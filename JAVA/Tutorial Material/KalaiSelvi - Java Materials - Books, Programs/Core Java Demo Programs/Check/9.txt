6.  Which statement is true?
 
A. The notifyAll() method must be called from a synchronized context. 
B. To call wait(), an object must own the lock on the thread. 
C. The notify() method is defined in class java.lang.Thread. 
D. The notify() method causes a thread to immediately release its locks. 
 
Answer & ExplanationAnswer: Option A

Explanation:


Option A is correct because the notifyAll() method (along with wait() and notify()) must always be called from within a synchronized context.

Option B is incorrect because to call wait(), the thread must own the lock on the object that wait() is being invoked on, not the other way around.

Option C is wrong because notify() is defined in java.lang.Object.

Option D is wrong because notify() will not cause a thread to release its locks. The thread can only release its locks by exiting the synchronized code.


 
