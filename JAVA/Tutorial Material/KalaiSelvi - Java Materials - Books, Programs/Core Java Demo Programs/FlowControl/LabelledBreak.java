public class LabelledBreak {
    
    /** Creates a new instance of Sample */
    public Sample() {
    }
    
    public void demo(){
        int age = 13;
        outer:
        while(age<=21){          
            age++;
            if(age==16){
                System.out.append("print age : licence");
                break outer;
            }else{
            System.out.println("Another Year");
            }
            
        }
     }
    
    public static void main(String[] args){
        LabelledBreak s = new LabelledBreak();
        s.demo();
        
    }
    
}
