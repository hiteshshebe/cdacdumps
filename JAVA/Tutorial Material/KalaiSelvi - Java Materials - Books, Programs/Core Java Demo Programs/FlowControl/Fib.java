class Fib
{
 public static void main(String args[])
 {
 int f1=0,f=0,f2=1,a,i=1;
 a=Integer.parseInt(args[0]);
 while(i<=a)
 {
  if(i==1)
   System.out.print(f1+" ");
  else if(i==2)
   System.out.print(f2+" ");
  else
  {
   f=f1+f2;
   System.out.print(f+" ");
   f1=f2;
   f2=f;
  }
  i++;
 }
 System.out.println();
 }
 }
