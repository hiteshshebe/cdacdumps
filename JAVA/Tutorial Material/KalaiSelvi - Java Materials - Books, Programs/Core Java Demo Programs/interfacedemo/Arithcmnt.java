interface Calculator1
{
   double pi = 3.14;
   double add(double num1,double num2);
   double subtract(double num1,double num2);
}

interface Calculator2
{
   double power(double num1,double num2);
   double mul(double num1,double num2);
   double div(double num1,double num2);
   double mysqrt(double number);
}

class Mathprocessor implements Calculator1,Calculator2
{
   public double add(double n1,double n2)
     {  double a=n1+n2;
	System.out.println("Add:"+a);
	return (a);
     }

   public double subtract(double n1,double n2)
     {
       double a=n1-n2;
	System.out.println("Sub:"+a);
	return (a);
     }
   public double mul(double n1,double n2)
     {
       double a =(n1*n2);
	System.out.println("Mul:"+a);
	return (a);
     }
   public double div(double n1,double n2)
     {
       double a=n1/n2;
	System.out.println("Div:"+a);
	return (a);
     }


   public double power(double n1,double n2)
     {
       double n3 = 1;
       for (int i=1; i<=n2; i++)
	   n3 *= n1;
	System.out.println("Pow:"+n3);
	   return n3;
     }
    
   public double mysqrt(double n)
     {
	double a=Math.sqrt(n);
	System.out.println("Sqrt:"+a);
	return a;
     }
}


class Arithcmnt
{
   public static void main(String args[])
    {
       Mathprocessor m=new Mathprocessor();
       double a = Double.parseDouble(args[0]);
       if (args.length==1)
         m.mysqrt(a);
       else
	 {
	 String str = new String(args[1]);
         double b = Double.parseDouble(args[2]);
	 switch(str.charAt(0))
	 {
	   case '+': m.add(a,b); break;
	   case '-': m.subtract(a,b); break;
	   case '*': m.mul(a,b); break;
	   case '/': m.div(a,b); break;
	   case '^': m.power(a,b); break;
	   
       }
     }
}
}


