interface Calculator1
{
double pi=3.14;
double sum(double n1,double n2);
double subract(double n1,double n2);
}

interface Calculator2
{
double power(int num1,int num2);
double sqrt(double num);
}

interface Calculator3 extends Calculator1,Calculator2 
{
double multiply(double num1,double num2);
double divide(double num1,double num2);
}

class MathProcessor implements Calculator3
{
public double sum(double n1,double n2)
{
 return (n1+n2);
}

public double multiply(double n1,double n2)
{
 return (n1*n2);
}

public double divide(double n1,double n2)
{
 return (n1/n2);
}

public double subract(double n1,double n2)
{
 return (n1-n2);
}

public double power(int n1,int n2)
{
double n3=1;
  for(int i=1;i<=n2;i++)
   n3*=n1;
   return n3;
}

public double sqrt(double n)
{
return Math.sqrt(n);
}
}

class Int 
{
public static void main(String args[])
  {
   double i,j,a=0;
   MathProcessor m= new MathProcessor();
   switch(args[1].charAt(0))
   {
    case '+':
   i=Double.parseDouble(args[0]);
   j=Double.parseDouble(args[2]);
	  a=m.sum(i,j);
	  break; 
    case '-':
   i=Double.parseDouble(args[0]);
   j=Double.parseDouble(args[2]);
	  a=m.subract(i,j);
	  break; 
    case '^':
   i=Double.parseDouble(args[0]);
   j=Double.parseDouble(args[2]);
	  a=(double)m.power((int)i,(int)j);
	  break; 
    case '*':
   i=Double.parseDouble(args[0]);
   j=Double.parseDouble(args[2]);
	  a=m.multiply(i,j);
	  break; 
    case '/':
   i=Double.parseDouble(args[0]);
   j=Double.parseDouble(args[2]);
	  a=m.divide(i,j);
	  break; 
    case 's':
   i=Double.parseDouble(args[0]);
	  a=m.sqrt(i);
	  break; 
   }
   System.out.println("Answer :"+a);
 }
}
 
