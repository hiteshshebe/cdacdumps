/**
It is a abstract class
@version 1.1.3
@author James Gosling
*/

abstract class Shape
{
double s1,s2;

 Shape(double s1,double s2)
 {
  this.s1=s1;
  this.s2=s2;
 }

 void show()
 {
  System.out.println("Inside Shape class ...");
 }
abstract void area();
abstract void perimeter();
}

/**
It is a Derived class
@version 1.1.3
@author James Gosling
*/

class Rectangle extends Shape
{
 Rectangle(double s1,double s2)
 { super(s1,s2); 
 }

 void area()
 {
  System.out.println("Area of the Rectangle :"+(s1*s2));
 }

 void rect()
 {
 System.out.println("rect");
 }

 void perimeter()
 {
  System.out.println("Perimeter of the Rectangle :"+(2*(s1+s2)));
 }
}

class RtTriangle extends Shape
{
 RtTriangle (double s1,double s2)
 { super(s1,s2); 
 }

 void area()
 {
  System.out.println("Area of the RtTriangle :"+(s1+s2+Math.sqrt((s1+s2))));
 }

 void perimeter()
 {
  System.out.println("Perimeter of the RtTriangle :"+(0.5*s1*s2));
 }
}

class Circle extends Shape
{
 Circle (double s1,double s2)
 { super(s1,s2); 
 }

 void area()
 {
  System.out.println("Area of the Circle :"+(s1*s2*s2));
 }

 void perimeter()
 {
  System.out.println("Perimeter of the Circle :"+(2*s1*s2));
 }
}

public class InheDemo
{
 public static void main(String args[])
  {
   Shape s;
   double pi=3.14;
   Rectangle r= new Rectangle(5.5,12.5);
   r.rect();
   s=r;
   s.area();
   s.perimeter();
   Shape s1 = new RtTriangle(3.4,6.7);
   s1.area();
   s1.perimeter();
   s1 = new Circle(pi,6.7);
   s1.area();
   s1.perimeter();
  }
}
