9+/*
 * Created on Dec 25, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author c3user15
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

 class Animal {
	public void sound(){
		System.out.println("Animal");
	}
}
 
 
 class Dog extends Animal {
	public void sound(){
		System.out.println("Dog");
	}
}
 
 class Cat extends Animal {
 
	public void sound(){
		System.out.println("Cat");
	}
}


public class Inheritance {

	public static void main(String[] args) {
		try{
		Animal animal = new Animal();
		animal.sound(); //Animal
		 
		Dog dog = new Dog();
		dog.sound(); //Dog
		 
		Cat cat = new Cat();
		cat.sound(); //Cat
		animal = new Dog();
		animal.sound(); // "Dog"
		Cat c =  new Cat();
		Animal a = c; // no upcast necessary, since every Cat IS-AN Animal.
		Dog d = (Dog)a; // ClassCastException at runtime
		//No. Casting does not change an object's type.
		//it will compile, because the compiler has no way of knowing that a will not refer to a Dog object at runtime, but when you run it, you'll get a ClassCastException.

        Dog b=(Dog)new Animal();
		b.sound();

		}catch(Exception e){}

		 

	}
}
