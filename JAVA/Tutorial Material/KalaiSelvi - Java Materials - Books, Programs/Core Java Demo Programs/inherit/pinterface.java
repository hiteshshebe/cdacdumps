/*
 * Created on Mar 27, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author kalaiselvi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
interface i1
{
    public static final int i=100; // data members
    void add(); // abstract methods
    abstract public void sub();
    }

interface i2
{
    public static final int i=100;  // data members
    void div();  //abstract method
    }

public class pinterface implements i1 ,i2{

    public void add(){System.out.println("I am add method"); //interface i1's method
    System.out.println("Default value of i = "+i1.i);} // to remove ambiguity differentiate by the name of interface
    public void sub(){System.out.println("I am sub method");}   //interface i1's method
    public void mul() {System.out.println("I am mul method");}   //class pinterface's method
    public void div() {System.out.println("I am div method");}   //interface i2's method
    public static void main(String[] args) {
        
       pinterface obj=new pinterface();
     //   i1 obj=new pinterface();  // calling be reference of interface
        obj.add();  // calling interface i1's method
        obj.sub();  //calling interface i1's method
        obj.mul();  //calling class pinterface's method
        obj.div();  //calling interface i2's method
    }
}
