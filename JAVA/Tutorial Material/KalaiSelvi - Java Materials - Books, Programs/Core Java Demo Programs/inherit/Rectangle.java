import superpack.*;

class Rectangle extends Shape1
{
 Rectangle(double s1,double s2)
 { super(s1,s2); 
 }

 public void area()
 {
  System.out.println("Area of the Rectangle :"+(s1*s2));
 }

 void rect()
 {
 System.out.println("rect");
 }

 public void perimeter()
 {
  System.out.println("Perimeter of the Rectangle :"+(2*(s1+s2)));
 }
}

class RtTriangle extends Shape1
{
 RtTriangle (double s1,double s2)
 { super(s1,s2); 
 }

 public void area()
 {
  System.out.println("Area of the RtTriangle :"+(s1+s2+Math.sqrt((s1+s2))));
 }

 public void perimeter()
 {
  System.out.println("Perimeter of the RtTriangle :"+(0.5*s1*s2));
 }
}

class Circle extends Shape1
{
 Circle (double s1,double s2)
 { super(s1,s2); 
 }

 public void area()
 {
  System.out.println("Area of the Circle :"+(s1*s2*s2));
 }

 public void perimeter()
 {
  System.out.println("Perimeter of the Circle :"+(2*s1*s2));
 }
}

class InheDemo
{
 public static void main(String args[])
  {
   Shape1 s;
   double pi=3.14;
   Rectangle r= new Rectangle(5.5,12.5);
   r.rect();
   s=r;
   s.area();
   s.perimeter();
  /* Shape1 s1 = new RtTriangle(3.4,6.7);
   s1.area();
   s1.perimeter();
   s1 = new Circle(pi,6.7);
   s1.area();
   s1.perimeter();*/
  }
}
