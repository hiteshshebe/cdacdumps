public class MathClassDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("The ceiling of 45.7 is " +
			      Math.ceil(45.7));
		 System.out.println("The floor of 45.7 is " +
			      Math.floor(45.7));
		 System.out.println("The round of 6.4 is= "+
				 Math.round(6.4));
		 System.out.println("The round of -4.5 is="+
				 Math.round(-4.5));
		 System.out.println("The round of -4.6 is="+
				 Math.round(-4.6));
		 
		 System.out.println("Max of -8 and -4 is " +
			      Math.max(-8, -4));
		 System.out.println("Min of -8 and -4 is " + 
			      Math.min(-8, -4));
		

	}

}
