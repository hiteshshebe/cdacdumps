class StrBufMethods
{
  public static void main(String args[])
   {
     StringBuffer sb = new StringBuffer();
     StringBuffer sb1 = new StringBuffer("Java World");
     StringBuffer sb2 = new StringBuffer("hello");
     System.out.println(sb1);
     System.out.println(sb1.length());
     System.out.println(sb1.capacity());
     System.out.println(sb2);
     System.out.println(sb2.length());
     System.out.println(sb2.capacity());
    // System.out.println(sb1.setCharAt(4,'_'));
     System.out.println(sb2.append(sb1));
     System.out.println(sb1.reverse());
     System.out.println(sb1.insert(4,"hai"));
     System.out.println(sb1.insert(4,'x'));
     System.out.println(sb1.delete(5,8));
     System.out.println(sb1.deleteCharAt(4));
     System.out.println(sb1.reverse());
     System.out.println(sb1.replace(1,4,"oly"));
      
   }
}
