class Alpha
{
 StringBuffer str;

 Alpha() { }

 Alpha(StringBuffer str)
 {
  this.str=str;
 }

 void arrange()
 {
 char t;
  for(int i=0;i<str.length();i++)
  {
    for(int j=i+1;j<str.length();j++)
    {
     if(str.charAt(i)>str.charAt(j))
       {
       t=str.charAt(i);
       str.deleteCharAt(i);
       str.insert(i,str.charAt(j-1));
       str.deleteCharAt(j);
       str.insert(j,t);
       }
    }
  }
 }

 public static void main(String args[])
 {
  StringBuffer sb=new StringBuffer(args[0]);
  Alpha a = new Alpha(sb);
  System.out.println("Input : "+a.str);
  a.arrange();
  System.out.println("Output : "+a.str);
 }
}
