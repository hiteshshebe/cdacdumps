 
import java.util.*;

public class StringReverse {

    public static void main(String[] args) {

        String input = "beginner java tutorial";
        Stack  stack = new Stack();	//A Stack is a Last In First Out (LIFO) Data Structure
        StringTokenizer stringTokenizer = new StringTokenizer(input);

        while (stringTokenizer.hasMoreTokens()) {
            stack.push(stringTokenizer.nextElement());
        }

        System.out.println("Original String: " + input);
        System.out.print("Reversed String by Words: ");
        while(!stack.empty()) {
            System.out.print(stack.pop());
            System.out.print(" ");
        }
        System.out.println();
        System.out.print("Reversed String by characters: ");
        StringBuffer rev = new StringBuffer(input).reverse();
        System.out.print(rev);

    }

}

