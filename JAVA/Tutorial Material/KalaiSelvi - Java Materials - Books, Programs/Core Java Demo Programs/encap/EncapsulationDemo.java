package com.vam.encap;

public class EncapsulationDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Customer cust = new Customer();
		cust.getAge();
		cust.setAge(100);
		

	}

}

class Customer{
	private byte age;
	private String name;
	
	public byte getAge() {
		return age;
	}
	public void setAge(byte age) {
		this.age = age;
	}
	 public void setAge(int age){
		 if (age <= 30){
			 this.age += age;
		 }
	 }
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}