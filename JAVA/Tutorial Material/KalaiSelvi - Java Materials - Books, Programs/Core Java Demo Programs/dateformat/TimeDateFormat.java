package dateformat;

import java.util.Calendar;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TimeDateFormat {

	/**
	 * from complete refference  page 519 on words
	 * this program is on page 525
	 */
	public static void main(String[] args) {
		Formatter fmt=new Formatter();
		Calendar cal=Calendar.getInstance();
		
		//Display standard time format
		fmt.format("%tr",cal);
		System.out.println(fmt);
		
		//display complete time date information
		fmt=new Formatter();
		fmt.format("%tc",cal);
		System.out.println(fmt);
		
		//Display just hour and minute
		fmt=new Formatter();
		fmt.format("%tl:%tM",cal,cal);
		System.out.println(fmt);
		
		//display month name and number
		fmt=new Formatter();
		fmt.format("%tB %tb %tm",cal, cal, cal);
		System.out.println(fmt);

		fmt.flush();
		fmt.format("%tm %te %tY", cal,cal,cal);
		System.out.println("US date format "+fmt);
		
		Calendar c = new GregorianCalendar(1995, Calendar.MAY, 23);
		String s = String.format("Duke's Birthday: %1$tm %1$te,%1$tY", c);
		 System.out.println("the date is "+s);
		String us=String.format(Locale.US, s,c);
		   System.out.println("the date is "+us);
		
		
	}

}
