class OverloadMethods
{
    void test()
    {
        System.out.println("NO PARAMETRES");
    }
    void test(int a)
    {
        System.out.println("Value of a is "+a);
    }
//    int test(int a) - Error

    void test(int a,int b)
    {
        System.out.println("Value of A+B is "+(a+b));
    }
    void test(double d)
    {
        System.out.println("Value of D is "+d);
    }
}
public class MethodOverload
{
    public static void main(String args[])
    {
        OverloadMethods om1=new OverloadMethods();
        om1.test();
        om1.test(3,4);
        om1.test(8);
        
        om1.test(123.45);
    }
} 

    