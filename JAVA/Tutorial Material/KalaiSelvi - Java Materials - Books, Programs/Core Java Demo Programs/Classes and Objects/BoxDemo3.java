class Boxnew1
{
	double width;
	double height;
	double depth;
	
	double volume()
	{
		return depth * height * width; 
	}
	void setDim(double w, double h, double d)
	{
		width = w;
		height = h;
		depth = d;
	}
}
public class BoxDemo3 {

	public static void main(String[] args) 
	{
		Boxnew1 mybox1 = new Boxnew1();
		Boxnew1 mybox2 = new Boxnew1();
		double vol;
		
		mybox1.setDim(10, 20, 30);
		vol = mybox1.volume();
		System.out.println("Volume " + vol);
		
		mybox2.setDim(20, 30, 40);
		vol = mybox2.volume();
		System.out.println("Volume " + vol);
		
		

	}

}
