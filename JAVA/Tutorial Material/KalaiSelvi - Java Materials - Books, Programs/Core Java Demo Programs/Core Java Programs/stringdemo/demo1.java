package stringdemo;

public class demo1 {
	public static void main(String[] args)
	{
		String str="raja rani manthri thanthri";
		String strUpper=str.toUpperCase();
		System.out.println(strUpper);
		
		String strLower=str.toLowerCase();
		System.out.println(strLower);
		
		int index=str.indexOf('m');
		System.out.println(index);
		
		int index1=str.lastIndexOf('t');
		System.out.println(index1);
		
		int pos=str.indexOf('r');
		System.out.println(pos+str.indexOf('r',pos+1));
		
		String str1=new String("chennai");
		String str2=new String("chennai");
		System.out.println(str1==str2);//false
		System.out.println(str1.equals(str2));//true
		
		String str3="blore";
		String str4="blore";
		
		System.out.println(str3==str4);//true
		System.out.println(str3.equals(str4));//true
		
		String c1="vani";
		String c2="mani";
		
		System.out.println("v="+(int)'v');//to get ascii value
		System.out.println("m="+(int)'m');
		
		System.out.println(c2.compareTo(c1));//compare
		
		String v="nnnn nnnnnnnn                          ";
		String w=  v.trim();
		System.out.println(v);
		System.out.println(w);
		
		String po="raja poda";
		String va=po.replace("poda", "vada");
		
		System.out.println(va);
		
		String val=po.replace('a','z');
		System.out.println(val);
		
		String catsound="mew";
		String dogsound="bow bow";
		String con=catsound.concat(dogsound);
		System.out.println(con);
		System.out.println(po.substring(2, 7));
		
		char c[]={'h','a','r','i','o','m','h','a','r','i'};
		String s=new String(c);
		
		char c11[]=new char[c.length-2];
		
		s.getChars(4,9,c11,0);
		
		String s1=new String(c11);
		
		System.out.println(s1);
		
		String bv1="Mythri Ho";//checks equal ingnors upperorlowercases 
		String bv2="mythri ho";
		
		System.out.println(bv1.equalsIgnoreCase(bv2));
		
		char ch[]=bv1.toCharArray();
		
		byte[] bb=bv1.getBytes();
		
		String cc=String.valueOf(23.f);
		String cc1=String.valueOf(true);
		String cc2=String.valueOf(12);
		String cc4=String.valueOf(12342333434L);
		System.out.println("****************");
		
		String raara="ha;hoy;jey;hu;he;hum;yim";
		String[] ara= raara.split(";");
		for(String b:ara)
		{
			System.out.println(b);
			
		}
		System.out.println("**************");
		
		String abc="hclTechnologiesChennai";
		System.out.println(abc.startsWith("hcl"));
		System.out.println(abc.endsWith("Chennai"));
		
		String rall="I am a humble man";
		String vall="We are  humble man";
		System.out.println(rall.regionMatches(6, vall, 7, 5));
		
		String abb="";
		System.out.println(abb.isEmpty());
		
		StringBuffer sb=new StringBuffer(abb);
		sb.append("raja");
		
		System.out.println(sb);
		sb.append("tail");
		System.out.println(sb);
		sb.replace(0, 3, "santhosh");
		System.out.println(sb);
		
		System.out.println(sb.reverse());
		
		sb.setCharAt(sb.length()-1, 'p');
		System.out.println(sb);
		
		sb.insert(5,"anu" );
		System.out.println(sb);
		StringBuilder sb1=new StringBuilder("Lunch Break Chayeha");
		
		System.out.println(sb1.capacity());
		sb1.append(" kamya sabudu");
		System.out.println(sb1);
		
		
}
}
