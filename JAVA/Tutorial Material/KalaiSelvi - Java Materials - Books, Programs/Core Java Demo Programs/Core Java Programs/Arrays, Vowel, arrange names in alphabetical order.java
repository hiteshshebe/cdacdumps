
public class Arrays {

	public void arraydemo()
	{
		int values[] = new int[5];
		int val[] = new int[]{1,2,3,5};
		int sum=0;
		
		for(int i=0;i<val.length;i++)
		{
			System.out.println(val[i]);
		}
		
		for (int x:val)
		{
			System.out.println(x);
			sum+=x;
			
		}
		System.out.println(sum);
		
	}
	
	static void arrange()
	{
		String name[] = {"Chennai", "Bangalore","Mumbai","Hyderabad","Pune","Calcutta"};        
		int size= name.length;
		String temp=null;

		for(int i=0;i<size;i++)
		{
			for(int j=i+1;j<size;j++)	{
				if(name[j].compareTo(name[i])<0) {

					temp=name[i];
					name[i]=name[j];
					name[j]=temp;

		}
	  }
	}
		for(int i=0;i<size;i++)
		{
  		   System.out.println(name[i]);

		}

	}
	
	static void vowel()
	{
		String names[] = {"Chennai", "Bangalore","Mumbai","Hyderabad","Pune","Calcutta"};
				int vowel=0;
				int consonant=0;
				for(int i=0;i<names.length;i++)
				{
					for(int j=0;j<names[i].length();j++)
					{
						if(names[i].charAt(j)=='a'||names[i].charAt(j)=='A'||names[i].charAt(j)=='e'||names[i].charAt(j)=='i'||names[i].charAt(j)=='I'||names[i].charAt(j)=='o'||names[i].charAt(j)=='O'||names[i].charAt(j)=='u'||names[i].charAt(j)=='U')
							vowel++;
						else
							consonant++;
					}
				}
				System.out.print("Number of Vowel is "+vowel+"\n");
				System.out.print("Number of consonant is "+consonant);
				
			}
	
	
	public static void main(String[] args) {
		new Arrays().arraydemo();
		int source[] = {1,2,3,4,5};
		int dest[] = new int[3];
		System.arraycopy(source, 1, dest, 0, 3);
		System.out.println("Array Copy");
		for (int x:dest)
		{
			System.out.println(x);
		
			
		}
		arrange();
		vowel();

	}

}
