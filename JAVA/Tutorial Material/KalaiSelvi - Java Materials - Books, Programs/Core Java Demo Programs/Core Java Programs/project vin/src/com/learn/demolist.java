package com.learn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;



public class demolist {
	public static void main(String[] args)
	{
		List<student> list=new ArrayList<student>();
		student s2=new student("Amir","khan");
		list.add(s2);
		list.add(new student("Mamtha","Banerjee"));
		list.add(new student("Salman","Khan"));
		list.add(new student("Ramon","Roy"));
		list.add(new student("Leela","Chitra"));
		
		list.remove(4);
		
		for(student c:list)
		{
			System.out.println(c.toString());
			
		}
		System.out.println(list.size());
		System.out.println(list.isEmpty());
		
		//list.clear();
		
		System.out.println(list.isEmpty());
		
		list.remove(s2);
		
		for(student c:list)
		{
			System.out.println(c);
		}
		System.out.println();
		
		Iterator<student> st_it=list.iterator();
		while(st_it.hasNext())
		{
			System.out.println(st_it.next()); 
		}
		
		System.out.println(list.contains(s2));
		
		list.add(0,new student("Ramesh","Rai"));
		
		System.out.println(list.get(2));
		
		System.out.println(list.indexOf(s2));
		
		list.set(3, new student("Raja","chola"));
		
		System.out.println(list.get(2));
		
		
		
		List<student> list1=new ArrayList<student>();
		list1.add(new student("Sundereshan","Erawada"));
		list1.add(new student("Mahesh","Bhupathi"));
		
		list.addAll(list1);
		
		for(student c:list)
		{
			System.out.println(c);
		}
		
		ListIterator<student> li=list1.listIterator();
		li.hasNext();
		
		li.nextIndex();
		
		li.previous();
		li.previousIndex();
		
		
	}

}
