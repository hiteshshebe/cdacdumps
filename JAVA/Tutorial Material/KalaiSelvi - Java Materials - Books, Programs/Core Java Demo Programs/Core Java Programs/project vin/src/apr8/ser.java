package apr8;

import java.io.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ser {
	
	public static void main(String[] args) 
	{
		Camel c=new Camel();
		c.capacity=125.25f;
		c.desert="Thar";
		c.height=25.65;
		serializeCamel(c);
		deserializecamel(c);
		
	}
	private static void deserializecamel(Camel c)
	{
		FileInputStream fis=null;
		ObjectInputStream ois=null;
		try
		{
			fis=new FileInputStream("c:\\camel.data");
			ois=new ObjectInputStream(fis);
			System.out.println(ois.readObject());
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				ois.close();
				fis.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
		}
		System.out.println("Object Deserialized");
	}
	private static void serializeCamel(Camel c)
	{
		FileOutputStream fos=null;
		ObjectOutputStream oos=null;
		try
		{
			fos=new FileOutputStream(new File("c:\\camel.data"));
			oos=new ObjectOutputStream(fos);
			oos.writeObject(c);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				fos.close();
				oos.close();
				
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		System.out.println("Object Serialised");
		
		}
	}




