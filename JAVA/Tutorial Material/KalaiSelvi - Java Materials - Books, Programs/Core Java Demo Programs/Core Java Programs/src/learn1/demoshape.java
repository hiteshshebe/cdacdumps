package com.learn;

public class demoshape {
public static void main(String[] args)
{
	shape s=new rectangle();
	s.draw();
	s.paint();
	shape s1=new circle();
	s1.draw();
	s1.paint();
	shape s2=new polygon();
	s2.draw();
	s2.paint();
	shape s3=new square();
	s3.draw();
	s3.paint();
	System.out.println(s3 instanceof polygon);
	System.out.println(s3 instanceof circle);
	System.out.println(s3 instanceof rectangle);
	System.out.println(s3 instanceof square);
	
}
}
