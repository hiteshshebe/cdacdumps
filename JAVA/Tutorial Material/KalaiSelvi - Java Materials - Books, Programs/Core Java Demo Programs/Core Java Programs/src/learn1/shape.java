package com.learn;

public interface shape {
	public abstract void draw();
	public abstract void paint();

}
class circle implements shape
{

	@Override
	public void draw() {
		System.out.println("In circle draw");
		
	}

	@Override
	public void paint() {
		System.out.println("In circle paint");
		
	}
	
}
class polygon implements shape
{

	@Override
	public void draw() {
		System.out.println("In polygon draw");
		
	}

	@Override
	public void paint() {
	System.out.println("In polygon paint");	
		
	}
	
}
class rectangle implements shape
{

	@Override
	public void draw() {
		System.out.println("In rectangle draw");
		
	}

	@Override
	public void paint() {
		System.out.println("In rectangle paint");
		
	}
	
}
class square implements shape
{

	@Override
	public void draw() {
			System.out.println("In square draw");	
	}

	@Override
	public void paint() {
		
		System.out.println("In square paint");
	}
}