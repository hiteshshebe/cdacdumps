package mypackage;

public class timetable {
	public static void main(String[] args)
	{
		period p1=new period("teaching","physcics","101","9.00am","10.00am");
		period p2=new period("teaching","maths","103","10.00am","11.00am");
		period p3=new period("teabreak",null,null,"11.00am","11.30am");
		period p4=new period("teaching","tamil","320","11.30am","12.30pm");
		period p5=new period("lunch",null,null,"12.30pm","2.00pm");
		period p6=new period("teaching","chemistry","120","2.00pm","3.00pm");
		period p7=new period("teaching","biology","220","3.00pm","4.00pm");
		dayschedule monschedule=new dayschedule("monday",new period[]{p1,p2,p3,p4,p5,p6,p7});
		
		period p11=new period("teaching","physcics","101","9.00am","10.00am");
		period p12=new period("teaching","maths","103","10.00am","11.00am");
		period p13=new period("teabreak",null,null,"11.00am","11.30am");
		period p14=new period("teaching","tamil","320","11.30am","12.30pm");
		period p15=new period("lunch",null,null,"12.30pm","2.00pm");
		period p16=new period("teaching","chemistry","120","2.00pm","3.00pm");
		period p17=new period("teaching","biology","220","3.00pm","4.00pm");
		dayschedule tueschedule=new dayschedule("tuesday",new period[]{p11,p12,p13,p14,p15,p16,p17});
		
		period p21=new period("teaching","physcics","101","9.00am","10.00am");
		period p22=new period("teaching","maths","103","10.00am","11.00am");
		period p23=new period("teabreak",null,null,"11.00am","11.30am");
		period p24=new period("teaching","tamil","320","11.30am","12.30pm");
		period p25=new period("lunch",null,null,"12.30pm","2.00pm");
		period p26=new period("teaching","chemistry","120","2.00pm","3.00pm");
		period p27=new period("teaching","biology","220","3.00pm","4.00pm");
		dayschedule wedschedule=new dayschedule("wednesday",new period[]{p21,p22,p23,p24,p25,p26,p27});
		
		period p31=new period("teaching","physcics","101","9.00am","10.00am");
		period p32=new period("teaching","maths","103","10.00am","11.00am");
		period p33=new period("teabreak",null,null,"11.00am","11.30am");
		period p34=new period("teaching","tamil","320","11.30am","12.30pm");
		period p35=new period("lunch",null,null,"12.30pm","2.00pm");
		period p36=new period("teaching","chemistry","120","2.00pm","3.00pm");
		period p37=new period("teaching","biology","220","3.00pm","4.00pm");
		dayschedule thurschedule=new dayschedule("thursday",new period[]{p31,p32,p33,p34,p35,p36,p37});
		
		period p41=new period("teaching","physcics","101","9.00am","10.00am");
		period p42=new period("teaching","maths","103","10.00am","11.00am");
		period p43=new period("teabreak",null,null,"11.00am","11.30am");
		period p44=new period("teaching","tamil","320","11.30am","12.30pm");
		period p45=new period("lunch",null,null,"12.30pm","2.00pm");
		period p46=new period("teaching","chemistry","120","2.00pm","3.00pm");
		period p47=new period("teaching","biology","220","3.00pm","4.00pm");
		dayschedule frischedule=new dayschedule("wednesday",new period[]{p41,p42,p43,p44,p45,p46,p47});
		
		weeklyschedule weekschedule=new weeklyschedule(new dayschedule[]{monschedule,tueschedule,wedschedule,thurschedule,frischedule}); 
		weekschedule.printtimetable();
	}
	

}
class period
{
	private String type;
	private String subjectname;
	private String lecturerhallno;
	private String starttime;
	private String endtime;
	public period(String type, String subjectname, String lecturerhallno,
			String starttime, String endtime) {
		super();
		this.endtime = endtime;
		this.lecturerhallno = lecturerhallno;
		this.starttime = starttime;
		this.subjectname = subjectname;
		this.type = type;
	}
	public void print()
	{
		if(type.equals("lunch"))
		{
			System.out.println("---------------------");
			System.out.println("lunchperiod");
			System.out.println("---------------------");
			return;
		}
		if(type.equals("teabreak"))
		{
			System.out.println("----------------");
			System.out.println("teabreakperiod ");
			System.out.println("----------------");
			return;
		}
		System.out.println(starttime+"\t"+endtime+"\t"+subjectname+"\t"+lecturerhallno+"\t"+"\t");
		//System.out.println("starttime"+starttime+"\t"+subjectname+"\t"+lecturerhallno+"\t"+endtime+"\t");
		/*System.out.println("endtime"+endtime+"\t");
		System.out.println("roomno"+lecturerhallno+"\t");
		System.out.println("subjectname"+subjectname+"\t");
		System.out.println("##############################");*/
	}
	
	
}
class dayschedule
{
	String dayname;
	period[] periods;
	public dayschedule(String dayname, period[] periods) {
		super();
		this.dayname = dayname;
		this.periods = periods;
	}
	public void printschedule()
	{
		
		System.out.println(dayname);
		for(period p:periods)
		{
			p.print();
		}
		System.out.println("***************************************");
	}
}
class weeklyschedule
{
	
	dayschedule[] dayschedule;

	public weeklyschedule(dayschedule[] dayschedule) {
		super();
		this.dayschedule = dayschedule;
	}
	public void printtimetable()
	{
		for(dayschedule d:dayschedule)
		{
			d.printschedule();
		}
	}
	
}