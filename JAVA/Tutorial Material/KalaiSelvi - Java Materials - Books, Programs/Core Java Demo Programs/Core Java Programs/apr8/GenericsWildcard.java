package apr8;

import java.util.ArrayList;
import java.util.Collection;

public class GenericsWildcard {
	static void printCollection(Collection<?> c){
		
		for(Object o: c)
			System.out.println(o);
	}
	public static void main(String[] args)
	{
		ArrayList<Integer> a=new ArrayList<Integer>(10);
		a.add(new Integer(20));
		a.add(new Integer(30));
		printCollection(a);
		ArrayList<Long> l=new ArrayList<Long>(10);
		l.add(new Long(12L));
		l.add(new Long(19L));
		printCollection(l);
		ArrayList<String> s=new ArrayList<String>(10);
		s.add("Saran");
		s.add("Vinoth");
		printCollection(s);
		
	}

}
