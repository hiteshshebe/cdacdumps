package apr8;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;

public class demo10 {
	public static void main(String[] args) {
		FileOutputStream fos=null;
		BufferedOutputStream bos=null;
		DataOutputStream dos=null;
		
		
		try
		{
		fos=new FileOutputStream(new File("c:\\nos.data"));
		bos=new BufferedOutputStream(fos);
		dos=new DataOutputStream(bos);
			for(int h=0;h<1000;h++)
			{
				dos.writeInt(h+9);
			}
			for(int h=0;h<1000;h++)
			{
				dos.writeDouble(h+9.4);
			}
			for(int h=0;h<1000;h++)
			{
				
				dos.writeLong(h+8);
			}
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
			
		}
        catch(IOException e)
        {
        	e.printStackTrace();
        }
	
	finally
	{
		try
		{
			
			dos.close();
			
			bos.close();
			
			fos.close();
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

}
}