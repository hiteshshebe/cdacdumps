public class app {

	public static void main(String[] args) {

		Base[] subs = new Base[2];
		subs[0] = new Sub();
		subs[1] = new SubSub();
		subs[1].hello();
		}
		}
		abstract class Base {

			public abstract void hello();

			}

			class Sub extends Base
			{
				public void hello() {
					System.out.println("I am a Sub");
					}
					}
					class SubSub extends Sub {public void hello() {System.out.println("I am a SubSub");}}