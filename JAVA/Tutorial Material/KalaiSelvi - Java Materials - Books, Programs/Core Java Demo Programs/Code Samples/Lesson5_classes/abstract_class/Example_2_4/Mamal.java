abstract class Mammal {
private String name;
public Mammal(String iName) {
setName(iName);
}
public boolean setName(String nName) {
if (nName.length() > 0) {
name = nName;
return true;
}
else {
name = "unknown";
return false;
}
}
public String getName() {
return name;
}
public abstract boolean setAge(int nAge);
public abstract int getAge();
public abstract String toString();
}
