public class Example_2 {
  String msg = "Hello";

  void doWork() {
    Inner i = new Inner();
    i.doTheWork();
    msg = "Goodbye";
    i.doTheWork();
  }

 class Inner {
    public void doTheWork() {
      // print member of enclosing class
      System.out.println(msg);
    }
  }


  public static void main(String[] av) {
    Example_2 p = new Example_2();
    p.doWork();
  }

}


