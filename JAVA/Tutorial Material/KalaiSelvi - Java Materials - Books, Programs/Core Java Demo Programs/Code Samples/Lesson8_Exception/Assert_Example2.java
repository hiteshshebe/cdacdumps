public class Assert_Example2 {

  public void m1( int value )
  {
    assert 0 <= value : "Value must be non-negative: value= " + value;
    System.out.println( "OK" );
  }
  public static void main( String[] args )
  {
    Assert_Example2 eb2 = new Assert_Example2();
    System.out.print( "bar.m1(  1 ): " );
    eb2.m1( 1 );
    System.out.print( "bar.m1( -1 ): " );
    eb2.m1( -1 );
  }
}

