

import java.io.*;
import java.util.*;

class Example_8_8
{
static int numberOfExceptionsDemonstrated = 10;

public static void main(String arg[])
  {
  for( int number = 1; number <= numberOfExceptionsDemonstrated; number++)
    try
      {
      // Dispatch on the argument.

      switch( number )
        {
        case 1: demoArithmeticException();              break;

        case 2: demoNumberFormatException();            break;

        case 3: demoNullPointerException();             break;

        case 4: demoClassCastException();               break;

        case 5: demoStringIndexOutOfBoundsException();  break;

        case 6: demoArrayIndexOutOfBoundsException();   break;

        case 7: demoNegativeArraySizeException();       break;

        case 8: demoArrayStoreException();              break;

        case 9: demoNoSuchElementException();           break;

        case 10: demoFileNotFoundException();           break;
        }
      }
    catch( Exception e )
      {
      System.out.println("*** case " + number + " caught: " + e);
      }
  }


static void demoArithmeticException()
  {
  message("Demonstrate ArithmeticException");
  int x = 1/0;
  }



static void demoNumberFormatException() /* RuntimeException not declared */
  {
  message("Demonstrate NumberFormatException");
  float f;
  f = Float.parseFloat("-1.23e-34");    // ok
  f = Float.parseFloat("foobar");       // invalid, can't convert
  }

static void demoNullPointerException() /* RuntimeException not declared */
  {
  message("Demonstrate NullPointerException");

  String s = null;

  int len = s.length();                 // invalid, null reference
  }



static void demoClassCastException() /* RuntimeException not declared */
  {
  message("Demonstrate ClassCastException");

  String s = "foo";

  Object x = s;

  Integer i = (Integer)x;               // invalid
  }




static void demoStringIndexOutOfBoundsException()
  {
  message("Demonstrate StringIndexOutOfBoundsException");

  String s = "Goodbye, cruel world";

  char c = s.charAt(100);               // invalid index
  }



static void demoArrayIndexOutOfBoundsException() /* RuntimeException */
  {
  message("Demonstrate ArrayIndexOutOfBoundsException");

  String a[] = new String[10];

  int len = a[10].length();             // invalid index
  }


static void demoNegativeArraySizeException()     /* RuntimeException */
  {
  message("Demonstrate NegativeArraySizeException");

  Object x[] = new Object[-1];                  // invalid index
  }

static void demoArrayStoreException() /* RuntimeException */
  {
  message("Demonstrate ArrayStoreException");

  Object x[] = new String[10];

  x[0] = new Integer(99);       // invalid, store Integer into String array
  }



static void demoNoSuchElementException() /* RuntimeException */
  {
  message("Demonstrate NoSuchElementException");

  StringTokenizer tokenizer = new StringTokenizer("That's all folks");

  while( tokenizer.hasMoreElements() )
    {
    tokenizer.nextElement();    // justified
    tokenizer.nextElement();    // unjustified
    }
  }



static void demoFileNotFoundException() throws FileNotFoundException
  {
  message("Demonstrate FileNotFoundException");

  String badFileName = "This file doesn't exist.";

  FileInputStream input;

  input = new FileInputStream(badFileName);
  }



static void message(String message)
  {
  System.out.println("\n" + message);
  }

}
