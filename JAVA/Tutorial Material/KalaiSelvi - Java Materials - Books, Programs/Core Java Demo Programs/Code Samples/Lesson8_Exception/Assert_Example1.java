
public class Assert_Example1
{
  public void m1( int value )
  {
    assert 0 <= value;
    System.out.println( "OK" );
  }
  public static void main( String[] args )
  {
    Assert_Example1 ae1 = new Assert_Example1();
    System.out.print( "  1 : " );
    ae1.m1( 1 );
    System.out.print( " -1 : " );
    ae1.m1( -1 );
  }
}

//javac -source 1.4 x.java

// java -ea x