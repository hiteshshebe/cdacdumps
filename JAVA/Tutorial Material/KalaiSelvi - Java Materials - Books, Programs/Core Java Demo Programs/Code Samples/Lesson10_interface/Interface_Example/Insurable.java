import java.util.*;
public interface Insurable {
    public void getDetails();
    public double calculatePremium();

}