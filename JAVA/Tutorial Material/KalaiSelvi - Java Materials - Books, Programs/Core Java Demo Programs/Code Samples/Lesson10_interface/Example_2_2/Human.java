public class Human {
    private String name;

    public Human(String name) { this.name = name; }
    public String getName()   { return name; }
    public String toString() { return "Human " + getName(); }
}