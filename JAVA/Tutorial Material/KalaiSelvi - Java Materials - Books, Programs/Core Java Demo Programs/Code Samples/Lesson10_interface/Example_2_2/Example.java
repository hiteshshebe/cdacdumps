class HandyPerson extends Human implements Electrician, Plumber {
    public        HandyPerson(String name) { super(name); }
    public String changeBulb() { return "HP: Unscrew old, screw in new";}
    public String unplugDrain(){ return "HP: Use plunger"; }
    @Override public String toString() { return "HandyPerson " + getName(); }
}


class NobodySpecial extends Human {
    public      NobodySpecial(String name) { super(name); }
    public void changeBulb() {System.out.print("NS: Unscrew old, screw in new");}
    public void unplugDrain() {System.out.println("NS: Use plunger"); }
    public String toString() { return "NobodySpecial " + getName(); }
}


class IndustrialPlumber extends Human implements Plumber {
    public        IndustrialPlumber(String name) { super(name); }
    public String unplugDrain() {return "IP: We'll add it to our list.";}
}

public class Example
{
	public static void main(String args[])
	{
	HandyPerson hp+