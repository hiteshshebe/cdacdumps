/**
@author K.Srivatsan
A <code>employee<code> represents the employee information
@version 1.0
*/

public class CommentExample {

	int empno;
	String empname;

	/**
	the constant value
	*/
public static final int empcount=1000;

	public CommentExample()
	{
		empno=0;
		empname=" ";
	}



	/** set the employee details
	@param eno represents employeeno
	@return void
	*/
	public void setData(int eno, String ename)
	{
		empno=eno;
		empname=ename;
}

/** get the employee details

	@return String
	*/

	public String getData()
	{
		return empname + " " +empno;

}

public static void main(String args[])
{
	 CommentExample ce= new CommentExample();

	   ce.setData(100,"ram");
	   System.out.println(ce.getData());

   }

}
