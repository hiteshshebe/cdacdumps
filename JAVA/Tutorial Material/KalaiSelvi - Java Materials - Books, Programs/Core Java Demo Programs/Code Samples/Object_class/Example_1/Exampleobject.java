
class Exampleobject {
	int orderno;
	String customername;

	public Exampleobject()
	{
	orderno=1020;
	customername="3i Infotech";
}

	public Exampleobject(int orderno, String customername)
	{

	this.orderno=orderno;
	this.customername=customername;
}

public String toString()
{
	  return orderno+":"+customername;

  }

  public int hashCode()
  {
	   return orderno+ 7 * orderno;

   }

public boolean equals(Object otherObject)
{
	// check objects are identical

   if(this==otherObject)
     return true ;

     //check for null

     if(otherObject==null)
        return false;

        if(getClass()!=otherObject.getClass())
           return false;

           Exampleobject eobj = (Exampleobject)otherObject;

              return customername.equals(eobj.customername)
                      && orderno==(eobj.orderno);

				  }

public static void main(String args[])
{

	Exampleobject eobj =new Exampleobject();

    Exampleobject eobj1 = new Exampleobject(100,"sri");
    Exampleobject eobj2 =new Exampleobject(101,"ram");
    Exampleobject eobj3 =new Exampleobject(101,"ram");

	System.out.println(" Class Name" + eobj.getClass());
	System.out.println(" Class Data" + eobj);
	System.out.println("hascode" + eobj.hashCode());
	System.out.println("hascode1" + eobj1.hashCode());
    System.out.println("equals" +eobj1.equals(eobj2));
    System.out.println("equals" +eobj2.equals(eobj3));

/*      String s1= new String("HELLO");
      String s2= s1;
   System.out.println(" Class Name" + s1.getClass());
	System.out.println(" Class Data" + s1);
	System.out.println("hascode" + s1.hashCode());
    System.out.println("equals" +s1.equals(s2));

*/

}

}
