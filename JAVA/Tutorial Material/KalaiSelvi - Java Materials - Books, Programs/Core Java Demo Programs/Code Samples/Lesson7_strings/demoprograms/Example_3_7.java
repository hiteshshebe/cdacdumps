class Example_3_7 {
  public static void main(String args[]) {
    String s = "This is a demo of the getChars method.";
    int start = 10;
    int end = 14;
    char buf[] = new char[end - start];

    s.getChars(start, end, buf,0);
    System.out.println(buf);
        StringBuffer sb = new StringBuffer("Hello");
	    System.out.println("buffer before = " + sb);
	    System.out.println("charAt(1) before = " + sb.charAt(1));
	      sb.setCharAt(1, 'i');
	      sb.setLength(3);
	      System.out.println("buffer after = " + sb);
	      System.out.println("charAt(1) after = " + sb.charAt(1));


  }
}


