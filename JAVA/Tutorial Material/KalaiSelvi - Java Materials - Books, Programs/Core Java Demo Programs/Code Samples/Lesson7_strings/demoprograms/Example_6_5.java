import java.util.*;

public class Example_6_5 {

public static void main(String args[]) {

String sentence = "Banks, Rob, 34, Ottawa";
StringTokenizer words = new StringTokenizer(sentence,", ");
while(words.hasMoreTokens()) {
    String aWord = words.nextToken();
    System.out.println(aWord);
}


StringTokenizer words1 = new StringTokenizer("Half of 12 is six");
System.out.println("The String has " + words1.countTokens() + " words." );
System.out.println("Here are the words, one by one:");
while(words1.hasMoreTokens()) {
    System.out.println(words1.nextToken());
}

}

}

