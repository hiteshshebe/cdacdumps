class Bufferexample {

	private int age;
	private String name;

	public Bufferexample(int age, String name)
	{
		this.age = age;
		this.name=name;
	}

	public String toString()
	{
		return age + " " +name;
	}

}
public class Stringbuffertest3
{

	public static void main(String args[])
	{

		StringBuffer sb=new StringBuffer("Hello Java World");
       Bufferexample be = new Bufferexample(45,"ram");
		sb.append(be);
		System.out.println(sb);
	}

}
