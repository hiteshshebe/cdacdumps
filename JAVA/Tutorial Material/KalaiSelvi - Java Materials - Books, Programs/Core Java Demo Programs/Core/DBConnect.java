/**
 * This class mainly used provide database connection to other clases
 */
package com.ea.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * @author Praveen
 * 
 */
public class DBConnect {

	Connection conn;

	/**
	 * 
	 * @return Database connection to other classes
	 */
	public synchronized Connection getConnect() 
	{
		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/eagency");
			conn = ds.getConnection();
		} catch (SQLException ex) {
			ex.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();

		}

		return conn;
	}

}
