
public class Outer {
	
	Outer()
	{
		System.out.println("Outer class constructor");
	}
	
	int outerData =  678;
	
	public void sub()
	{   
		System.out.println("outer class method");
		
		final class MethodLocal
		{
			int a= 100;
			
			public void method1()
			{
				System.out.println("inside a method local class");
			}
		}
		
		MethodLocal obj = new MethodLocal();
		obj.method1();
		System.out.println(obj.a);
		
		
	}
	
	static class Inner
	{
		
		Inner()
		{
			System.out.println("Inner class constructor");
		}
		
		static int  innerData = 900;
		
		public void add()
		{
			System.out.println("inner class method");
		}
	}
	

	public static void main(String[] args) {
		//object created for inner class 
		Outer outerobj = new Outer();
		/*outerobj.sub();
		System.out.println(outerobj.outerData);*/
		
		Outer.Inner stat = new Outer.Inner();
		stat.add();
		System.out.println(stat.innerData);
		//Outer.Inner obj1 = outerobj.new Inner();
		
	//	Outer.Inner obj = new Outer().new Inner();
		//System.out.println(obj.innerData);
		//obj.add();
		
		

	}

}
