
public class InnerStaticDemo {
     static class StaticClass
     {
    	 static void innermethod()
   	  	{
   		  System.out.println("inner class method");
   		  
   	  	}
    	
     }
    	 public void outermethod()
    	 {
    		 StaticClass.innermethod();
    	 }
   
	
	public static void main(String[] args) {
		 InnerStaticDemo obj = new InnerStaticDemo();
		 obj.outermethod();
		 InnerStaticDemo.StaticClass.innermethod();
		                           
	}

}
