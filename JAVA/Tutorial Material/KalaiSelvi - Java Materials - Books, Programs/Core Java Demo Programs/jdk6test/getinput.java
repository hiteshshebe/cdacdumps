package jdk6test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class getinput {

	/**
	 * @param args
	 */
	public static void main(String[] args){
	
		try
		{

			InputStreamReader in=new InputStreamReader(System.in);
			BufferedReader br=new BufferedReader(in);
			System.out.println("Type the number :");
			String name=br.readLine();
			int mark=Integer.parseInt(name);
			
			System.out.println("enter the second number:");
			
			String sc=br.readLine();
			int m2=Integer.parseInt(sc);
			int tot=mark+m2;
			
		System.out.println("the given input using buffered input stream is="+tot);

		}
		catch (IOException e) {
			System.out.println("ioexception==="+e);
		}
			
	/*		
			System.out.println("enter single integer for read():");
			byte[] b=new byte[3];
			int pass=System.in.read(b);
			System.out.println("the input integer only system.in.read() method is==="+pass);
			
			byte[] bArray=new byte[256];
			System.out.println("\n enter the integer:");
			int nRead = System.in.read(bArray);
			String strInts = new String(bArray, 0, nRead-1);
			System.out.println("the input integer is = "+strInts);
		}
	*/
		
		
		

	}

}
