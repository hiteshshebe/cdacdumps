class animal{
	public void eat(){
		System.out.println("animal eating");
	}
}
class horse extends animal{
	public void eat(){
        super.eat();
		System.out.println("horse eating");
	}
	public void buck(){}
}
public class testoverride
{
	public static void main(String[] arg){
       // animal a=new animal();
        animal b=new horse();//call at run-time based on object
		b.eat();
       // b.buck();//this will show an error
    }       
}
