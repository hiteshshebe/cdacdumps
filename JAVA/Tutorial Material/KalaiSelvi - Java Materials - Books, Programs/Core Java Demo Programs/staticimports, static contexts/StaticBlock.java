class StaticBlock
{
   static int i=2;
   int j;

   static
   {
     i = i*3;
     System.out.println("Inside Static Block:"+i);
   }

   StaticBlock(int j)
   {
       this.j=j;
       System.out.println("Inside Parameter Cons.:"+j);
   }


   public static void main(String args[])
   {
     System.out.println("Inside Main");
     StaticBlock s1 = new StaticBlock(25);
     System.out.println("i"+i);
     System.out.println(s1.j);
   }
}



