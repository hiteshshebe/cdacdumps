class Rectangle
{
     int length;
     int width;

     Rectangle(int x,int y)	//Constructor method
     {
         length = x;
         width = y;
     }
     int rectArea()
     {
       return(length * width);
     }
}
class RectangleArea
{
     public static void main(String args[])
      {
	Rectangle rect1 = new Rectangle(15,10);   //Calling the constructor
	int area1 = rect1.rectArea();
	System.out.println("Area1 = " +area1);
      }
}