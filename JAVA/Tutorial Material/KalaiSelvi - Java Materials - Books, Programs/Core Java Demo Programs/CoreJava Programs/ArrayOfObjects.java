import java.io.DataInputStream;

class Student
{
	int id;
	String name;
	public void getData()
	{   
		System.out.println("Enter the Student Id and Name");
		DataInputStream dis=new DataInputStream(System.in);
		try
		{   
			id=Integer.parseInt(dis.readLine());
			name=dis.readLine();
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
}

public class ArrayOfObjects {


	public static void main(String[] args) {
		
		//creation of array for the Student class
		int i=0;
		
		Student stuarr[]=new Student[5];
		for (Student sarr:stuarr)
		{   
			sarr=new Student();
			System.out.println(sarr);
			
		}		
		for (int j=0; j<5; j++)
		{
			stuarr[j]=new Student();
			stuarr[j].getData();
			System.out.println("Student "+j+" Id "+stuarr[j].id);
			System.out.println("Student "+j+" Name "+stuarr[j].name);
		}		
	}
}
