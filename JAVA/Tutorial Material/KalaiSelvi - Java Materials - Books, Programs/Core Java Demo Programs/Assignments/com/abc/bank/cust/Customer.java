package com.abc.bank.cust;

public class Customer {
  int cid;  //default
  private String custName; //private
  protected void setData() //protected
  {
	  cid=100; 
	  custName="Deepika";
  }
  public void getData() //public
  {
	  setData();
	  System.out.println("Cust Id :"+cid);
	  System.out.println("Cust Name :"+custName);	  
  }
}

class Access
{
	public void check()
	{
		System.out.println("Specifiers");
		Customer cobj=new Customer();
		System.out.println(cobj.cid);
		cobj.setData();
		cobj.getData();
	}
}