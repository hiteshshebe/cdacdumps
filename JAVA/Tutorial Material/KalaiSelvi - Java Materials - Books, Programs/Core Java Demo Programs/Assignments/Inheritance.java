class Operations
{   int result;
   Operations(int s)
   {
	   System.out.println("Parent class - Default Constructor :"+s);
   }

   public int add(int a,int b)
   {   result = a+b;
	   System.out.println("Result (a+b) ="+result);
	   return result;
   }
   public int sub(int a,int b)
   {
	   if (a>b)
		   return a-b;
	   else
		   return b-a;
   }
}
public class Inheritance extends Operations {
	Inheritance(int a)
	{   super(200);
		System.out.println("Child class - Def Cons : "+a);
		super.add(50, 50);
		System.out.println(super.result);
	}
	
	//overridden method
    public int add(int a,int b)
    {
    	System.out.println("overridden");
    	return a+b;
    }
    //original method
	public int mul(int a,int b)
	{   System.out.println("a*b :"+a*b);
		return a*b;
	}
	
	public static void main(String[] args) {
      Operations obj=new Operations(500);
      obj.add(12, 4);
      Inheritance oo=new Inheritance(100);
      oo.mul(12, 2);
      Operations ooo=new Inheritance(100);
      ooo.add(2, 2);
      ooo.sub(34,56);
      //Inheritance check=(Inheritance)new Operations();
      
      

	}

}
