 class Rect
	{
	  public void area()
	   {
		   System.out.println("area - rect");
	   }
	}
 
 class Check extends Rect
 {
	 public void area()
	 {
		 System.out.println();
	 }
 }
	
public class Triangle extends Shape{
	
	int area;
	
	public void calcArea()
	{   area=(int)0.5f*2*3;
		System.out.println(area);
	}
	
	public static void main(String[] args) {
        Triangle obj=new Triangle();
        obj.calcArea();
        Shape sobj=new Triangle();
        sobj.calcArea();
        sobj.draw();
        Rect robj=new Rect();
        robj.area();
	}
	
	
}
