/*

* Account.java

* The class file encapsulates the Account of a person

*/

public class Account 
{
    private double balance;

    // default constructor to initialize balance
    public Account(){
        balance = 0.0;
    }

    // Overloaded constructor 
    public Account( double amount ){
        balance = amount;
    }


    public void deposit( double amount ){
	balance += amount;
    }

    public double withdraw( double amount ){
        // See if amount can be withdrawn
	
	if(balance >= amount){
	    balance -= amount;
            return amount;
	}else
        // Withdrawal not allowed
        return 0.0;
   }

   public double getbalance(){
       return balance;
   }
}  
