package com.abc.bank.customer.account.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection {
	
	
	Connection conn = null;
	PreparedStatement pstmt;
	ResultSet rs = null;
	boolean flag = false;
	
	public boolean dbConnect(String custid,String accno)
	{
		
		
		try {
			//Load the driver - default and register
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
	        
			//Connection Establishment with db
			conn = DriverManager.getConnection("jdbc:odbc:ABCdsn","root","root");		
			
	        //Statement = Query
			
			pstmt=conn.prepareStatement("select * from account where accountno=? and custid=?");
			
			pstmt.setString(1,accno);
			pstmt.setString(2,custid);
			
			rs = pstmt.executeQuery();
			
			if (rs.next())
				flag = true;
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(SQLException sqle)
		{
			sqle.printStackTrace();
		}
		finally
		{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return flag;
	}
}
