package com.abc.bank.customer.account.pojo;

import com.abc.bank.customer.account.bslogic.AccountValidation;

public class AccountBean {
	//properties
	
	private String custid;
	private String accno;
	
	//mutators - setter methods
	
	public void setCustid(String custid)
	{
		this.custid = custid;
	}
	
	public void setAccno(String accno)
	{
		this.accno = accno;
	}
	
	// accessors - getter
	
	
	public String getCustid()
	{
		return custid;
	}
	public String getAccno()
	{
		return accno;
	}
	
	public boolean validate(String custid, String accno)
	{
		AccountValidation accvali = new AccountValidation();
		return accvali.accValidate(custid, accno);
		
	}
	
	

}
