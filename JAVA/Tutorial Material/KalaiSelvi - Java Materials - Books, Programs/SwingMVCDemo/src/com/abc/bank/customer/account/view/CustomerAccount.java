package com.abc.bank.customer.account.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.abc.bank.customer.account.pojo.AccountBean;

public class CustomerAccount extends JApplet implements ActionListener{
	
	JLabel lblCustid, lblAccNo;	
	JTextField txtCustid, txtAccno;	
	JButton btnOk;
	String message="";
	
	public void init()
	{
		lblCustid = new JLabel("CustId");
		lblAccNo = new JLabel("Accno");
		txtCustid = new JTextField(10);
		txtAccno = new JTextField(10);
		btnOk = new JButton("Ok");
		
	}
	
	public void start()
	{
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		c.add(lblCustid);
		c.add(txtCustid);
		c.add(lblAccNo);
		c.add(txtAccno);
		btnOk.addActionListener(this);
		c.add(btnOk);
		
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		if (ae.getSource()==btnOk)
		{
			String custid = txtCustid.getText();
			String accno = txtAccno.getText();
			//Move to the bean
			AccountBean accBean = new AccountBean();
			accBean.setCustid(custid);
			accBean.setAccno(accno);
			boolean status = accBean.validate(custid,accno);
			if (status)
				message = "Correct AccNo";
			else
				message = "Incorrect Credentials";
			repaint();
		}
		
	}
	
	public void paint(Graphics g)
	{		
		g.drawString(message, 60, 100);
	}
}
