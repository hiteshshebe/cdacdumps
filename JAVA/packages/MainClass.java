package pack2;
import pack1.A;
import pack1.B;

class MainClass
{
	public static void main(String args[])
	{
		A a1=new A();
		a1.display();
		B b1=new B();
		b1.display();
		C c1=new C();
		c1.display();
		D d1=new D();
		d1.display();
	}
}