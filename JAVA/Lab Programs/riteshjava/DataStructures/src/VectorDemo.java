import java.util.Vector;
import java.util.Iterator;

class Product
{
	int pid;
	String pname;
	
	public Product(int pid,String pname)
	{
		this.pid=pid;
		this.pname=pname;
	}
}


public class VectorDemo {


	public static void main(String[] args) {
		
		Vector v=new Vector();
		v.add(10);
		v.add(20);
		v.add(30);
		v.add(new Product(1, "TV"));
		System.out.println("Size of Vector object "+v.size());
		System.out.println("Capacity of vector object "+v.capacity());
		
		Iterator i=v.iterator();
		
		while(i.hasNext())
		{
			Object ob=i.next();
			if(ob instanceof Product)
			{
				Product p=(Product)ob;
				System.out.println(p.pid+" "+p.pname);				
			}
			else
				System.out.println(ob);
		}
		
		
		

	}

}
