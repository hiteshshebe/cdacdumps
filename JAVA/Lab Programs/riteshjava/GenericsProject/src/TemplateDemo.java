import java.util.List;
import java.util.ArrayList;


public class TemplateDemo {

	public void print(List<?> list)
	{
		for(Object obj:list)
		{
			System.out.println(obj);
		}
	}
	
	public static void main(String[] args) {
		
		List<Person> list=new ArrayList<Person>();
		
		list.add(new Students(1,"Shailesh"));
		list.add(new Students(2,"Mahesh"));
		
		System.out.println(list);
		
		TemplateDemo td=new TemplateDemo();
		td.print(list);
		
		
		
	}

}
