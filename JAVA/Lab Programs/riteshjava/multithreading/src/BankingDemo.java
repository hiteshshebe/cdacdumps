class BankingResource
{
	int balance;
	
	public BankingResource(int bal)
	{
		this.balance=bal;
	}
	
	public synchronized int deposite(int damt)
	{
		if(balance>=1500)
		{
			balance+=damt;
			notify();
		}
		else
			try
			{
				wait();
			}
			catch(InterruptedException ie)
			{
				System.out.println(ie.getMessage());
			}
		return balance;
	}
	
	public int withdraw(int wamt)
	{
		if(balance-wamt>=500)
		{
			balance-=wamt;
			notify();
		}
		else
			try
			{
				wait();
			}
			catch(InterruptedException ie)
			{
			System.out.println(ie.getMessage());
			}
		return balance;
	}
	
	public void display()
	{
		System.out.println("Current balance is "+balance);
	}
}


class Customer extends Thread
{
	BankingResource br;
	int amt;
	int balance;
		
	public Customer(BankingResource br)
	{
		this.br=br;
		start();
	}
	
	public void run()
	{
			balance=br.deposite(200);
			System.out.println("Current balance is Rs."+balance);
	}
}



public class BankingDemo {

	
	public static void main(String[] args) {
	
		BankingResource br1=new BankingResource(2000);
		br1.display();
		
		Customer c1=new Customer(br1);
		Customer c2=new Customer(br1);

	}

}
