import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;


class Students
{
	
	
	String name;
	int rollno;
	String dept;
	
	
	public Students(String name, int rollno, String dept) 
	{
		super();
		this.name = name;
		this.rollno = rollno;
		this.dept = dept;
	}


	@Override
	public String toString() 
	{
		return "name=" + name + ", rollno=" + rollno + ", dept="
				+ dept + "";
	}
	

}



public class HashMapDemo 
{
	


	public static void main(String[] args) 
	{
	//	Map map=new HashMap();
		
		
		
		
		Map map=Collections.synchronizedMap(new HashMap());
		
		//Map map=new HashMap();
		
		map.put(100,new Students("sid",381,"IT"));
		
		map.put(200,"Lava");
		map.put(300,"Bava");
		map.put(400,"Kava");
		map.put(500,"Pava");
		
		map.put(700,new Students("syz",400,"TP"));
		map.put(800,new Students("vkt",423,"LD"));
		map.put(900,new Students("sss",373,"MAT"));
		
		
		

		
		System.out.println("traversing by iterator");

		Set set = map.entrySet();
		Iterator it = set.iterator();
		
		
		while(it.hasNext())
		{
			Object obj=it.next();
			
			Map.Entry me = (Map.Entry)obj;
			
			System.out.print(me.getKey());
			System.out.println(" "+me.getValue());
		}

			
		
	}

}
