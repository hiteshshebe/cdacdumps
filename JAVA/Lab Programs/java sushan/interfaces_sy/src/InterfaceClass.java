
public class InterfaceClass implements i1,i2 
{
	
	public void m1()
	{
		System.out.println("aalu leloooooo....");
	}
	
	public void m2()
	{
		System.out.println("kande leloooooo...");
	}
	
	
	public void m3()
	{
		System.out.println("subah se na ek aalu bika hai...........");
	}
	
	public void m4()
	{
		System.out.println("na bika hai adha kanda.......");
	}
	
	public static void main(String[] args) 
	{
		final int x=10;					//final keyword is used to finalize the value
		
		System.out.println("the value of x is "+x);
			
		
		InterfaceClass ic=new InterfaceClass();
		System.out.println("\n");
		ic.m1();
		ic.m2();
		
		System.out.println("\n");
		
		ic.m3();
		ic.m4();
		
		i2 ii=new InterfaceClass();
//		ii.m1();
//		ii.m2();
		ii.m3();
		ii.m4();
	}
}
