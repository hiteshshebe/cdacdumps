class Thread1 extends Thread
{
	public void run()
	{
		for(int i=0;i<10;i++)
		System.out.println("hey thread 1 run()");
	}
	
	public void dead()
	{
		System.out.println("i m done....");
	}
}

class Thread2 implements Runnable
{
	public void run()
	{
		for(int i=0;i<10;i++)
		System.out.println("hey thread 2 run()");
	}
}



public class MulThreading {
	public static void main(String[] args) {
		
		Thread1 t1=new Thread1();
		Thread2 t22=new Thread2();
				
		Thread t2=new Thread(t22);
		
		
		t1.start();
	    t1.dead();
				
						
		System.out.println();
		
		t2.start();
			
	}
}
