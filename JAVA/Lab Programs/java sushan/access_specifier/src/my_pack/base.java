package my_pack;
class base
{
	long x=50;
	
	base()
	{
		System.out.println("base default constructor");
		x=200;
	}
	
	base(int a)
	{
		System.out.println("base parameterised constructor");
		x=a;
	}
	
	
	void display()
	{
		System.out.println("Hello Moto");
		System.out.println(x);
	}
}