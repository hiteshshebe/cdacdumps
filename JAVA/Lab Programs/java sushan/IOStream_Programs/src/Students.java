import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.xml.sax.InputSource;




public class Students implements Serializable
{	
//	File fobj1=new File("Kasab.txt");
	File fobj2=new File("Hang.txt");
	
	int rollno;
	String name;
	
	
	public Students(int rollno, String name) 
	{
		super();
		this.rollno = rollno;
		this.name = name;
	}
	
	public void readData()
	{
		Students temp;
		
		try
		{
			FileInputStream fis=new FileInputStream(fobj2);
		    ObjectInputStream ois=new ObjectInputStream(fis);
		    temp=(Students)ois.readObject();
		    
			System.out.println("readData rollno::"+temp.rollno);
			System.out.println("readData name::"+temp.name);
		}
		
		catch(ClassNotFoundException cne)
		{
			cne.getMessage();
		}
		catch(FileNotFoundException fnfe)
		{
			fnfe.getMessage();
		} 	
		catch(IOException ioe)
		{
			ioe.getMessage();
		}
		
		
	}
	
	public void writeData(Students s)
	{
		
		Students temp=s;
		
		System.out.println(temp.rollno);
		System.out.println(temp.name);
		
		try
		{
			FileOutputStream fos=new FileOutputStream(fobj2);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			
			oos.writeObject(temp);
			
			System.out.println("written............");
		}
		catch(IOException ioe)
		{
			ioe.getMessage();
		}
		
	}

	
	public static void main(String[] args) 
	{
		Students std=new Students(302,"kasab is hanged.....");
		
		std.writeData(std);
		std.readData();
	}

}
