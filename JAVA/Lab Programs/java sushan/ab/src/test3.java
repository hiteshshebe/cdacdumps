class Base
{
	Base()
	{
//		this(10);
		System.out.println("\nbase constructer....");
	}
	
	Base(int a)
	{
		System.out.println("\nbase parameterised constructer...."+a);
	}
	
	protected void met()
	{
		System.out.println("\nmethod of base class...");
	}
}




public class test3 extends Base
{
	test3()
	{
		super.met();
		System.out.println("\ntest3 constructer....");
	}
	
	
	public static void main(String[] args)  			//main function 
	{
		test3 t=new test3();
		Base b=new Base();
		
		Integer i1=new Integer(95);
		Integer i2=new Integer(95);
		
		String s1="Java";
		String s2="Java";
		
		
		String s3=new String("hello");
		String s4=new String("HeLLo");
		
		int arr[]=new int [5];
		
		for(int i=0;i<arr.length;i++)
		{
			arr[i]=i+1;
		}
		
		System.out.println("\ntest3 hash code is:::::"+t.hashCode());
		System.out.println("Base  hash code is:::::"+b.hashCode());
		
		
		System.out.println("\nint i1 hash code is:::::"+i1.hashCode());
		System.out.println("int i2 hash code is:::::"+i2.hashCode());
		
		System.out.println("\nString s1 hash code is:::::"+s1.hashCode());
		System.out.println("String s2 hash code is:::::"+s2.hashCode());
		
		t.testMet();
		
		System.out.println("\nfor s1 and s2");
		if(s1==s2)
			System.out.println("both are same");
		else
			System.out.println("not same");
		
		
		System.out.println("\nfor s3 and s4");
		if(s3.equalsIgnoreCase(s4))
			System.out.println("both are same");
		else
			System.out.println("not same");
		
		
		System.out.println("\n");
		for(int i=0;i<arr.length;i++)
		{
			System.out.println(arr[i]);
			
		}
		
		System.out.println("\n");
		for(int i:arr)
			System.out.println(i);
	}
	
	
	void testMet()
	{
			super.met();
			System.out.println("called from testMet method");
	}
	
	
}
