
public class VariableArgumentsDemo {

	
	public int add(int...arr)
	{
		int sum=0;
		
		for(int i:arr)
		sum+=i;
		
		
		
		return sum;
		
	}
	
	public static void main(String[] args) {
	
		
		VariableArgumentsDemo vad=new VariableArgumentsDemo();
		
		System.out.println(vad.add(1, 2, 3, 4, 5, 6, 7));
		

	}

}
