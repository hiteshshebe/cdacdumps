import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

class Template1<Uttara,NameType>
{
	 Uttara t1;
	 NameType name;
	
	 public void setData(Uttara u1,NameType nm)
	 {
		 t1=u1;
		 this.name=nm;
	 }
	public Uttara getData()
	{
		return t1;
	}
}

public class WildCardDemo {

	public void printData(Template1<?,?> c)
	{
		System.out.println("Hello Moto  "+c.getData());
	}
	
	
	public static void main(String[] args) {
		
		Template1<Integer,String> t1=new Template1<Integer,String>();
		t1.setData(25,"Sushan");
		System.out.println(t1.getData());
		
		new WildCardDemo().printData(t1);
		
		Template1<String,String> t2=new Template1<String,String>();
		t2.setData("Shailesh","Sonare");
		System.out.println(t2.getData());
		
		new WildCardDemo().printData(t2);
	}

}
