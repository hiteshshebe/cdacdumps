import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class HashMapDemo {

	public static void main(String[] args) {

		Map map=new HashMap();
		
		map.put(1,"Hello");
		map.put(2,"World");
		
		Set s=map.entrySet();
		
		Iterator i=s.iterator();
		
		while(i.hasNext())
		{
			Object obj=i.next();
			Map.Entry me=(Map.Entry)obj;
			System.out.println(me.getKey()+" : "+me.getValue());
			
		}

	}

}
