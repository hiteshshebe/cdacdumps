import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import java.lang.Comparable;

class Students implements Comparable 
{
	int rollno;
	String name;
	
	Students(int rn,String name)
	{
		this.rollno=rn;
		this.name=name;
	}
	 
	public String toString()
	{
		return ""+rollno+  " Name :"+name;
	}

	@Override
	public int compareTo(Object obj1) {
		// TODO Auto-generated method stub
		
		int no1=this.rollno;
		int no2=((Students)obj1).rollno;
		
		if(no1<no2)
			return -1;
		else
			return (no1==no2?0:1);
		
	}
	
	
}

public class ArrayListDemo {

	
	public static void main(String[] args) {
	
		List alist=new ArrayList();
		
		alist.add(new Students(25,"Shailesh"));
		alist.add(new Students(35,"Mahesh"));
		alist.add(new Students(30,"Shilpa"));
		
		Collections.sort(alist);
		
		System.out.println(alist);
		

	}

}
