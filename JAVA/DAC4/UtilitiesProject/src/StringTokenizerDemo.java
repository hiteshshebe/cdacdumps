import java.util.StringTokenizer;

public class StringTokenizerDemo {

	
	public static void main(String[] args) {
	
		String str="My name is Anthony Gonsalvis....";
		StringTokenizer st=new StringTokenizer(str,"\n");
		
		System.out.println("No of tokens......"+st.countTokens());
		
		while(st.hasMoreTokens())
		{
			System.out.println(st.nextToken());
		}

	}

}
