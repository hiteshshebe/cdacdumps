package com.abc.bank.transaction.exceptions;

public class DepositException extends Exception{
	int damt;
	public DepositException(int damt) {
		this.damt = damt;
	}
	
	
	public void message()
	{
		System.out.println("Should deposit a min of Rs. 1000 but you are depositing only Rs."+this.damt); 
	}

}
