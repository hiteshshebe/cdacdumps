package com.abc.bank.account.exceptions;

public class AccountNumberException extends Exception{
	int len;
	public AccountNumberException(int len)
	{
		this.len = len;
	}
	
	public String toString()
	{
		return "Account Number is "+len +"digits and it can be less than or equal to 7 digits";
	}
	
	

}
