import java.io.DataInputStream;
import java.io.IOException;
import java.util.Scanner;


public class ExceptionDemo {
	
	static void check()throws IOException, StringIndexOutOfBoundsException,ArrayIndexOutOfBoundsException
	{
		DataInputStream dis = new DataInputStream(System.in);
		/*try
		{
		*/	System.out.println("Enter a number : ");
			int s = Integer.parseInt(dis.readLine());
			System.out.println("Value : "+s);
		/*}	catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		finally
		{
			dis = null;
			System.out.println("finally");
		}*/
		
		
		
	}
	
	
	public void demo()
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Enter nr, dr : ");
		int nr = input.nextInt();
		int dr = input.nextInt();
		
		String s="java";
		//s=null;
		System.out.println(s.indexOf('s'));
		System.out.println(s.length());
		System.out.println(s.charAt(4));
		
		int a[]=new int[12];
	
		try
		{
		    if (dr==0)
		    	throw new ArithmeticException("Denominator is zero");
		    else
		    {
			int res = nr/dr;
			System.out.println(res);
		    }
			a[12]=23;
		}catch(ArithmeticException ae)
		{
			System.out.println(ae.getMessage());
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("Array index "+aie.getMessage()+" is out of bounds");
			throw aie; //rethrow
		}
		finally
		{
			input = null;
			a=null;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	/*	try {
			check();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		
		new ExceptionDemo().demo();
	}

	 	
		
}
