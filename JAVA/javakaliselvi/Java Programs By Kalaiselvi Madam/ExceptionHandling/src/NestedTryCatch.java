
public class NestedTryCatch {

	static void nested()
	{
		try //outer try
		{
		
			int a[] = new int[2];
			a[8]=90;
			
			
			//inner try 
			 try
			 {
				 a=null;
				 System.out.println(a[0]);
			 }catch(ArithmeticException ne)
			 {
				 System.out.println("null pointer");
			 }
			finally
			{
				System.out.println("inner finally");
			}
			
			
			
		}catch(ArithmeticException ne)
		 {
			 System.out.println("null pointer");
		 }
		
		/*catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println(e.getMessage());
		}
		catch(StringIndexOutOfBoundsException e)
		{
			System.out.println(e.getMessage());
		}
		
		
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}*/
		
		finally
		{
			System.out.println("outer finally");
		}
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//nested();
		//System.out.println(args[2]);

	}

}
