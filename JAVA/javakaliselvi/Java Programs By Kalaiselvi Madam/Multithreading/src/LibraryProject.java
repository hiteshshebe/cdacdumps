class BookTransaction
{
	int stock;
	
	public BookTransaction(int stock) {
		this.stock = stock;
	}
	
	public synchronized int issue(int bookqty)
	{
		return stock-bookqty;
	}
	
	public synchronized int returnb(int bookqty)
	{
		return stock+bookqty;
	}
		
}


class BookIssue extends Thread
{
	BookTransaction booktrans;
	int qty;
	
	public BookIssue(BookTransaction booktrans,int qty) {
		this.booktrans = booktrans;
		this.qty = qty;
		start();
	}
	
	public void run()
	{
		System.out.println("Book Issue");
		System.out.println("Stock : "+booktrans.stock);
		int stock = booktrans.issue(qty);
		System.out.println("Issued : "+qty);
		System.out.println("Curr Stock : "+stock);
	}	
}

class BookReturn extends Thread
{
	BookTransaction booktrans;
	int qty;
	
	public BookReturn(BookTransaction booktrans,int qty) {
		this.booktrans = booktrans;
		this.qty = qty;
		start();
	}
	
	public void run()
	{   System.out.println("Book Return");
		System.out.println("Stock : "+booktrans.stock);
		int stock = booktrans.returnb(qty);
		System.out.println("Returned : "+qty);
		System.out.println("Curr Stock : "+stock);
	}	
}





public class LibraryProject {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BookTransaction bookobj = new BookTransaction(1200);
		BookIssue bookIssue = new BookIssue(bookobj, 5);
		BookReturn bookReturn = new BookReturn(bookobj, 3);

	}

}
