package newChat;


import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;



public class FrameDemo1 extends Applet implements WindowListener
{
	
	Label lblv1 , lblv2 , res;
	TextField txtv1, txtv2, txtres;
	Button  add, sub, mul, div;
	
	Button btnok;
	Frame frame= new Frame();
	public void DisplayFrame()
	{
		btnok = new Button("OK");
		lblv1= new Label("Value 1:");
		lblv2= new Label("Value 2:");
		res= new Label("Result:       ");
		
		txtv1= new TextField(10);
		txtv2= new TextField(10);
		txtres= new TextField(10);
		
		
		
		lblv1.setAlignment(1);
		frame.add(lblv1);
		frame.add(txtv1);
		frame.add(lblv2);
		frame.add(txtv2);
		frame.add(res);
		frame.add(txtres);
		frame.add(btnok);
		

		frame.setTitle("Welcome");
		
		frame.setTitle("Welcome to Frame!");
		frame.setSize(500, 700);
		System.out.println("Good Morning!");
		frame.setLayout(new FlowLayout());
		
		frame.setVisible(true);
		frame.addWindowListener(this);
		

		txtres.addFocusListener(new FocusAdapter()
		{
			
			public void focusGained(FocusEvent e)
			{
				System.out.println("gained");
				setBackground(Color.RED);
				int n1=Integer.parseInt(txtv1.getText());
				int n2=Integer.parseInt(txtv2.getText());
				Integer result =n1+n2;// Autoboxing
				
				
				String s=result.toString();
				txtres.setText(s);
				paint();
			}
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				System.out.println("lost");
			}
		});
	}
	@Override
	public void windowOpened(WindowEvent e) 
	{
		System.out.println("Window Opened");
		
	}


	@Override
	public void windowClosing(WindowEvent e)
	{
		System.out.println("Window Closing");
		
		frame.setVisible(false);
		frame.dispose();
	}


	@Override
	public void windowClosed(WindowEvent e) 
	{
		System.out.println("Window Closed");
	}


	@Override
	public void windowIconified(WindowEvent e)
	{
		System.out.println("Window Iconified used bto minimized");
		
	}


	@Override
	public void windowDeiconified(WindowEvent e)
	{
		System.out.println("Window Deiconified");
		
	}


	@Override
	public void windowActivated(WindowEvent e)
	{
		System.out.println("Window Activated");
		
	}


	@Override
	public void windowDeactivated(WindowEvent e) 
	{
		System.out.println("Window Deactivated");
		
	}
	
	
	
	public void paint()
	{
		System.out.println("in paint");
	}
	public static void main(String[] args) 
	{
		new FrameDemo1().DisplayFrame();
	}	
	
	
}

