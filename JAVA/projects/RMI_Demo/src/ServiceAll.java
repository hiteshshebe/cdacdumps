import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ServiceAll extends Remote 
{
	public int add(int a,int b) throws RemoteException; 
	
	public String chatbox(String str) throws RemoteException;
	
}
