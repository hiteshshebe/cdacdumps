import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Login implements ActionListener{

	Frame f;
	Label lblUserName;
	Label lblPassword;
	
	TextField txtUserName;
	TextField txtPassword;
	
	Button btnSubmit;
	
	public void displayFrame()
	{
		f=new Frame();
		//f.setTitle("Windows AWT");
		f.setSize(300, 400);
		f.setVisible(true);

		txtUserName=new TextField(20);
		txtPassword=new TextField(20);
		
		lblUserName = new Label("Username");
		lblPassword = new Label("Password");
		
		btnSubmit = new Button("Submit");
		
		
		f.setLayout(new FlowLayout());
		
		txtPassword.setEchoChar('*');
		f.add(lblUserName);
		f.add(txtUserName);
		f.add(lblPassword);
		f.add(txtPassword);
		f.add(btnSubmit);
		btnSubmit.addActionListener(this);
		
		
		f.addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent we)
			{
				f.setVisible(false);
				f.dispose();
			}
			public void windowOpened(WindowEvent we)
			{
				f.setTitle("Welcome");
			}
			public void windowDeiconified(WindowEvent we)
			{
				f.setTitle("Hello");
			}
		}
		);
	}
	
	public static void main(String[] args) {
		
		new Login().displayFrame();
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		
		System.out.println(ae.getSource().equals(btnSubmit));
		
		System.out.println(txtUserName.getText());
		System.out.println(txtPassword.getText());
	}

}
