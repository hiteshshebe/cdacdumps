

import java.applet.Applet;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Calculator extends Applet implements ActionListener{

	Label lblUserName;
	Label lblPassword;
	
	TextField txtUserName;
	TextField txtPassword;
	
	Button btnSubmit;
	
	
	public void init()
	{
		txtUserName=new TextField(20);
		txtPassword=new TextField(20);
		
		lblUserName = new Label("Username");
		lblPassword = new Label("Password");
		
		btnSubmit = new Button("Submit");
		
	}
	public void start()
	{
		txtPassword.setEchoChar('*');
		add(lblUserName);
		add(txtUserName);
		add(lblPassword);
		add(txtPassword);
		add(btnSubmit);
		btnSubmit.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		System.out.println(ae.getActionCommand().equals("Button"));
		System.out.println(ae.equals(btnSubmit));
		System.out.println(ae.getSource().equals(btnSubmit));
		System.out.println(txtUserName.getText());
		System.out.println(txtPassword.getText());
	}
	
	public void graphics(Graphics g)
	{
		g.drawString("shailesh", 100, 200);
		
	}
		

}
