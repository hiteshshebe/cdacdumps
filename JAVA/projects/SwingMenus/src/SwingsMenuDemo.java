import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class SwingsMenuDemo implements ActionListener{

	JFrame jf;
	JMenuBar jmb;
	JMenu file;
	JMenuItem exit,validate;
	Container c;
	JLabel result;
	
	public void show()
	{
		jf=new JFrame("Hello India");
		c=jf.getContentPane();
		jf.setSize(400, 400);
		jf.setVisible(true);
		jf.setLayout(new FlowLayout());
	
		jmb=new JMenuBar();
		file=new JMenu("File");
		exit=new JMenuItem("Exit");
		validate=new JMenuItem("Valid");
		file.add(validate);
		file.add(exit);
		jmb.add(file);
		exit.addActionListener(this);
		validate.addActionListener(this);
		
		
		result=new JLabel("Hello World");
		
		c.add(jmb);
		c.add(result);
	}
	
	public static void main(String[] args) {

		new SwingsMenuDemo().show();
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		if(ae.getSource().equals(exit))
			jf.dispose();
		
		if(ae.getSource().equals(validate))
		{
			BsLogic bs=new BsLogic();
			if(bs.valid())
			{
				result.setText("Bye WOrld...");
				result.setForeground(Color.blue);
			}
		}
	}

}
