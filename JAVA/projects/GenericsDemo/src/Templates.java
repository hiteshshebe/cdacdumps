class Student
 {
	 public Student()
	 {
		 System.out.println("I M Student...");
	 }
	
	
	 // UpperBound concept
	 public static <Type extends Number> double add(Type a,Type b)
	 {
		 return a.doubleValue()+b.doubleValue();
	 }
 }
public class Templates<Type> extends Student
{
	public void display(Type ag)
	{
		System.out.println("Hello..."+ag);
	}
	

	public static void main(String[] args) {
		Templates<Integer> t=new Templates<Integer>();
		t.display(1234);
		Templates<Number> t1=new  Templates<Number>();
		t1.display(12);
		t1.display(3.456);
		t1.display(2.67f);
		
		Templates<Student> s=new Templates<Student>();
		s.display(new Student());
	
		// for upper
		System.out.println(add(10.10,10.10));
		
		
		
		
	}
}
