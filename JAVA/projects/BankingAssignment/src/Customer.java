import java.io.Serializable;


public class Customer extends Person implements Serializable{

	Account acc;
	public Customer() {
		super();
		acc=new Account();
	}
	
	public Customer(String name, int age, String address,int account_number,int balance) {
		super(name, age, address);
		this.acc=new Account(account_number, balance);
	
	}
	
	
	public void showCustomerDetails()
	{
		System.out.println("Name     	::"+name);
		System.out.println("Age      	::"+age);
		System.out.println("Addreass	::"+address);
		System.out.println("Account No	::"+acc.account_number);
		System.out.println("Balance  	::"+acc.balance);
	}
	
	
	public String toString()
	{
		String details="Name     	::"+name+"\nAge      	::"+age+"\nAddreass	::"+address+"\nAccount No	::"+acc.account_number+"\nBalance  	::"+acc.balance+"\n\n";
		return details;
	}
	

}
