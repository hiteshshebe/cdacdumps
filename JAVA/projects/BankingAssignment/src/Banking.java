import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

class Student implements Serializable
{
	int x;
	public Student(int x)
	{
		this.x=x;
	}
}

public class Banking {
	
	public void writeObject()
	{
		File f=new File("CustomerAccountDetails.dat");
		
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(f);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			Customer c=new Customer("Shailesh", 23, "Koradi", 1, 500);
			
			oos.writeObject(c);
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
		
				
	}	

	public void readObject()
	{
		File file=new File("CustomerAccountDetails.dat");
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			ObjectInputStream ois=new ObjectInputStream(fis);
			Customer c=(Customer)ois.readObject();
			System.out.println(c);
		}
		catch(ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace();
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		
		
	}
	
	public static void main(String[] args) throws IOException {

		
		
		new Banking().writeObject();		
		new Banking().readObject();
	}

}
