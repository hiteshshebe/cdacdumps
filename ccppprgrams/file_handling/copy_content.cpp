#include<iostream>
#include<stdlib.h>
#include<fstream>
using namespace std;

int main(void)
{
    char buff[1];
    
    ifstream input("file1.txt",ios::in);
    ofstream output("file2.txt",ios::out);
    
    if(input.fail())
    {
     cout<<"Unable to open the file";
     exit(1);
    }
    if(output.fail())
    {
     cout<<"Unable to open the file";
     exit(1);
    }
    
    do
    {
     input.read(buff,1);
     if(input.good())
     {
      output.write(buff,1);
     }
    }while(!input.eof());
    
    
    input.close();
    output.close();
}
