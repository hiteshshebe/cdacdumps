#include<iostream>
using namespace std;

struct Node
{
	int data;
	Node *next;
};

class LStack
{
	Node *top;

	public:
		LStack()
		{
			top=NULL;
		}
		void push(int x)
		{
			Node *new_node=new Node;
			new_node->data=x;
			

			if(top==NULL)
			{
				new_node->next=NULL;
				top=new_node;
			}
			else
			{
				new_node->next=top;
				top=new_node;
			}
		}
		void pick()
		{
			cout<<top->data<<endl;
				
		}

		void pop()
		{
			if(top==NULL)
			{
				cout<<"List is empty"<<endl;
				return;
			}
			Node *temp=top;
			top=top->next;
			int x=temp->data;
			delete temp;
			cout<<x<<"deleted"<<endl;
		}
};

int main()
{
	LStack l1;
	l1.push(10);
	l1.push(20);
	l1.push(30);
	l1.pick();
	l1.pop();
	l1.pop();
	l1.pop();
	l1.pop();
}
