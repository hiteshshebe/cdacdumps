#include<iostream>
using namespace std;

class Node
{
      public:
             int data;
             Node *link;
};

class LStack
{
      Node *top;
      public:
             LStack()
             {
                  top=NULL;
             }
             void push(int x)
             {
                  if(top==NULL)
                  {
                      top=new Node;
                      top->data=x;
                  }
                  else
                  {
                      Node *temp=new Node;
                      temp->data=x;
                      temp->link=top;
                      top=temp;
                  }
             }
             int pop()
             {
                  Node *temp=top;
                  int x=top->data;
                  top=top->link;
                  delete temp;
                  return x;
             }
             void pick()
             {
                  cout<<top->data;
             }
};

int main()
{
    LStack l1;
    l1.push(10);
    l1.push(20);
    l1.push(30);
    l1.pick();
    l1.pop();
    l1.pick();
}
