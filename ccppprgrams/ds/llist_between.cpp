#include<iostream>
using namespace std;

struct Node
{
       public:
              int data;
              Node *next;
};

class LList
{
      Node *head;
      public:
             LList()
             {                       
              head=NULL;
             }
             void insert(int x,int i)
             {
                  Node *newNode=new Node;
                  newNode->data=x;
                  newNode->next=NULL;
                  
                  if(i<0)
                  {
                         cout<<"Invalid insertion";
                         return;
                  }
                  
                  if(i==0)
                  {
                   head=newNode;
                   return;
                  }
                  else
                  {
                  int currIndex=1;
                  Node *p=head;
                  while(p && i>currIndex)
                  {
                   p=p->next;
                   currIndex++;
                  }
             
                  newNode->next=p->next;
                  p->next=newNode;
                  
                  }
             }
             
             void display()
             {
              Node *p;
              p=head;
              while(p!=NULL)
              {
               cout<<p->data<<endl;
               p=p->next;
              }
             }
      
      
};

int main(void)
{
    LList l1;
    l1.insert(10,0);
    l1.insert(20,1);
    l1.insert(30,2);
    l1.insert(40,3);
    l1.display();
    cout<<endl;
    l1.insert(50,1);
    l1.display();
}
