<?php
// file type
header("Content-type: image/png");
// create image
$img = imagecreatetruecolor(400, 400);
// set background to light gray
imagefill($img,0,0,imagecolorallocate($img, 221, 221, 221));
// draw ellipses with random colors
for ($i=20;$i>=1;$i--) {
 imagefilledellipse($img, 200,200,$i*14,$i*10,
 imagecolorallocate($img, rand(50,255), rand(50,255), rand(50,255)));
}
// display image
imagepng($img);
// release image from memory
imagedestroy($img);
?>