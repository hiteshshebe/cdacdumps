<?php
$bad_ext=="true";
// This is the temporary file created by PHP
$uploadedfile = $_FILES['userfile']['tmp_name'];
//get the extension to determine the file type
$ext=exif_imagetype($uploadedfile);
//IMAGETYPE_GIF
if ($ext==1) {
   // Create an Image from it so we can do the resize
   $src = @imagecreatefromgif($uploadedfile) or die ($errmsg);
   $bad_ext="false";
}
//IMAGETYPE_JPEG
if ($ext==2) {
   // Create an Image from it so we can do the resize
   $src = @imagecreatefromjpeg($uploadedfile) or die ($errmsg);
   $bad_ext="false";
}
if ($bad_ext=="false") {
   // Capture the original size of the uploaded image
   list($width,$height)=@getimagesize($uploadedfile);
   // The image is resized, while keeping the height and width ratio intact
   $newwidth=200;
   $newheight=($height/$width)*200;
   $tmp=@imagecreatetruecolor($newwidth,$newheight);
   // this line resizes the image, copying the original image into $tmp
   imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
   // The resized image is written to disk.
   $filename = $_FILES['userfile']['name'];
   if ($ext==1) {
      imagegif($tmp,$filename,100);
      $newfilename="myphoto.gif";
   }
   if ($ext==2) {
      imagejpeg($tmp,$filename,100);
      $newfilename="myphoto.jpg";
   }
   imagedestroy($src);
   imagedestroy($tmp); 
   // remove file if it is already there
   if (file_exists($newfilename)) {
      unlink ($newfilename);
   }
   rename($filename,$newfilename);
   echo header("Location: form1.php");
} else {
   echo header("Location: form1.php?Error=true");
}
?>