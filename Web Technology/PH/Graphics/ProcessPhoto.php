<?php
// File and rotation
$filename = 'myphoto.jpg';
// Content type
header('Content-type: image/jpeg');
// Get new sizes
list($width, $height) = getimagesize($filename);
$newwidth=$_REQUEST['resize'];
$newheight=($height/$width)*$_REQUEST['resize'];
// Load
$dest = imagecreatetruecolor($newwidth, $newheight);
$source = imagecreatefromjpeg($filename);
// Resize
imagecopyresized($dest,$source,0,0,0,0,$newwidth,$newheight,$width,$height);
imagejpeg($dest, $filename);
$source = imagecreatefromjpeg($filename);
// Rotate
$degrees = $_REQUEST['rotation']; 
$rotate = imagerotate($source, $degrees, 0);
// Output
imagejpeg($rotate);
?>