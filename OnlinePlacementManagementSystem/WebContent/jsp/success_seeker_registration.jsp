<%@page import="java.sql.Statement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Seeker Registration Successfully</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">


<%
		out.println("<h3>Your account has been created successfully.</h3>");
		out.println("<a href='login_seeker.jsp'>Login Now</a>");
%>


</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
</body>
</html>