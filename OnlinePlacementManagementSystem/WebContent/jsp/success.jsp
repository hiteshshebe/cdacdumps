<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
</head>
<body>
<jsp:include page="header_menu.jsp"/>

<div id="templatemo_main">
<Marquee><h3>Successfully Registered.</h3></marquee>
</div>
</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>
</body>
</html>