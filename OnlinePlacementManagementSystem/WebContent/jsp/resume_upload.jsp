<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>
<%
String sid=null;
int seeker_id=0;
try
{
	sid=session.getAttribute("seeker_session_id").toString();
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_seeker.jsp");
	}
%>
<body id="home">

<jsp:include page="header_menu.jsp"/>

<div id="templatemo_main" style="height:400px">

		<a href="home_seeker.jsp">Back</a>
		<a href="show_jobs.jsp">Show Jobs</a>  
		<a href="show_applied_jobs.jsp">Show Applied Jobs</a>
		<a href="logout.jsp">Logout</a>

<div align="center">
<h3>Upload Resume</h3>
<s:actionerror/>
<s:form action="uploadAction" method="POST" enctype="multipart/form-data">
   <s:file name="uploadFile" label="Choose File" size="40" />
   <s:submit value="Upload" name="submit" />
</s:form>
</div>
</div>

<jsp:include page="footer.jsp"/>
</body>
</html>