<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Resume Upload</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />
</head>

<body id="home">

<jsp:include page="header_menu.jsp"/>

<div id="templatemo_main" style="height:400px">

<a href="home_seeker.jsp">Home</a>
<a href="resume_upload.jsp">Back</a>
<a href="logout.jsp">Logout</a><br><br>


<center>
<h3>Resume uploaded successfully...!!!</h3>
</center>

   File Name : <s:property value="uploadFileFileName"/><br>
   <%-- Content Type : <s:property value="uploadFileContentType"/> <br>
   Temp File Name : <s:property value="uploadFile"/> --%>

</div><!-- END OF CONTENT HERE -->


<jsp:include page="footer.jsp"/>


   
</body>
</html>