<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Seekers</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />


</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" >

<a href="logout.jsp">Logout</a><br><br>
<%

String sid=null;
int admin_id;
try
{
	sid=session.getAttribute("admin_session_id").toString();
	
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_admin.jsp");
	}
%>
<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;

try 
{
		conn=new DBConnection().getConnection();		
		
		sql="select s.status,s.seeker_id, s.email_id,s.password,s.first_name,s.last_name,to_char(s.dob,'dd-Mon-yy') Dob,s.qualification,s.stream,s.percentage,s.experience,s.address,s.city,s.country,s.pin,s.contact_number from seekers_tb s";
			
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		out.println("<h3>List of Seekers</h3>");
		
		/* out.println("<table border='1'><th>seeker id<th>Email id<th>Password<th>First Name<th>Last Name<th>Date of Birth<th>Qualification<th>Stream<th>Percentage<th>Experience<th>Address<th>city<th>Country<th>Pin<th>Contact Number</th>"); */
		out.println("<table border='1' align='center'><th>seeker id<th>Email id<th>First Name<th>Last Name<th>Contact Number<th>Current Status<th>Action");
		
		while(rs.next())
		{
			int cs=rs.getInt("status");
			String curr_status;
			
			if(cs==0)
				curr_status="Blocked";
			else
				curr_status="Active";
					
			out.println("<tr><td>"+rs.getInt("seeker_id")+
							"</td><td>"+rs.getString("email_id")+
							"</td><td>"+rs.getString("first_name")+
							"</td><td>"+rs.getString("last_name")+
							"</td><td>"+rs.getString("contact_number")+
							"</td><td><b>"+curr_status+"</b></td>"+
							"<td><form action='update_seeker_acc.jsp'>"+
							"<input type='hidden' name='seeker_id' value='"+rs.getInt("seeker_id")+"'>"+
							"<input type='radio' name='status' value='1'>Activate"+
							"<input type='radio' name='status' value='0'>Block"+
							"<input type='submit' value='Update'></td>"+
							"</tr></form>"
					);
							
		}
		out.println("</table>");
		
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>

 </div><!-- END OF CONTENT HERE -->

</body>
</html>