package com.pojos;

import java.util.Date;

public class SeekerPojo 
{
	int seeker_id;
	String email_id;  
	String password;
	String first_name;
	String last_name;
	String gender;
	String dob;
	String qualification;
	String stream;
	double percentage;
	String experience; 
	String address;
	String city;
	String country;
	int pin;
	String contact_number;
	int status;
	String resume_path;
	public SeekerPojo() {
		super();
	}
	public SeekerPojo(int seeker_id, String email_id, String password,
			String first_name, String last_name, String gender, String dob,
			String qualification, String stream, double percentage,
			String experience, String address, String city, String country,
			int pin, String contact_number, int status, String resume_path) {
		super();
		this.seeker_id = seeker_id;
		this.email_id = email_id;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.dob = dob;
		this.qualification = qualification;
		this.stream = stream;
		this.percentage = percentage;
		this.experience = experience;
		this.address = address;
		this.city = city;
		this.country = country;
		this.pin = pin;
		this.contact_number = contact_number;
		this.status = status;
		this.resume_path = resume_path;
	}
	public int getSeeker_id() {
		return seeker_id;
	}
	public void setSeeker_id(int seeker_id) {
		this.seeker_id = seeker_id;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getStream() {
		return stream;
	}
	public void setStream(String stream) {
		this.stream = stream;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getResume_path() {
		return resume_path;
	}
	public void setResume_path(String resume_path) {
		this.resume_path = resume_path;
	}
	
}
