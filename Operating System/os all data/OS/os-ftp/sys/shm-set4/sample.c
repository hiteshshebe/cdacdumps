#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid,ret;
	void* pv;
	shmid=shmget(KEY,256,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}
	ret=fork();
	if(ret==0)
	{
		pv=shmat(shmid,0,0);
		//sleep(1);
		strcpy((char*)pv,"SHM with fork");
		shmdt(pv);
	}
	else { //parent
		pv=shmat(shmid,0,0);
		sleep(1);
		printf("child says:%s\n",(char*)pv);
		shmdt(pv);
	}
	return 0;
}
	
