#include<unistd.h>
#include<sys/shm.h>

#define KEY 1234

int main()
{
	int shmid;
	shmid=shmget(KEY,256,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}
	
	void *pv;
	pv=shmat(shmid,0,0); //pv=malloc(256);

	int* ptr=pv;
	*ptr=18;  //print *ptr

	char* ps;
	ps=pv+4;
	strcpy(ps,"Hello SHM\n"); //print ps
	printf("data written to shm\n");

	shmdt(pv);
	return 0;
}
