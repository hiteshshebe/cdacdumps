#include<unistd.h>
#include<sys/stat.h>

int main(int argc,char* argv[])
{
	int k;
	struct stat s1;

	if(argc==2)
		k=lstat(argv[1],&s1);
	//else
	//	k=lstat("simple.dat",&s1);
	if(k<0) {
		perror("lstat");
		exit(0);
	}
	printf("file name: %s\n",argv[1]);
	printf("inode no:%d\n",s1.st_ino);
	printf("no.of links:%d\n",s1.st_nlink);
	printf("mode is %o\n",s1.st_mode);
	printf("uid=%d,gid=%d\n",s1.st_uid,s1.st_gid);
	printf("size=%d bytes\n",s1.st_size);
	printf("last mod time %s\n",ctime(&s1.st_mtime));
	if(S_ISREG(s1.st_mode))
		printf("regular file\n");
	else if(S_ISDIR(s1.st_mode))
		printf("dir file\n");
	else if(S_ISLNK(s1.st_mode))
		printf("symlink\n");
	else
		printf("other file\n");
	return 0;
}

