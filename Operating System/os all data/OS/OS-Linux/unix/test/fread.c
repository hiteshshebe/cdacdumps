#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("simple.dat",O_RDONLY);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	char buf[64];
	k=read(fd,buf,64);
	if(k<0) {
		perror("read");
		exit(0);
	}
	write(1,buf,k);
	k=write(fd,"abcd",4);
	if(k<0) {
		perror("write");
		exit(0);
	}
	close(fd);
	return 0;
}


