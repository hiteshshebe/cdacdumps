#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("alpha",O_WRONLY|O_CREAT,0666);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	k=write(fd,"ABCDEFGHIJKLMNOPQRSTUVWXYZ\n",28);
	close(fd);

	char buf[100];
	fd=open("alpha",O_RDONLY);

	k=read(fd,buf,5);
	write(1,buf,k);
	printf("\nsecond read\n");

	lseek(fd,10,SEEK_SET);//offset-10
	//lseek(fd,10,SEEK_CUR);//cur-5, new offset-15
	//lseek(fd,-12,SEEK_END);//28-12=16
	//lseek(fd,-6,SEEK_CUR); //16-6=10
	k=read(fd,buf,5);
	write(1,buf,k);

	close(fd);


	return 0;
}
