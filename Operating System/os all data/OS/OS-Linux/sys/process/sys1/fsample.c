#include<unistd.h>

int main()
{
	int ret,i,max=100;
	printf("Welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		_exit(0);
	}
	if(ret==0)
	{
		printf("child-pid=%d,ppid=%d\n",getpid(),getppid());
		for(i=0;i<max;i++)
			printf("child-Thank you-%d\n",i+1);
		_exit(0);
	}
	else //ret>0
	{
		printf("parent-ret=%d\n",ret);
		printf("parent-pid=%d,ppid=%d\n",getpid(),getppid());
		for(i=0;i<max;i++)
			printf("parent-Thank you-%d\n",i+1);

	}
	return 0;
}
