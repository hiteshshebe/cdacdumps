#include<unistd.h>
#include<sys/msg.h>

#define KEY 0x1234

int main()
{
	int mid,k;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}

	char buf[]="0000ABCDEFGHIJKLMNOPQRST\n";
	k=msgsnd(mid,buf,sizeof(buf),0);
	if(k<0) {
		perror("msgsnd");
		exit(0);
	}
	printf("msg sent successfully\n");
	return 0;
}
