#include<pthread.h>

const int max=10;
int i;
pthread_mutex_t m1;
void* efun1(void* pv)
{
	//int i;
	printf("A:Hello\n");
	printf("i am thread:A\n");
	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++)
	{
		printf("thread A--%d\n",i);
		//access to shared res
		sleep(1);
	}
	pthread_mutex_unlock(&m1);
}
void* efun2(void* pv)
{
	printf("B:Hello\n");
	printf("i am thread:B\n");

	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++)
	{
		printf("thread B--%d\n",i);
		//access to shared res
		sleep(1);
	}
	pthread_mutex_unlock(&m1);
}

int main()
{
	pthread_t t1,t2;
	
	pthread_mutex_init(&m1,NULL);

	pthread_create(&t1,NULL,efun1,NULL);
	pthread_create(&t2,NULL,efun2,NULL);

	//sleep(5);
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	pthread_mutex_destroy(&m1);
	printf("main--thank you\n");
	return 0;
}


