#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k;
	fd=open("sample.dat",O_WRONLY|O_CREAT,0666);
	if(fd<0) {
		perror("open");
		exit(0);
	}
	char buf[]="ABCDEFGHIJKLMNOPQRST\n";
	k=write(fd,buf,sizeof(buf));
	if(k<0) {
		perror("write");
		exit(0);
	}
	printf("written to file,k=%d\n",k);
	close(fd);
	return 0;
}

