#include<unistd.h>
#include<fcntl.h>

int main()
{
	int k,ret,fd;
	char buf[20];
	k=mkfifo("/tmp/f1",0666);
	if(k<0) {
		perror("pipe");
		exit(0);
	}
	ret=fork();
	if(ret==0)  //writing
	{
		fd=open("/tmp/f1",O_WRONLY);
		write(fd,"Hello FIFO\n",12);
		close(fd);
		exit(0);
	}
	else 	    //reading
	{
		fd=open("/tmp/f1",O_RDONLY);
		write(fd,"Hello FIFO\n",12);
		k=read(fd,buf,20);
		printf("in parent\n");
		write(1,buf,k);
		close(fd);
	}
	return 0;
}
