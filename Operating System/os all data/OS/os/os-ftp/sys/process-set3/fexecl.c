#include<unistd.h>

int main()
{
	int ret,k,x;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) {
		printf("child-welcome\n");
		k=execl("/bin/lt","ls",NULL);
	       //k=execl("/usr/bin/cal","cal","10","2015",NULL);
		//replacement of current workspace
		if(k<0)
			perror("execl");
		printf("child--thank you\n");
		//unreachable on success of execl
		exit(0);
	}
	else {//ret>0
		printf("parent-thank you\n");
		waitpid(-1,&x,0);
	}
	//stmt
	return 0;
}

