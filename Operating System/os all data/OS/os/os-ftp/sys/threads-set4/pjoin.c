#include<pthread.h>

void* efun1(void* ptr)
{
	int i;
	printf("I am thread-A\n");
	for(i=1;i<=100;i++){
		printf("A--%d\n",i);
	}
	pthread_exit(NULL);
}
void* efun2(void* ptr)
{
	int i;
	printf("I am thread-B\n");
	for(i=1;i<=100;i++) {
		printf("B--%d\n",i);
	}
	pthread_exit(NULL);
}
int main()
{
	int i;
	pthread_t pt1,pt2;
	//efun1(NULL);
	//efun2(NULL);
	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	for(i=1;i<=10;i++){
		printf("main--%d\n",i);
	}
	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);
	printf("main--thank you\n");

	return 0;
}
