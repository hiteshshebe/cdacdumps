#include<unistd.h>
#include<sys/stat.h>

int main(int argc,char* argv[])
{
	int k;
	struct stat sb;
	k=lstat(argv[1],&sb);
	if(k<0)
	{
		perror("lstat");
		exit(0);
	}
	printf("inode no:%d\n",sb.st_ino);
	printf("link count:%d\n",sb.st_nlink);
	printf("mode : %o\n",sb.st_mode);
	printf("uid=%d,gid=%d\n",	
			sb.st_uid,sb.st_gid);
	printf("size=%d,blksize=%d,nblocks=%d\n",
		sb.st_size,sb.st_blksize,sb.st_blocks);
	printf("mod time is %s\n",
			ctime(&sb.st_mtime));
	if(S_ISREG(sb.st_mode))
		printf("regular file\n");
	if(S_ISDIR(sb.st_mode))
		printf("dir file\n");
	if(S_ISLNK(sb.st_mode))
		printf("sym link file\n");
	
	return 0;
}
