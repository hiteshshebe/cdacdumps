#include<unistd.h>
#include<sys/msg.h>

#define KEY 0x1234

int main()
{
	int mid,k;
	char buf[]="    ABCDEFGHIJKLMNOPQRST\n";
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	k=msgsnd(mid,buf,sizeof(buf),0);
	if(k<0) {
		perror("msgsnd");
		exit(0);
	}
	printf("msg was sent successfully\n");
	return 0;
}


