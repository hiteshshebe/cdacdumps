#include<unistd.h>
#include<sys/msg.h>

#define KEY 0x1234

int main()
{
	int mid,k;
	char buf[100]={0};
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	//k=msgrcv(mid,buf,30,0,0);
	//k=msgrcv(mid,buf,30,0,IPC_NOWAIT);
	//k=msgrcv(mid,buf,10,0,0);
	k=msgrcv(mid,buf,10,0,MSG_NOERROR);
	//k=msgrcv(mid,buf,30,0x44434241,MSG_NOERROR|IPC_NOWAIT);
	if(k<0) {
		perror("msgrcv");
		exit(0);
	}
	printf("msg length=%d\n",k);
	printf("buf=%s\n",buf);
	return 0;
}


