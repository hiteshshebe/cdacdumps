#include<unistd.h>
#include<sys/msg.h>

#define KEY 0x1234

int main()
{
	int mid,k;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}

	char str[128]={};
	k=msgrcv(mid,str,40,0,0);
	//k=msgrcv(mid,str,40,0,IPC_NOWAIT);//msgrcv-non block
	//k=msgrcv(mid,str,10,0,0); //error,lengthy msg>10
	//k=msgrcv(mid,str,10,0,MSG_NOERROR);
		//truncate msg and copy only 10 bytes,
		//remaining msg is lost
	//k=msgrcv(mid,str,40,5,0);

	if(k<0) {
		printf("k=%d\n",k);
		perror("msgrcv");
		exit(0);
	}
	printf("k=%d, str=%s\n",k,str);
	return 0;
}
