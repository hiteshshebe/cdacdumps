#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,st,a;
	ret=fork();
	if(ret==0) //child process
	{
		sleep(1);
		printf("child ,process,pid=%d,ppid=%d\n",
				getpid(),getppid());
		printf("child--thank you\n");
		exit(0); //_exit(0);
	}
	else //ret>0
	{
		printf("parent process-pid=%d,ppid=%d\n",
				getpid(),getppid());
		getchar();
		//sleep(5);
		waitpid(-1,&st,0);
		if(WIFEXITED(st))
			printf("normal--status=%d\n",WEXITSTATUS(st));
		else
			printf("abnormal termination\n");
		//waitpid(ret,&st,0);//waiting for particular child
		//whose pid=ret */
		printf("parent-thank you\n");
	}
	return 0;
}

