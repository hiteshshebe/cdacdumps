#include<pthread.h>

const int max=50;
void* efun1(void* pv)
{
	int i;
	printf("Hello, i am thread:A\n");
	for(i=1;i<=max;i++)
		printf("thread A--%d\n",i);
}
void* efun2(void* pv)
{
	int i;
	printf("Hello, i am thread:B\n");
	for(i=1;i<=max;i++)
		printf("thread B--%d\n",i);
}

int main()
{
	pthread_t t1,t2;

	pthread_create(&t1,NULL,efun1,NULL);
	pthread_create(&t2,NULL,efun2,NULL);

	//sleep(5);
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	printf("main--thank you\n");
	pthread_exit(NULL); //--not suggestible
	return 0;
}


