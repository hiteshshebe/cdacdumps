
fact()
{
	if [ $1 -eq 0 ]
	then
		return 1
	fi
	let m=$1-1
	fact $m      #fact `expr $1 - 1`
	let f=$?*$1
	return $f    #return `expr $? \* $1`
}

fact 5
echo "fact is $?"
