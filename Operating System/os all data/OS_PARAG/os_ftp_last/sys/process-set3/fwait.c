#include<unistd.h>
#include<sys/wait.h>

int main()
{
	int ret,i,st;
	printf("welcome\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) {
		int *ptr=NULL;
		for(i=1;i<=10;i++)
		printf("child-pid=%d,ppid=%d\n",
			getpid(),getppid());
		//*ptr=10;
		exit(5);
	}
	else {//ret>0
		printf("parent-pid=%d,ppid=%d,ret=%d\n",
			getpid(),getppid(),ret);
		//waitpid(ret,&st,0);
		waitpid(-1,&st,0);//blocking call
		if(WIFEXITED(st))
		  printf("normal,exit status=%d\n",
				WEXITSTATUS(st));
		else
		  printf("abnormal termination\n");
		printf("parent-thank you\n");
	}
	return 0;
}

