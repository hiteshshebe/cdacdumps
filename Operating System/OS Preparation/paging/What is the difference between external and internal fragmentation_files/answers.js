(function() {
    if (window.chitika_units === undefined) {
	window.chitika_units = [];
    }
    
    var unit = {
	'client':   'ann_0102',
	'type':     'mpu',
	'width':    623,
	'height':   181,
	'cid':      'bottom',
	'sid':      typeof window.chitika_category != 'undefined' ? window.chitika_category : 'default',
	'fluidH':   true
    };
    
    function ch_ad_locate(obj) {
        var objPos = {'x': 0, 'y': 0};
        try {
            if (obj.offsetParent) {
                while (obj) {
                    objPos.x += obj.offsetLeft;
                    objPos.y += obj.offsetTop;
                    obj = obj.offsetParent;
                }
            } else if (obj.x && obj.y) {
                objPos.x += obj.x;
                objPos.y += obj.y;
            }
        } catch (err) {
            objPos.x = -1;
            objPos.y = -1;
        }
        return objPos;
    }
    
    try{
    	unit.query = document.getElementById('catsList').getElementsByTagName('a')[0].innerHTML;
    }catch(e){}

    var placement_id = window.chitika_units.length;
    window.chitika_units.push(unit);

    document.write('<div id="chitikaAdBlock-' + placement_id.toString() + '"></div>');
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'http://scripts.chitika.net/eminimalls/amm-yahoo.js';
    
    if(typeof window.chitika_category != 'undefined' && window.chitika_category == 'Array'){
    	unit.foundAChannel = false;
    	try{
    		var spans=document.getElementById('outer3').getElementsByTagName('span');
			for( i=0;i<spans.length;i++) {
				if(!unit.foundAChannel && spans[i].getAttribute('typeof') && spans[i].getAttribute('typeof').indexOf('v:Breadcrumb') != -1){
					var as = spans[i].getElementsByTagName('a');
					for(c=0;c<as.length;c++){
						var a = as[c];
						if(a.href && /answers\.com\/Q\/FAQ\/[0-9]+/.test(a.href.toString())){
							unit.sid = a.href.toString().match(/answers\.com\/Q\/FAQ\/([0-9]+)/)[1];
							unit.cid = a.href.toString().match(/answers\.com\/Q\/FAQ\/([0-9]+)/)[1];
							unit.foundAChannel = true;
							break;
						}
					}
				}
			}
			var beacon = document.getElementById("chitikaAdBlock-"+placement_id.toString() );
	    	if(ch_ad_locate(beacon).y < 1000){
	    		unit.cid += '_mid';
	    		unit.sid += '_mid';
	    	}
	    	else{
	    		unit.cid += '_bot';
	    		unit.sid += '_bot';
	    	}
    	}catch(e){}
    }
    
    try {
	document.getElementsByTagName('head')[0].appendChild(s);
    } catch(e) {
	document.write(s.outerHTML);
    }
}());
