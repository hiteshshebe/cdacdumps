(function () {
    "use strict";
    // Some global variables; very boring
    var ch_ad_url           = '';
    var ch_amm_version      = "1.5.4";
    var snippet_count       = 1;
    var snippet_length      = 100;
    var snippet_priority    = ['title'];

    ////////////////////////////////////////////////////////////////
    // Shorthand functions
    function dq(s) { return (s !== null) ? '"' + s + '"' : '""'; }                  // Return input surrounded by double-quotes
    function ch_au(p, v) { if (v) { ch_ad_url += '&' + p + '=' + v; } }             // Append key/value pair to active ad URL query string
    function ch_aue(p, v) { if (v) { ch_au(p, encodeURIComponent(v)); } }           // Append key/ESCAPED value pair to active ad URL query string
    function ch_def(v, def) { if (v) { return v; } else { return def; } }           // If defined, return v, else return def(ault)

    function ch_add_script(url, d) {
        if (d === undefined) {d = document;}
        if (typeof(url) !== 'string') {return undefined;}
        var h = d.getElementsByTagName('head')[0];
        if (!h) {return undefined;}
        var s = d.createElement('script');
        s.type = 'text/javascript';
        s.src = url;
        h.appendChild(s);
        return s;
    }

    function get_real_height(body) {
        if (navigator.userAgent.match(/MSIE [6-8]\.0/)) {
            return body.scrollHeight;
        }
        return body.offsetHeight;
    }

    // Set the callback function within the iframe.
    function create_callback(frame, clones, w) {
        frame.contentWindow.render_ad = function(response) {
            if (response === undefined) {response = frame.contentWindow.ch_mmhtml;}
            if (clones === undefined ||
                clones == null ||
                clones.length === undefined) {
                clones = new Array();
            }

            if (response === undefined) {
                frame.style.display = 'none';
                // Make sure you always close your iframe no matter what!
                frame.contentWindow.document.close();
                for (var i = 0; i < clones.length; i++) {
                    clones[i].style.display = 'none';
                }
                return;
            }

            // Validate the response.
            if (response.output) {
                try {
                    // Fall back to writing the content, which breaks in most browsers, just not IE6
                    frame.contentWindow.document.open();
                    frame.contentWindow.document.write(response.output);
                    frame.contentWindow.document.close();

                    // If fluid height has been enabled, enact it.
                    if (frame.ch_resize !== undefined) {
                        setTimeout(function() { frame.ch_resize(); }, 1);                   // 1 ms to make IE6 behave (halves height otherwise)
                    }
                } catch (e) {
                    // Try setting the innerHTML; this works for most browsers, just not IE6
                    frame.contentWindow.document.innerHTML = response.output;
                }

                // Go through cloned brethren and also render ads.
                for (var i = 0; i < clones.length; i++) {
                    var cloneFrame = clones[i];
                    try {
                        // Fall back to writing the content, which breaks in most browsers, just not IE6
                        cloneFrame.contentWindow.document.open();
                        cloneFrame.contentWindow.document.write(response.output);
                        cloneFrame.contentWindow.document.close();

                        // If fluid height has been enabled, enact it.
                        if (cloneFrame.ch_resize !== undefined) {
                            setTimeout(function() { cloneFrame.ch_resize(); }, 1);      // 1 ms to make IE6 behave (halves height otherwise)
                        }
                    } catch (e) {
                        // Try setting the innerHTML; this works for most browsers, just not IE6
                        cloneFrame.contentWindow.document.innerHTML = response.output;
                    }
                    // Do frame specific callbacks
                    if (w.cloneCallbacks !== undefined &&
                        w.cloneCallbacks[i.toString()]) {
                        w.cloneCallbacks[i.toString()](cloneFrame);
                    }
                }
            } else if (response.alturl) {
                frame.src = response.alturl;
                for (var i = 0; i < clones.length; i++) {
                    clones[i].src = response.alturl;
                }
            } else {
                frame.style.display = 'none';
                frame.contentWindow.document.close();
                for (var i = 0; i < clones.length; i++) {
                    clones[i].style.display = 'none';
                }
                return;
            }
        }
    }

    ////////////////////////////////////////////////////////////////
    // get_snippets
    //
    // Gathers snippets of text from the page which can be used for
    // targeting.
    // No return; automagically appends to request URL
    function get_snippets() {
        var data    = {},
            meta    = undefined,
            h1      = undefined,
            id      = undefined,
            count   = 0;

        // 2. Grab the title if possible.
        if (document.title) {
            data['title'] = document.title;
        }

        // 3. Take the first snippet_count snippets and append them to the URL,
        //    taken in the order of snippet_priority
        for (var i = 0; i < snippet_priority.length && count < snippet_count; i++) {
            id = snippet_priority[i];

            if (data[id]) {
                ch_aue('snip_' + id, data[id].substring(0, snippet_length));
                ++count;
            }
        }
    }

    ////////////////////////////////////////////////////////////////
    // Used for appending functionality to events.
    // window.onload = append_func(window.onload, newFunction);
    // Returns anonymous function which executes window.onload then
    // newFunction
    function append_func(o, a) {
        return function (e) {
            if (typeof o === "function") { o(e); }
            return a(e);
        };
    }

    ////////////////////////////////////////////////////////////////
    // FLUID WIDTH FUNCTIONALITY
    // Causes the Ad Unit to stretch to 100% of available width
    function ch_apply_fluid_width(ctr, obj) {
        try {
            //try { ctr.style.zoom = 1; } catch (eZoom) {}                            // Makes IE6 behave. Zoom is not a standard property, so suppress any possible errors in other browsers
            ctr.style.width = "auto";                                               // Width auto and overflow hidden trigger a special box rendering mode, which causes
            ctr.style.overflow = "hidden";                                          // the element to not flow under floated elements
            obj.style.width = "100%";                                               // Thus allowing us to specify 100% width on the iframe without breaking the page
        } catch (error) {
        }
    }

    ////////////////////////////////////////////////////////////////
    // Needed for IE6; can't append a script tag without a head
    // element. This is supplied by most browsers implicitly, just
    // not IE6
    function ch_write_empty_iframe(i) {
        var w = i.contentWindow;
        var d = w.document;

        d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head></head><body></body></html>');
    }

    ////////////////////////////////////////////////////////////////
    // FLUID HEIGHT FUNCTIONALITY
    // Causes the Ad Unit to use only as much vertical space as
    // it's content requires
    function ch_apply_fluid_height(obj) {
        // We define the function here to create a closure around obj
        var ch_resize_height = function () {
            try {
                // obj will retain its value due to closure, so use it here to be explicit about what
                // we're using ("this" would be the other option, and would not allow resizing of the
                // external iframe element)
                var height = get_real_height(obj.contentWindow.document.body);
                //var height = obj.contentWindow.document.body.scrollHeight;

                // Set event listeners for resizing on the first pass through
                if (!obj.bResizeSet) {
                    if (window.addEventListener) {
                        // If browser is standards compliant, set onresize event directly
                        obj.contentWindow.onresize = append_func(obj.contentWindow.onresize, obj.onload);
                    } else {
                        // Or, if it's IE, use attachEvent
                        obj.contentWindow.attachEvent("onresize", obj.onload);
                    }
                    obj.bResizeSet = true;
                }
                obj.style.height = height + "px";
            } catch (e) {
            }
            return true;
        };

        // We need to monitor for a few events, including window and iframe resizing.
        // This helps to ensure fluid height plays nicely with fluid width.
        if (window.addEventListener) {                                          // This is the standardized method, but we only use it for browser
            obj.onload      = append_func(obj.onload, ch_resize_height);        // detection since we can assign functions directly
            window.onresize = append_func(window.onresize, ch_resize_height);
        } else {                                                                // IE doesn't use the standardized function and we have to
            obj.attachEvent("onload",                                           // use attachEvent, because it doesn't like direct assignment
                          append_func(obj.onload, ch_resize_height));           // of onload.
            window.attachEvent("onresize",
                               append_func(window.onreszize, ch_resize_height));
        }

        obj.ch_resize = ch_resize_height;
    }

    ////////////////////////////////////////////////////////////////
    // Walks up the DOM tree from obj, accumulating offset left and
    // top to find X, Y position for obj
    function ch_ad_locate(obj) {
        var objPos = {'x': 0, 'y': 0};
        try {
            if (obj.offsetParent) {
                while (obj) {
                    objPos.x += obj.offsetLeft;
                    objPos.y += obj.offsetTop;
                    obj = obj.offsetParent;
                }
            } else if (obj.x && obj.y) {
                objPos.x += obj.x;
                objPos.y += obj.y;
            }
        } catch (err) {
            objPos.x = -1;
            objPos.y = -1;
        }
        return objPos;
    }

    ////////////////////////////////////////////////////////////////
    // Shorthand for creating an iframe element.
    function ch_create_iframe(parameters) {
        // Define a function for setting iframe attributes from a hash. Needs to function
        // recursively for properties like style.
        function set(o, p) {
            var k = null;
            for (k in p) {
                if (typeof p[k] !== 'function') {
                    if (typeof p[k] === 'object') {
                        set(o[k], p[k]);
                    } else {
                        o[k] = p[k];
                    }
                }
            }
        }

        // Make sure we have a parameters object, even if it's empty.
        if (parameters === undefined) {
            parameters = {};
        }

        // Set default parameter values; use ch_def here to avoid overriding supplied parameters
        parameters.src              = ch_def(parameters.src,            'about:blank');
        parameters.border           = ch_def(parameters.border,         '0');
        parameters.padding          = ch_def(parameters.padding,        '0');
        parameters.frameBorder      = ch_def(parameters.frameBorder,    '0');
        parameters.marginWidth      = ch_def(parameters.marginWidth,    '0');
        parameters.marginHeight     = ch_def(parameters.margineHeight,  '0');
        parameters.vspace           = ch_def(parameters.vspace,         '0');
        parameters.hspace           = ch_def(parameters.hspace,         '0');
        parameters.scrolling        = ch_def(parameters.scrolling,      'no');
        parameters.className        = ch_def(parameters.className,      '');
        parameters.width            = ch_def(parameters.width,          '0');
        parameters.height           = ch_def(parameters.height,         '0');
        parameters.style            = ch_def(parameters.style,          {});
        parameters.style.margin     = ch_def(parameters.style.margin,   '0');
        parameters.style.padding    = ch_def(parameters.style.padding,  '0');

        // Create the iframe
        var i = document.createElement('iframe');

        // Set the parameters
        set(i, parameters);

        // Return the iframe.
        return i;
    }

    ////////////////////////////////////////////////////////////////
    // Gets the APPLIED (read: internal browser state) styleProp
    // for element x.
    function ch_get_style(x, styleProp) {
        var style = null;
        try {
            if (x.currentStyle) {
                return x.currentStyle[styleProp];
            } else if (window.getComputedStyle) {
                style = document.defaultView.getComputedStyle(x, null);
                if (style) {
                    return style.getPropertyValue(styleProp)
                }
            }
        } catch (e) {}
        return null;
    }

    ////////////////////////////////////////////////////////////////
    // Main function; generates Chitika request
    function ch_execute() {
        // Shorthand accessor for window
        var amm_host        = "mm.chitika.net",
            ch_screen       = screen.width + "x" + screen.height,               // Actual desktop width/height
            ch_window       = null,
            ch_canvas       = null,
            i               = null,
            k               = null,
            derived_height  = null,
            unit_id         = null,
            ad_beacon       = null,
            ad_loc          = null,
            s               = null,
            fid             = '',
            fobj            = null;

        var units = window.chitika_units;
        for (unit_id in units) {
            var w = units[unit_id];
            if (w === null || typeof(w) != 'object') { continue; }

            // Get window size and visible page size; accessed differently for MSIE vs. standards-compliant browsers
            ch_canvas = document.body.clientWidth + "x" + document.body.clientHeight;
            if (navigator.appName.indexOf("Microsoft") !== -1 &&
                navigator.userAgent.match(/MSIE ([^\.]+)/)[1]*1 < 9) {
                ch_window = document.documentElement.clientWidth + "x" + document.documentElement.clientHeight;
            } else {
                ch_window = window.innerWidth + "x" + window.innerHeight;
            }

            // Get Configuration ID; default to unit-N or SID, if unavailable
            if (!w.cid) {
                if (!w.sid ||
                    w.sid == 'Chitika Default') {
                    w.cid = 'unit-'+ch_unit_id;
                }
                else {
                    w.cid = w.sid;
                }
            }
            w.impsrc = ch_def(w.impsrc, 'amm_yahoo');

            // Use ch_host if defined; default to mm.chitika.net
            ch_ad_url = 'http://' + amm_host + '/minimall?&output=simplejs&callback=render_ad';

            ////////////////////////////////////////////////////////////////
            // This is where we start appending the necessary parameters
            // for the ad server.
            ch_aue('w',             w.width);               // Unit's width
            ch_aue('h',             w.height);              // Unit's height
            ch_aue('client',        w.client);               // Publisher username
            ch_aue('sid',           w.sid);
            ch_aue('cid',           w.cid);
            ch_aue('nump',          w.nump);                 // Number of ads to be requested
            ch_aue('query',         w.query);                // Publisher-specified targeting
            ch_aue('type',          w.type);                 // "type" of ad unit; mpu, local, mobile, map, etc.

            // If multiple queries have been specified, add them to request URL as
            // mquery; mquery defaults to ch_query
            if (w.queries &&
                w.queries.constructor.toString().indexOf("Array") !== -1) {
                ch_aue('mquery', w.queries.join('|'));
            } else if (w.query) {
                ch_aue('mquery', w.query);
            }

            ch_aue('tptracker',             w.third_party_tracker);  // Third-party tracking URL; Chitika clicks will be routed through this URL
            ch_aue('cttarget',              w.target);               // Set the arget window of Chitika clicks
            ch_aue('backfill',              w.backfill);             // Set backfill (if enabled, render something even if no ads were found)
            ch_aue('noborders',             w.noborders);            // Disables the default border that renders around ads
            ch_aue('cl_border',             w.color_border);         // Border color (default #cccccc)
            ch_aue('cl_bg',                 w.color_bg);             // Background color (default #ffffff)
            ch_aue('cl_title',              w.color_title);          // Font-color for ad Title (default #0000cc)
            ch_aue('cl_text',               w.color_text);           // Font-color for ad Text (default #000000)
            ch_aue('cl_site_link',          w.color_site_link);      // Font-color for ad Tagline (default #0000cc)
            ch_aue('fn_title',              w.font_title);           // Font-family for ad Title (default Arial, Helvetica, sans-serif)
            ch_aue('fn_text',               w.font_text);            // Font-family for ad Text/Tagline (default Arial, Helvetica, sans-serif)
            ch_aue('alturl',                w.alternate_ad_url);     // If Chitika has no ads, create iframe with this URL instead
            ch_aue('altcss',                w.alternate_css_url);    // Used by Chitika; if specified, CSS file will be included via "link" tag in Chitika's HTML
            ch_aue('behavioral_window',     w.behavioral_window);    // Max time to consider cookie retargeting (applicable for only SOME providers, yahoo not included)
            ch_aue('previous_format',       units.previous_format);      // Historical method for detecting previous ads
            ch_aue('screenres',             ch_screen);
            ch_aue('winsize',               ch_window);
            ch_aue('canvas',                ch_canvas);
            ch_aue('where',                 w.where);                // City, State for search; used for local/map units

            // Detects whether or not we're in a frame
            ch_aue('frm',                   window.top.location !== document.location ? 1 : 0);
            ch_aue('history',               history.length);            // Number of pages viewed BEFORE this view
            ch_aue('city',                  w.city);                    // More reliable than ch_where; specify city name
            ch_aue('state',                 w.state);                   // More reliable than ch_where; specify state (2 letter code or full name)
            ch_aue('zip',                   w.zip);                     // More reliable than ch_where; specify zip/postal code
            ch_aue('impsrc',                w.impsrc);                  // Indicate which version of AMM.js is running
            ch_aue('extra_subid_info',      w.extra_subid_info);        // Arbitrary string for generating subid patterns
            ch_aue('vsn',                   ch_amm_version);
            ch_aue('url',                   document.location.href);    // Page URL
            ch_aue('ref',                   document.referrer);         // Referrer URL
            ch_au('cb',                     unit_id);                   // Random number to associate with this unit.
            ch_au('dpr',                    window.devicePixelRatio);

            if (navigator.userAgent.match(/Chrome/) &&
                document.webkitVisibilityState !== undefined &&
                document.webkitVisibilityState == "prerender") {
                ch_au('prerender', 1);
            }

            if (document.location.href.indexOf('##chitika_ab=') !== -1) {
                ch_au('ab_overlay_which', document.location.href.match(/##chitika_ab=([^&]+)/)[1]);
            }

            get_snippets();

            // Append this format to our list of previous formats (so subsequent requests will know what we've requested so far)
            if (units.previous_format) {
                units.previous_format += ',';
            } else {
                units.previous_format = '';
            }
            units.previous_format += w.width + "x" + w.height;

            ad_beacon = document.getElementById('chitikaAdBlock-' + unit_id);
            ad_loc = ch_ad_locate(ad_beacon);
            ch_aue("loc", ad_loc.x + "," + ad_loc.y);

            ch_ad_url = ch_ad_url.substring(0, 2048);       // Trim request URL to 2048 characters
            ch_ad_url = ch_ad_url.replace(/%\w?$/, '');     // Remove any trailing malformed URL encoded character

            // Handle multiple instances of the same unit...
            w.hasClones = ch_def(w.hasClones, 0);
            var frames = new Array();

            for (var i = 0; i <= w.hasClones; i++) {
                var beacon_id = 'chitikaAdBlock-' + unit_id;
                if (i > 0) beacon_id = beacon_id + '-' + i;
                ad_beacon = document.getElementById(beacon_id);

                fid     = "ch_ad" + unit_id;
                if (i > 0) fid = fid + '-' + i;
                fobj    = ch_create_iframe({
                    'id':                   fid,
                    'class':                'chitikaAdBlock',
                    'allowTransparency':    'allowTransparency',    // Enables transparent BG in iframes for IE6
                    'name':                 fid,
                    'width':                w.width,
                    'height':               (typeof w.height === 'string' ? 0 : ch_def(w.height, 0))
                });

                if (w.fluidW) {
                    ch_apply_fluid_width(ad_beacon, fobj);
                }

                if (w.fluidH) {
                    ch_apply_fluid_height(fobj);
                }

                ad_beacon.appendChild(fobj);
                if (i == 0) ch_write_empty_iframe(fobj);
                frames.push(fobj);
            }

            // Only create the callback in the first one, and pass the rest so that the call back knows about them.
            var anchor_frame = frames.shift();
            create_callback(anchor_frame, frames, w);

            // Add a javascript tag to the header of the iframe, ONLY FOR THE FIRST ONE!
            ch_add_script(ch_ad_url, anchor_frame.contentWindow.document);

            // Don't leave the unit configuration lying around.
            window.chitika_units[unit_id] = null;
        }
    }

    function ch_eh(m, u, l) {
        ch_execute();
        return true;
    }

    // Set our error handler.
    window.onerror = function() {
        window.chitika_units = [];
    };

    // Start execution.
    ch_execute();
}());
