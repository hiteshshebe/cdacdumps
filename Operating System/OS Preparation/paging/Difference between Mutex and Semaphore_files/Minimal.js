﻿/*	
	resizeDivToWidestElement = scans the child elements of the specified div
	and resizes the div to be the same width as that widest element. 
	
	'divSelector' parameter should be in the format '.divClassName' or '#divIdName', 
	i.e. WITH the period or hash at the front 
*/
var originalOuterWidth = 0;

function resizeDivToWidestElement(divToCheck, outerDivToResize) {
	var padding = 25;
	var currentWidth = $(divToCheck).width();
	var maxWidth = currentWidth;
	var debugMode = false;

	if (originalOuterWidth == 0) originalOuterWidth = $(outerDivToResize).width();

	// if browser window size is already shrunk to be smaller than the outerDivToResize, then shrink outerDivToResize to that size to force wrapping. 
	if ($(window).width() < originalOuterWidth) {
		$(outerDivToResize).width($(window).width());
	} else {
		$(outerDivToResize).width(originalOuterWidth);
	}


	$(divToCheck).find("*").not('script').each(function () {
		var elWidth = $(this).width();

		if (elWidth > maxWidth + padding) {
			maxWidth = elWidth - padding;
			//if (debugMode) alert(maxWidth + " , element: " + $(this).html());
		}
	});
	//if (debugMode) alert(maxWidth);
	if (debugMode) $("#debugOutput").html("maxWidth: " + maxWidth + ", currentWidth: " + currentWidth + ", $(outerDivToResize).width(): " + $(outerDivToResize).width());

	// resize div that we're checking
	$(divToCheck).width(maxWidth);

	// finally, set the width of the additional outer specified div
	var difference = maxWidth - currentWidth;
	var newWidth = $(outerDivToResize).width() + difference; 
	$(outerDivToResize).width(newWidth);
}


function showShareButtons(shareLink) {
	if ($(shareLink).next().is(":visible")) {
		$(shareLink).next().fadeOut("fast");
		//$(shareLink).hide();
	} else {
		$(shareLink).next().fadeIn("fast");
		//$(shareLink).fadeIn("fast");
	}
}

