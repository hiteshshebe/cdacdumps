package com.actions;
import com.bslogics.*;
import com.pojos.*;

public class RegisterJSeekersAction 
{
	private Integer jseekid;
	private String email;
	private String firstName;
	private String lastName;
	private String userName;
	private String passwd;
	
	public RegisterJSeekersAction() {
		super();
	}
	public RegisterJSeekersAction(Integer jseekid, String email,
			String firstName, String lastName, String userName, String passwd) {
		super();
		this.jseekid = jseekid;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.passwd = passwd;
	}

	
	
	public Integer getJseekid() {
		return jseekid;
	}
	public void setJseekid(Integer jseekid) {
		this.jseekid = jseekid;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String execute()
	{
		System.out.println(jseekid);
		System.out.println(email);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(userName);
		System.out.println(passwd);
		
		JSeekersBsLogic jslogic=new JSeekersBsLogic();
		JSeekersPojo jsp=new JSeekersPojo(jseekid, email, firstName, lastName, userName, passwd);
		jslogic.insert(jsp);
		
		return "success";
	}
}
