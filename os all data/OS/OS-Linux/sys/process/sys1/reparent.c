#include<unistd.h>

int main()
{
	int ret;
	ret=fork();
	if(ret==0)
	{
		sleep(1);
		printf("child-pid=%d,ppid=%d\n",getpid(),getppid());
		exit(0);
	}
	else //ret>0
	{
		printf("parent-ret=%d\n",ret);
		printf("parent-pid=%d,ppid=%d\n",getpid(),getppid());
	}
	return 0;
}
