#include<unistd.h>
#include<fcntl.h>

int main()
{
	int fd,k,r;
	char buf[100];
	fd=open("sample.dat",O_RDONLY);
	//if(fd<0) perror("open");
	k=read(fd,buf,100);
	r=write(1,buf,k);
	close(fd);

	fd=open("alpha",O_WRONLY|O_CREAT,0666);
	write(fd,"ABCDEFGHIJKLMNOPQRSTUVWXYZ\n",28);
	close(fd);

	char str[100]={};
	fd=open("alpha",O_RDONLY);
	k=read(fd,str,5);
	printf("str=%s\n",str);

	//lseek(fd,10,SEEK_SET); //off=10
	//lseek(fd,10,SEEK_CUR); //off=15(5+10)
	lseek(fd,-2,SEEK_CUR); //off=3(5-2)
	//lseek(fd,-14,SEEK_END);
	k=read(fd,str,8);
	printf("str=%s\n",str);
	close(fd);



}
