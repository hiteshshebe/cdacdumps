#include<pthread.h>
const int max=10;

pthread_t pt1,pt2,ptm;
int arr[10],sum;
pthread_mutex_t m1;

void* efun1(void* pv)
{
	int i;
	printf("Hello, i am thread-abc\n");
	sleep(1);
	printf("before loop--abc\n");

	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++)
	{
		printf("abc--%d\n",i);
		//arr[i]=rand();
		//sum+=arr[i];
		sleep(1);
	}
	pthread_mutex_unlock(&m1);
	pthread_exit(NULL);
}

void* efun2(void* pv)
{
	int i;
	printf("Hello,i am thread-xyz\n");
	sleep(1);
	printf("before loop--xyz\n");

	pthread_mutex_lock(&m1);
	for(i=1;i<=max;i++) {
		printf("xyz--%d\n",i);
		//arr[i]=rand();
		//sum+=arr[i];
		sleep(1);
	}
	pthread_mutex_unlock(&m1);
	pthread_exit(NULL);
}

int main()
{
	ptm=pthread_self();

	pthread_mutex_init(&m1,NULL);

	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	pthread_join(pt1,NULL);
	pthread_join(pt2,NULL);

	pthread_mutex_destroy(&m1);

	printf("Main--thank you\n");

	return 0;
}
