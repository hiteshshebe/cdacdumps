#include<unistd.h>

int main()
{
	int ret,max=10,i;
	int x[]={10,20,30,40,50};
	printf("Hello World\n");
	ret=fork();
	if(ret<0) {
		perror("fork");
		exit(0);
	}
	if(ret==0) //child process
	{
		printf("child process,pid=%d,ppid=%d\n",
				getpid(),getppid());
		for(i=0;i<5;i++)
			printf("child--%d\n",x[i]);
		x[1]=25;
		exit(0); //_exit(0);
	}
	else //ret>0
	{
		x[0]++;
		printf("parent process-pid=%d,ppid=%d\n",
				getpid(),getppid());
		sleep(2);
		for(i=0;i<5;i++)
			printf("parent--%d\n",x[i]);
	}
	return 0;
}

