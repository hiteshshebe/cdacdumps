#include<unistd.h>

int main()
{
	int ret;
	ret=fork();
	if(ret==0) //child process
	{
		printf("child,before sleep-pid=%d,ppid=%d\n",
				getpid(),getppid());
		sleep(1);
		printf("child after sleep,process,pid=%d,ppid=%d\n",
				getpid(),getppid());
		exit(0); //_exit(0);
	}
	else //ret>0
	{
		printf("parent process-pid=%d,ppid=%d\n",
				getpid(),getppid());
	}
	return 0;
}

