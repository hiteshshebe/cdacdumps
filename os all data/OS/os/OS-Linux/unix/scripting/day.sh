
echo "enter a value(1-7)"
read day

case $day in
	1) echo "sunday"
	   echo "holiday";;
	2) echo "monday";;
	# 3) 4) 5) 6)
	7) echo "saturday"; echo "weekend starts";;
	*) echo "invalid choice"
esac

echo "enter a char"
read ch
case $ch in
	[aeiou]) echo "vowel";;
	[yn]|[YN] ) echo "y or n";;
	[a-z]|[A-Z]) echo "alphabet";;
	[0-9]) echo "digit";;
	\*) echo "given char is *";;
	*) echo "unknown char";;
esac
