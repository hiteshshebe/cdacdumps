
for p in `ls`
do
	if [ -f $p ]
	then
		echo "$p is a file"
	elif [ -d $p ]
	then
		echo "$p is a dir"
	else
		echo "unknown file"
	fi
done

for i in `seq 1 10`
do
	if [ ! -e sample$i ]
	then
		mkdir sample$i
		echo "dir sample$i created"
	else
		echo "sample$i exists already"
	fi
done




