#include<unistd.h>
#include<sys/msg.h>

#define KEY 1234

int main()
{
	int mid,k;
	mid=msgget(KEY,IPC_CREAT|0666);
	if(mid<0) {
		perror("msgget");
		exit(0);
	}
	char str[]="ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";
	k=msgsnd(mid,str,28,0);
	if(k<0) {
		perror("msgsnd");
		exit(0);
	}
	printf("message sent successfully\n");
	return 0;
}


