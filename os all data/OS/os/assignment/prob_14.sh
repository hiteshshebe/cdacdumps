#!/bin/sh

echo "Enter a year :"
read y
temp=`expr $y % 400`
temp1=`expr $y % 4`
temp2=`expr $y % 100`
if [ $temp1 -eq 0 ]
then
	if [ $temp -eq 0 -o $temp2 -ne 0 ]
	then
		echo "$y is a leap year."
	else
		echo "$y is not a leap year."
	fi

fi

