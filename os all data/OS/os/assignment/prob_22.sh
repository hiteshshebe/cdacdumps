#!/bin/sh

echo "Enter a number :"
read n
temp=$n
sum=0
rev_temp=0
rev=0
while [ $temp -ne 0 ]
do
	let rem=$temp%10
	let rev_temp=$rev*10
	let rev=$rev_temp+$rem
	let temp=$temp/10
	let sum=$sum+$rem
done
echo "The reverse of $n is :$rev"
echo  "The sum of digits is :$sum"

