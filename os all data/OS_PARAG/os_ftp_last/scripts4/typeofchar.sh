
echo "enter a character"
read ch

case $ch in
	[aeiou]) echo "vowel,lowercase";;
	[AEIOU]) echo "vowel,uppercase";;
	# [aeiou]|[AEIOU]) echo "vowel";;
	*) echo "consonant";;
esac

case $ch in
	[A-Z]) echo "uppercase alphabet";;
	[a-z]) echo "lowercase alphabet";;
	[0-9]) echo "digit";;
	[+\-\*/%] ) echo "operator";;
	*) echo "unknown char";;
esac
