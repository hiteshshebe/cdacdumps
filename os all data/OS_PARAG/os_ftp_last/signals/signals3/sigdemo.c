#include<signal.h>

void handler_for_int(int signo)
{
	printf("SIGINT signal arrived\n");
}
void handler_for_other(int signo)
{
	printf("A signal arrived\n");
	switch(signo)
	{
		case SIGQUIT: printf("quit signal\n"); break;
		case SIGTSTP: printf("TSTP signal\n"); break;
	        case SIGTERM: printf("term signal\n"); exit(0);
	}
}

int main()
{
	signal(SIGINT,handler_for_int);
	
	signal(SIGTERM,handler_for_other);
	signal(SIGQUIT,handler_for_other);
	signal(SIGTSTP,handler_for_other);

	signal(SIGKILL,handler_for_other); //no effect

	while(1);
	printf("thank you");
	return 0;
}

