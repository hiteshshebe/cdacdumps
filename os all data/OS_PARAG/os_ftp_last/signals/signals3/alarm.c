#include<signal.h>
#include<sys/time.h>

void handler_for_alarm(int signo)
{
	//printf("custom alarm handler\n");
	time_t t1=time(0); //no.of secs from UTC
	printf("cur time is %s\n",ctime(&t1));
	alarm(1);
}
int main()
{
	signal(SIGALRM,handler_for_alarm);
	alarm(2);
	while(1);
		pause(); //block the process for a signal
	printf("thank you\n");
}
