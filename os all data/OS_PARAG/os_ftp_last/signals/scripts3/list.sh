
for k in `seq 1 10`
do
	if [ -e exam$k ]
	then
		echo "exam$k already exists"
		continue
	fi
	mkdir exam$k
	echo "exam$k created successfully"
done

for p in `ls`
do
	if [ -f $p ]
	then
		echo "$p is a file"
	elif [ -d $p ]
	then
		echo "$p is a directory"
	else
		echo "$p is an unknown file"
	fi
done






