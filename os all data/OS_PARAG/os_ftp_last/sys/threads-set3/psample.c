#include<pthread.h>
const int max=10;
void* efun1(void* pv)
{
	int i;
	printf("Thread--A\n");
	for(i=1;i<=max;i++) {
		printf("thread--A--%d\n",i);
		sleep(1);
	}
}
void* efun2(void* pv)
{
	int i;
	printf("Thread--B\n");
	for(i=1;i<=max;i++){
		printf("thread--B--%d\n",i);
		sleep(1);
	}
}

int main()
{
	int i;
	pthread_t pt1,pt2,ptm;
	//efun1(NULL);
	//efun2(NULL);
	ptm=pthread_self();
	pthread_create(&pt1,NULL,efun1,NULL);
	pthread_create(&pt2,NULL,efun2,NULL);

	for(i=1;i<=max;i++) {
		printf("main--%d\n",i);
		sleep(1);
	}
	printf("main--thank you\n");
	return 0;
}
