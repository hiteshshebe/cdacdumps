#include<unistd.h>
#include<fcntl.h>

int main()
{
	int k,ret,fds[2];
	char buf[20];
	k=pipe(fds);
	if(k<0) {
		perror("pipe");
		exit(0);
	}
	//int fd=open("sample.dat",O_RDONLY);
	printf("fds0=%d,fds1=%d\n",fds[0],fds[1]);
	ret=fork();
	if(ret==0)  //writing
	{
		close(fds[0]); //no reading
		sleep(5);
		write(fds[1],"Hello FIFO\n",12);
		close(fds[1]);
		exit(0);
	}
	else 	    //reading
	{
		close(fds[1]); //no writing
		k=read(fds[0],buf,20);
		printf("in parent\n");
		write(1,buf,k);
		close(fds[0]);
	}
	return 0;
}
