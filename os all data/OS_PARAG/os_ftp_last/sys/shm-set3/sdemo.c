#include<unistd.h>
#include<sys/shm.h>

#define KEY 1235

int main()
{
	int shmid,ret;
	void* pv;
	shmid=shmget(KEY,256,IPC_CREAT|0666);
	if(shmid<0) {
		perror("shmget");
		exit(0);
	}
	ret=fork();
	if(ret==0) {
		pv=shmat(shmid,0,0);
		//sleep(1);
		printf("in child--%s\n",(char*)pv);
		shmdt(pv);
	}
	else {
		pv=shmat(shmid,0,0);
		//sleep(1)
		strcpy((char*)pv,"Hello SHM\n");
		shmdt(pv);
	}


	return 0;
}



