<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.SQLException,java.sql.DriverManager,java.sql.PreparedStatement,java.sql.Connection,java.io.PrintWriter,java.sql.ResultSet,com.connections.DBConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Seekers</title>
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
<link href="../css/templatemo_style.css" rel="stylesheet" type="text/css" />


</head>
<body id="home">
<jsp:include page="header_menu.jsp"/>

	
<div id="templatemo_main" style="height:400px">
<a href="home_admin.jsp">Back</a>
<a href="logout.jsp">Logout</a><br><br>

<%

String sid=null;
int admin_id;
try
{
	sid=session.getAttribute("admin_session_id").toString();
	
}
	catch(Exception e){}
	if(sid==null)
	{
		response.sendRedirect("login_admin.jsp");
	}
%>

<%

Connection conn;
String sql=null;
PreparedStatement pstmt=null;
ResultSet rs;

try 
{
		conn=new DBConnection().getConnection();		
		
		sql="select recruiter_id,email_id,status from recruiters_tb";
			
		pstmt=conn.prepareStatement(sql);
		
		rs=pstmt.executeQuery();
		
		out.println("<h3>List of Recruiter</h3>");
		
		
		out.println("<table border='1' align='center'><th>Recruiter id<th>Email id<th>Current Status<th>Action");
		
		
		
		while(rs.next())
		{
			int cs=rs.getInt("status");
			String curr_status;
			
			if(cs==0)
				curr_status="Blocked";
			else
				curr_status="Active";
					
			out.println("<tr><td>"+rs.getInt("recruiter_id")+
							"<td>"+rs.getString("email_id")+
							"<td><b>"+curr_status+"</b>"+
							"<td><form action='update_recruiter_acc.jsp'>"+
							"<input type='hidden' name='recruiter_id' value='"+rs.getInt("recruiter_id")+"'>"+
							"<input type='radio' name='status' value='1'>Activate"+
							"<input type='radio' name='status' value='0'>Block"+
							"<input type='submit' value='Update'>"+
							"</form>"
					);
							
		}
		out.println("</table>");
		
		conn.close();
}catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

%>
 </div><!-- END OF CONTENT HERE -->
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>