package com.actions;

import com.bslogics.RegisterBsLogic;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class SeekerLoginAction extends ActionSupport
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int seeker_id;
	String email_id;  
	String password;
	
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getSeeker_id() {
		return seeker_id;
	}
	public void setSeeker_id(int seeker_id) {
		this.seeker_id = seeker_id;
	}
	
	public String execute() throws Exception
	{	
		RegisterBsLogic rbl= new RegisterBsLogic();
		if(rbl.loginSeeker(this))
		{
			if(rbl.getAccStatus()==1)
			{
				setSeeker_id(rbl.getSeeker_id());
				ActionContext.getContext().getSession().put("seeker_session_id", getSeeker_id());
				return "success";
			}
			else
			{
				return "deactivated";
			}
		}
		else
			return "failure";
		
		
		
	}
}
