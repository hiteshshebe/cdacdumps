package com.actions;

import com.bslogics.RegisterBsLogic;
import com.opensymphony.xwork2.ActionContext;




public class RecruiterLoginAction 
{
	
	String email_id; 
	String password;
	int recruiter_id;
	
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getRecruiter_id() {
		return recruiter_id;
	}
	public void setRecruiter_id(int recruiter_id) {
		this.recruiter_id = recruiter_id;
	}
	public String execute()
	{	
		
		RegisterBsLogic rbl= new RegisterBsLogic();
		if(rbl.loginRecruiter(this))
		{
			setRecruiter_id(rbl.getRecruiter_id());
			ActionContext.getContext().getSession().put("session_id", getRecruiter_id());
			return "success";
		}
		else
			return "failure";
			}
}
