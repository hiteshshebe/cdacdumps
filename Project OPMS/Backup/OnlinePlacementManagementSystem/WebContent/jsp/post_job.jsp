<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="../css/default.css"/>
</head>
<body>
<jsp:include page="header.html"/>

<%

String rid=null;
	try
{
	rid=session.getAttribute("session_id").toString();
}
	catch(Exception e){}
	if(rid==null)
	{
		response.sendRedirect("login_recruiter.jsp");
	}
	
%>

<div id="content">
<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div>
				<s:form action="post_job" id="registration_form">
					<div>
					<s:label><h3 style="text-align: center;">Post Job</h3></s:label>
					<s:hidden name="job_id" value="0"></s:hidden>
					<s:textfield name="job_title" label="*Job Title "></s:textfield>
					<s:textfield name="company_name" label="*Company Name "></s:textfield>
					<s:textfield name="location" label="*Location "></s:textfield>
					<s:textfield name="position" label="*Position "></s:textfield>
					<s:textarea name="job_description" label="*Description" cols="14"></s:textarea>
					<s:textfield name="eligibility" label="*Eligibility"></s:textfield>
					<s:textfield name="contact_email" label="*Emailid "></s:textfield>
					<s:textfield name="contact_number" label="*Contact Number "></s:textfield>
					<s:textfield name="sal_package" label="*Package "></s:textfield>
					
					<%-- PID<input type="text" name="providers_id" value="<%=rid%>"/> --%>
					
					
					</div>
					<s:submit value="Register"></s:submit>
				</s:form>
				</div><!-- END OF REGISTRATION FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->



<div id="footer">
copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>
</body>
</html>