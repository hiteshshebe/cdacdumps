/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function isAlphabet(name1)
{
    	var alphaExp = /^[a-zA-Z]+$/;
        if(name1.match(alphaExp)){
            return true;
	}else{
		return false;
	}
}

function isNumeric(number)
{
	var numericExpression = /^[0-9]+$/;
	if(number.match(numericExpression)){
              return true;
	}else{
	      return false;
	}
}

function isEmail(elem)
{
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.match(emailExp)){
		return true;
	}else{
		return false;
	}
}
function validateAddUser()
{
    var atmNumber=document.adduser.atmnumber.value;
    var pin=document.adduser.pin.value;
    var firstName=document.adduser.firstname.value;
    var lastName=document.adduser.lastname.value;
    var panCardNumber=document.adduser.pancardnumber.value;
    var mobileNumber=document.adduser.mobilenumber.value;
    var emailAddress=document.adduser.emailaddress.value;
    var address=document.adduser.address.value;
    if(atmNumber=="" && pin=="" && firstName=="" && lastName=="" && panCardNumber=="" && mobileNumber=="" && emailAddress=="" && address=="")
    {
        alert("All field must be filled out");
        return false;
    }
    if(atmNumber=="")
    {
        alert("Enter ATM Number");
        return false;
    }
    if(!isNumeric(atmNumber))
    {
        alert("ATM Number should be Numeric");
        return false;
    }
    if(atmNumber.length!=16)
    {
        alert("ATM Number should be 16 digit");
        return false;
    }
    if(pin=="")
    {
        alert("Enter Pin Number");
        return false;
    }
    if(!isNumeric(pin))
    {
        alert("Pin Number should be Numeric");
        return false;
    }
    if(pin.length!=4)
    {
        alert("Pin Number should be 4 digit");
        return false;
    }
    if(firstName=="")
    {
        alert("Enter first name");
        return false;
    }
    if(!isAlphabet(firstName))
    {
        alert("First name should be character");
        return false;
    }
    if(lastName=="")
    {
        alert("Enter last name");
        return false;
    }
    if(!isAlphabet(lastName))
    {
        alert("Last name should be character");
        return false;
    }
    if(panCardNumber=="")
    {
        alert("Enter PAN Card number");
        return false;
    }
    if(mobileNumber=="")
    {
        alert("Enter mobile number");
        return false;
    }
    if(!isNumeric(mobileNumber))
    {
        alert("Mobile Number should be Numeric");
        return false;
    }
    if(mobileNumber.length!=10)
    {
        alert("Mobile Number should be 10 digit");
        return false;
    }
    if(emailAddress=="")
    {
        alert("Enter email address");
        return false;
    }
    if(!isEmail(emailAddress))
    {
        alert("Enter Valid email address");
        return false;
    }
    if(address=="")
    {
        alert("Enter address");
        return false;
    }
    return true;
}

function validateAmount(amount)
{
    if(amount=="")
    {
        alert("Please enter amount");
        document.withdrawal.amount.focus();
    }
    else
    {
        if(isNaN(amount))
        {
            alert("Amount should be Numeric");
            document.withdrawal.amount.focus();
        }
        else
        {
            if((amount%100)!=0)
            {
                alert("Amount should be multiple of 100");
                document.withdrawal.amount.focus();
            }
        }
    }
}


function validateAdminLogin()
{
    var username=document.login.username.value;
    var password=document.login.password.value;
    //alert(atmNumber +" "+ pin );
    if(username=="" && password =="")
    {
        alert("All fields must be filled out");
        return false;
    }
    if(username=="")
    {
        alert("Please Enter Username");
        return false;
    }
    
    if(password=="")
    {
        alert("Please Enter Password");
        return false;
    }
    return true;
}
function validateATMNumber()
{
    var atmNumber=document.updateuser1.atm_number.value;
    if(atmNumber=="")
    {
        alert("Enter ATM Number");
        return false;
    }
    if(!isNumeric(atmNumber))
    {
        alert("ATM Number should be numeric");
        return false;
    }
    if(atmNumber.length!=16)
    {
        alert("ATM Number should be 16 digit");
        return false;
    }
    return true;
}

function validateUpdateUser()
{
    var firstName=document.updateuser2.firstname.value;
    var lastName=document.updateuser2.lastname.value;
    var panCardNumber=document.updateuser2.pancardnumber.value;
    var mobileNumber=document.updateuser2.mobilenumber.value;
    var emailAddress=document.updateuser2.emailaddress.value;
    var address=document.updateuser2.address.value;
    if(firstName=="" && lastName=="" && panCardNumber=="" && mobileNumber=="" && emailAddress=="" && address=="")
    {
        alert("All field must be filled out");
        return false;
    }
    if(firstName=="")
    {
        alert("Enter first name");
        return false;
    }
    if(!isAlphabet(firstName))
    {
        alert("First name should be character");
        return false;
    }
    if(lastName=="")
    {
        alert("Enter last name");
        return false;
    }
    if(!isAlphabet(lastName))
    {
        alert("Last name should be character");
        return false;
    }
    if(panCardNumber=="")
    {
        alert("Enter PAN Card number");
        return false;
    }
    if(mobileNumber=="")
    {
        alert("Enter mobile number");
        return false;
    }
    if(!isNumeric(mobileNumber))
    {
        alert("Mobile Number should be Numeric");
        return false;
    }
    if(mobileNumber.length!=10)
    {
        alert("Mobile Number should be 10 digit");
        return false;
    }
    if(emailAddress=="")
    {
        alert("Enter email address");
        return false;
    }
    if(!isEmail(emailAddress))
    {
        alert("Enter Valid email address");
        return false;
    }
    if(address=="")
    {
        alert("Enter address");
        return false;
    }
    return true;
}
