<%-- 
    Document   : adduser
    Created on : Mar 9, 2012, 7:46:50 PM
   
--%>

<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <script type="text/javascript" src="js/java_script.js"></script>
        <title>
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            -
            ${sessionScope.admUserName}
            :
            <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
            <jsp:getProperty name="links" property="linkLabel" />
        </title>
    </head>
    <body>
        <%
        if (session.getAttribute("loggedin") == null) {
            response.sendRedirect("index.jsp");
        }
        %>
        <!--style="background-color:#B9CAFF"-->
        <div id="container">
            <%@include file="header.jsp" %>
            <div id="wrapper">
                <div id="content">
                    <div id="context">
                        <strong>
                            <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
                            <jsp:getProperty name="links" property="linkLabel" />
                        </strong>
                        <form class="adduserform" id="adduser" name="adduser" action="AddUser.web" method="post" onsubmit="return validateAddUser();">
                            <table id="newspaper-a">
                                <tbody>
                                    <c:if test="${requestScope.successMessage != null && requestScope.successMessage != ''}">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <span id="successMessage" style="color:blue;font-size:14px;font-weight:bold">${requestScope.successMessage}</span>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${requestScope.errorMessage != null && requestScope.errorMessage != ''}">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <span id="errorMessage" style="color:red;font-size:14px;font-weight:bold">${requestScope.errorMessage}</span>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="ATM_NUMBER" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="atmnumber"  value ="<% if (request.getAttribute("atmnumber") != null) {%><%= request.getAttribute("atmnumber")%><%}%>" size="25" maxlength="16"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="PIN" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="pin"  value ="<% if (request.getAttribute("pin") != null) {%><%= request.getAttribute("pin")%><%}%>" size="25" maxlength="4"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="ACCOUNT_TYPE" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <select name="accounttype">
                                                <option value="1">
                                                    <jsp:setProperty name="links" property="linkName" value="SAVING_ACCOUNT" />
                                                    <jsp:getProperty name="links" property="linkLabel" />
                                                </option>
                                                <option value="2">
                                                    <jsp:setProperty name="links" property="linkName" value="CURRENT_ACCOUNT" />
                                                    <jsp:getProperty name="links" property="linkLabel" />
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="FIRST_NAME" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="firstname"  value ="<% if (request.getAttribute("firstname") != null) {%><%= request.getAttribute("firstname")%><%}%>" size="25" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="LAST_NAME" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="lastname"  value ="<% if (request.getAttribute("lastname") != null) {%><%= request.getAttribute("lastname")%><%}%>" size="25" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="PAN_CARD_NUMBER" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="pancardnumber"  value ="<% if (request.getAttribute("pancardnumber") != null) {%><%= request.getAttribute("pancardnumber")%><%}%>" size="25" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="MOBILE_NUMBER" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="mobilenumber"  value ="<% if (request.getAttribute("mobilenumber") != null) {%><%= request.getAttribute("mobilenumber")%><%}%>" size="25" />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="EMAIL_ADDRESS" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="emailaddress"  value ="<% if (request.getAttribute("emailaddress") != null) {%><%= request.getAttribute("emailaddress")%><%}%>" size="25" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <jsp:setProperty name="links" property="linkName" value="ADDRESS" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </td>
                                        <td>
                                            <input type="text" name="address"  value ="<% if (request.getAttribute("address") != null) {%><%= request.getAttribute("address")%><%}%>" size="70" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="reset"
                                                   value=<jsp:setProperty name="links" property="linkName" value="RESET" />
                                                   <jsp:getProperty name="links" property="linkLabel" />
                                                   name="Reset" />
                                            &nbsp;&nbsp;
                                            <input type="submit"
                                                   value=<jsp:setProperty name="links" property="linkName" value="SUBMIT" />
                                                   <jsp:getProperty name="links" property="linkLabel" />
                                                   name="Submit" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <strong>
                    <ul>
                        <li>
                            <a href="main.jsp">
                                <jsp:setProperty name="links" property="linkName" value="LIST_USERS" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="adduser.jsp">
                                <jsp:setProperty name="links" property="linkName" value="ADD_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="update_user.jsp">
                                <jsp:setProperty name="links" property="linkName" value="UPDATE_USER" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <!--<li>
                            <a href="myaccount.jsp">
                                <jsp:setProperty name="links" property="linkName" value="MY_ACCOUNT" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>-->
                    </ul>
                </strong>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>
