/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.connection;
import java.sql.*;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;
/**
 *
 *
 */
public class DBConnectionBean {
    private static Connection con;
    private Logger conLogger;
    private ResourceBundleBean rsBundle;

    public DBConnectionBean() {
        rsBundle = new ResourceBundleBean();
        conLogger = Logger.getLogger("com.web.connection");
    }

    public Connection getCon() {
        if (con!=null){
            return con;
        }else{
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hmm","root","root");
                conLogger.info(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
            } catch (Exception ex) {
                conLogger.fatal(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                ex.printStackTrace();
            }
            return con;
        }
    }

    public void setCon(Connection con) {
        DBConnectionBean.con = con;
    }
}
