/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.web.locale.ResourceBundleBean;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 */
public class LogOut extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Logger adminLoginLogger = Logger.getLogger("com.web.Admin");
        PrintWriter out = response.getWriter();
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            out.print("Base 2 ");
            HttpSession session = request.getSession();
            out.print("Base 3 ");
            if (session != null) {
                out.print("Base 4.1 ");
                adminLoginLogger.error(rsBundle.getErrorMessage("SESSION_LOGOUT")+ " " + session.getAttribute("admUserName"));
                out.print("Base 4 ");
                session.invalidate();
                response.sendRedirect("index.jsp");
            } else {
                out.print("Base 5 ");
                adminLoginLogger.error(rsBundle.getErrorMessage("SESSION_EXPIRED")+ " " + session.getAttribute("admUserName"));
                response.sendRedirect("index.jsp");
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
