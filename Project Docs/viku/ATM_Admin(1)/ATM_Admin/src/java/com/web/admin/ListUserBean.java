/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class ListUserBean {

    private String firstName,  lastName,  creditCardNumber,  panCardNumber,  mobileNumber,  address,  pinNumber;
    private int id,  accountType;
    private Connection con;
    private Logger listUserBeanLogger;
    private int insertId;
    private ResourceBundleBean rsBundle;

    public ListUserBean() {
        rsBundle = new ResourceBundleBean();
        listUserBeanLogger = Logger.getLogger("com.web.User.ListUserBean");
    }

    public void setConection() {
        con = new DBConnectionBean().getCon();
    }

    public int getAccountType() {
        return accountType;
    }

    public String getAddress() {
        return address;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public Logger getListUserBeanLogger() {
        return listUserBeanLogger;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPanCardNumber() {
        return panCardNumber;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setPanCardNumber(String panCardNumber) {
        this.panCardNumber = panCardNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public ResultSet getUsersList() {
        ResultSet rs = null;
        try {
            String query = "SELECT hmm_user.Id, hmm_user.FirstName, hmm_user.LastName,hmm_account.Id,hmm_account.AccountType,hmm_account.Balance, hmm_account.ATMNumber FROM hmm_user INNER JOIN hmm_account WHERE hmm_user.Id = hmm_account.UserId GROUP BY hmm_user.Id;";
            listUserBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            listUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_LIST_USER_BEAN") + " getUsersList");
            ex.printStackTrace();
        } catch (Exception ex) {
            listUserBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_LIST_USER_BEAN") + " getUsersList");
            ex.printStackTrace();
        }
        return rs;
    }
}
