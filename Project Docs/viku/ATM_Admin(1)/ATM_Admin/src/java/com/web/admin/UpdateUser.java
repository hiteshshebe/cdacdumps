/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.web.admin;

import com.web.connection.DBConnectionBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 */
public class UpdateUser extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Connection con = new DBConnectionBean().getCon();
            if (con != null)
            {
                HttpSession session = request.getSession();
                String atmNumber=request.getParameter("atm_number");
                String userId=request.getParameter("user_id");
                String firstname = request.getParameter("firstname");
                String lastname = request.getParameter("lastname");
                String mobilenumber = request.getParameter("mobilenumber");
                String pancardnumber = request.getParameter("pancardnumber");
                String address = request.getParameter("address");
                String emailaddress = request.getParameter("emailaddress");
                out.print("Base 1");

                String query="update hmm_user set PanCardNumber='"+ pancardnumber +"', FirstName='"+ firstname +"', LastName='"+lastname+"', MobileNumber='"+mobilenumber+"', Address='"+address+"', EmailAddress='"+emailaddress+"' where Id='"+userId+"'";
                Statement stmt=con.createStatement();
                if(stmt.executeUpdate(query)==1)
                {
                    request.setAttribute("atm_number", atmNumber);
                    request.setAttribute("successMessage", "Successfully Done");
                }
                else
                {
                    request.setAttribute("atm_number", atmNumber);
                    request.setAttribute("errorMessage", "Not Done");
                }
            }
            else
            {
                request.setAttribute("errorMessage", "Not Done");
            }
        }catch(Exception e)
        {
            System.out.print(e);
        }

        ServletContext ctx = this.getServletContext();
        RequestDispatcher rd = ctx.getRequestDispatcher("/update_user.jsp");
        rd.include(request, response);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
