/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class UserInfoBean {

    Connection con;
    Logger userInfoBeanLogger;

    public UserInfoBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        userInfoBeanLogger = Logger.getLogger("com.web.User.UserInfoBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            userInfoBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            userInfoBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public ResultSet getUserInfo(int userId) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        ResultSet rs = null;
        try {
            Statement stmt = con.createStatement();
            String query = "SELECT hmm_user.Id, hmm_user.FirstName, hmm_user.LastName, hmm_user.MobileNumber,hmm_user.Address, hmm_user.EmailAddress,hmm_user.PanCardNumber, hmm_account.Id,hmm_account.AccountType,hmm_account.Balance, hmm_account.ATMNumber FROM hmm_user INNER JOIN hmm_account WHERE hmm_user.Id = "+userId+" AND hmm_user.Id = hmm_account.UserId;";
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            userInfoBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_FORGOT_PASSWORD_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            userInfoBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_FORGOT_PASSWORD_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return rs;
    }
}
