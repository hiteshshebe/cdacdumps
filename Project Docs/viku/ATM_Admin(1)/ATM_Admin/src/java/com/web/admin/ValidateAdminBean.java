/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.admin;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * 
 */
public class ValidateAdminBean {

    Connection con;
    Logger validateAdminBeanLogger;
    int loginID;
    int admId;
    String  admFirstName,
            admLastName,
            admUserName;
    byte admIsSuper;

    public ValidateAdminBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        validateAdminBeanLogger = Logger.getLogger("com.web.User.ValidateAdminBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            validateAdminBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            validateAdminBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
        loginID = 0;
    }

    public void setLoginID(int loginID) {
        this.loginID = loginID;
    }

    public void setAdmFirstName(String admFirstName) {
        this.admFirstName = admFirstName;
    }

    public void setAdmId(int admId) {
        this.admId = admId;
    }

    public void setAdmIsSuper(byte admIsSuper) {
        this.admIsSuper = admIsSuper;
    }

    public void setAdmLastName(String admLastName) {
        this.admLastName = admLastName;
    }

    public void setAdmUserName(String admUserName) {
        this.admUserName = admUserName;
    }

    public int getAdmId() {
        return admId;
    }

    public byte getAdmIsSuper() {
        return admIsSuper;
    }

    public String getAdmFirstName() {
        return admFirstName;
    }

    public String getAdmLastName() {
        return admLastName;
    }

    public String getAdmUserName() {
        return admUserName;
    }

    public int getLoginID() {
        return loginID;
    }

    boolean isValidAdmin(String username, String password) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Id, FirstName, LastName, UserName, Password, IsSuperUser  FROM hmm_admin WHERE UserName = '" + username + "'");
            if (rs != null && rs.next()) {
                setLoginID(rs.getInt("Id"));
                if (password.equals(rs.getString("Password")) ) {
                    setAdmFirstName(rs.getString("FirstName"));
                    setAdmLastName(rs.getString("LastName"));
                    setAdmUserName(rs.getString("UserName"));
                    setAdmIsSuper(rs.getByte("IsSuperUser"));
                    validateAdminBeanLogger.info(rsBundle.getErrorMessage("LOGIN_SUCCESSFULL") + "Username: " + admUserName);
                    return true;
                } else {
                    validateAdminBeanLogger.warn(rsBundle.getErrorMessage("LOGIN_FAILED") + "Username: " + username);
                }
            } else {
                validateAdminBeanLogger.warn(rsBundle.getErrorMessage("LOGIN_FAILED") + "Username: " + username);
            }
        } catch (SQLException ex) {
            validateAdminBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_VALIDATE_ADMIN_BEAN") + " isValidUser");
            ex.printStackTrace();
        } catch (Exception ex) {
            validateAdminBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_VALIDATE_ADMIN_BEAN") + " isValidUser");
            ex.printStackTrace();
        }
        return false;
    }
}
