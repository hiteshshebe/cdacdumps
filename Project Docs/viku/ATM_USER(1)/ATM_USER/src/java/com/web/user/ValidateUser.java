/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.user;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import javax.servlet.http.*;
import com.web.locale.ResourceBundleBean;
import java.sql.Connection;
import com.web.connection.DBConnectionBean;
import com.web.hmm.OnlineTimeRecorderBean;
import java.util.Date;

/**
 *
 * 
 */
public class ValidateUser extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger validateUserLogger = Logger.getLogger("com.web.User");
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        HttpSession session = request.getSession();
        try {
            Connection con = new DBConnectionBean().getCon();
            String ATMNumber = request.getParameter("atmnumber");
            String pin = request.getParameter("pin");
            if(request.getParameter("h_no")==null || request.getParameter("h_no")=="")
            {
                if (con != null) {
                    validateUserLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                    ValidateUserBean obj = new ValidateUserBean(con);
                    if (obj.isValidUser(ATMNumber, pin)) {
                        request.setAttribute("successMessage", rsBundle.getText("LOGIN_SUCCESSFULL"));
                        session.setAttribute("usrId", obj.getUsrId());
                        session.setAttribute("usrFirstName", obj.getUsrFirstName());
                        session.setAttribute("usrLastName", obj.getUsrLastName());
                        session.setAttribute("usrATMNumber", obj.getATMNumber());
                        session.setAttribute("usrMobileNumber", obj.getMobileNumber());
                        session.setAttribute("accountType", obj.getAccountType());
                        session.setAttribute("accountId", obj.getAccountId());
                        session.setAttribute("loggedin", true);
                        OnlineTimeRecorderBean recorder = new OnlineTimeRecorderBean(con);
                        int loginId = recorder.recordLoginTime(obj.getAccountId());
                        session.setAttribute("loginId", loginId);

                        session.setAttribute("pageVisitSeq", "");
                        session.setAttribute("logintime", new Date());
                        request.getRequestDispatcher("main.jsp").forward(request, response);
                    }
                    else
                    {
                        if(request.getParameter("h_no")==null || request.getParameter("h_no")=="")
                        {
                            request.setAttribute("h_no","1");
                            request.setAttribute("h_atm_no",ATMNumber);
                        }
                        else
                        {
                            if((request.getParameter("h_atm_no").toString()).equals(ATMNumber))
                            {
                                int i=Integer.parseInt(request.getParameter("h_no"));
                                i++;
                                request.setAttribute("h_no",i);
                                System.out.print("second time");
                                request.setAttribute("h_atm_no",ATMNumber);
                            }
                        }
                        request.setAttribute("errorMessage", rsBundle.getText("INVALID_USERNAME_OR_PASSWORD"));
                        request.getRequestDispatcher("index.jsp").forward(request, response);
                    }
                } else {
                    validateUserLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                    request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                }
            }
            else
            {
                if(Integer.parseInt(request.getParameter("h_no"))>=3 && (request.getParameter("h_atm_no").toString()).equals(ATMNumber))
                {
                    int i=Integer.parseInt(request.getParameter("h_no"));
                    i++;
                    request.setAttribute("h_no",i);
                    request.setAttribute("h_atm_no",ATMNumber);
                    request.setAttribute("errorMessage","Your Card has been blocked Please try after some time");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
                else
                {
                    if (con != null) {
                        validateUserLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_ESTABLISHED"));
                        ValidateUserBean obj = new ValidateUserBean(con);
                        if (obj.isValidUser(ATMNumber, pin)) {
                            request.setAttribute("successMessage", rsBundle.getText("LOGIN_SUCCESSFULL"));
                            session.setAttribute("usrId", obj.getUsrId());
                            session.setAttribute("usrFirstName", obj.getUsrFirstName());
                            session.setAttribute("usrLastName", obj.getUsrLastName());
                            session.setAttribute("usrATMNumber", obj.getATMNumber());
                            session.setAttribute("usrMobileNumber", obj.getMobileNumber());
                            session.setAttribute("accountType", obj.getAccountType());
                            session.setAttribute("accountId", obj.getAccountId());
                            session.setAttribute("loggedin", true);
                            OnlineTimeRecorderBean recorder = new OnlineTimeRecorderBean(con);
                            int loginId = recorder.recordLoginTime(obj.getAccountId());
                            session.setAttribute("loginId", loginId);

                            session.setAttribute("pageVisitSeq", "");
                            session.setAttribute("logintime", new Date());
                            request.getRequestDispatcher("main.jsp").forward(request, response);
                        }
                        else
                        {
                            if(request.getParameter("h_no")==null || request.getParameter("h_no")=="")
                            {
                                request.setAttribute("h_no","1");
                                request.setAttribute("h_atm_no",ATMNumber);
                            }
                            else
                            {
                                if((request.getParameter("h_atm_no").toString()).equals(ATMNumber))
                                {
                                    int i=Integer.parseInt(request.getParameter("h_no"));
                                    i++;
                                    request.setAttribute("h_no",i);
                                    System.out.print("second time");
                                    request.setAttribute("h_atm_no",ATMNumber);
                                }
                            }
                            request.setAttribute("errorMessage", rsBundle.getText("INVALID_USERNAME_OR_PASSWORD"));
                            request.getRequestDispatcher("index.jsp").forward(request, response);
                        }
                    } else {
                        validateUserLogger.error(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                        request.setAttribute("errorMessage", rsBundle.getText("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
                    }
                }



                
            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
