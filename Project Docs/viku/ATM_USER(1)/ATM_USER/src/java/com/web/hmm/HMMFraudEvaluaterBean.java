/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.hmm;

import java.util.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;

/**
 *
 * @author Amrit
 */
public class HMMFraudEvaluaterBean {

    Logger HMMFraudEvaluaterBeanLogger;
    ResourceBundleBean rsBundle;

    public HMMFraudEvaluaterBean() {
        rsBundle = new ResourceBundleBean();
        HMMFraudEvaluaterBeanLogger = Logger.getLogger("com.web.User.HMMFraudEvaluaterBean");
    }

    //phase 1
    private float CalculateAlpha(float[] observedValues) {
        float[] l_ObservedValues = new float[observedValues.length];
        for (int i = 0; i < observedValues.length; i++) {
            l_ObservedValues[i] = observedValues[i];
        }

        Arrays.sort(l_ObservedValues);

        float f_centriod1 = (l_ObservedValues[0] + l_ObservedValues[1] + l_ObservedValues[2]) / 3;
        float f_centriod2 = (l_ObservedValues[3] + l_ObservedValues[4] + l_ObservedValues[5] + l_ObservedValues[6] + l_ObservedValues[7]) / 5;
        float f_centriod3 = (l_ObservedValues[8] + l_ObservedValues[9]) / 2;

        int p1, p2, p3;
        int lowerProfile = 0,
                mediumProfile = 0,
                highProfile = 0;

        for (int i = 0; i < observedValues.length; i++) {
            p1 = Math.abs((int) (observedValues[i] - f_centriod1));
            p2 = Math.abs((int) (observedValues[i] - f_centriod2));
            p3 = Math.abs((int) (observedValues[i] - f_centriod3));

            if (p1 > p2 && p1 > p3) {
                lowerProfile++;
            } else if (p2 <= p1 && p2 < p3) {
                mediumProfile++;
            } else {
                highProfile++;
            }
        }

        float probLow = (float) lowerProfile / 10;
        float probMedium = (float) mediumProfile / 10;
        float probHigh = (float) highProfile / 10;
        HMMFraudEvaluaterBeanLogger.info(rsBundle.getErrorMessage("INSIDE_CALCULATE_ALPHA") + " probLow:" + probLow + " probMedium:" + probMedium + " probHigh:" + probHigh);
        return probLow * probMedium * probHigh;
    }

    //phase 2
    public boolean isFraudulentBehavior(float[] observedValues, float currentObservation) {
        boolean result = true;
        float alpha1 = CalculateAlpha(observedValues);
        HMMFraudEvaluaterBeanLogger.info(rsBundle.getErrorMessage("ALPHA1")+":"+alpha1);

        observedValues[observedValues.length - 1] = currentObservation;
        float alpha2 = CalculateAlpha(observedValues);
        HMMFraudEvaluaterBeanLogger.info(rsBundle.getErrorMessage("ALPHA2")+":"+alpha2);

        if ((alpha1 - alpha2) >= 0) {
            HMMFraudEvaluaterBeanLogger.info(rsBundle.getErrorMessage("TRANSACTION_NOT_FRAUDLENT"));
            // Transaction is not Fraudulent in behavior
            result = false;
        }
        return result;
    }
}
