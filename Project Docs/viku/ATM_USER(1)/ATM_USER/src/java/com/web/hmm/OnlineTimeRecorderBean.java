/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.hmm;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;

/**
 *
 * @author Amrit
 */
public class OnlineTimeRecorderBean {

    Connection con;
    Logger onlineTimeRecorderBeanLogger;

    public OnlineTimeRecorderBean(Connection con) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        this.con = con;
        onlineTimeRecorderBeanLogger = Logger.getLogger("com.web.User.OnlineTimeRecorderBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            onlineTimeRecorderBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            onlineTimeRecorderBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public int recordLoginTime(int accountId) {
        int loginId = 0;
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Statement stmt = con.createStatement();
            java.util.Date date = new java.util.Date();
            String dateStr = ResourceBundleBean.toMysqlDateStr(new Date(date.getTime()));
            String query = "INSERT INTO hmm_loginhistory (AccountId, LoginTime)" +
                    "VALUES (" + accountId + " , " + dateStr + ");";
            onlineTimeRecorderBeanLogger.debug(query);
            stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs != null && rs.next()) {
                loginId = rs.getInt(1);
                onlineTimeRecorderBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                onlineTimeRecorderBeanLogger.info(rsBundle.getErrorMessage("LOGIN_TIME_RECORDED") + " LoginTime: " + dateStr);
            } else {
                onlineTimeRecorderBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                onlineTimeRecorderBeanLogger.info(rsBundle.getErrorMessage("LOGIN_TIME_NOT_RECORDED") + " LoginTime: " + dateStr);
            }
        } catch (SQLException ex) {
            onlineTimeRecorderBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ONLINE_TIME_RECORDER_BEAN") + " recordLoginTime");
            ex.printStackTrace();
        } catch (Exception ex) {
            onlineTimeRecorderBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ONLINE_TIME_RECORDER_BEAN") + " recordLoginTime");
            ex.printStackTrace();
        }
        return loginId;
    }

    public int recordLogoutTime(int loginId) {
        ResourceBundleBean rsBundle = new ResourceBundleBean();
        try {
            Statement stmt = con.createStatement();
            java.util.Date date = new java.util.Date();
            String dateStr = ResourceBundleBean.toMysqlDateStr(new Date(date.getTime()));
            String query = "UPDATE hmm_loginhistory SET LogoutTime = "+dateStr+" WHERE Id ="+loginId+";";
            onlineTimeRecorderBeanLogger.debug(query);
            stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs != null && rs.next()) {
                onlineTimeRecorderBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                onlineTimeRecorderBeanLogger.info(rsBundle.getErrorMessage("LOGOUT_TIME_RECORDED") + " LoginTime: " + dateStr);
            } else {
                onlineTimeRecorderBeanLogger.debug("QUERY_EXECUTED_SUCCESSFULLY");
                onlineTimeRecorderBeanLogger.info(rsBundle.getErrorMessage("LOGOUT_TIME_NOT_RECORDED") + " LoginTime: " + dateStr);
            }
        } catch (SQLException ex) {
            onlineTimeRecorderBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ONLINE_TIME_RECORDER_BEAN") + " recordLogoutTime");
            ex.printStackTrace();
        } catch (Exception ex) {
            onlineTimeRecorderBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_ONLINE_TIME_RECORDER_BEAN") + " recordLogoutTime");
            ex.printStackTrace();
        }
        return loginId;
    }
}
