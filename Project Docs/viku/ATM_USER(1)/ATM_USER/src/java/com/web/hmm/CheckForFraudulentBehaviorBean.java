/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.hmm;

import java.sql.*;
import com.web.locale.ResourceBundleBean;
import org.apache.log4j.Logger;
import com.web.connection.DBConnectionBean;
import java.util.*;

/**
 *
 * @author Amrit
 */
public class CheckForFraudulentBehaviorBean {

    Connection con;
    Logger checkForFraudulentBehaviorBeanLogger;
    ResourceBundleBean rsBundle;

    public CheckForFraudulentBehaviorBean(Connection con) {
        rsBundle = new ResourceBundleBean();
        this.con = con;
        checkForFraudulentBehaviorBeanLogger = Logger.getLogger("com.web.Hmm.CheckForFraudulentBehaviorBean");
        if (con == null) {
            con = (Connection) new DBConnectionBean().getCon();
            checkForFraudulentBehaviorBeanLogger.debug(rsBundle.getErrorMessage("DB_CONNECTION_REESTABLISHED"));
        } else {
            checkForFraudulentBehaviorBeanLogger.debug(rsBundle.getErrorMessage("UNABLE_TO_ESTABLISHED_DB_CONNECTION"));
        }
    }

    public boolean isFraudulentBehavior(int accountId, int n, float ammount, long withdrawalTime, String pageVisitSeq) {
        boolean result = true;
        boolean withdrawResult = observeWithdrawals(accountId, n, ammount);
        checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("IS_FRAUDLANT_BEHAVIOR") + " withdrawResult:" + withdrawResult);

        boolean withdrawalTimeResult = observeWithdrawalTime(accountId, n, withdrawalTime);
        checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("IS_FRAUDLANT_BEHAVIOR") + " withdrawalTimeResult:" + withdrawalTimeResult);

        boolean pageVisitSeqResult = observePageVisitSequence(accountId, n, pageVisitSeq);
        checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("IS_FRAUDLANT_BEHAVIOR") + " pageVisitSeqResult:" + pageVisitSeqResult);

        if (withdrawResult == false && withdrawalTimeResult == false && pageVisitSeqResult == false) {
            result = false;
        }

        return result;
    }

    private boolean observeWithdrawals(int accountId, int n, float ammount) {
        try {
            String query = "SELECT hmm_transaction.Amount FROM hmm_transaction WHERE hmm_transaction.AccountId=" + accountId + " and hmm_transaction.Type = 2 ORDER BY hmm_transaction.`Date` DESC LIMIT 0," + n + ";";
            checkForFraudulentBehaviorBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int rowcount = 0;
            if (rs.last()) {
                rowcount = rs.getRow();
            }
            if (rowcount < n) {
                checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("NEED_MORE_TRANSACTIONS_WITHDRAWAL") + rowcount);
                return false;
            }

            float[] observedValues = new float[10];
            rs.beforeFirst();
            int i = 0;
            while (rs.next()) {
                observedValues[i] = rs.getFloat("hmm_transaction.Amount");
                checkForFraudulentBehaviorBeanLogger.debug(observedValues[i]);
                i++;
            }
            HMMFraudEvaluaterBean eval = new HMMFraudEvaluaterBean();
            return eval.isFraudulentBehavior(observedValues, ammount);
        } catch (SQLException ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawals");
            ex.printStackTrace();
        } catch (Exception ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawals");
            ex.printStackTrace();
        }
        return true;
    }

    private boolean observeWithdrawalTime(int accountId, int n, long withdrawalTime) {
        try {
            String query = "SELECT hmm_transaction.WithdrawalTime FROM hmm_transaction WHERE hmm_transaction.AccountId=" + accountId + " and hmm_transaction.Type = 2 ORDER BY hmm_transaction.`Date` DESC LIMIT 0," + n + ";";
            checkForFraudulentBehaviorBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int rowcount = 0;
            if (rs.last()) {
                rowcount = rs.getRow();
            }
            if (rowcount < n) {
                checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("NEED_MORE_TRANSACTIONS_WITHDRAWALTIME") + rowcount);
                return false;
            }
            checkForFraudulentBehaviorBeanLogger.debug(" withdrawalTime: " + withdrawalTime);
            float[] observedValues = new float[10];
            rs.beforeFirst();
            int i = 0;
            while (rs.next()) {
                observedValues[i] = rs.getInt("hmm_transaction.WithdrawalTime");
                checkForFraudulentBehaviorBeanLogger.debug(observedValues[i]);
                i++;
            }
            HMMFraudEvaluaterBean eval = new HMMFraudEvaluaterBean();
            return eval.isFraudulentBehavior(observedValues, withdrawalTime);
        } catch (SQLException ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawalTime");
            ex.printStackTrace();
        } catch (Exception ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawalTime");
            ex.printStackTrace();
        }
        return true;
    }

    private boolean observePageVisitSequence(int accountId, int n, String pageVisitSeq) {
        try {
            String query = "SELECT hmm_transaction.PageVisitSeqence FROM hmm_transaction WHERE hmm_transaction.AccountId=" + accountId + " and hmm_transaction.Type = 2 ORDER BY hmm_transaction.`Date` DESC LIMIT 0," + n + ";";
            checkForFraudulentBehaviorBeanLogger.debug(query);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int rowcount = 0;
            if (rs.last()) {
                rowcount = rs.getRow();
            }
            if (rowcount < n) {
                checkForFraudulentBehaviorBeanLogger.info(rsBundle.getErrorMessage("NEED_MORE_TRANSACTIONS_PAGESEQUENCE") + rowcount);
                return false;
            }

            String[] observedValues = new String[10];
            rs.beforeFirst();
            int i = 0;
            while (rs.next()) {
                observedValues[i] = rs.getString("hmm_transaction.PageVisitSeqence");
                checkForFraudulentBehaviorBeanLogger.debug(observedValues[i]);
                i++;
            }

            //remove duplicates form observations
            List<String> list = Arrays.asList(observedValues);
            Set<String> set = new HashSet<String>(list);
            String[] result = new String[set.size()];

            set.toArray(result);
            checkForFraudulentBehaviorBeanLogger.debug(rsBundle.getErrorMessage("AFTER_REMOVING_DUPLICATES"));
            for (String s : result) {
                checkForFraudulentBehaviorBeanLogger.debug(s);
            }

            if (result.length > 6) {
                //Dont consider this observation type if sequence pattern is not consistent
                checkForFraudulentBehaviorBeanLogger.debug(rsBundle.getErrorMessage("DONT_CONSIDER_THIS_TYPE"));
                return true;
            }

            int occurence = 0;
            for (String s : observedValues) {
                if (s.equals(pageVisitSeq)) {
                    occurence++;
                }
            }
            checkForFraudulentBehaviorBeanLogger.debug("occurence: " + occurence + "  pageVisitSeq " + pageVisitSeq);
            if (occurence > 1) {
                return false;
            }

        } catch (SQLException ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawalTime");
            ex.printStackTrace();
        } catch (Exception ex) {
            checkForFraudulentBehaviorBeanLogger.error(ex.getLocalizedMessage() + ": " + rsBundle.getErrorMessage("INSIDE_CheckForFraudulentBehaviorBean") + " observeWithdrawalTime");
            ex.printStackTrace();
        }
        return true;
    }
}
