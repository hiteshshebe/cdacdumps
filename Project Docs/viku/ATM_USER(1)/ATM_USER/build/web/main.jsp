<%-- 
    Document   : main
    Created on : Mar 6, 2012, 10:53:45 PM
    
--%>

<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <title>
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            :
            <jsp:setProperty name="links" property="linkName" value="WELCOME" />
            <jsp:getProperty name="links" property="linkLabel" />
            &nbsp;${sessionScope.usrFirstName}&nbsp;${sessionScope.usrLastName}
        </title>
    </head>
    <body>
        <%
        if (session.getAttribute("loggedin") == null) {
            response.sendRedirect("index.jsp");
        }
        session.setAttribute("pageVisitSeq",session.getAttribute("pageVisitSeq").toString()+"1");
        %>
        <!--style="background-color:#B9CAFF"-->
        <div id="container">
            <%@include file="header.jsp" %>
            <div id="wrapper">
                <div id="content">
                    <div id="context">
                        <strong>
                            <jsp:setProperty name="links" property="linkName" value="WELCOME" />
                            <jsp:getProperty name="links" property="linkLabel" />
                            :
                            ${sessionScope.usrFirstName}&nbsp;${sessionScope.usrLastName}
                        </strong>
                    </div>
                </div>
            </div>
            <div id="navigation">
                <strong>
                    <ul>
                        <li>
                            <a href="depositamount.jsp">
                                <jsp:setProperty name="links" property="linkName" value="DEPOSIT_AMOUNT" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="withdrawal.jsp">
                                <jsp:setProperty name="links" property="linkName" value="WITHDRAWAL" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="CheckBalance.web">
                                <jsp:setProperty name="links" property="linkName" value="CHECK_AVAILABLE_BALANCE" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="AccountInfo.web">
                                <jsp:setProperty name="links" property="linkName" value="ACCOUNT_INFO" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                        <li>
                            <a href="changepin.jsp">
                                <jsp:setProperty name="links" property="linkName" value="CHANGE_PIN" />
                                <jsp:getProperty name="links" property="linkLabel" />
                            </a>
                        </li>
                    </ul>
                </strong>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>
