<%-- 
    Document   : index
    Created on : Mar 6, 2012, 7:21:45 PM
    
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="error.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean class="com.web.locale.ResourceBundleBean" id="links" scope="application" />

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css"/>
        <script type="text/javascript" src="js/j_script.js"></script>
        <title>
            
            <jsp:setProperty name="links" property="linkName" value="APPLICATION_WINDOW_TITLE" />
            <jsp:getProperty name="links" property="linkLabel" />
            :
            <jsp:setProperty name="links" property="linkName" value="PLEASE_LOGIN" />
            <jsp:getProperty name="links" property="linkLabel" />
        </title>
    </head>
    <body>
        <div id="container" >
            <%@include file="header.jsp" %>
            <div id="mainpagelogo" align="right">
                <div id="content">
                    <img src="images/main_page_image.jpg" style="padding:40px" width="60%" >
                </div>
            </div>
            <div id="login">
                <form class="loginform" name="login" id="login" action="ValidateUser.web" method="post" onsubmit="return validateUserLogin();">
                    <table border="0" style="background-color:white;padding:60px">
                        <tbody>
                        <input type="hidden" name="h_no" value ="<% if (request.getAttribute("h_no") != null) {%><%= request.getAttribute("h_no")%><%}%>">
                        <input type="hidden" name="h_atm_no" value ="<% if (request.getAttribute("h_atm_no") != null) {%><%= request.getAttribute("h_atm_no")%><%}%>">
                            <tr>
                                <td colspan="3" align="center">
                                    <b>
                                        <h2>
                                            <jsp:setProperty name="links" property="linkName" value="PLEASE_LOGIN" />
                                            <jsp:getProperty name="links" property="linkLabel" />
                                        </h2>
                                    </b>
                                </td>
                            </tr>
                            <!--<c:if test="${requestScope.successMessage != null && requestScope.successMessage != ''}">
                                <tr>
                                    <td colspan="3" align="center">
                                        <span id="successMessage" style="color:blue;font-size:14px;font-weight:bold">${requestScope.successMessage}</span>
                                    </td>
                                </tr>
                            </c:if>-->
                            <c:if test="${requestScope.errorMessage != null && requestScope.errorMessage != ''}">
                                <tr>
                                    <td colspan="3" align="center">
                                        <span id="errorMessage" style="color:red;font-size:14px;font-weight:bold">${requestScope.errorMessage}</span>
                                    </td>
                                </tr>
                            </c:if>
                            <tr>
                                <td align="left">
                                    <strong>
                                        <jsp:setProperty name="links" property="linkName" value="ATM_CARD_NUMBER" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </strong>
                                </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="atmnumber"  value ="<% if (request.getAttribute("atmnumber") != null) {%><%= request.getAttribute("atmnumber")%><%}%>" size="25" maxlength="16"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <strong>
                                        <jsp:setProperty name="links" property="linkName" value="PIN" />
                                        <jsp:getProperty name="links" property="linkLabel" />
                                    </strong>
                                </td>
                                <td>:</td>
                                <td>
                                    <input type="password" name="pin"  size="25" maxlength="4"/>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td align="left">
                                    <input type="submit"
                                           value=<jsp:setProperty name="links" property="linkName" value="SUBMIT" />
                                           <jsp:getProperty name="links" property="linkLabel" />
                                           name="Submit" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div id="extra">
                <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
            </div>
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>
<%
if(request.getAttribute("withDrawalSuccessMessage")!=null)
{
    %>
    <script>
        alert("<%=request.getAttribute("withDrawalSuccessMessage")%>");
    </script>
    <%
}
%>
