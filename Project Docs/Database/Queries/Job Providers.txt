1. Table for job providers

create table job_provider(
jprovider_id number(10) primary key,
person_name varchar2(30) not null,
email varchar2(30) unique,
passwd varchar2(20) not null,
stdcode number(5),
phone number(10),
job_title varchar2(30),
corp_name varchar2(50),
industry varchar2(50),
company_type varchar2(50),
mobile number(12),
address varchar2(100),
city varchar2(20),
state varchar2(30),
zip varchar2(20),
country varchar2(20)
)
/



2. Sequence for job provider id

create sequence jprovider_sequence
start with 1000
increment by 1
/


3.

create or replace trigger job_provider_trigger
before insert
on job_provider
referencing new as new
for each row
begin
select jprovider_sequence.nextval into:new.jprovider_id from dual;
end;
/


4.

insert into job_provider(jprovider_id,person_name,email,passwd) values(0,'narayan murthy','narayan@gmail.com','123456')
/
