#include <fstream>
#include <iostream>


using namespace std;
int main()
{
   char buffer[255];    

   ofstream fout("text.txt");  
   fout << "This line written directly to the file...\n";
   cout << "Enter text for the file: ";
   
   cin.getline(buffer,255);

   fout << buffer << "\n";
   fout.close();         

   ifstream fin("text.txt");    
   char ch;
   while (fin.get(ch))
      cout << ch;

   fin.close();
   return 0;
}