/*Define a C++ class fraction as follows						
Class fraction 
{        long numerator;
		         long denominator;
		public :  fraction (long n=0,long d=1);
		}    	
Overload the following operators appropriately as member or friend
Unary : ++(both pre and post)
Overload as friend functions operators <<,>>
*/
#include<iostream.h>
class fraction
{
	long numerator;
	long denominator;
	public:
	fraction(long n=0,long d=1)
	{
		numerator=n;
		denominator=1;
	}
	fraction operator ++(int)
	{
		return fraction(++numerator,++denominator);
	}
	fraction operator ++(void)
	{
		return fraction(numerator++,denominator++);
	}
      friend	istream& operator >> (istream&, fraction&);
      friend	ostream& operator << (ostream&, fraction&);
};
istream&  operator >> (istream& in, fraction& f)
{
	cout<<"\n Numerator = ";
	in>>f.numerator;
	cout<<"\n Denominator :";
	in>>f.denominator;
	return in;
}
ostream& operator << (ostream& out,fraction& f)
{
       out<<"\n Fraction : "<<f.numerator<<"/"<<f.denominator;
       return out;
}
void main()
{
	fraction F1,F2,F3,F4;
	clrscr();
	cout<<"\n Enter Fraction :-";
	cin>>F1;
	F4=F1;
	cout<<"\n F1 :-"<<F1;
	F2=F1++;
	cout<<"\n\n F2 :-"<<F2;
	F3=++F2;
	cout<<"\n\n F3 :"<<F3;
	getch();
}

	/* Output :-

	 Enter Fraction :-
	 Numerator = 5

	 Denominator :6

	 F1 :-
	 Fraction : 5/6

	 F2 :-
	 Fraction : 6/1

	 F3 :
	 Fraction : 6/1
	*/
