
#include <iostream>
using namespace std;


class Myclass
{
private:
		int n;
public:
		Myclass()
		{
			n=200;
		}
		int getval()
		{
			return n;
		}

};
void throwobject() throw (Myclass)
{
	Myclass mobj;

	try{

	if(mobj.getval()>100)
		throw mobj;
	}catch(Myclass m)
	{
		cout<<"\nCatch : Value of object is :"<<m.getval()<<"\n";
	}

}


void myFunction(int test)
{
  cout << "Inside myFunction, test is: " << test << "\n";
  if(test) throw test;
}
int main()
{
  cout << "Start\n";
  try { 
    cout << "Inside try block\n";
    myFunction(0);
    myFunction(1);
    myFunction(2);
  }
  catch (int i) { 
    cout << "Caught an exception -- value is: ";
    cout << i << "\n";
  }

  throwobject();

  cout << "End\n";
  return 0;
}