#include<iostream>
#include<string>
using namespace std;

#define XWIN 1
#define XDE 2

// family 1 - XWin Tool kit

class XWinButton
{
	// some data here
public:
	void show() 
	{
		cout<<"XWinButton"<<endl;
	}
	void hide() {}
	void setText(string text) {}
	string getText() { return ""; }
	void setSize(int size) {}
	// etc...
};

class XWinLabel
{
	// some data here...
public:
	void show()
	{
		cout<<"XWinLabel"<<endl;
	}
	void hide() {}
	void setLabel(string lbl) {}
	string getLabel() { return ""; }
	void setFont(int fontType) {}
	void setStyle(int style) {}
	// etc..
};

class XWinWindow
{
	// some data here
public:
	void setVisible(bool flag)
	{
		if(flag)
			cout<<"XWinWindow"<<endl;
	}
	void add() {}
	void remove() {}
	void setTitle(string title) {}
	// etc...
};

// and other components of the family go here...

// family 2 - XDE Tool kit

class XDEButton
{
	// some data here
public:
	void show() 
	{
		cout<<"XDEButton"<<endl;
	}
	void hide() {}
	void setText(string text) {}
	string getText() { return ""; }
	void setSize(int size) {}
	// etc...
};

class XDELabel
{
	// some data here...
public:
	void show()
	{
		cout<<"XDELabel"<<endl;
	}
	void hide() {}
	void setLabel(string lbl) {}
	string getLabel() { return ""; }
	void setFont(int fontType) {}
	void setStyle(int style) {}
	// etc..
};

class XDEWindow
{
	// some data here
public:
	void setVisible(bool flag)
	{
		if(flag)
			cout<<"XDEWindow"<<endl;
	}
	void add() {}
	void remove() {}
	void setTitle(string title) {}
	// etc...
};

// and other components of the family go here...

class AddAccountForm
{
public:
	void showForm(int lfType)
	{
		// this form is very tightly coupled with the family
		// and the products as it has to create the components

		if(lfType==XWIN)
		{
			XWinWindow *w1 = new XWinWindow;
			XWinButton *b1 = new XWinButton;
			XWinLabel *l1 = new XWinLabel;

			w1->setVisible(true);
			b1->show();
			l1->show();
		}
		else if(lfType==XDE)
		{
			XDEWindow *w1 = new XDEWindow;
			XDEButton *b1 = new XDEButton;
			XDELabel *l1 = new XDELabel;

			w1->setVisible(true);
			b1->show();
			l1->show();
		}
	}
};

// and MANY such forms go here...

void main()
{
	int lfType;
	cout<<"Select look and feel (1.XWin, 2.XDE) : ";
	cin>>lfType;

	AddAccountForm *form = new AddAccountForm;
	form->showForm(lfType);
}