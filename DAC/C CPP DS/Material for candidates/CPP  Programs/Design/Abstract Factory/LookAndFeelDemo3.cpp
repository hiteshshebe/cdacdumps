#include<iostream>
#include<string>
#include<map>
using namespace std;

#define XWIN 1
#define XDE 2

// product 1 - Button

struct IButton
{
	virtual void show() = 0;
	virtual void hide() = 0;
	virtual void setText(string text) = 0;
	virtual string getText() =0 ;
	virtual void setSize(int size) = 0;
};

class XWinButton : public IButton
{
	// some data here
public:
	void show() 
	{
		cout<<"XWinButton"<<endl;
	}
	void hide() {}
	void setText(string text) {}
	string getText() { return ""; }
	void setSize(int size) {}
	// etc...
};

class XDEButton : public IButton
{
	// some data here
public:
	void show() 
	{
		cout<<"XDEButton"<<endl;
	}
	void hide() {}
	void setText(string text) {}
	string getText() { return ""; }
	void setSize(int size) {}
	// etc...
};

// product 2 - Label

class ILabel
{
	public:

	virtual void show() = 0;
	virtual void hide() =0;
	virtual void setLabel(string lbl) = 0;
	virtual string getLabel() = 0;
	virtual void setFont(int fontType) =0;
	virtual void setStyle(int style) = 0;
	// etc..

};

class XWinLabel : public ILabel
{
	// some data here...
public:
	void show()
	{
		cout<<"XWinLabel"<<endl;
	}
	void hide() {}
	void setLabel(string lbl) {}
	string getLabel() { return ""; }
	void setFont(int fontType) {}
	void setStyle(int style) {}
	// etc..
};

class XDELabel : public ILabel
{
	// some data here...
public:
	void show()
	{
		cout<<"XDELabel"<<endl;
	}
	void hide() {}
	void setLabel(string lbl) {}
	string getLabel() { return ""; }
	void setFont(int fontType) {}
	void setStyle(int style) {}
	// etc..
};

// product 3 - Window

class IWindow
{
	// some data here
public:
	virtual void setVisible(bool flag) = 0;	
	virtual void add()  = 0;
	virtual void remove()  = 0;
	virtual void setTitle(string title) = 0;
	// etc...
};

class XWinWindow : public IWindow
{
	// some data here
public:
	void setVisible(bool flag)
	{
		if(flag)
			cout<<"XWinWindow"<<endl;
	}
	void add() {}
	void remove() {}
	void setTitle(string title) {}
	// etc...
};

class XDEWindow : public IWindow
{
	// some data here
public:
	void setVisible(bool flag)
	{
		if(flag)
			cout<<"XDEWindow"<<endl;
	}
	void add() {}
	void remove() {}
	void setTitle(string title) {}
	// etc...
};

// abstract factory

struct AbstractFactory
{
	virtual ILabel*		CreateLabel()	= 0;
	virtual IWindow*	CreateWindow()	= 0;
	virtual IButton*	CreateButton()	= 0;
};

class XWinFactory : public AbstractFactory
{
	public:

	ILabel*	CreateLabel()
	{
		return new XWinLabel;
	}

	IWindow* CreateWindow()
	{
		return new XWinWindow;		
	}
	IButton* CreateButton()
	{
		return new XWinButton;
	}
};

class XDEFactory : public AbstractFactory
{
	public:

	ILabel*	CreateLabel()
	{
		return new XDELabel;
	}

	IWindow* CreateWindow()
	{
		return new XDEWindow;		
	}
	IButton* CreateButton()
	{
		return new XDEButton;
	}
};

class AbstractFactoryManager		// factory method
{
	map<int,AbstractFactory*> family;
public:
	AbstractFactoryManager()
	{
		family.insert(pair<int,AbstractFactory*>(XWIN,new XWinFactory));
		family.insert(pair<int,AbstractFactory*>(XDE,new XDEFactory));
	}
	AbstractFactory* CreateFactory(int nFamily)
	{
		AbstractFactory* pFactory = NULL;
		
		map<int,AbstractFactory*>::iterator pItr =  family.find(nFamily);
		if(pItr!=family.end())
			pFactory = pItr->second;

		return pFactory;
	}

};

// and other components of the family go here...

class AddAccountForm
{
public:
	void showForm(int lfType)
	{
		// this form is very tightly coupled with the family
		// and the products as it has to create the components

		AbstractFactoryManager* mgr = new AbstractFactoryManager;

		AbstractFactory* pFamilyFactory = mgr->CreateFactory(lfType);

		IWindow *w1 = pFamilyFactory->CreateWindow();
		IButton *b1 = pFamilyFactory->CreateButton();
		ILabel *l1	= pFamilyFactory->CreateLabel();

		w1->setVisible(true);
		b1->show();
		l1->show();


		/*if(lfType==XWIN)
		{
			XWinWindow *w1 = new XWinWindow;
			XWinButton *b1 = new XWinButton;
			XWinLabel *l1 = new XWinLabel;

			w1->setVisible(true);
			b1->show();
			l1->show();
		}
		else if(lfType==XDE)
		{
			XDEWindow *w1 = new XDEWindow;
			XDEButton *b1 = new XDEButton;
			XDELabel *l1 = new XDELabel;

			w1->setVisible(true);
			b1->show();
			l1->show();
		}*/
	}
};

// and MANY such forms go here...

void main()
{
	int lfType;
	cout<<"Select look and feel (1.XWin, 2.XDE) : ";
	cin>>lfType;

	AddAccountForm *form = new AddAccountForm;
	form->showForm(lfType);
}