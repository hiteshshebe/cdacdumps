#include<iostream>
#include<vector>
using namespace std;

class A
{
	int *x;	
	int size;
public:
	A() : x(0), size(0) {	}
	A(int sz) : x(0), size(sz)
	{
		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = i+1;
	}
	A(const A& a) : size(a.size)
	{
		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = a.x[i];
	}
	~A()
	{
		delete[] x;
	}
	A& operator=(const A& a)
	{
		if(this==&a)
			;
		else
		{
			if(size!=a.size)
			{
				delete[] x;
				size = a.size;
				x = new int[size];
			}

			for(int i=0;i<size;++i)
				x[i] = a.x[i];
		}

		return *this;
	}	
	int& operator[](int index)
	{
		return x[index];
	}
};

A f1(A obj)
{
	A a1(6);
	return a1;
}

void main()
{
	A a1(5),a2(6);
	a2 = f1(a1);

	A a3;

	a3 = a2 = a1;

	a3 = a3;
}