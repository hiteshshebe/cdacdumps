#include<iostream>
#include<algorithm>
#include<functional>
#include<vector>
#include<string>
#include<list>
using namespace std;

#define BY_AGE 1

class Employee
{
	string name;
	int age;
public:
	Employee() : age(0) {}
	Employee(string nm,int a) : name(nm), age(a) {}
	int getAge()
	{
		return age;
	}
	string getName()
	{
		return name;
	}
	friend ostream& operator<<(ostream& stream,const Employee& e)
	{
		stream<<endl<<e.name<<"\t"<<e.age;		
		return stream;
	}
	bool operator>(const Employee& e)
	{
		return age > e.age;
	}
};

class EmpCriteria	// My unary function object
{
	int age;
public:
	EmpCriteria(int a) : age(a) {}
	Employee* operator()(Employee *e)
	{
		return (e->getAge()>age) ? e : 0;
	}
};

template<class II>
void display(II F,II L)
{
	for(;F!=L;++F)
		if(*F!=NULL)
			cout<<**F<<"\t";
	cout<<endl;
}

class DBMgr
{
public:
	static vector<Employee*>* getAllEmployees()
	{
		// fires the SQL and returns a collection of Employees

		// example...

		vector<Employee*> *v1 = new vector<Employee*>;

		v1->push_back(new Employee("ABC",25));
		v1->push_back(new Employee("ABC",23));
		v1->push_back(new Employee("ABC",22));
		v1->push_back(new Employee("ABC",21));
		v1->push_back(new Employee("ABC",20));
		v1->push_back(new Employee("ABC",28));
		v1->push_back(new Employee("ABC",30));
		v1->push_back(new Employee("ABC",31));

		return v1;
	}
};

class BusinessCriteriaMgr
{
public:
	static vector<Employee*>* getByCriteria(vector<Employee*> *v1,int type,int val)
	{
		vector<Employee*> *v2 = new vector<Employee*>(v1->size());
		if(type==BY_AGE)
			transform(v1->begin(),v1->end(),v2->begin(),EmpCriteria(val));

		return v2;
	}
};

void main()
{
	vector<Employee*> *v1 = DBMgr::getAllEmployees();

	int age;
	cout<<"Enter age criteria : ";
	cin>>age;

	vector<Employee*> *v2 = BusinessCriteriaMgr::getByCriteria(v1,BY_AGE,age);

	display(v1->begin(),v1->end());	
	display(v2->begin(),v2->end());
}