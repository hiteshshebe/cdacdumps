#include<iostream>
#include<string>
using namespace std;

int operation(int arg1,int arg2,char op)
{
	switch(op)
	{
	case '+':return arg1 + arg2;
	case '-':return arg1 - arg2;
	case '*':return arg1 * arg2;
	case '/':return arg1 / arg2;
	// and so on
	}
	return 0;
}

void Transform(int *F1,int *L1,int *F2,int *F3,char op)
{
	for(;F1!=L1;++F1,++F2,++F3)
		*F3 = operation(*F1,*F2,op);
}

void display(int *F,int *L)
{
	for(;F!=L;++F)
		cout<<*F<<"\t";
	cout<<endl;
}

void main()
{
	int a[] = {1,5,9,6,3};
	int b[] = {2,7,5,20,1};
	int c[] = {0,0,0,0,0,0,0,0,0,0};

	Transform(a+1,a+5,b+0,c+0,'+');

	display(a,a+5);
	display(b,b+5);
	display(c,c+5);
}