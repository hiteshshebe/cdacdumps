#include<iostream>
using namespace std;

class A
{
	int *x;	
	int size;
public:
	A() : x(0), size(0)
	{
		cout<<"A()"<<this<<endl;
	}
	A(int sz) : x(0), size(sz)
	{
		cout<<"A(int)"<<this<<endl;
		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = i+1;
	}
	A(const A& a) : size(a.size)
	{
		cout<<"A(A&)"<<this<<endl;

		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = a.x[i];
	}
	~A()
	{
		cout<<"~A()"<<this<<endl;
		delete[] x;
	}
	void operator=(const A& a)
	{
		cout<<"operator="<<this<<endl;
		cout<<&a<<endl;

		if(size!=0)
			delete[] x;
		
		size = a.size;

		x = new int[size];
		for(int i=0;i<size;++i)
			x[i] = a.x[i];
	}	
};

A f1(A obj)
{
	A a1(6);
	return a1;
}

void main()
{
	A a1(5),a2(3);
	a2 = f1(a1);
}