#include<iostream>
#include<string>
#include<vector>
using namespace std;

// class Allocator

template<class T>
class Allocator
{
	typedef T value;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
public:
	void allocate(pointer &p,int size)
	{
		p = new T[size];
	}
	void deallocator(pointer p,int size)
	{
		// ignore size

		delete[] p;
	}
	void construct(reference target,value source)
	{
		cout<<"Allocator"<<endl;
		target = source;
	}
};

// partial specialization for <char*>

void Allocator<char*>::allocate(pointer &p,int size)
{
	p = new T[size];
	for(int i=0;i<size;++i)
		p[i] = new char[20];
}
void Allocator<char*>::deallocator(pointer p,int size)
{
	// use size

	for(int i=0;i<size;++i)
		delete[] p[i];
	delete[] p;
}
void Allocator<char*>::construct(reference target,value source)
{
	strcpy(target,source);
}

// class MyAllocator

template<class T>
class MyAllocator : public Allocator<T>
{
public:
	void construct(reference target,value source)
	{
		Allocator<T>::construct(target,source);
	}
};

// partial specialization for <char*>

void MyAllocator<char*>::construct(reference target,value source)
{
	cout<<"MyAllocator"<<endl;

	if(strlen(target)!=0)
		delete target;

	target = new char[20];
	strcpy(target,source);
}

template<class T,class A = Allocator<T> >
class Array
{
	T *arr;
	int size;
	A allocator;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],initVal);
	}
	Array(int sz,T *x) : size(sz)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],x[i]);
	}
	Array(const Array<T,A>& a) : size(a.size)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],a[i]);
	}
	Array<T,A>& operator=(const Array<T,A>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			allocator.allocate(arr,size);

			for(int i=0;i<size;++i)
				allocator.construct(arr[i],a[i]);
		}
		return *this;
	}
	~Array() 
	{
		allocator.deallocator(arr,size);
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

void main()
{
	int x[] = {1,2,3};

	Array<int> a1(3,x);
	cout<<a1;

	char* xStr[] = {"ABC","XYZ","PQR"};

	Array<char*,MyAllocator<char*> > a2(3,xStr);
	cin>>a2;
	cout<<a2;
}