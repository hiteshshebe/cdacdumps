#include<iostream>
#include<string>
using namespace std;

#define MAX_TASKS 20
#define WINDOWS 1
#define LINUX 2

// class Allocator

template<class T>
class Allocator
{
	typedef T value;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
public:
	void allocate(pointer &p,int size)
	{
		p = new T[size];
	}
	void deallocator(pointer p,int size)
	{
		// ignore size

		delete[] p;
	}
	void construct(reference target,value source)
	{
		target = source;
	}
};

// partial specialization for <char*>

void Allocator<char*>::allocate(pointer &p,int size)
{
	p = new T[size];
	for(int i=0;i<size;++i)
		p[i] = new char[20];
}
void Allocator<char*>::deallocator(pointer p,int size)
{
	// use size

	for(int i=0;i<size;++i)
		delete[] p[i];
	delete[] p;
}
void Allocator<char*>::construct(reference target,value source)
{
	strcpy(target,source);
}

// class Array

template<class T,class A = Allocator<T> >
class Array
{
	T *arr;
	int size;
	A allocator;
public:
	Array()	: arr(0), size(0) {}
	Array(int sz,T initVal=T()) : size(sz) 
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],initVal);
	}
	Array(int sz,T *x) : size(sz)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],x[i]);
	}
	Array(const Array<T,A>& a) : size(a.size)
	{
		allocator.allocate(arr,size);
		for(int i=0;i<size;++i)
			allocator.construct(arr[i],a[i]);
	}
	Array<T,A>& operator=(const Array<T,A>& a) 
	{
		if(this==&a)
			;
		else
		{
			if(size) this->~Array();

			size = a.size;
			allocator.allocate(arr,size);

			for(int i=0;i<size;++i)
				allocator.construct(arr[i],a[i]);
		}
		return *this;
	}
	~Array() 
	{
		allocator.deallocator(arr,size);
		arr = 0;
		size = 0;
	}
	T& operator[](size_t index)
	{
		return arr[index];
	}
	const T& operator[](size_t index) const
	{
		return arr[index];
	}
	friend ostream& operator<<(ostream& stream,const Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream<<a[i]<<"\t";
		stream<<endl;
		return stream;
	}
	friend istream& operator>>(istream& stream,Array<T,A>& a)
	{
		for(int i=0;i<a.size;++i)
			stream>>a[i];
		return stream;
	}
};

template<class OS>
struct Task
{
	virtual void showTask()=0;
};

template<class OS>
class TaskManager
{
	static TaskManager<OS>* instance;
	OS os;
	int numTasks;
	Array<Task<OS>*> *taskList;
	TaskManager() : numTasks(0)
	{
		taskList = new Array<Task<OS>*>(MAX_TASKS);
	}
public:
	static TaskManager<OS>* getInstance()
	{
		if(instance==0)
			instance = new TaskManager<OS>;
		return instance;
	}
	void add(Task<OS>* task)
	{
		(*taskList)[numTasks++] = task;
	}
	void showOS()
	{
		os.show();	// delegation
	}
	void showAllTasks()
	{
		for(int i=0;i<numTasks;++i)
		(*taskList)[i]->showTask();
	}
};

template<class OS>
TaskManager<OS>* TaskManager<OS>::instance;

template<class OS>
class Word : public Task<OS>
{
public:
	Word()
	{
		// register with the TaskManager

		TaskManager<OS>::getInstance()->add(this);
	}
	void showTask()
	{
		cout<<"Executing Word"<<typeid(OS).name()<<endl;
	}
};

template<class OS>
class Spreadsheet : public Task<OS>
{
public:
	Spreadsheet()
	{
		// register with the TaskManager

		TaskManager<OS>::getInstance()->add(this);
	}
	void showTask()
	{
		cout<<"Executing Spreadsheet : "<<typeid(OS).name()<<endl;
	}
};

template<class OS>
class CPPIDE : public Task<OS>
{
public:
	CPPIDE()
	{
		// register with the TaskManager

		TaskManager<OS>::getInstance()->add(this);
	}
	void showTask()
	{
		cout<<"Executing CPPIDE"<<typeid(OS).name()<<endl;
	}
};

class Windows
{
public:
	void show()
	{
		cout<<"OS : Windows"<<endl;
	}
};

class Linux
{
public:
	void show()
	{
		cout<<"OS : Linux"<<endl;
	}
};

template<class OS>
class Client
{
public:
	void execute()
	{
		Word<OS> *w1 = new Word<OS>;
		Word<OS> *w2 = new Word<OS>;

		Spreadsheet<OS> *s1 = new Spreadsheet<OS>;

		CPPIDE<OS> *c1 = new CPPIDE<OS>;

		TaskManager<OS>::getInstance()->showAllTasks();
	}
};
void main()
{
	int osType;

	cout<<"Select OS (1.Windows,2.Linux) : ";
	cin>>osType;

	if(osType==WINDOWS)
	{
		Client<Windows> *client = new Client<Windows>;
		client->execute();
	}
	else if(osType==LINUX)
	{
		Client<Linux> *client = new Client<Linux>;
		client->execute();
	}
}