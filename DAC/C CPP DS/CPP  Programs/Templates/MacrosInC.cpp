#include<iostream>
using namespace std;

#define SWAP(a,b,temp) \
	temp = a;	\
	a = b;		\
	b = temp;

void main()
{
	int a=10;
	int b = 20;

	float x = 12.3f;
	float y = 21.5f;

	{
	int temp;
	cout<<a<<"\t"<<b<<endl;

	SWAP(a,b,temp);
	}

	cout<<a<<"\t"<<b<<endl;
	{
	float temp2;
	
	cout<<x<<"\t"<<y<<endl;
	
	SWAP(x,y,temp2);
	}
	cout<<x<<"\t"<<y<<endl;
}