#include<iostream>
#include<string>
using namespace std;

// class Allocator

template<class T>
class Allocator
{
	typedef T value;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
public:
	void allocate(pointer &p,int size)
	{
		p = new T[size];
	}
	void deallocate(pointer p,int size)
	{
		// ignore size

		delete[] p;
	}
	void construct(reference target,value source)
	{
		target = source;
	}
};

// partial specialization for <char*>

void Allocator<char*>::allocate(pointer &p,int size)
{
	p = new T[size];
	for(int i=0;i<size;++i)
		p[i] = new char[20];
}
void Allocator<char*>::deallocate(pointer p,int size)
{
	// use size

	for(int i=0;i<size;++i)
		delete[] p[i];
	delete[] p;
}
void Allocator<char*>::construct(reference target,value source)
{
	strcpy(target,source);
}

// class LinkedList

template<class T,class A = Allocator<T> >
class LinkedList
{
	struct Node
	{
		Node* prev;
		Node* next;
		T *data;
	public:
		Node(T _data) : prev(0), next(0)
		{
			A().allocate(data,1);
			*data = _data;
		}
		~Node()
		{
			cout<<*data<<endl;
			A().deallocate(data,1);
		}
	};
	Node *start;
	Node *last;	
public:
	LinkedList() : start(0), last(0) {}
	~LinkedList() 
	{
 		cout<<"Deleting the linked list"<<endl;

		Node *iterator = last ? last->prev : 0;
		while(iterator)
		{
			delete iterator->next;
			iterator = iterator->prev;
		}
		delete start;
	}
	void push_back(T data)
	{
		if(start==0)
			last = start = new Node(data);
		else
		{
			last->next = new Node(data);
			last->next->prev = last;
			last = last->next;
		}
	}
	void display() 
	{
		Node *iterator = start;
		while(iterator)
		{
			cout<<*(iterator->data)<<"\t";
			iterator = iterator->next;
		}
		cout<<endl;
	}
};

void main()
{
	LinkedList<int> l1;

	for(int i=0;i<5;++i)
		l1.push_back(i+1);

	l1.display();
}