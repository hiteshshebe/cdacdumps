#include<iostream>
#include<string>
using namespace std;

class TaskManager
{
	static TaskManager* instance;
public:
	TaskManager() {}
	void* operator new(size_t size)
	{
		if(instance==0)
			instance = (TaskManager*) malloc(size);;
		return instance;
	}
};

TaskManager* TaskManager::instance;

// many clients

void client1()
{
	TaskManager *tm = new TaskManager;
	cout<<tm<<endl;
}

void client2()
{
	TaskManager *tm = new TaskManager;
	cout<<tm<<endl;
}

void main()
{
	TaskManager *tm = new TaskManager;
	cout<<tm<<endl;

	client1();

	client2();
}