/*program for allocating memory for string at runtime
using pointer */

#include<conio.h>
#include<stdio.h>
#include<string.h>
#include<alloc.h>
void main()
{
	int ln;
	char *pstr;
	clrscr();
	printf("\nEnter Length Of String (in BYTES) : ");
	scanf("%d",&ln);
	pstr=(char *)malloc(ln+1);
	printf("\nEnter String : ");
	fflush(stdin);
	gets(pstr);
	puts(strupr(pstr));

}/*end of main */
