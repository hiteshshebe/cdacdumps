/*
Design a base class person(name, address, phone_no).
Derive a class  employee(e_no, e_name) from person.Derive a class manager (designation, department, basic_salary) from employee.
Write a menu driven program to
-Accept all details of �n� manager
-Display manager having highest salary.
(Use private inheritance)
*/				

#include<iostream.h>
class person
{
	char name[20],addr[20];
	long int phone;
	public:
	void accept(void);
	void display(void);
};
void person::accept()
{
	cout<<"Enter the Name Of Person:-";
	cin>>name;
	cout<<"Enter its Address:-";
	cin>>addr;
	cout<<"Enter Phone no:- ";
	cin>>phone;
}
void person::display()
{
	cout<<"\nName     :";
	cout<<name;
	cout<<"\nAddress  :";cout<<addr;
	cout<<"\nPhone no.:";cout<<phone;
}
class employee:private person
{
	int e_no;
	public:
	void accept(void);
	void display(void);
};
void employee::accept()
{
	person::accept();
	cout<<"Enter Employee no.:";
	cin>>e_no;
}
void employee:: display()
{
	person::display();
	cout<<"\nEmployee number:-";cout<<e_no;cout<<"\n";
}
class manager:private employee
{
	int salary;
	char designation[10],department[10];
	public:
	void accept(void);
	void display(void);
	int sal(void);
};
void manager::accept()
{
	employee::accept();
	cout<<"Enter Its Designation:-";
	cin>>designation;
	cout<<"Enter the Department in which he works:-";
	cin>>department;
	cout<<"Enter his Salary";
	cin>>salary;
}
void manager::display()
{
	employee::display();
	cout<<"\nDesignation:-";cout<<designation;
	cout<<"\nDepartment :-";cout<<department;
	cout<<"\nSalary     :-";cout<<salary;
}
int manager::sal()
{
	return salary;
}
int main()
{
	manager M[100];
	int n,i,salary=0,k,A,ch;
//	clrscr();
	cout<<"Enter How Many managers:-";
	cin>>n;
	for(i=1;i<=n;i++)
	{
		cout<<"Enter Data For "<<i<<" Manager:-\n";
		M[i].accept();
	}
	for(;;)
	{
		cout<<"\n1:Add Manager \n";
		cout<<"2:Retrive Data For Manager\n";
		cout<<"3:Display All Managers\n";
		cout<<"4:Manager With High Salary\n";
		cout<<"5:Exit\n";
		cout<<"Enter your Choice:";
		cin>>ch;
		switch(ch)
		{
			case 1: M[i].accept();
				i++;
				n++;
				break;
			case 2: cout<<"Enter Which Manager:-";
				cin>>A;
				M[A].display();
				break;
			case 3:for(i=1;i<=n;i++)
				{
					cout<<"\nDetails Of "<<i<<" Manager:-\n";
					M[i].display();
				}
				break;
			case 4:for(i=1;i<=n;i++)
				{
					if(M[i].sal()>salary)
					{
						k=i;
						salary=M[i].sal();
					}
				}
				M[k].display();
				break;
			case 5:exit(0);
		}
	}
    return 0;
}
