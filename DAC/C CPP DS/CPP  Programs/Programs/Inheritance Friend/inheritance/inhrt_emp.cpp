/*
Write class declarations and member function definitions for a 
c++ base class to represent an employee (code, name, designation).
Design derived classes  as emp_account( accounting number ,joining date) from employee and emp_sal(basic+pay, earnings ,deduction) from emp_accout.
Write a menu driven program to
-Build a master table
-Sort all entries 
-Display
(Use array of objects) */

#include<iostream.h>
#include<conio.h>

int n;

class employee
{
	int code;
	char name[30],designation[30];
	public:
	void accept(void);
	void display(void);
	int search(int);
};
void employee::accept()
{
	cout<<"\nEnter the Code Of Employee:";
	cin>>code;
	cout<<"Enter Name Of The Employee:";
	cin>>name;
	cout<<"Enter Designation Of Employee:";
	cin>>designation;
}
void employee::display()
{
	cout<<"\nCode Of Employee:"<<code;
	cout<<"\nName Of Employee:"<<name;
	cout<<"\nDesignation Of Employee:"<<designation;
}
int employee::search(int k)
{
		if(code==k)
		return 0;
		else
		return 1;
}
class emp_account: virtual public employee
{
	int acc_no;
	char date[20];
	public:
	void accept(void);
	void display(void);
	void friend search(int);
} ;
void emp_account::accept()
{

	cout<<"\nEnter Account Number:";
	cin>>acc_no;
	cout<<"Enter Joining Date:";
	cin>>date;
}
void emp_account::display()
{

	cout<<"\nAccount Number:"<<acc_no;
	cout<<"\nJoining Date:"<<date;
}
class emp_sal: virtual public employee
{
	float pay,earn,ded;
	public:
	void accept(void);
	void display(void);
};
void emp_sal:: accept()
{
	cout<<"\nEnter Employee's Basic+Pay:";
	cin>>pay;
	cout<<"\nEnter His Earning:";
	cin>>earn;
	cout<<"\nEnter His Deduction:";
	cin>>ded;
}
void emp_sal::display()
{

	cout<<"\nEmployee's Basic+Pay:";
	cout<<pay;
	cout<<"\nHis Earning:";
	cout<<earn;
	cout<<"\nHis Deduction:";
	cout<<ded;
}
int main()
{
	employee ob[100];
	emp_account ob1[100];
	emp_sal ob2[100];
	int i,n,k,ch,flag=1;
	for(;;)
	{
		cout<<"\n1:Build Master Table.";
		cout<<"\n2:Search An Entry.";
		cout<<"\n3:Display.";
		cout<<"\n4:Exit.";
		cout<<"\n Enter Your Choice:";
		cin>>ch;
		switch(ch)
		{
			case 1: cout<<"\nHow Many Employees:";
				cin>>n;
				for(i=0;i<n;i++)
				{
					cout<<"\nEnter Data For "<<i+1 <<" Employee:";
					ob[i].accept();
					ob1[i].accept();
					ob2[i].accept();
				}
				break;
			



	case 2:cout<<"Enter the Employee code:";
				cin>>k;
				for(i=0;i<n;i++)
				{
					if(ob[i].search(k)==0)
					{
						cout<<"Record Found!!";
						flag=0;
						ob[i].display();
						ob1[i].display();
						ob2[i].display();
						break;
					}
					else
					flag=1;
				}
				if(flag==1)
				cout<<"Sorry Data Not Found!!";
				break;
			case 3:for(i=0;i<n;i++)
				{
					cout<<"\n Details Of "<<i+1<< "Employee:";
					ob[i].display();
					ob1[i].display();
					ob2[i].display();
				}
				break;
			case 4:return(0);
		}
	}
	getch();
}
	


/* OUTPUT:-
	1:Build Master Table.
	2:Search An Entry.
	3:Display.
	4:Exit.
	 Enter Your Choice:1
	How Many Employees:2
	Enter Data For 1 Employee:
	Enter the Code Of Employee:001
	Enter Name Of The Employee:Sameer
	Enter Designation Of Employee:comp
	Enter Account Number:123
	Enter Joining Date:1Jan
	Enter Employee's Basic+Pay:250
	Enter His Earning:23
	Enter His Deduction:23
	Enter Data For 2 Employee:
	Enter the Code Of Employee:002
	Enter Name Of The Employee:Avisek
	Enter Designation Of Employee:Comp
	Enter Account Number:124
	Enter Joining Date:2Jan
	Enter Employee's Basic+Pay:250
	Enter His Earning:23
	Enter His Deduction:23
	
	1:Build Master Table.
	2:Search An Entry.
	3:Display.
	4:Exit.
	 Enter Your Choice:2
	Enter the Employee code:1
	Record Found!!
	Code Of Employee:1
	Name Of Employee:Sameer
	Designation Of Employee:Comp
	Account Number:123
	Joining Date:1Jan
	Employee's Basic+Pay:250
	His Earning:23
	His Deduction:23
	1:Build Master Table.
	2:Search An Entry.
	3:Display.
	4:Exit.
	 Enter Your Choice:3

	Code Of Employee:1
	Name Of Employee:Sameer
	Designation Of Employee:Comp
	Account Number:123
	Joining Date:1Jan
	Employee's Basic+Pay:250
	His Earning:23
	His Deduction:23

	Code Of Employee:2
	Name Of Employee:Avisek
	Designation Of Employee:Comp
	Account Number:124
	Joining Date:2Jan
	Employee's Basic+Pay:250
	His Earning:23
	His Deduction:23
	1:Build Master Table.
	2:Search An Entry.
	3:Display.
	4:Exit.
	 Enter Your Choice:4
*/
