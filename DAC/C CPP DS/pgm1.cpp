#include<iostream>
using namespace std;

struct Node
{
	int data;
	Node * left, * right;
};

class Tree
{
	Node *root;
	Node *p,*q;

	public:

	Tree()
	{
		p=q=root=NULL;

	}

	Node* rroot()
	{
		return root;
	}

	void insert(int data)
	{
		Node* n=new Node;

		n->data=data;
		n->left = n->right = NULL;

		if(root==NULL)
		{
			root = n;
			return ;
		}
		
		p=q=root;

		while(p->data!=data && q!=NULL)
		{
			p=q;
			
			if(p->data < data)
			{
				q=q->right;
			}
			else
			{
				q=q->left;
			}

			if(p->data==data)
				cout<<"Duplication is not allowed"<<endl;

			if(p->data < data)
			{
				p->right=n;
			}
			else
			{
				p->left=n;
			}

		}
	}

	void inorder(Node * root)
	{
		inorder(root->left);
		cout<<root->data<<endl;
		inorder(root->right);
	}
};


int main(void)
{
	Tree t;
	t.insert(50);
//	t.insert(69);
//	t.insert(96);

//	t.inorder(t.rroot());

	return 0;
}



