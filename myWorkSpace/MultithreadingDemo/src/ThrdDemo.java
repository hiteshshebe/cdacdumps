class Thread1 extends Thread
{

	public void run()
	{
		for(int i=100;i<1000;i++)
		System.out.println("i m thread 1....");
	}

}

class Thread2 extends Thread
{

	public void run()
	{
		for(int i=100;i<1000;i++)
		System.out.println("i m thread 2....");
	}

}

class Thread3 implements Runnable
{
	public void run()
	{
		for(int i=0;i<100;i++)
		System.out.println("i m thread 3 "+i);
	}
}


public class ThrdDemo {
	
	
	public static void main(String[] args) 
	{
		Thread1 t1=new Thread1();
		Thread2 t2=new Thread2();
		Thread3 t3=new Thread3();
		
		Thread t33=new Thread(t3);
		
		t1.start();
		t2.start();
		t33.start();	
		
	}

}
