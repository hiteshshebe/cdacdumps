class ResourceMy
{
	public void display()
	{
		for(int i=0;i<1000;i++)
			System.out.println(i);
	}
}

class ThreadMy1 extends Thread
{
	ResourceMy r=null;
	public ThreadMy1(ResourceMy r)
	{
		this.r=r;
		start();
	}
	public void run()
	{
		r.display();
	}
}


class ThreadMy2 extends Thread
{
	ResourceMy r=null;
	public ThreadMy2(ResourceMy r)
	{
		this.r=r;
		start();
	}
	public void run()
	{
		r.display();
	}
}


public class RohitsProblemDemo {

	public static void main(String[] args) {
		
		ResourceMy r=new ResourceMy();
		ThreadMy1 tm1=new ThreadMy1(r);
		ThreadMy2 tm2=new ThreadMy2(r);
		
	}

}
