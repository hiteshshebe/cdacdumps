

import java.util.Iterator;

import org.hibernate.cfg.Configuration;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class LoginAccess 
{
	Session session=null;
	Configuration cnfg;
	SessionFactory sf=null;
	SessionFactory sessionFactory=null;
	Transaction transaction=null;
	
		
	/*
	public void get()
	{
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
			System.out.println("Session factory...");
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			LoginPojo login = (LoginPojo) session.get(LoginPojo.class, new String("sss") );
			System.out.println("Userid. : " + login.getUsername());
			System.out.println("Password : " + login.getPassword());
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();
			
		}
	}
	*/
	
	static void getValues()
	{
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		
		Query query=session.createQuery("from LoginPojo lp");
		
		Iterator it=query.iterate();
		
		while(it.hasNext())
		{
			LoginPojo lp=(LoginPojo)it.next();
			System.out.println(lp.getUsername());
			System.out.println(lp.getPassword());
		}
	}
	
	public static void main(String[] args) {
		
		new LoginAccess().getValues();
		
	}
}
