package com.bslogics;

import org.springframework.context.ApplicationContext;

import com.dao.JProviderDAO;
import com.dbconn.DBConnection;
import com.pojos.JProviderPojo;

public class JProviderBsLogic 
{
	ApplicationContext context;
	JProviderDAO rjpdao;
	
	public void getBean()
	{
		System.out.println("Alo JProviders Bs Logic madhe...");
		context=new DBConnection().getContext();
		rjpdao=(JProviderDAO)context.getBean("registerjpdao");
	}
	
	public String insert(JProviderPojo jpp)
	{
		getBean();
		return rjpdao.saveUser(jpp);
	}
	
	public JProviderPojo select(String email,String passwd)
	{
		getBean();
		return rjpdao.getUser(email, passwd);
	}
}
