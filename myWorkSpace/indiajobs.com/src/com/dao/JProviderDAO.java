package com.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.pojos.JProviderPojo;

public class JProviderDAO 
{
	private SessionFactory sessionFactory;
	private String status = "failure";
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public String saveUser(JProviderPojo jpp)
	{
		Session session = getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(jpp);
		System.out.println("data saved ........");
		tx.commit();
		status="success";
		return status;
	}
	
	public JProviderPojo getUser(String email,String passwd)
	{		
		JProviderPojo jpp=null;
		Session session=getSessionFactory().openSession();
		Criteria criteria=session.createCriteria(JProviderPojo.class);
		criteria.add(Restrictions.ilike("email",email))
				.add(Restrictions.ilike("passwd",passwd));
		
		List list=criteria.list();
		Iterator it=list.iterator();
		
		while(it.hasNext())
		{
			jpp=(JProviderPojo)it.next();
		}
		
		return jpp;
	}
}
