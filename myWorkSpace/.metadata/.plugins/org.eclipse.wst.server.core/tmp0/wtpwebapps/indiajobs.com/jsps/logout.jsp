<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Log Out Page</title>
<%session.invalidate();%>
</head>
<body>

<div id="header">
	<h1 style="margin:50px 0px 5px 20px">IndiaJobs.com</h1>
</div>


<div id="content" style="height:400px">

<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div >
				You have successfully logged out.
				<a href="../jsps/jseek_login.jsp">Login here</a>	
				<a href="../jsps/jseek_home.jsp">Home</a>
			</div>
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->


<div id="footer">
copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>

</body>
</html>