<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="../Style/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<s:a href="../jsps/jseek_login.jsp">Login</s:a>
<div id="header">
	<h1 style="margin:50px 0px 5px 20px">IndiaJobs.com</h1>
</div>


<div id="content">

<table>
	<tr>
		<td style="width:300px"></td>
		<td align="center" width="400">
			<div>
				<s:form action="registers" id="registration_form">
					<div>
					<s:label><h3 style="text-align: center;">Get Started With IndiaJobs</h3></s:label>
					<s:hidden name="jseekid" value="0"></s:hidden>
					<s:textfield name="email" label="*Email address "></s:textfield>
					<s:textfield name="firstName" label="*First name "></s:textfield>
					<s:textfield name="lastName" label="Last name "></s:textfield>
					<s:textfield name="userName" label="*Desired username "></s:textfield>
					<s:password name="passwd" label="*Choose a password "></s:password>
					<s:password name="passwd_conf" label="*Re-enter password"></s:password> 
					</div>
					<s:submit value="Register"></s:submit>
				</s:form>
				</div><!-- END OF REGISTRATION FORM HERE-->
		</td>
		<td style="width:300px"></td>
	</tr>
</table>

</div><!-- END OF CONTENT HERE -->



<div id="footer">
copyright &copy; 2012 | indiajobs.com | Powered By: 4SUV Technologies
</div>
</body>
</html>