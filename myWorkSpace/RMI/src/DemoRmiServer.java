import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class DemoRmiServer extends UnicastRemoteObject implements DemoRmiService
{

	public DemoRmiServer() throws RemoteException{}
		
	public int add(int num1, int num2) throws RemoteException 
	{
		return num1+num2;
	}
	
	public int sub(int num1, int num2) throws RemoteException 
	{
		return num1 - num2;
	}
	
	public static void main(String[] args) 
	{
		try 
		{
			DemoRmiServer d = new DemoRmiServer();
			Registry reg =LocateRegistry.createRegistry(5525);
			reg.bind("serv1", d);
			System.out.println("server connected with registry...");
		}
		
		catch (RemoteException e) 
		{
			e.printStackTrace();
		}
		
		catch (AlreadyBoundException e) 
		{
			e.printStackTrace();
		}
	}
}
