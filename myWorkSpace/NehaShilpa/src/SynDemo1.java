
class SynRes
{
	public synchronized void display(String msg)
	{
		System.out.println("(");
		System.out.println(msg);
		System.out.println(")");
	}
}

class SynThread1 extends Thread
{
	SynRes s=new SynRes();
	String msg1;
	public SynThread1(SynRes s1, String msg)
	{
		this.s=s1;
		this.msg1=msg;
	}
	
	public void run()
	{
		s.display(msg1);
	}
}

public class SynDemo1 
{
	public static void main(String[] args)
	{
		SynRes s=new SynRes();
		SynThread1 st1=new SynThread1(s,"HI");
		st1.start();
		
		SynThread1 st2=new SynThread1(s,"Neha");
		st2.start();
	}

}
