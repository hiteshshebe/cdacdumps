import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionDemo {

	Connection conn;
	
	public boolean connect()
	{
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
		
			conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","system","admin");
			
			System.out.println("Connected");
			
			return true;
		}
		catch(ClassNotFoundException sqle)
		{
			sqle.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static void main(String[] args) {

		new DBConnectionDemo().connect();
	}

}
