class Thread1 implements Runnable
{
	public void run()
	{
		for (int i=1;i<=500; i++)
		{
			if (i==250)
			{
				System.out.println(" Thread 1 is yielding the resource..............");
				
					Thread.yield();
					
			}
			else
				
				System.out.println("Thread1 : "+i);
		}
	}
	
}

class Thread2 extends Thread
{
	public void run()
	{
		for (int i=501;i<=1000; i++)
		{
			if (i==600)
				try {
					sleep(15,1000000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
			System.out.println("Thread2 : "+i);
		}
	}
	
}




public class Demo1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Priority : "+Thread.currentThread().getPriority());
		
		// Runnable Interface
		
		Thread1 tobj = new Thread1();
		Thread t1 = new Thread(tobj);  
		System.out.println("Priority : "+t1.getPriority());
		t1.setName("ThreadOne");
		t1.setPriority(9);
		t1.start();
		Thread2 t2 = new Thread2();
		t2.start();
		t1.start();
		for (int i=1001; i<=1500; i++)
		{
			System.out.println("Main Thread : "+i);
		}
		
		
		System.out.println("Id : "+t1.getId());
		System.out.println("Id : "+t2.getId());
		System.out.println("Id : "+Thread.currentThread().getId());
		
		
		System.out.println("Priority : "+t1.getPriority());
		System.out.println("Name : "+t1.getName());
		System.out.println("Name : "+t2.getName());
		System.out.println("Name : "+Thread.currentThread().getName());
		System.out.println("T1 Is Alive : "+t1.isAlive());
		System.out.println("T2 Is Alive : "+t2.isAlive());
		System.out.println("MAIN Is Alive : "+Thread.currentThread().isAlive());
		
		try {
			t1.join();
			t2.join();
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("DEAD");
		System.out.println("ThreadT1 Is Alive : "+t1.isAlive());
		System.out.println("T2 Is Alive : "+t2.isAlive());

	}

}
