using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Language_Reference
{
    class Program
    {
        static Semaphore smp;

        static void threadProc(object obj)
        {
            Console.WriteLine((int)obj);
            smp.WaitOne(Timeout.Infinite, true);
            for (int i = 100; i < 300; i++)
            {
                Thread.Sleep(100);
                Console.WriteLine("Thread1:number={0}", i);
            }
            smp.Release();
        }

        static void Main(string[] args)
        {
            Thread thread1;

            smp = new Semaphore(2, 2, "MySemaphore");

            ParameterizedThreadStart entrypoint = new ParameterizedThreadStart(threadProc);

            thread1 = new Thread(entrypoint);
            thread1.Name = "thread1";
            thread1.Start(10);
        }
    }
}