﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

namespace ConsoleApplication
{

    class MyArray<T> where T : class
    {
        public MyArray(int dimension)
        {
            array = new T[dimension];
            nextindex = 0;
        }

        public void Add(T value)
        {
            if (nextindex < array.Length)
            {
                array[nextindex] = value;
                nextindex++;
            }
        }

        public T this[int index]
        {
            get { return array[index]; }
        }

        T[] array;
        int nextindex;
    }

    class Person
    {
        public string Name
        {
            set;
            get;
        }
    }

    struct Point
    {
        public int X
        {
            set;
            get;
        }

        public int Y
        {
            set;
            get;
        }
    }

    class Program
    {
        static void Main()
        {
            MyArray<Person> people = new MyArray<Person>(3);

            people.Add(new Person() { Name = "A" });
            people.Add(new Person() { Name = "B" });
            people.Add(new Person() { Name = "C" });



            Console.WriteLine("Name={0}", people[0].Name);
        }
    }
}