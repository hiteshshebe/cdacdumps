﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CurrencyConsole.MyCur;

namespace CurrencyConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            double usd, inr, rate;
            Console.Write("Enter USD : ");
            usd = double.Parse(Console.ReadLine());
            CurrencyConvertor con = new CurrencyConvertor();
            rate = con.ConversionRate(Currency.USD, Currency.INR);
            inr = usd * rate;
            Console.WriteLine("INR : " + inr);
        }
    }
}
