﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using CalcLib;

namespace CalcHost
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri uri = new Uri("http://localhost:8534/myobj");
            ServiceHost host = new ServiceHost(typeof(CalcService), uri);
            Console.WriteLine("Server Ready");
            host.Open();
            Console.ReadLine();
            host.Close();
        }
    }
}
