﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace MyService
{
    
    [ServiceContract]
    public interface IMath
    {

        [OperationContract]
        int Add(int a,int b);
        [OperationContract]
        int Sub(int a, int b);
        
    }


    
    
}
