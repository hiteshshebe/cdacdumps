using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeCheck
{
    class User
    {
        public static string Password
        {
            set;//public setting of Password property
            get;//public getting of Password property
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            User u = new User();
            User.Password = "ilovemyindiangirl";
            Console.WriteLine(User.Password);

        }
    }
}
