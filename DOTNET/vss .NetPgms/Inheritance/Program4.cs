﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication4
{
    abstract class Banking
    {
        public void Deposite()
        {
            Console.WriteLine("DEPOSITE...");
        }
        public void Withdraw()
        {
            Console.WriteLine("WITHDRAW...");
        }

        public abstract void InetBanking();
        public abstract void BillPayment();
    }

    sealed class SBI:Banking
    {
        public override void InetBanking()
        {
            throw new NotImplementedException();
        }

        public override void BillPayment()
        {
            throw new NotImplementedException();
        }
        
    }

    sealed class Central:Banking
    {
        public override void InetBanking()
        {
            throw new NotImplementedException();
        }

        public override void BillPayment()
        {
            throw new NotImplementedException();
        }
    }

    
    class Program
    {
        static void Main(string[] args)
        {
            //SBI sbi = new SBI();
            //Central central = new Central();

            //sbi.Deposite();
            //sbi.Withdraw();

            //central.Deposite();
            //central.Withdraw();

            Banking b = new SBI();
            b.BillPayment();
            b.Deposite();
            b.InetBanking();
            b.Withdraw();
        
            
        }
    }
}
