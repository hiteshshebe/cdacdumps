﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace outparameter
{
    class Numbers
    {
        int[] arr;

        public Numbers(int x)
        {
            arr = new int[x];
            int cnt = 1;
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = cnt++;
            }
        }

        public int this[int c]
        {
            set { arr[c]=value;  }
            get { return arr[c]; }
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Numbers n = new Numbers(3);
            Console.WriteLine(n[1]);
        }
    }
}
