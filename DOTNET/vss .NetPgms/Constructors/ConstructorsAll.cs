using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeCheck
{
    class Person    //class
    {
        public int age;     //by default this class variable is private 
        public int age2;
        public int age3;
        public int age4;
        public Person() : this(18)     //constructor/parameterless constructor/default constructor/special type of method() having same name as that of class name so we should not call this method by creating instance of class where as to normal methods we need instance of class and one more thing it will call implicitly by compiler so it should be public 
        {
            Console.WriteLine("I m in default contructor person");
        }

        public Person(int age)
        {
            Console.WriteLine("I m in parameterized contructor person");
            //age=age;  //it wont work because both age is of Person(int age) which are nothing but formal arguments
            this.age = age; //this.age is a class variable age and age is formal arg of Person(int age)
        }

    }

    class Student
    {
        static int rollno;
        int x;

        static Student()
        {
            rollno = 10;    //allow bcoz roll no is static and constructor is also static 
            //x=25;           //doesnt allow because variable is not static(non static = instance variable)
        }
        public Student()
        {
            Console.WriteLine("Default constructor");
            //this.rollno=15;   //disallow because it doesn`t need instance
            x=15;               //allows because x is instance variable
        }
    }
    class Program
    {
        int x;
        Program()
        {
            Console.WriteLine("I m in contructor");
        }

        Program(int x)
        {
            this.x = x;
            Console.WriteLine("I m in contructor");
        }
        

        static void Main(string[] args)
        {
            Program prog=new Program();//

            Person p=new Person();
            Console.WriteLine(prog.x);
            Console.WriteLine(p.age);

            Person p1 = new Person { age = 24 };
            Person p2 = new Person { age=28, age2=30};
            Person p3 = new Person { age = 28, age2 = 30 , age3=40};
            Person p4 = new Person { age = 28, age2 = 30, age3 = 40 ,age4=50};

            Student s = new Student();
        }
    }
}
