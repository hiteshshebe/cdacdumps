using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeCheck
{
    class Program
    {

        //public static int Add(int a,int b);       //function declaration
        //public static int Add(int,int);           //function prototype
        //public static int Add(int a,float b)          //function definition
        //{
        //    int sum = 0;
        //    sum = a + (int)b;
        //    return sum;
        //}


        //public static float Add(float a, float b)
        //{
        //    float sum = 0;
        //    sum = a + b;
        //    return sum;
        //}

        public static int Add(params int[] numbers)
        {
            int sum=0;
            for (int i = 0; i < numbers.Length; i++)
            {
                sum += (int)numbers[i];
            }
            return sum;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(Add(5, 6));
            Console.WriteLine(Add(5, 6,7));
            String s = "shailesh";
            Console.WriteLine(s.Length);
        }
    }
}
