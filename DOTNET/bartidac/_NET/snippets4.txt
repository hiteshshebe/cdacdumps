1
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "a.txt";

            if(File.Exists(filename))
                File.Delete(filename);

            File.WriteAllText(filename, "Hi, World");
            File.AppendAllText(filename, "Hello, World");

            string filename2 = "b.txt";

            if (File.Exists(filename2))
                File.Delete(filename2);

            File.Copy(filename, filename2);

            string directory = "data";

            if (Directory.Exists(directory))
                Directory.Delete(directory);

            Directory.CreateDirectory("data");

            File.Move(filename2, directory+@"\" + filename2);
          }
    }
}
----
2
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "a.txt";
            FileInfo fileInfo1 = new FileInfo(filename);

            if(fileInfo1.Exists)
                fileInfo1.Delete();

            StreamWriter sw = fileInfo1.AppendText();
            sw.Write("Hello, World");
            sw.Close();

            string filename2 = "b.txt";
            FileInfo fileInfo2 = new FileInfo(filename2);

            if (fileInfo2.Exists)
                fileInfo2.Delete();

            fileInfo1.CopyTo(filename2);

            string directory = "data";
            DirectoryInfo directoryInfo1 = new DirectoryInfo(directory);

            if (directoryInfo1.Exists)
                directoryInfo1.Delete(true);

            directoryInfo1.Create();

            fileInfo2.MoveTo(directoryInfo1.ToString() + @"\" + fileInfo2.ToString());
          }
    }
}
----
3
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "a.txt";
            FileInfo fileInfo = new FileInfo(filename);

            //Getting FileStream Object For Reading 
            FileStream fs1 = File.OpenRead(filename);
            FileStream fs2 = fileInfo.OpenRead();
            FileStream fs3 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Read);

            //Getting FileStream Object For Writing
            FileStream fs4 = File.OpenWrite(filename);
            FileStream fs5 = fileInfo.OpenWrite();
            FileStream fs6 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);

            //Getting FileStream Object For Reading / Writing / Both
            FileStream fs7 = File.Open(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            FileStream fs8 = fileInfo.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite);
            FileStream fs9 = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            
            //Getting StreamReader Object
            StreamReader sr1 = File.OpenText(filename);
            StreamReader sr2 = fileInfo.OpenText();
            StreamReader sr3 = new StreamReader(fs3);

            //Getting StreamWriter Object
            StreamWriter sw1 = File.AppendText(filename);
            StreamWriter sw2 = File.CreateText(filename);
            StreamWriter sw3 = fileInfo.AppendText();
            StreamWriter sw4 = fileInfo.CreateText();
            StreamWriter sw5 = new StreamWriter(fs4);

        }
    }
}
----
4
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

//FileIO - Writing Using FileStream
namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                byte[] byData;
                char[] charData;

                FileStream aFile = new FileStream("a.txt", FileMode.Create, FileAccess.Write);
                charData = "Hello, World.".ToCharArray();
                byData = new byte[charData.Length];
                Encoder e = Encoding.UTF8.GetEncoder();
                e.GetBytes(charData, 0, charData.Length, byData, 0, true);
                aFile.Write(byData, 0, byData.Length);
                aFile.Close();
            }
            catch (IOException ex)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
                return;
            }
        }
    }
}
----
5
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] byData = new byte[16];

            try
            {
                FileStream aFile = new FileStream("a.txt", FileMode.Open, FileAccess.Read);
                aFile.Seek(0, SeekOrigin.Begin);
                aFile.Read(byData, 0, 16);
                UTF8Encoding e = new UTF8Encoding();
                string s = e.GetString(byData);
                Console.Out.WriteLine(s);
                aFile.Close();
            }
            catch (IOException ex)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
                return;
            }
        }
    }
}
----
6
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] byteData = new byte[16];
            char[] charData = new char[16];

            try
            {
                FileStream file = new FileStream("a.txt", FileMode.Open, FileAccess.Read);

                Decoder d = Encoding.UTF8.GetDecoder();

                int bytesRead;

                file.Seek(7, SeekOrigin.Begin);

                bytesRead = file.Read(byteData, 0, 16);

                while (bytesRead > 0)
                {
                    d.GetChars(byteData, 0, byteData.Length, charData, 0);
                    Console.Out.WriteLine(charData);
                    bytesRead = file.Read(byteData, 0, 16);
                }

                file.Close();
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
----
7
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileStream aFile = new FileStream("a.txt", FileMode.Truncate,
                                                            FileAccess.Write);
                StreamWriter sw = new StreamWriter(aFile);

                sw.WriteLine("File Handling using StreamWrite and StreamReader");
                sw.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(e.ToString());
                Console.ReadLine();
                return;
            }
        }
    }
}
----
8
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileStream aFile = new FileStream("a.txt", FileMode.Open);
                StreamReader sr = new StreamReader(aFile);

                string line;
                line = sr.ReadLine();
                while (line != null)
                {
                    Console.WriteLine(line);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(e.ToString());
                Console.ReadLine();
                return;
            }
        }
    }
}
----
9
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileStream file = new FileStream("a.txt", FileMode.Open);
                
                StreamReader sr = new StreamReader(file);

                int charData;

                charData = sr.Read();
                
                while (charData != -1)
                {
                    Console.WriteLine(Convert.ToChar(charData));
                    charData = sr.Read();
                }

                sr.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
----
10
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Language_Reference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FileStream file = new FileStream("a.txt", FileMode.Open);

                StreamReader sr = new StreamReader(file);

                string s;

                s = sr.ReadToEnd();

                Console.WriteLine(s);

                sr.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
----
11
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization; 
using System.Runtime.Serialization.Formatters.Binary;

namespace Language_Reference
{
    [Serializable]
    class X
    {
        public int N
        {
            get;
            set;
        }

        public double R
        {
            get;
            set;
        }

        [NonSerialized]
        string s;
        string S
        {
            get { return s; }
            set { s = value; }
        }

        public X() : this(0, 0, "")
        {
        }

        public X(int n, double r, string s)
        {
            this.N = n;
            this.R = r;
            this.S = s;
        }

        public override string ToString()
        {
            return string.Format("N = {0}, R = {1}, S = {2}", N, R, S);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                X x = new X(1, 1.0, "Hello, World");
                Console.WriteLine(x);

                IFormatter serializer = new BinaryFormatter();
                FileStream saveFile = new FileStream("a.bin",
                            FileMode.Create,
                            FileAccess.Write);
                serializer.Serialize(saveFile, x);
                saveFile.Close();

                FileStream loadFile = new FileStream("a.bin",
                                        FileMode.Open,
                                        FileAccess.Read);
                X x2 = serializer.Deserialize(loadFile) as X;
                loadFile.Close();
                Console.WriteLine(x2);
            }
            catch (IOException e)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(e.ToString());
            }
        }
    }
}
----
12
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization; 
using System.Runtime.Serialization.Formatters.Soap;

namespace Language_Reference
{
    [Serializable]
    class X
    {
        public int N
        {
            get;
            set;
        }

        public double R
        {
            get;
            set;
        }
        
        [NonSerialized]
        string S
        {
            get;
            set;
        }

        public X()
            : this(0, 0, "")
        {
        }

        public X(int n, double r, string s)
        {
            this.N = n;
            this.R = r;
            this.S = s;
        }

        public override string ToString()
        {
            return string.Format("N = {0}, R = {1}, S = {2}", N, R, S);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                X x = new X(1, 1.0, "Hello, World");
                Console.WriteLine(x);

                IFormatter serializer = new SoapFormatter();
                FileStream saveFile = new FileStream("a.soap", FileMode.Create, FileAccess.Write);
                serializer.Serialize(saveFile, x);
                saveFile.Close();

                FileStream loadFile = new FileStream("a.soap", FileMode.Open, FileAccess.Read);
                X x2 = serializer.Deserialize(loadFile) as X;
                loadFile.Close();
                Console.WriteLine(x2);
            }
            catch (IOException e)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(e.ToString());
                return;
            }
        }
    }
}
----
13
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization; 
using System.Runtime.Serialization.Formatters.Soap;

namespace Language_Reference
{
    [Serializable]
    class W : ISerializable
    {
        public int N
        {
            get;
            set;
        }

        public W(int n)
        {
            this.N = n;
        }

        public override string ToString()
        {
            return string.Format("N = {0}", N);
        }
    }

    [Serializable]
    class X : W
    {
        public int N
        {
            get;
            set;
        }

        public double R
        {
            get;
            set;
        }

        [NonSerialized]
        string S
        {
            get;
            set;
        }

        public X()
            : this(0, 0, "")
        {
        }

        public X(int n, double r, string s)
            : base(N * 3)
        {
            this.N = n;
            this.R = r;
            this.S = s;
        }

        public override string ToString()
        {
            return string.Format("N = {0}, R = {1}, S = {2}", N, R, S);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                W w = new Y(1, 1.2F, "Hello, World");
                Console.Out.WriteLine(x);

                IFormatter serializer = new SoapFormatter();
                FileStream saveFile = new FileStream("a.bin", FileMode.Create, FileAccess.Write);
                serializer.Serialize(saveFile, x);
                saveFile.Close();

                FileStream loadFile = new FileStream("a.bin", FileMode.Open, FileAccess.Read);
                X x2 = serializer.Deserialize(loadFile) as X;
                loadFile.Close();
                Console.Out.WriteLine(x2);
            }
            catch (IOException e)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(e.ToString());
                Console.ReadLine();
                return;
            }
        }
    }
}