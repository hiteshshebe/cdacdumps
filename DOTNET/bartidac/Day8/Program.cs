﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System.Reflection;

namespace ConsoleApplication
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple=true)]
    class CommentAttribute : Attribute
    {
        public string Comment
        {
            get;
            set;
        }

        public CommentAttribute(string comment)
        {
            this.Comment = comment;
        }
    }

    [Comment("Person class")]
    [Comment("Ver 1.0")]
    class Person : ICloneable
    {
        int age;
        public int Age
        {
            get { return age; }
            set
            {
                age = value;
            }
        }

        static int maxAge = 150;

        public object Clone()
        {
            Person clone = new Person();
            clone.Age = Age;
            return clone;
        }
    }

    class Program
    {
        static void Main()
        {
            Type t = typeof(Person);

            CommentAttribute[] comments = 
                t.GetCustomAttributes(typeof(CommentAttribute),
                false) as CommentAttribute[];

            if (comments == null) return;

            foreach (CommentAttribute item in comments)
            {
                Console.WriteLine(item.Comment);
            }

        }

    }
}


