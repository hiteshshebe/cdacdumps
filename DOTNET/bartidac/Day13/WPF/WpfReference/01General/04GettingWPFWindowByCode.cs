﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace WpfReference
{
    class MainWindow : Window
    {
        public Grid grid;

        public MainWindow()
        {
            grid = new Grid();
            this.Content = grid;
        }
    }

    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application application = new Application();

            MainWindow window = new MainWindow();
            window.Title = "WPF Basic Window";

            application.Run(window);
        }
    }
}
