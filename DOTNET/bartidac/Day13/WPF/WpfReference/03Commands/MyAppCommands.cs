﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace WpfReference
{
    public class MyAppCommands
    {
        public static RoutedUICommand OpenCommand;

        static MyAppCommands()
        {
            OpenCommand = new RoutedUICommand();
        }
    }
}
