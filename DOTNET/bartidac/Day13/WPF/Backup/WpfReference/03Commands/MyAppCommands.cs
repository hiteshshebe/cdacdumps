﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace WpfReference
{
    public class MyAppCommands
    {
        public static RoutedUICommand LoadCommand;

        static MyAppCommands()
        {
            InputGestureCollection loadInputs = new InputGestureCollection();
            loadInputs.Add(new KeyGesture(Key.L, ModifierKeys.Control));
            LoadCommand = new RoutedUICommand("Load Command", "Load", typeof(MyAppCommands), loadInputs);
        }
    }
}
