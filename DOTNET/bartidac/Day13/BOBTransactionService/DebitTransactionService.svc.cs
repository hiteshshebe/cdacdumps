﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BOBTransactionService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class DebitTransactionService : IDebitTransactionService
    {
        public double Debit(int accountNo, double amount)
        {
            if (accountNo < 0)
                return -1; // Invalid account no

            if (amount < 0)
                return -2; // Invalid amount

            string connectionString = ConfigurationManager.ConnectionStrings["BOB"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand balancesCommand = connection.CreateCommand();
            balancesCommand.CommandType = CommandType.Text;
            balancesCommand.CommandText = "SELECT AccountNo, Balance FROM Balances WHERE AccountNo = @accountNo";

            SqlParameter accountNoParam = new SqlParameter("@accountNo", accountNo);
            balancesCommand.Parameters.Add(accountNoParam);

            DataSet bob = new DataSet();
            SqlDataAdapter balancesAdapter = new SqlDataAdapter(balancesCommand);
            SqlCommandBuilder balancesCommandBuilder = new SqlCommandBuilder(balancesAdapter);

            balancesAdapter.Fill(bob, "Balances");

            double currentBalance = (double) bob.Tables["Balances"].Rows[0]["Balance"];

            if (amount > currentBalance)
                return -3; //Insufficient balance

            double newBalacne = currentBalance - amount;
            bob.Tables["Balances"].Rows[0]["Balance"] = newBalacne ;

            balancesAdapter.Update(bob, "Balances");

            return newBalacne;
        }
    }
}
