using system;
using system.collections.generic;
using system.text;
using system.threading;

namespace language_reference
{
    class program
    {
        static void threadproc1()
        {
            for (int i = 100; i < 400; i++)
                console.writeline("Thread1 : "+i);
        }

        static void threadproc2()
        {
            for (int i = 500; i < 800; i++)
                console.writeline("Thread2 : "+i);
        }

        static void main(string[] args)
        {
            threadstart entrypoint;

            entrypoint = new threadstart(threadproc1);
            thread thread1 = new thread(entrypoint);
            //thread1.name = "thread1";
            thread1.priority = threadpriority.abovenormal;

            entrypoint = new threadstart(threadproc2);
            thread thread2 = new thread(entrypoint);
            thread2.name = "thread2";

            thread1.start();
            thread2.start();
        }
    }
}