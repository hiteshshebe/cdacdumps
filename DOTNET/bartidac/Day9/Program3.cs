﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

namespace ConsoleApplication
{
    class Util
    {
        public static int Search<T>(T value, T[] array)
        {
            int i;
            for (i = 0; i < array.Length; i++)
            {
                if (array[i].Equals(value))
                    break;
            }

            if (i >= array.Length)
                return -1;

            return i;
        }
    }


    class Program
    {
        static void Main()
        {
            int[] array = new int[] { 1, 5, 2, 6, 8 };

            int index;

            index = Util.Search<int>(9, array);
        }
    }
}