using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            string mrsSQL = "SELECT * FROM Publishers; SELECT * FROM Authors";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand mrsCmd = new SqlCommand(mrsSQL, connection);
                connection.Open();

                using (SqlDataReader mrsReader = mrsCmd.ExecuteReader())
                {
                    do
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            mrsReader.Read();
                            Console.WriteLine("{0}\t\t{1}", mrsReader[0], mrsReader[1]);
                        }
                        Console.WriteLine();
                    } while (mrsReader.NextResult());
                }
            }
        }
    }
}