using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand authorsCommand = connection.CreateCommand();
            authorsCommand.CommandType = CommandType.Text;
            authorsCommand.CommandText = "SELECT Au_ID, Author, [Year Born] FROM Authors";

            DataSet authorsDS1 = new DataSet();
            DataSet authorsDS2 = new DataSet();

            SqlDataAdapter authorsAdapter = new SqlDataAdapter(authorsCommand);
            SqlCommandBuilder authorsCommandBuilder = new SqlCommandBuilder(authorsAdapter);

            authorsAdapter.Fill(authorsDS1, "Authors");
            authorsDS1.Tables["Authors"].PrimaryKey = new DataColumn[] { authorsDS1.Tables["Authors"].Columns["Au_ID"] };

            try
            {
                //Make Changes to authorDS1 
                authorsDS1.Tables["Authors"].Rows[0]["Year Born"] = 1999;
                authorsAdapter.Update(authorsDS1.Tables["Authors"]);
            }
            catch (DBConcurrencyException e)
            {
                authorsAdapter.Fill(authorsDS2, "Authors");
                authorsDS2.Tables["Authors"].PrimaryKey = new DataColumn[] { authorsDS2.Tables["Authors"].Columns["Au_ID"] };

                authorsDS1.Tables["Authors"].Merge(authorsDS2.Tables["Authors"], true, MissingSchemaAction.Ignore);
                authorsAdapter.Update(authorsDS1.Tables["Authors"]);
            }
        }
    }
}