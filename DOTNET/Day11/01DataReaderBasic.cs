using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Language_Reference.ADO.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.
                            ConnectionStrings["Biblio"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand authorCommand = connection.CreateCommand();
            authorCommand.CommandType = CommandType.Text;
            authorCommand.CommandText = "SELECT Au_ID, Author FROM Authors";

            SqlDataReader authorReader = authorCommand.ExecuteReader();
            while (authorReader.Read())
            {
                Console.WriteLine("\t{0}\t{1}", authorReader["Au_ID"],
                                                 authorReader["Author"]);
            }

            authorReader.Close();
            connection.Close();
        }
    }
}