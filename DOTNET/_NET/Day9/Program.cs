﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

namespace ConsoleApplication
{

    class MyArray<T> where T : struct
    {
        public MyArray(int dimension)
        {
            array = new T[dimension];
            nextindex = 0;
        }

        public void Add(T value)
        {
            if (nextindex < array.Length)
            {
                array[nextindex] = value;
                nextindex++;
            }
        }

        public T this[int index]
        {
            get { return array[index]; }
        }

        T[] array;
        int nextindex;
    }

    class Person
    {
    }

    struct Point
    {
        public int X
        {
            set;
            get;
        }

        public int Y
        {
            set;
            get;
        }
    }


    class Program
    {
        static void Main()
        {
            MyArray<int> integers = new MyArray<int>(10);

            for (int i = 0; i < 10; i++)
            {
                integers.Add(i);
            }

            Console.WriteLine(integers[5]);

            MyArray<Point> polygon = new MyArray<Point>(10);

            polygon.Add(new Point { X = 1, Y = 1 });
            polygon.Add(new Point { X = 2, Y = 2 });
            polygon.Add(new Point { X = 3, Y = 3 });

            Console.WriteLine("x={0}, y={1}", polygon[2].X, polygon[2].Y);
        }
    }
}