﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using IntegerMath;

namespace Language_Reference
{
    class Person
    {
        Person()
        {
        }

        public void SayHello()
        {
            Console.WriteLine("Say Hello");
        }
    }
    
    class SingletonException : Exception
    {
        public SingletonException(string message)
            : base(message)
        {
        }
    }

    class Singleton<T> where T : class
    {
        public static T CreateInstance()
        {
            if (_instance == null)
            {
                ConstructorInfo constructor = null;
                try
                {
                    constructor = typeof(T).GetConstructor(
                        BindingFlags.Instance |
                        BindingFlags.NonPublic, null, new Type[0], null);
                }
                catch (Exception exception)
                {
                    throw new SingletonException(exception.Message);
                }

                if (constructor == null || constructor.IsAssembly)
                    // Also exclude internal constructors.
                    throw new SingletonException(string.Format("A private or " +
                          "protected constructor is missing for '{0}'.",
                          typeof(T).Name));

                _instance = (T)constructor.Invoke(null);
            }

            return _instance;
        }

        static T _instance;
    }

    class X<T>
    {
    }

    class Program
    {
        public static void Main(string[] args)
        {
            Singleton<Person>.CreateInstance().SayHello();
            Singleton<Person>.CreateInstance().SayHello();

            X<int> x = new X<int>();
            X<char> y = new X<char>();

            if(x.GetType() == y.GetType())
                Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine(IntMath.Add(1, 1));
            Console.ReadKey();
        }
    }
}
