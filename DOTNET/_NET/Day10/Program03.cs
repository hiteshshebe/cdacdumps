using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Language_Reference
{
    class Program
    {
        static int number = 0;

        static void threadProc()
        {
            for (int i = 100; i < 120; i++)
            {
                if (Thread.CurrentThread.Name == "thread1")
                {
                    Console.WriteLine("Thread1:Befor Incr number={0}|", number);
                    Thread.Sleep(100);
                    number++;
                    Console.WriteLine("Thread1:After Incr number={0}|", number);
                }
                else if (Thread.CurrentThread.Name == "thread2")
                {
                    Console.WriteLine("Thread2:Befor Incr number={0}|", number);
                    Thread.Sleep(100);
                    number++;
                    Console.WriteLine("Thread2:After Incr number={0}|", number);
                }
            }
        }

        static void Main(string[] args)
        {
            Thread thread1, thread2;
            
            ThreadStart entrypoint = new ThreadStart(threadProc);
            thread1 = new Thread(entrypoint);
            thread1.Name = "thread1";

            entrypoint = new ThreadStart(threadProc);
            thread2 = new Thread(entrypoint);
            thread2.Name = "thread2";

            thread1.Start();
            thread2.Start();
        }
    }
}