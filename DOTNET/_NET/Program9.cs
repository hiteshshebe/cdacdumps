﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace ConsoleApplication
{
    class Program
    {
        static void Main()
        {
            byte[] byData = new byte[64];

            try
            {
                FileStream aFile = new FileStream("a.txt", FileMode.Open, FileAccess.Read);
                aFile.Seek(0, SeekOrigin.Begin);
                aFile.Read(byData, 0, 64);
                UTF32Encoding e = new UTF32Encoding();
                string s = e.GetString(byData);
                Console.Out.WriteLine(s);
                aFile.Close();
            }
            catch (IOException ex)
            {
                Console.WriteLine("An IO exception has been thrown!");
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
                return;
            }
        }
    }
}
