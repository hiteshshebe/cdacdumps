﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using BOBDebitTransactionServiceClient = ATMWorkflow.BOBDebitTransactionService.DebitTransactionServiceClient;
using SBIDebitTransactionServiceClient = ATMWorkflow.SBIDebitTransactionService.DebitTransactionServiceClient;
using System.Threading;

namespace ATMWorkflow
{
    public sealed partial class ATMTransactionWorkflow : SequentialWorkflowActivity
    {
        string response;
        string bankers;
        int accountNo;
        double amountToWithdraw;
        double newBalance;

        public ATMTransactionWorkflow()
        {
            InitializeComponent();
        }

        private void GatherInformation(object sender, EventArgs e)
        {
            bankers = "";
            accountNo = 0;
            amountToWithdraw = 0;

            Console.Write("Enter Bankers Name BOB/SBI: ");
            response = Console.ReadLine();
            bankers = response;

            Console.Write("Enter AccountNo: ");
            response = Console.ReadLine();
            accountNo = Convert.ToInt32(response);

            Console.Write("Enter Amount to Withdraw:");
            response = Console.ReadLine();
            amountToWithdraw = Convert.ToDouble(response);

            newBalance = 0;
        }

        private void PerformTransaction(object sender, EventArgs e)
        {
            if (bankers == "BOB")
            {
                BOBDebitTransactionServiceClient client = new BOBDebitTransactionServiceClient();
                newBalance = client.Debit(accountNo, amountToWithdraw);
                client.Close();
            }
            else if (bankers == "SBI")
            {
                SBIDebitTransactionServiceClient client = new SBIDebitTransactionServiceClient();
                newBalance = client.Debit(accountNo, amountToWithdraw);
                client.Close();
            }
        }

        private void IsTransactionSuccessfull(object sender, ConditionalEventArgs e)
        {
            e.Result = newBalance > 0;
        }

        private void DisburseCash(object sender, EventArgs e)
        {
            Console.WriteLine("Dispensing Cash {0}", amountToWithdraw);
            Console.WriteLine("Balance {0}", newBalance);
            Thread.Sleep(1000);
            Console.WriteLine("Please Collect Cash.");
        }

        private void LogTransaction(object sender, EventArgs e)
        {
            Console.WriteLine("Logging Transaction.");
        }

        private void PromptFailuer(object sender, EventArgs e)
        {
            Console.WriteLine("This transaction couldnot complete.");
        }

        private void WelcomeMessage(object sender, EventArgs e)
        {
            Console.Clear();
            Console.WriteLine("Welcome to BOB ATM Console.");
        }

        private void IsCardInserted(object sender, ConditionalEventArgs e)
        {
            ConsoleKeyInfo key = Console.ReadKey();
            ;

            e.Result = key.Key != ConsoleKey.Escape;
        }

        private void Initialize(object sender, EventArgs e)
        {
            Console.WriteLine("Initializing BOB ATM Console...");
        }

        private void ShutDown(object sender, EventArgs e)
        {
            Console.WriteLine("Shutting Down BOB ATM Console...");
        }
    }

}
