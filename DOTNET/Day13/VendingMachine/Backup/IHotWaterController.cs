﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Workflow.Activities;

namespace VendingMachine
{
    [ExternalDataExchange]
	public interface IHotWaterController
	{
        void OpenValve();
        void CloseValve();
	}
}
