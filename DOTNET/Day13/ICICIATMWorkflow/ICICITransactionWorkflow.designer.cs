﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace ICICIATMWorkflow
{
    partial class ICICITransactionWorkflow
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        [System.CodeDom.Compiler.GeneratedCode("", "")]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            this.promtFailuerCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.delayActivity1 = new System.Workflow.Activities.DelayActivity();
            this.disburseCashCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.ifElseBranchActivity2 = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseBranchActivity1 = new System.Workflow.Activities.IfElseBranchActivity();
            this.logCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.ifElseActivity1 = new System.Workflow.Activities.IfElseActivity();
            this.performTransactionCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.gatherInformationCodeActivity = new System.Workflow.Activities.CodeActivity();
            this.sequenceActivity1 = new System.Workflow.Activities.SequenceActivity();
            this.whileActivity1 = new System.Workflow.Activities.WhileActivity();
            // 
            // promtFailuerCodeActivity
            // 
            this.promtFailuerCodeActivity.Name = "promtFailuerCodeActivity";
            this.promtFailuerCodeActivity.ExecuteCode += new System.EventHandler(this.PromptFailure);
            // 
            // delayActivity1
            // 
            this.delayActivity1.Name = "delayActivity1";
            this.delayActivity1.TimeoutDuration = System.TimeSpan.Parse("00:00:05");
            // 
            // disburseCashCodeActivity
            // 
            this.disburseCashCodeActivity.Name = "disburseCashCodeActivity";
            this.disburseCashCodeActivity.ExecuteCode += new System.EventHandler(this.DisburseCash);
            // 
            // ifElseBranchActivity2
            // 
            this.ifElseBranchActivity2.Activities.Add(this.promtFailuerCodeActivity);
            this.ifElseBranchActivity2.Name = "ifElseBranchActivity2";
            // 
            // ifElseBranchActivity1
            // 
            this.ifElseBranchActivity1.Activities.Add(this.disburseCashCodeActivity);
            this.ifElseBranchActivity1.Activities.Add(this.delayActivity1);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IsTransactionSuccessfull);
            this.ifElseBranchActivity1.Condition = codecondition1;
            this.ifElseBranchActivity1.Name = "ifElseBranchActivity1";
            // 
            // logCodeActivity
            // 
            this.logCodeActivity.Name = "logCodeActivity";
            this.logCodeActivity.ExecuteCode += new System.EventHandler(this.LogTransaction);
            // 
            // ifElseActivity1
            // 
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity1);
            this.ifElseActivity1.Activities.Add(this.ifElseBranchActivity2);
            this.ifElseActivity1.Name = "ifElseActivity1";
            // 
            // performTransactionCodeActivity
            // 
            this.performTransactionCodeActivity.Name = "performTransactionCodeActivity";
            this.performTransactionCodeActivity.ExecuteCode += new System.EventHandler(this.PerformTransaction);
            // 
            // gatherInformationCodeActivity
            // 
            this.gatherInformationCodeActivity.Name = "gatherInformationCodeActivity";
            this.gatherInformationCodeActivity.ExecuteCode += new System.EventHandler(this.GatherInformation);
            // 
            // sequenceActivity1
            // 
            this.sequenceActivity1.Activities.Add(this.gatherInformationCodeActivity);
            this.sequenceActivity1.Activities.Add(this.performTransactionCodeActivity);
            this.sequenceActivity1.Activities.Add(this.ifElseActivity1);
            this.sequenceActivity1.Activities.Add(this.logCodeActivity);
            this.sequenceActivity1.Name = "sequenceActivity1";
            // 
            // whileActivity1
            // 
            this.whileActivity1.Activities.Add(this.sequenceActivity1);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.IsCardInserted);
            this.whileActivity1.Condition = codecondition2;
            this.whileActivity1.Name = "whileActivity1";
            // 
            // ICICITransactionWorkflow
            // 
            this.Activities.Add(this.whileActivity1);
            this.Name = "ICICITransactionWorkflow";
            this.CanModifyActivities = false;

        }

        #endregion

        private SequenceActivity sequenceActivity1;

        private WhileActivity whileActivity1;

        private CodeActivity promtFailuerCodeActivity;

        private CodeActivity logCodeActivity;

        private DelayActivity delayActivity1;

        private CodeActivity disburseCashCodeActivity;

        private IfElseBranchActivity ifElseBranchActivity2;

        private IfElseBranchActivity ifElseBranchActivity1;

        private IfElseActivity ifElseActivity1;

        private CodeActivity performTransactionCodeActivity;

        private CodeActivity gatherInformationCodeActivity;







    }
}
