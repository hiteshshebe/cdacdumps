﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using BOBDebitTransactionServiceClient = ICICIATMWorkflow.BOBDebitTransactionService.DebitTransactionServiceClient;
using ICICIDebitTransactionServiceClient = ICICIATMWorkflow.ICICIDebitTransactionService.DebitTransactionServiceClient;
using System.Threading;

namespace ICICIATMWorkflow
{

    public sealed partial class ICICITransactionWorkflow : SequentialWorkflowActivity
    {
        string bankers;
        int accountNo;
        double amountToWithdraw;
        double newBalance;

        public ICICITransactionWorkflow()
        {
            InitializeComponent();
        }

        private void GatherInformation(object sender, EventArgs e)
        {
            string response;
            newBalance = 0;

            bankers = "";
            accountNo = 0;
            amountToWithdraw = 0;

            Console.Write("Enter Bankers Name BOB/ICICI: ");
            response = Console.ReadLine();
            bankers = response;

            Console.Write("Enter AccountNo: ");
            response = Console.ReadLine();
            accountNo = Convert.ToInt32(response);

            Console.Write("Enter Amount to Withdraw:");
            response = Console.ReadLine();
            amountToWithdraw = Convert.ToDouble(response);
        }

        private void PerformTransaction(object sender, EventArgs e)
        {
            if (bankers == "BOB")
            {
                BOBDebitTransactionServiceClient client = new BOBDebitTransactionServiceClient();
                newBalance = client.Debit(accountNo, amountToWithdraw);
                client.Close();
            }
            else if (bankers == "ICICI")
            {
                ICICIDebitTransactionServiceClient client = new ICICIDebitTransactionServiceClient();
                newBalance = client.Debit(accountNo, amountToWithdraw);
                client.Close();
            }

        }

        private void IsTransactionSuccessfull(object sender, ConditionalEventArgs e)
        {
            e.Result = newBalance > 0;
        }

        private void DisburseCash(object sender, EventArgs e)
        {
            Console.WriteLine("Dispensing Cash {0}", amountToWithdraw);
            Console.WriteLine("Balance {0}", newBalance);
            Thread.Sleep(1000);
            Console.WriteLine("Please Collect Cash.");

        }

        private void LogTransaction(object sender, EventArgs e)
        {
            Console.WriteLine("Logging Transaction.");
        }

        private void PromptFailure(object sender, EventArgs e)
        {
            Console.WriteLine("This transaction couldnot complete.");
        }

        private void IsCardInserted(object sender, ConditionalEventArgs e)
        {
            ConsoleKeyInfo key = Console.ReadKey();
            e.Result = key.Key != ConsoleKey.Escape;

        }
    }

}
